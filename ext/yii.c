
/* This file was generated automatically by Zephir do not modify it! */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <php.h>

#if PHP_VERSION_ID < 50500
#include <locale.h>
#endif

#include "php_ext.h"
#include "yii.h"

#include <ext/standard/info.h>

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"

zend_class_entry *yii_base_arrayable_ce;
zend_class_entry *yii_base_bootstrapinterface_ce;
zend_class_entry *yii_base_viewcontextinterface_ce;
zend_class_entry *yii_web_linkable_ce;
zend_class_entry *yii_web_urlruleinterface_ce;
zend_class_entry *yii_base_object_ce;
zend_class_entry *yii_base_component_ce;
zend_class_entry *yii_base_exception_ce;
zend_class_entry *yii_di_servicelocator_ce;
zend_class_entry *yii_base_userexception_ce;
zend_class_entry *yii_base_event_ce;
zend_class_entry *yii_base_module_ce;
zend_class_entry *yii_base_action_ce;
zend_class_entry *yii_base_application_ce;
zend_class_entry *yii_base_behavior_ce;
zend_class_entry *yii_base_controller_ce;
zend_class_entry *yii_validators_validator_ce;
zend_class_entry *yii_web_assetbundle_ce;
zend_class_entry *yii_web_compositeurlrule_ce;
zend_class_entry *yii_web_httpexception_ce;
zend_class_entry *yii_base_actionevent_ce;
zend_class_entry *yii_base_actionfilter_ce;
zend_class_entry *yii_base_arrayabletrait_ce;
zend_class_entry *yii_base_arrayaccesstrait_ce;
zend_class_entry *yii_base_errorexception_ce;
zend_class_entry *yii_base_errorhandler_ce;
zend_class_entry *yii_base_exitexception_ce;
zend_class_entry *yii_base_inlineaction_ce;
zend_class_entry *yii_base_invalidcallexception_ce;
zend_class_entry *yii_base_invalidconfigexception_ce;
zend_class_entry *yii_base_invalidparamexception_ce;
zend_class_entry *yii_base_invalidrouteexception_ce;
zend_class_entry *yii_base_mailevent_ce;
zend_class_entry *yii_base_model_ce;
zend_class_entry *yii_base_modelevent_ce;
zend_class_entry *yii_base_notsupportedexception_ce;
zend_class_entry *yii_base_request_ce;
zend_class_entry *yii_base_response_ce;
zend_class_entry *yii_base_unknownclassexception_ce;
zend_class_entry *yii_base_unknownmethodexception_ce;
zend_class_entry *yii_base_unknownpropertyexception_ce;
zend_class_entry *yii_base_viewevent_ce;
zend_class_entry *yii_base_viewrenderer_ce;
zend_class_entry *yii_base_widget_ce;
zend_class_entry *yii_baseyii_ce;
zend_class_entry *yii_console_application_ce;
zend_class_entry *yii_di_container_ce;
zend_class_entry *yii_di_instance_ce;
zend_class_entry *yii_helpers_arrayhelper_ce;
zend_class_entry *yii_helpers_inflector_ce;
zend_class_entry *yii_helpers_stringhelper_ce;
zend_class_entry *yii_helpers_url_ce;
zend_class_entry *yii_log_logger_ce;
zend_class_entry *yii_validators_requiredvalidator_ce;
zend_class_entry *yii_validators_validationasset_ce;
zend_class_entry *yii_web_application_ce;
zend_class_entry *yii_web_badrequesthttpexception_ce;
zend_class_entry *yii_web_controller_ce;
zend_class_entry *yii_web_cookie_ce;
zend_class_entry *yii_web_erroraction_ce;
zend_class_entry *yii_web_groupurlrule_ce;
zend_class_entry *yii_web_link_ce;
zend_class_entry *yii_web_test_ce;

ZEND_DECLARE_MODULE_GLOBALS(yii)

#define ZEPHIR_NUM_PREALLOCATED_FRAMES 25

void zephir_initialize_memory(zend_zephir_globals_def *zephir_globals_ptr TSRMLS_DC)
{
	zephir_memory_entry *start;
	size_t i;

	start = (zephir_memory_entry *) pecalloc(ZEPHIR_NUM_PREALLOCATED_FRAMES, sizeof(zephir_memory_entry), 1);
/* pecalloc() will take care of these members for every frame
	start->pointer      = 0;
	start->hash_pointer = 0;
	start->prev = NULL;
	start->next = NULL;
*/
	for (i = 0; i < ZEPHIR_NUM_PREALLOCATED_FRAMES; ++i) {
		start[i].addresses       = pecalloc(24, sizeof(zval*), 1);
		start[i].capacity        = 24;
		start[i].hash_addresses  = pecalloc(8, sizeof(zval*), 1);
		start[i].hash_capacity   = 8;

#ifndef ZEPHIR_RELEASE
		start[i].permanent = 1;
#endif
	}

	start[0].next = &start[1];
	start[ZEPHIR_NUM_PREALLOCATED_FRAMES - 1].prev = &start[ZEPHIR_NUM_PREALLOCATED_FRAMES - 2];

	for (i = 1; i < ZEPHIR_NUM_PREALLOCATED_FRAMES - 1; ++i) {
		start[i].next = &start[i + 1];
		start[i].prev = &start[i - 1];
	}

	zephir_globals_ptr->start_memory = start;
	zephir_globals_ptr->end_memory   = start + ZEPHIR_NUM_PREALLOCATED_FRAMES;

	zephir_globals_ptr->fcache = pemalloc(sizeof(HashTable), 1);
	zend_hash_init(zephir_globals_ptr->fcache, 128, NULL, NULL, 1); // zephir_fcall_cache_dtor

	/* 'Allocator sizeof operand mismatch' warning can be safely ignored */
	ALLOC_INIT_ZVAL(zephir_globals_ptr->global_null);
	Z_SET_REFCOUNT_P(zephir_globals_ptr->global_null, 2);

	/* 'Allocator sizeof operand mismatch' warning can be safely ignored */
	ALLOC_INIT_ZVAL(zephir_globals_ptr->global_false);
	Z_SET_REFCOUNT_P(zephir_globals_ptr->global_false, 2);
	ZVAL_FALSE(zephir_globals_ptr->global_false);

	/* 'Allocator sizeof operand mismatch' warning can be safely ignored */
	ALLOC_INIT_ZVAL(zephir_globals_ptr->global_true);
	Z_SET_REFCOUNT_P(zephir_globals_ptr->global_true, 2);
	ZVAL_TRUE(zephir_globals_ptr->global_true);

	//zephir_globals_ptr->initialized = 1;
}

int zephir_cleanup_fcache(void *pDest TSRMLS_DC, int num_args, va_list args, zend_hash_key *hash_key)
{
	zephir_fcall_cache_entry **entry = (zephir_fcall_cache_entry**)pDest;
	zend_class_entry *scope;
	uint len = hash_key->nKeyLength;

	assert(hash_key->arKey != NULL);
	assert(hash_key->nKeyLength > 2 * sizeof(zend_class_entry**));

	memcpy(&scope, &hash_key->arKey[len - 2 * sizeof(zend_class_entry**)], sizeof(zend_class_entry*));

/*
#ifndef ZEPHIR_RELEASE
	{
		zend_class_entry *cls;
		memcpy(&cls, &hash_key->arKey[len - sizeof(zend_class_entry**)], sizeof(zend_class_entry*));

		fprintf(stderr, "func: %s, cls: %s, scope: %s [%u]\n", (*entry)->f->common.function_name, (cls ? cls->name : "N/A"), (scope ? scope->name : "N/A"), (uint)(*entry)->times);
	}
#endif
*/

#ifndef ZEPHIR_RELEASE
	if ((*entry)->f->type != ZEND_INTERNAL_FUNCTION || (scope && scope->type != ZEND_INTERNAL_CLASS)) {
		return ZEND_HASH_APPLY_REMOVE;
	}
#else
	if ((*entry)->type != ZEND_INTERNAL_FUNCTION || (scope && scope->type != ZEND_INTERNAL_CLASS)) {
		return ZEND_HASH_APPLY_REMOVE;
	}
#endif

#if PHP_VERSION_ID >= 50400
	if (scope && scope->type == ZEND_INTERNAL_CLASS && scope->info.internal.module->type != MODULE_PERSISTENT) {
		return ZEND_HASH_APPLY_REMOVE;
	}
#else
	if (scope && scope->type == ZEND_INTERNAL_CLASS && scope->module->type != MODULE_PERSISTENT) {
		return ZEND_HASH_APPLY_REMOVE;
	}
#endif

	return ZEND_HASH_APPLY_KEEP;
}

void zephir_deinitialize_memory(TSRMLS_D)
{
	size_t i;
	zend_zephir_globals_def *zephir_globals_ptr = ZEPHIR_VGLOBAL;

	//if (zephir_globals_ptr->initialized != 1) {
	//	zephir_globals_ptr->initialized = 0;
	//	return;
	//}

	if (zephir_globals_ptr->start_memory != NULL) {
		zephir_clean_restore_stack(TSRMLS_C);
	}

	//zephir_orm_destroy_cache(TSRMLS_C);

	zend_hash_apply_with_arguments(zephir_globals_ptr->fcache TSRMLS_CC, zephir_cleanup_fcache, 0);

#ifndef ZEPHIR_RELEASE
	assert(zephir_globals_ptr->start_memory != NULL);
#endif

	for (i = 0; i < ZEPHIR_NUM_PREALLOCATED_FRAMES; ++i) {
		pefree(zephir_globals_ptr->start_memory[i].hash_addresses, 1);
		pefree(zephir_globals_ptr->start_memory[i].addresses, 1);
	}

	pefree(zephir_globals_ptr->start_memory, 1);
	zephir_globals_ptr->start_memory = NULL;

	zend_hash_destroy(zephir_globals_ptr->fcache);
	pefree(zephir_globals_ptr->fcache, 1);
	zephir_globals_ptr->fcache = NULL;

	for (i = 0; i < 2; i++) {
		zval_ptr_dtor(&zephir_globals_ptr->global_null);
		zval_ptr_dtor(&zephir_globals_ptr->global_false);
		zval_ptr_dtor(&zephir_globals_ptr->global_true);
	}

	//zephir_globals_ptr->initialized = 0;
}

static PHP_MINIT_FUNCTION(yii)
{
#if PHP_VERSION_ID < 50500
	char* old_lc_all = setlocale(LC_ALL, NULL);
	if (old_lc_all) {
		size_t len = strlen(old_lc_all);
		char *tmp  = calloc(len+1, 1);
		if (UNEXPECTED(!tmp)) {
			return FAILURE;
		}

		memcpy(tmp, old_lc_all, len);
		old_lc_all = tmp;
	}

	setlocale(LC_ALL, "C");
#endif

	ZEPHIR_INIT(yii_base_Arrayable);
	ZEPHIR_INIT(yii_base_BootstrapInterface);
	ZEPHIR_INIT(yii_base_ViewContextInterface);
	ZEPHIR_INIT(yii_web_Linkable);
	ZEPHIR_INIT(yii_web_UrlRuleInterface);
	ZEPHIR_INIT(yii_base_Object);
	ZEPHIR_INIT(yii_base_Component);
	ZEPHIR_INIT(yii_base_Exception);
	ZEPHIR_INIT(yii_di_ServiceLocator);
	ZEPHIR_INIT(yii_base_UserException);
	ZEPHIR_INIT(yii_base_Event);
	ZEPHIR_INIT(yii_base_Module);
	ZEPHIR_INIT(yii_base_Action);
	ZEPHIR_INIT(yii_base_Application);
	ZEPHIR_INIT(yii_base_Behavior);
	ZEPHIR_INIT(yii_base_Controller);
	ZEPHIR_INIT(yii_validators_Validator);
	ZEPHIR_INIT(yii_web_AssetBundle);
	ZEPHIR_INIT(yii_web_CompositeUrlRule);
	ZEPHIR_INIT(yii_web_HttpException);
	ZEPHIR_INIT(yii_BaseYii);
	ZEPHIR_INIT(yii_base_ActionEvent);
	ZEPHIR_INIT(yii_base_ActionFilter);
	ZEPHIR_INIT(yii_base_ArrayAccessTrait);
	ZEPHIR_INIT(yii_base_ArrayableTrait);
	ZEPHIR_INIT(yii_base_ErrorException);
	ZEPHIR_INIT(yii_base_ErrorHandler);
	ZEPHIR_INIT(yii_base_ExitException);
	ZEPHIR_INIT(yii_base_InlineAction);
	ZEPHIR_INIT(yii_base_InvalidCallException);
	ZEPHIR_INIT(yii_base_InvalidConfigException);
	ZEPHIR_INIT(yii_base_InvalidParamException);
	ZEPHIR_INIT(yii_base_InvalidRouteException);
	ZEPHIR_INIT(yii_base_MailEvent);
	ZEPHIR_INIT(yii_base_Model);
	ZEPHIR_INIT(yii_base_ModelEvent);
	ZEPHIR_INIT(yii_base_NotSupportedException);
	ZEPHIR_INIT(yii_base_Request);
	ZEPHIR_INIT(yii_base_Response);
	ZEPHIR_INIT(yii_base_UnknownClassException);
	ZEPHIR_INIT(yii_base_UnknownMethodException);
	ZEPHIR_INIT(yii_base_UnknownPropertyException);
	ZEPHIR_INIT(yii_base_ViewEvent);
	ZEPHIR_INIT(yii_base_ViewRenderer);
	ZEPHIR_INIT(yii_base_Widget);
	ZEPHIR_INIT(yii_console_Application);
	ZEPHIR_INIT(yii_di_Container);
	ZEPHIR_INIT(yii_di_Instance);
	ZEPHIR_INIT(yii_helpers_ArrayHelper);
	ZEPHIR_INIT(yii_helpers_Inflector);
	ZEPHIR_INIT(yii_helpers_StringHelper);
	ZEPHIR_INIT(yii_helpers_Url);
	ZEPHIR_INIT(yii_log_Logger);
	ZEPHIR_INIT(yii_validators_RequiredValidator);
	ZEPHIR_INIT(yii_validators_ValidationAsset);
	ZEPHIR_INIT(yii_web_Application);
	ZEPHIR_INIT(yii_web_BadRequestHttpException);
	ZEPHIR_INIT(yii_web_Controller);
	ZEPHIR_INIT(yii_web_Cookie);
	ZEPHIR_INIT(yii_web_ErrorAction);
	ZEPHIR_INIT(yii_web_GroupUrlRule);
	ZEPHIR_INIT(yii_web_Link);
	ZEPHIR_INIT(yii_web_Test);

#if PHP_VERSION_ID < 50500
	setlocale(LC_ALL, old_lc_all);
	free(old_lc_all);
#endif
	return SUCCESS;
}

#ifndef ZEPHIR_RELEASE
static PHP_MSHUTDOWN_FUNCTION(yii)
{

	zephir_deinitialize_memory(TSRMLS_C);

	//assert(ZEPHIR_GLOBAL(orm).parser_cache == NULL);
	//assert(ZEPHIR_GLOBAL(orm).ast_cache == NULL);

	return SUCCESS;
}
#endif

/**
 * Initialize globals on each request or each thread started
 */
static void php_zephir_init_globals(zend_yii_globals *zephir_globals TSRMLS_DC)
{

	/* Memory options */
	zephir_globals->active_memory = NULL;

	/* Virtual Symbol Tables */
	zephir_globals->active_symbol_table = NULL;

	/* Recursive Lock */
	zephir_globals->recursive_lock = 0;


}

static PHP_RINIT_FUNCTION(yii)
{

	zend_yii_globals *zephir_globals_ptr = ZEPHIR_VGLOBAL;

	php_zephir_init_globals(zephir_globals_ptr TSRMLS_CC);
	//zephir_init_interned_strings(TSRMLS_C);

	zephir_initialize_memory(zephir_globals_ptr TSRMLS_CC);

	return SUCCESS;
}

static PHP_RSHUTDOWN_FUNCTION(yii)
{

	zephir_deinitialize_memory(TSRMLS_C);
	return SUCCESS;
}

static PHP_MINFO_FUNCTION(yii)
{
	php_info_print_box_start(0);
	php_printf("%s", PHP_YII_DESCRIPTION);
	php_info_print_box_end();

	php_info_print_table_start();
	php_info_print_table_header(2, PHP_YII_NAME, "enabled");
	php_info_print_table_row(2, "Author", PHP_YII_AUTHOR);
	php_info_print_table_row(2, "Version", PHP_YII_VERSION);
	php_info_print_table_row(2, "Powered by Zephir", "Version " PHP_YII_ZEPVERSION);
	php_info_print_table_end();


}

static PHP_GINIT_FUNCTION(yii)
{
	php_zephir_init_globals(yii_globals TSRMLS_CC);
}

static PHP_GSHUTDOWN_FUNCTION(yii)
{

}

zend_module_entry yii_module_entry = {
	STANDARD_MODULE_HEADER_EX,
	NULL,
	NULL,
	PHP_YII_EXTNAME,
	NULL,
	PHP_MINIT(yii),
#ifndef ZEPHIR_RELEASE
	PHP_MSHUTDOWN(yii),
#else
	NULL,
#endif
	PHP_RINIT(yii),
	PHP_RSHUTDOWN(yii),
	PHP_MINFO(yii),
	PHP_YII_VERSION,
	ZEND_MODULE_GLOBALS(yii),
	PHP_GINIT(yii),
	PHP_GSHUTDOWN(yii),
	NULL,
	STANDARD_MODULE_PROPERTIES_EX
};

#ifdef COMPILE_DL_YII
ZEND_GET_MODULE(yii)
#endif
