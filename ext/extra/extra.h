#include <Zend/zend_execute.h>

void yii_web_response_http_statuses(zval **arr);
void yii_helpers_inflector_plurals(zval **arr);
void yii_helpers_inflector_singulars(zval **arr);
void yii_helpers_inflector_specials(zval **arr);
void yii_helpers_inflector_transliteration(zval **arr);
void yii_validators_validator_built_in_validators(zval **arr);
ZEND_API void zend_trait_use(zend_class_entry *class_entry TSRMLS_DC, int num_trait, ...);
