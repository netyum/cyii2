#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include <Zend/zend.h>

#include "php_yii.h"
#include "kernel/memory.h"
#include "kernel/main.h"
#include "extra.h"


void a(zval *arr) {
	add_index_string(arr, 1, "Hello", 1);
	Z_ADDREF_P(arr);
}

void yii_web_response_http_statuses(zval **arr) {
	if (Z_TYPE_P(*arr) != IS_ARRAY) {
		array_init(*arr);
	}
	add_index_string(*arr, 100, "Continue", 1);
        add_index_string(*arr, 101, "Switching Protocols", 1);
        add_index_string(*arr, 102, "Processing", 1);
        add_index_string(*arr, 118, "Connection timed out", 1);
        add_index_string(*arr, 200, "OK", 1);
        add_index_string(*arr, 201, "Created", 1);
        add_index_string(*arr, 202, "Accepted", 1);
        add_index_string(*arr, 203, "Non-Authoritative", 1);
        add_index_string(*arr, 204, "No Content", 1);
        add_index_string(*arr, 205, "Reset Content", 1);
        add_index_string(*arr, 206, "Partial Content", 1);
        add_index_string(*arr, 207, "Multi-Status", 1);
        add_index_string(*arr, 208, "Already Reported", 1);
        add_index_string(*arr, 210, "Content Different", 1);
        add_index_string(*arr, 226, "IM Used", 1);
        add_index_string(*arr, 300, "Multiple Choices", 1);
        add_index_string(*arr, 301, "Moved Permanently", 1);
        add_index_string(*arr, 302, "Found", 1);
        add_index_string(*arr, 303, "See Other", 1);
        add_index_string(*arr, 304, "Not Modified", 1);
        add_index_string(*arr, 305, "Use Proxy", 1);
        add_index_string(*arr, 306, "Reserved", 1);
        add_index_string(*arr, 307, "Temporary Redirect", 1);
        add_index_string(*arr, 308, "Permanent Redirect", 1);
        add_index_string(*arr, 310, "Too many Redirect", 1);
        add_index_string(*arr, 400, "Bad Request", 1);
        add_index_string(*arr, 401, "Unauthorized", 1);
        add_index_string(*arr, 402, "Payment Required", 1);
        add_index_string(*arr, 403, "Forbidden", 1);
        add_index_string(*arr, 404, "Not Found", 1);
        add_index_string(*arr, 405, "Method Not Allowed", 1);
        add_index_string(*arr, 406, "Not Acceptable", 1);
        add_index_string(*arr, 407, "Proxy Authentication Required", 1);
        add_index_string(*arr, 408, "Request Time-out", 1);
        add_index_string(*arr, 409, "Conflict", 1);
        add_index_string(*arr, 410, "Gone", 1);
        add_index_string(*arr, 411, "Length Required", 1);
        add_index_string(*arr, 412, "Precondition Failed", 1);
        add_index_string(*arr, 413, "Request Entity Too Large", 1);
        add_index_string(*arr, 414, "Request-URI Too Long", 1);
        add_index_string(*arr, 415, "Unsupported Media Type", 1);
        add_index_string(*arr, 416, "Requested range unsatisfiable", 1);
        add_index_string(*arr, 417, "Expectation failed", 1);
        add_index_string(*arr, 418, "I\'m a teapot", 1);
        add_index_string(*arr, 422, "Unprocessable entity", 1);
        add_index_string(*arr, 423, "Locked", 1);
        add_index_string(*arr, 424, "Method failure", 1);
        add_index_string(*arr, 425, "Unordered Collection", 1);
        add_index_string(*arr, 426, "Upgrade Required", 1);
        add_index_string(*arr, 428, "Precondition Required", 1);
        add_index_string(*arr, 429, "Too Many Requests", 1);
        add_index_string(*arr, 431, "Request Header Fields Too Large", 1);
        add_index_string(*arr, 449, "Retry With", 1);
        add_index_string(*arr, 450, "Blocked by Windows Parental Controls", 1);
        add_index_string(*arr, 500, "Internal Server Error", 1);
        add_index_string(*arr, 501, "Not Implemented", 1);
        add_index_string(*arr, 502, "Bad Gateway ou Proxy Error", 1);
        add_index_string(*arr, 503, "Service Unavailable", 1);
        add_index_string(*arr, 504, "Gateway Time-out", 1);
        add_index_string(*arr, 505, "HTTP Version not supported", 1);
        add_index_string(*arr, 507, "Insufficient storage", 1);
        add_index_string(*arr, 508, "Loop Detected", 1);
        add_index_string(*arr, 509, "Bandwidth Limit Exceeded", 1);
        add_index_string(*arr, 510, "Not Extended", 1);
        add_index_string(*arr, 511, "Network Authentication Required", 1);
	Z_ADDREF_P(*arr);

}

void yii_helpers_inflector_plurals(zval **arr) {
	if (Z_TYPE_P(*arr) != IS_ARRAY) {
		array_init(*arr);
	}
	add_assoc_string(*arr, "/([nrlm]ese|deer|fish|sheep|measles|ois|pox|media)$/i", "\\1", 1);
        add_assoc_string(*arr, "/^(sea[- ]bass)$/i", "\\1", 1);
        add_assoc_string(*arr, "/(m)ove$/i", "\\1oves", 1);
        add_assoc_string(*arr, "/(f)oot$/i", "\\1eet", 1);
        add_assoc_string(*arr, "/(h)uman$/i", "\\1umans", 1);
        add_assoc_string(*arr, "/(s)tatus$/i", "\\1tatuses", 1);
        add_assoc_string(*arr, "/(s)taff$/i", "\\1taff", 1);
        add_assoc_string(*arr, "/(t)ooth$/i", "\\1eeth", 1);
        add_assoc_string(*arr, "/(quiz)$/i", "\\1zes", 1);
        add_assoc_string(*arr, "/^(ox)$/i", "\\1\\2en", 1);
        add_assoc_string(*arr, "/([m|l])ouse$/i", "\\1ice", 1);
        add_assoc_string(*arr, "/(matr|vert|ind)(ix|ex)$/i", "\\1ices", 1);
        add_assoc_string(*arr, "/(x|ch|ss|sh)$/i", "\\1es", 1);
        add_assoc_string(*arr, "/([^aeiouy]|qu)y$/i", "\\1ies", 1);
        add_assoc_string(*arr, "/(hive)$/i", "\\1s", 1);
        add_assoc_string(*arr, "/(?:([^f])fe|([lr])f)$/i", "\\1\\2ves", 1);
        add_assoc_string(*arr, "/sis$/i", "ses", 1);
        add_assoc_string(*arr, "/([ti])um$/i", "\\1a", 1);
        add_assoc_string(*arr, "/(p)erson$/i", "\\1eople", 1);
        add_assoc_string(*arr, "/(m)an$/i", "\\1en", 1);
        add_assoc_string(*arr, "/(c)hild$/i", "\\1hildren", 1);
        add_assoc_string(*arr, "/(buffal|tomat|potat|ech|her|vet)o$/i", "\\1oes", 1);
        add_assoc_string(*arr, "/(alumn|bacill|cact|foc|fung|nucle|radi|stimul|syllab|termin|vir)us$/i", "\\1i", 1);
        add_assoc_string(*arr, "/us$/i", "uses", 1);
        add_assoc_string(*arr, "/(alias)$/i", "\\1es", 1);
        add_assoc_string(*arr, "/(ax|cris|test)is$/i", "\\1es", 1);
        add_assoc_string(*arr, "/s$/", "s", 1);
        add_assoc_string(*arr, "/^$/", "", 1);
        add_assoc_string(*arr, "/$/", "s", 1);
	Z_ADDREF_P(*arr);
}

void yii_helpers_inflector_singulars(zval **arr) {
	if (Z_TYPE_P(*arr) != IS_ARRAY) {
		array_init(*arr);
	}
        add_assoc_string(*arr, "/([nrlm]ese|deer|fish|sheep|measles|ois|pox|media|ss)$/i", "\\1", 1);
        add_assoc_string(*arr, "/^(sea[- ]bass)$/i", "\\1", 1);
        add_assoc_string(*arr, "/(s)tatuses$/i", "\\1tatus", 1);
        add_assoc_string(*arr, "/(f)eet$/i", "\\1oot", 1);
        add_assoc_string(*arr, "/(t)eeth$/i", "\\1ooth", 1);
        add_assoc_string(*arr, "/^(.*)(menu)s$/i", "\\1\\2", 1);
        add_assoc_string(*arr, "/(quiz)zes$/i", "\\\\1", 1);
        add_assoc_string(*arr, "/(matr)ices$/i", "\\1ix", 1);
        add_assoc_string(*arr, "/(vert|ind)ices$/i", "\\1ex", 1);
        add_assoc_string(*arr, "/^(ox)en/i", "\\1", 1);
        add_assoc_string(*arr, "/(alias)(es)*$/i", "\\1", 1);
        add_assoc_string(*arr, "/(alumn|bacill|cact|foc|fung|nucle|radi|stimul|syllab|termin|viri?)i$/i", "\\1us", 1);
        add_assoc_string(*arr, "/([ftw]ax)es/i", "\\1", 1);
        add_assoc_string(*arr, "/(cris|ax|test)es$/i", "\\1is", 1);
        add_assoc_string(*arr, "/(shoe|slave)s$/i", "\\1", 1);
        add_assoc_string(*arr, "/(o)es$/i", "\\1", 1);
        add_assoc_string(*arr, "/ouses$/", "ouse", 1);
        add_assoc_string(*arr, "/([^a])uses$/", "\\1us", 1);
        add_assoc_string(*arr, "/([m|l])ice$/i", "\\1ouse", 1);
        add_assoc_string(*arr, "/(x|ch|ss|sh)es$/i", "\\1", 1);
        add_assoc_string(*arr, "/(m)ovies$/i", "\\1\\2ovie", 1);
        add_assoc_string(*arr, "/(s)eries$/i", "\\1\\2eries", 1);
        add_assoc_string(*arr, "/([^aeiouy]|qu)ies$/i", "\\1y", 1);
        add_assoc_string(*arr, "/([lr])ves$/i", "\\1f", 1);
        add_assoc_string(*arr, "/(tive)s$/i", "\\1", 1);
        add_assoc_string(*arr, "/(hive)s$/i", "\\1", 1);
        add_assoc_string(*arr, "/(drive)s$/i", "\\1", 1);
        add_assoc_string(*arr, "/([^fo])ves$/i", "\\1fe", 1);
        add_assoc_string(*arr, "/(^analy)ses$/i", "\\1sis", 1);
        add_assoc_string(*arr, "/(analy|diagno|^ba|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i", "\\1\\2sis", 1);
        add_assoc_string(*arr, "/([ti])a$/i", "\\1um", 1);
        add_assoc_string(*arr, "/(p)eople$/i", "\\1\\2erson", 1);
        add_assoc_string(*arr, "/(m)en$/i", "\\1an", 1);
        add_assoc_string(*arr, "/(c)hildren$/i", "\\1\\2hild", 1);
        add_assoc_string(*arr, "/(n)ews$/i", "\\1\\2ews", 1);
        add_assoc_string(*arr, "/eaus$/", "eau", 1);
        add_assoc_string(*arr, "/^(.*us)$/", "\\\\1", 1);
        add_assoc_string(*arr, "/s$/i", "", 1);
	Z_ADDREF_P(*arr);
}

void yii_helpers_inflector_specials(zval **arr) {
	if (Z_TYPE_P(*arr) != IS_ARRAY) {
		array_init(*arr);
	}
        add_assoc_string(*arr, "atlas", "atlases", 1);
        add_assoc_string(*arr, "beef", "beefs", 1);
        add_assoc_string(*arr, "brother", "brothers", 1);
        add_assoc_string(*arr, "cafe", "cafes", 1);
        add_assoc_string(*arr, "child", "children", 1);
        add_assoc_string(*arr, "cookie", "cookies", 1);
        add_assoc_string(*arr, "corpus", "corpuses", 1);
        add_assoc_string(*arr, "cow", "cows", 1);
        add_assoc_string(*arr, "curve", "curves", 1);
        add_assoc_string(*arr, "foe", "foes", 1);
        add_assoc_string(*arr, "ganglion", "ganglions", 1);
        add_assoc_string(*arr, "genie", "genies", 1);
        add_assoc_string(*arr, "genus", "genera", 1);
        add_assoc_string(*arr, "graffito", "graffiti", 1);
        add_assoc_string(*arr, "hoof", "hoofs", 1);
        add_assoc_string(*arr, "loaf", "loaves", 1);
        add_assoc_string(*arr, "man", "men", 1);
        add_assoc_string(*arr, "money", "monies", 1);
        add_assoc_string(*arr, "mongoose", "mongooses", 1);
        add_assoc_string(*arr, "move", "moves", 1);
        add_assoc_string(*arr, "mythos", "mythoi", 1);
        add_assoc_string(*arr, "niche", "niches", 1);
        add_assoc_string(*arr, "numen", "numina", 1);
        add_assoc_string(*arr, "occiput", "occiputs", 1);
        add_assoc_string(*arr, "octopus", "octopuses", 1);
        add_assoc_string(*arr, "opus", "opuses", 1);
        add_assoc_string(*arr, "ox", "oxen", 1);
        add_assoc_string(*arr, "penis", "penises", 1);
        add_assoc_string(*arr, "sex", "sexes", 1);
        add_assoc_string(*arr, "soliloquy", "soliloquies", 1);
        add_assoc_string(*arr, "testis", "testes", 1);
        add_assoc_string(*arr, "trilby", "trilbys", 1);
        add_assoc_string(*arr, "turf", "turfs", 1);
        add_assoc_string(*arr, "wave", "waves", 1);
        add_assoc_string(*arr, "Amoyese", "Amoyese", 1);
        add_assoc_string(*arr, "bison", "bison", 1);
        add_assoc_string(*arr, "Borghese", "Borghese", 1);
        add_assoc_string(*arr, "bream", "bream", 1);
        add_assoc_string(*arr, "breeches", "breeches", 1);
        add_assoc_string(*arr, "britches", "britches", 1);
        add_assoc_string(*arr, "buffalo", "buffalo", 1);
        add_assoc_string(*arr, "cantus", "cantus", 1);
        add_assoc_string(*arr, "carp", "carp", 1);
        add_assoc_string(*arr, "chassis", "chassis", 1);
        add_assoc_string(*arr, "clippers", "clippers", 1);
        add_assoc_string(*arr, "cod", "cod", 1);
        add_assoc_string(*arr, "coitus", "coitus", 1);
        add_assoc_string(*arr, "Congoese", "Congoese", 1);
        add_assoc_string(*arr, "contretemps", "contretemps", 1);
        add_assoc_string(*arr, "corps", "corps", 1);
        add_assoc_string(*arr, "debris", "debris", 1);
        add_assoc_string(*arr, "diabetes", "diabetes", 1);
        add_assoc_string(*arr, "djinn", "djinn", 1);
        add_assoc_string(*arr, "eland", "eland", 1);
        add_assoc_string(*arr, "elk", "elk", 1);
        add_assoc_string(*arr, "equipment", "equipment", 1);
        add_assoc_string(*arr, "Faroese", "Faroese", 1);
        add_assoc_string(*arr, "flounder", "flounder", 1);
        add_assoc_string(*arr, "Foochowese", "Foochowese", 1);
        add_assoc_string(*arr, "gallows", "gallows", 1);
        add_assoc_string(*arr, "Genevese", "Genevese", 1);
        add_assoc_string(*arr, "Genoese", "Genoese", 1);
        add_assoc_string(*arr, "Gilbertese", "Gilbertese", 1);
        add_assoc_string(*arr, "graffiti", "graffiti", 1);
        add_assoc_string(*arr, "headquarters", "headquarters", 1);
        add_assoc_string(*arr, "herpes", "herpes", 1);
        add_assoc_string(*arr, "hijinks", "hijinks", 1);
        add_assoc_string(*arr, "Hottentotese", "Hottentotese", 1);
        add_assoc_string(*arr, "information", "information", 1);
        add_assoc_string(*arr, "innings", "innings", 1);
        add_assoc_string(*arr, "jackanapes", "jackanapes", 1);
        add_assoc_string(*arr, "Kiplingese", "Kiplingese", 1);
        add_assoc_string(*arr, "Kongoese", "Kongoese", 1);
        add_assoc_string(*arr, "Lucchese", "Lucchese", 1);
        add_assoc_string(*arr, "mackerel", "mackerel", 1);
        add_assoc_string(*arr, "Maltese", "Maltese", 1);
        add_assoc_string(*arr, "mews", "mews", 1);
        add_assoc_string(*arr, "moose", "moose", 1);
        add_assoc_string(*arr, "mumps", "mumps", 1);
        add_assoc_string(*arr, "Nankingese", "Nankingese", 1);
        add_assoc_string(*arr, "news", "news", 1);
        add_assoc_string(*arr, "nexus", "nexus", 1);
        add_assoc_string(*arr, "Niasese", "Niasese", 1);
        add_assoc_string(*arr, "Pekingese", "Pekingese", 1);
        add_assoc_string(*arr, "Piedmontese", "Piedmontese", 1);
        add_assoc_string(*arr, "pincers", "pincers", 1);
        add_assoc_string(*arr, "Pistoiese", "Pistoiese", 1);
        add_assoc_string(*arr, "pliers", "pliers", 1);
        add_assoc_string(*arr, "Portuguese", "Portuguese", 1);
        add_assoc_string(*arr, "proceedings", "proceedings", 1);
        add_assoc_string(*arr, "rabies", "rabies", 1);
        add_assoc_string(*arr, "rice", "rice", 1);
        add_assoc_string(*arr, "rhinoceros", "rhinoceros", 1);
        add_assoc_string(*arr, "salmon", "salmon", 1);
        add_assoc_string(*arr, "Sarawakese", "Sarawakese", 1);
        add_assoc_string(*arr, "scissors", "scissors", 1);
        add_assoc_string(*arr, "series", "series", 1);
        add_assoc_string(*arr, "Shavese", "Shavese", 1);
        add_assoc_string(*arr, "shears", "shears", 1);
        add_assoc_string(*arr, "siemens", "siemens", 1);
        add_assoc_string(*arr, "species", "species", 1);
        add_assoc_string(*arr, "swine", "swine", 1);
        add_assoc_string(*arr, "testes", "testes", 1);
        add_assoc_string(*arr, "trousers", "trousers", 1);
        add_assoc_string(*arr, "trout", "trout", 1);
        add_assoc_string(*arr, "tuna", "tuna", 1);
        add_assoc_string(*arr, "Vermontese", "Vermontese", 1);
        add_assoc_string(*arr, "Wenchowese", "Wenchowese", 1);
        add_assoc_string(*arr, "whiting", "whiting", 1);
        add_assoc_string(*arr, "wildebeest", "wildebeest", 1);
        add_assoc_string(*arr, "Yengeese", "Yengeese", 1);
	Z_ADDREF_P(*arr);
}


void yii_helpers_inflector_transliteration(zval **arr) {
	if (Z_TYPE_P(*arr) != IS_ARRAY) {
		array_init(*arr);
	}
	// Latin
        add_assoc_string(*arr, "À", "A", 1);
        add_assoc_string(*arr, "Á", "A", 1);
        add_assoc_string(*arr, "Â", "A", 1);
        add_assoc_string(*arr, "Ã", "A", 1);
        add_assoc_string(*arr, "Ä", "A", 1);
        add_assoc_string(*arr, "Å", "A", 1);
        add_assoc_string(*arr, "Æ", "AE", 1);
        add_assoc_string(*arr, "Ç", "C", 1);
        add_assoc_string(*arr, "È", "E", 1);
        add_assoc_string(*arr, "É", "E", 1);
        add_assoc_string(*arr, "Ê", "E", 1);
        add_assoc_string(*arr, "Ë", "E", 1);
        add_assoc_string(*arr, "Ì", "I", 1);
        add_assoc_string(*arr, "Í", "I", 1);
        add_assoc_string(*arr, "Î", "I", 1);
        add_assoc_string(*arr, "Ï", "I", 1);
        add_assoc_string(*arr, "Ð", "D", 1);
        add_assoc_string(*arr, "Ñ", "N", 1);
        add_assoc_string(*arr, "Ò", "O", 1);
        add_assoc_string(*arr, "Ó", "O", 1);
        add_assoc_string(*arr, "Ô", "O", 1);
        add_assoc_string(*arr, "Õ", "O", 1);
        add_assoc_string(*arr, "Ö", "O", 1);
        add_assoc_string(*arr, "Ő", "O", 1);
        add_assoc_string(*arr, "Ø", "O", 1);
        add_assoc_string(*arr, "Ù", "U", 1);
        add_assoc_string(*arr, "Ú", "U", 1);
        add_assoc_string(*arr, "Û", "U", 1);
        add_assoc_string(*arr, "Ü", "U", 1);
        add_assoc_string(*arr, "Ű", "U", 1);
        add_assoc_string(*arr, "Ý", "Y", 1);
        add_assoc_string(*arr, "Þ", "TH", 1);
        add_assoc_string(*arr, "ß", "ss", 1);
        add_assoc_string(*arr, "à", "a", 1);
        add_assoc_string(*arr, "á", "a", 1);
        add_assoc_string(*arr, "â", "a", 1);
        add_assoc_string(*arr, "ã", "a", 1);
        add_assoc_string(*arr, "ä", "a", 1);
        add_assoc_string(*arr, "å", "a", 1);
        add_assoc_string(*arr, "æ", "ae", 1);
        add_assoc_string(*arr, "ç", "c", 1);
        add_assoc_string(*arr, "è", "e", 1);
        add_assoc_string(*arr, "é", "e", 1);
        add_assoc_string(*arr, "ê", "e", 1);
        add_assoc_string(*arr, "ë", "e", 1);
        add_assoc_string(*arr, "ì", "i", 1);
        add_assoc_string(*arr, "í", "i", 1);
        add_assoc_string(*arr, "î", "i", 1);
        add_assoc_string(*arr, "ï", "i", 1);
        add_assoc_string(*arr, "ð", "d", 1);
        add_assoc_string(*arr, "ñ", "n", 1);
        add_assoc_string(*arr, "ò", "o", 1);
        add_assoc_string(*arr, "ó", "o", 1);
        add_assoc_string(*arr, "ô", "o", 1);
        add_assoc_string(*arr, "õ", "o", 1);
        add_assoc_string(*arr, "ö", "o", 1);
        add_assoc_string(*arr, "ő", "o", 1);
        add_assoc_string(*arr, "ø", "o", 1);
        add_assoc_string(*arr, "ù", "u", 1);
        add_assoc_string(*arr, "ú", "u", 1);
        add_assoc_string(*arr, "û", "u", 1);
        add_assoc_string(*arr, "ü", "u", 1);
        add_assoc_string(*arr, "ű", "u", 1);
        add_assoc_string(*arr, "ý", "y", 1);
        add_assoc_string(*arr, "þ", "th", 1);
        add_assoc_string(*arr, "ÿ", "y", 1);

	// Latin symbols
        add_assoc_string(*arr, "©", "(c)", 1);

	// Greek
        add_assoc_string(*arr, "Α", "A", 1);
        add_assoc_string(*arr, "Β", "B", 1);
        add_assoc_string(*arr, "Γ", "G", 1);
        add_assoc_string(*arr, "Δ", "D", 1);
        add_assoc_string(*arr, "Ε", "E", 1);
        add_assoc_string(*arr, "Ζ", "Z", 1);
        add_assoc_string(*arr, "Η", "H", 1);
        add_assoc_string(*arr, "Θ", "8", 1);
        add_assoc_string(*arr, "Ι", "I", 1);
        add_assoc_string(*arr, "Κ", "K", 1);
        add_assoc_string(*arr, "Λ", "L", 1);
        add_assoc_string(*arr, "Μ", "M", 1);
        add_assoc_string(*arr, "Ν", "N", 1);
        add_assoc_string(*arr, "Ξ", "3", 1);
        add_assoc_string(*arr, "Ο", "O", 1);
        add_assoc_string(*arr, "Π", "P", 1);
        add_assoc_string(*arr, "Ρ", "R", 1);
        add_assoc_string(*arr, "Σ", "S", 1);
        add_assoc_string(*arr, "Τ", "T", 1);
        add_assoc_string(*arr, "Υ", "Y", 1);
        add_assoc_string(*arr, "Φ", "F", 1);
        add_assoc_string(*arr, "Χ", "X", 1);
        add_assoc_string(*arr, "Ψ", "PS", 1);
        add_assoc_string(*arr, "Ω", "W", 1);
        add_assoc_string(*arr, "Ά", "A", 1);
        add_assoc_string(*arr, "Έ", "E", 1);
        add_assoc_string(*arr, "Ί", "I", 1);
        add_assoc_string(*arr, "Ό", "O", 1);
        add_assoc_string(*arr, "Ύ", "Y", 1);
        add_assoc_string(*arr, "Ή", "H", 1);
        add_assoc_string(*arr, "Ώ", "W", 1);
        add_assoc_string(*arr, "Ϊ", "I", 1);
        add_assoc_string(*arr, "Ϋ", "Y", 1);
        add_assoc_string(*arr, "α", "a", 1);
        add_assoc_string(*arr, "β", "b", 1);
        add_assoc_string(*arr, "γ", "g", 1);
        add_assoc_string(*arr, "δ", "d", 1);
        add_assoc_string(*arr, "ε", "e", 1);
        add_assoc_string(*arr, "ζ", "z", 1);
        add_assoc_string(*arr, "η", "h", 1);
        add_assoc_string(*arr, "θ", "8", 1);
        add_assoc_string(*arr, "ι", "i", 1);
        add_assoc_string(*arr, "κ", "k", 1);
        add_assoc_string(*arr, "λ", "l", 1);
        add_assoc_string(*arr, "μ", "m", 1);
        add_assoc_string(*arr, "ν", "n", 1);
        add_assoc_string(*arr, "ξ", "3", 1);
        add_assoc_string(*arr, "ο", "o", 1);
        add_assoc_string(*arr, "π", "p", 1);
        add_assoc_string(*arr, "ρ", "r", 1);
        add_assoc_string(*arr, "σ", "s", 1);
        add_assoc_string(*arr, "τ", "t", 1);
        add_assoc_string(*arr, "υ", "y", 1);
        add_assoc_string(*arr, "φ", "f", 1);
        add_assoc_string(*arr, "χ", "x", 1);
        add_assoc_string(*arr, "ψ", "ps", 1);
        add_assoc_string(*arr, "ω", "w", 1);
        add_assoc_string(*arr, "ά", "a", 1);
        add_assoc_string(*arr, "έ", "e", 1);
        add_assoc_string(*arr, "ί", "i", 1);
        add_assoc_string(*arr, "ό", "o", 1);
        add_assoc_string(*arr, "ύ", "y", 1);
        add_assoc_string(*arr, "ή", "h", 1);
        add_assoc_string(*arr, "ώ", "w", 1);
        add_assoc_string(*arr, "ς", "s", 1);
        add_assoc_string(*arr, "ϊ", "i", 1);
        add_assoc_string(*arr, "ΰ", "y", 1);
        add_assoc_string(*arr, "ϋ", "y", 1);
        add_assoc_string(*arr, "ΐ", "i", 1);

	// Turkish
        add_assoc_string(*arr, "Ş", "S", 1);
        add_assoc_string(*arr, "İ", "I", 1);
        add_assoc_string(*arr, "Ç", "C", 1);
        add_assoc_string(*arr, "Ü", "U", 1);
        add_assoc_string(*arr, "Ö", "O", 1);
        add_assoc_string(*arr, "Ğ", "G", 1);
        add_assoc_string(*arr, "ş", "s", 1);
        add_assoc_string(*arr, "ı", "i", 1);
        add_assoc_string(*arr, "ç", "c", 1);
        add_assoc_string(*arr, "ü", "u", 1);
        add_assoc_string(*arr, "ö", "o", 1);
        add_assoc_string(*arr, "ğ", "g", 1);

	// Russian
        add_assoc_string(*arr, "А", "A", 1);
        add_assoc_string(*arr, "Б", "B", 1);
        add_assoc_string(*arr, "В", "V", 1);
        add_assoc_string(*arr, "Г", "G", 1);
        add_assoc_string(*arr, "Д", "D", 1);
        add_assoc_string(*arr, "Е", "E", 1);
        add_assoc_string(*arr, "Ё", "Yo", 1);
        add_assoc_string(*arr, "Ж", "Zh", 1);
        add_assoc_string(*arr, "З", "Z", 1);
        add_assoc_string(*arr, "И", "I", 1);
        add_assoc_string(*arr, "Й", "J", 1);
        add_assoc_string(*arr, "К", "K", 1);
        add_assoc_string(*arr, "Л", "L", 1);
        add_assoc_string(*arr, "М", "M", 1);
        add_assoc_string(*arr, "Н", "N", 1);
        add_assoc_string(*arr, "О", "O", 1);
        add_assoc_string(*arr, "П", "P", 1);
        add_assoc_string(*arr, "Р", "R", 1);
        add_assoc_string(*arr, "С", "S", 1);
        add_assoc_string(*arr, "Т", "T", 1);
        add_assoc_string(*arr, "У", "U", 1);
        add_assoc_string(*arr, "Ф", "F", 1);
        add_assoc_string(*arr, "Х", "H", 1);
        add_assoc_string(*arr, "Ц", "C", 1);
        add_assoc_string(*arr, "Ч", "Ch", 1);
        add_assoc_string(*arr, "Ш", "Sh", 1);
        add_assoc_string(*arr, "Щ", "Sh", 1);
        add_assoc_string(*arr, "Ъ", "", 1);
        add_assoc_string(*arr, "Ы", "Y", 1);
        add_assoc_string(*arr, "Ь", "", 1);
        add_assoc_string(*arr, "Э", "E", 1);
        add_assoc_string(*arr, "Ю", "Yu", 1);
        add_assoc_string(*arr, "Я", "Ya", 1);
        add_assoc_string(*arr, "а", "a", 1);
        add_assoc_string(*arr, "б", "b", 1);
        add_assoc_string(*arr, "в", "v", 1);
        add_assoc_string(*arr, "г", "g", 1);
        add_assoc_string(*arr, "д", "d", 1);
        add_assoc_string(*arr, "е", "e", 1);
        add_assoc_string(*arr, "ё", "yo", 1);
        add_assoc_string(*arr, "ж", "zh", 1);
        add_assoc_string(*arr, "з", "z", 1);
        add_assoc_string(*arr, "и", "i", 1);
        add_assoc_string(*arr, "й", "j", 1);
        add_assoc_string(*arr, "к", "k", 1);
        add_assoc_string(*arr, "л", "l", 1);
        add_assoc_string(*arr, "м", "m", 1);
        add_assoc_string(*arr, "н", "n", 1);
        add_assoc_string(*arr, "о", "o", 1);
        add_assoc_string(*arr, "п", "p", 1);
        add_assoc_string(*arr, "р", "r", 1);
        add_assoc_string(*arr, "с", "s", 1);
        add_assoc_string(*arr, "т", "t", 1);
        add_assoc_string(*arr, "у", "u", 1);
        add_assoc_string(*arr, "ф", "f", 1);
        add_assoc_string(*arr, "х", "h", 1);
        add_assoc_string(*arr, "ц", "c", 1);
        add_assoc_string(*arr, "ч", "ch", 1);
        add_assoc_string(*arr, "ш", "sh", 1);
        add_assoc_string(*arr, "щ", "sh", 1);
        add_assoc_string(*arr, "ъ", "", 1);
        add_assoc_string(*arr, "ы", "y", 1);
        add_assoc_string(*arr, "ь", "", 1);
        add_assoc_string(*arr, "э", "e", 1);
        add_assoc_string(*arr, "ю", "yu", 1);
        add_assoc_string(*arr, "я", "ya", 1);

	// Ukrainian
        add_assoc_string(*arr, "Є", "Ye", 1);
        add_assoc_string(*arr, "І", "I", 1);
        add_assoc_string(*arr, "Ї", "Yi", 1);
        add_assoc_string(*arr, "Ґ", "G", 1);
        add_assoc_string(*arr, "є", "ye", 1);
        add_assoc_string(*arr, "і", "i", 1);
        add_assoc_string(*arr, "ї", "yi", 1);
        add_assoc_string(*arr, "ґ", "g", 1);

	// Czech
        add_assoc_string(*arr, "Č", "C", 1);
        add_assoc_string(*arr, "Ď", "D", 1);
        add_assoc_string(*arr, "Ě", "E", 1);
        add_assoc_string(*arr, "Ň", "N", 1);
        add_assoc_string(*arr, "Ř", "R", 1);
        add_assoc_string(*arr, "Š", "S", 1);
        add_assoc_string(*arr, "Ť", "T", 1);
        add_assoc_string(*arr, "Ů", "U", 1);
        add_assoc_string(*arr, "Ž", "Z", 1);
        add_assoc_string(*arr, "č", "c", 1);
        add_assoc_string(*arr, "ď", "d", 1);
        add_assoc_string(*arr, "ě", "e", 1);
        add_assoc_string(*arr, "ň", "n", 1);
        add_assoc_string(*arr, "ř", "r", 1);
        add_assoc_string(*arr, "š", "s", 1);
        add_assoc_string(*arr, "ť", "t", 1);
        add_assoc_string(*arr, "ů", "u", 1);
        add_assoc_string(*arr, "ž", "z", 1);

	// Polish
        add_assoc_string(*arr, "Ą", "A", 1);
        add_assoc_string(*arr, "Ć", "C", 1);
        add_assoc_string(*arr, "Ę", "e", 1);
        add_assoc_string(*arr, "Ł", "L", 1);
        add_assoc_string(*arr, "Ń", "N", 1);
        add_assoc_string(*arr, "Ó", "o", 1);
        add_assoc_string(*arr, "Ś", "S", 1);
        add_assoc_string(*arr, "Ź", "Z", 1);
        add_assoc_string(*arr, "Ż", "Z", 1);
        add_assoc_string(*arr, "ą", "a", 1);
        add_assoc_string(*arr, "ć", "c", 1);
        add_assoc_string(*arr, "ę", "e", 1);
        add_assoc_string(*arr, "ł", "l", 1);
        add_assoc_string(*arr, "ń", "n", 1);
        add_assoc_string(*arr, "ó", "o", 1);
        add_assoc_string(*arr, "ś", "s", 1);
        add_assoc_string(*arr, "ź", "z", 1);
        add_assoc_string(*arr, "ż", "z", 1);

	// Latvian
        add_assoc_string(*arr, "Ā", "A", 1);
        add_assoc_string(*arr, "Č", "C", 1);
        add_assoc_string(*arr, "Ē", "E", 1);
        add_assoc_string(*arr, "Ģ", "G", 1);
        add_assoc_string(*arr, "Ī", "i", 1);
        add_assoc_string(*arr, "Ķ", "k", 1);
        add_assoc_string(*arr, "Ļ", "L", 1);
        add_assoc_string(*arr, "Ņ", "N", 1);
        add_assoc_string(*arr, "Š", "S", 1);
        add_assoc_string(*arr, "Ū", "u", 1);
        add_assoc_string(*arr, "Ž", "Z", 1);
        add_assoc_string(*arr, "ā", "a", 1);
        add_assoc_string(*arr, "č", "c", 1);
        add_assoc_string(*arr, "ē", "e", 1);
        add_assoc_string(*arr, "ģ", "g", 1);
        add_assoc_string(*arr, "ī", "i", 1);
        add_assoc_string(*arr, "ķ", "k", 1);
        add_assoc_string(*arr, "ļ", "l", 1);
        add_assoc_string(*arr, "ņ", "n", 1);
        add_assoc_string(*arr, "š", "s", 1);
        add_assoc_string(*arr, "ū", "u", 1);
        add_assoc_string(*arr, "ž", "z", 1);

	//Vietnamese
        add_assoc_string(*arr, "Ấ", "A", 1);
        add_assoc_string(*arr, "Ầ", "A", 1);
        add_assoc_string(*arr, "Ẩ", "A", 1);
        add_assoc_string(*arr, "Ẫ", "A", 1);
        add_assoc_string(*arr, "Ậ", "A", 1);
        add_assoc_string(*arr, "Ắ", "A", 1);
        add_assoc_string(*arr, "Ằ", "A", 1);
        add_assoc_string(*arr, "Ẳ", "A", 1);
        add_assoc_string(*arr, "Ẵ", "A", 1);
        add_assoc_string(*arr, "Ặ", "A", 1);
        add_assoc_string(*arr, "Ố", "O", 1);
        add_assoc_string(*arr, "Ồ", "O", 1);
        add_assoc_string(*arr, "Ổ", "O", 1);
        add_assoc_string(*arr, "Ỗ", "O", 1);
        add_assoc_string(*arr, "Ộ", "O", 1);
        add_assoc_string(*arr, "Ớ", "O", 1);
        add_assoc_string(*arr, "Ờ", "O", 1);
        add_assoc_string(*arr, "Ở", "O", 1);
        add_assoc_string(*arr, "Ỡ", "O", 1);
        add_assoc_string(*arr, "Ợ", "O", 1);
        add_assoc_string(*arr, "Ế", "E", 1);
        add_assoc_string(*arr, "Ề", "E", 1);
        add_assoc_string(*arr, "Ể", "E", 1);
        add_assoc_string(*arr, "Ễ", "E", 1);
        add_assoc_string(*arr, "Ệ", "E", 1);
        add_assoc_string(*arr, "ấ", "a", 1);
        add_assoc_string(*arr, "ầ", "a", 1);
        add_assoc_string(*arr, "ẩ", "a", 1);
        add_assoc_string(*arr, "ẫ", "a", 1);
        add_assoc_string(*arr, "ậ", "a", 1);
        add_assoc_string(*arr, "ắ", "a", 1);
        add_assoc_string(*arr, "ằ", "a", 1);
        add_assoc_string(*arr, "ẳ", "a", 1);
        add_assoc_string(*arr, "ẵ", "a", 1);
        add_assoc_string(*arr, "ặ", "a", 1);
        add_assoc_string(*arr, "ố", "o", 1);
        add_assoc_string(*arr, "ồ", "o", 1);
        add_assoc_string(*arr, "ổ", "o", 1);
        add_assoc_string(*arr, "ỗ", "o", 1);
        add_assoc_string(*arr, "ộ", "o", 1);
        add_assoc_string(*arr, "ớ", "o", 1);
        add_assoc_string(*arr, "ờ", "o", 1);
        add_assoc_string(*arr, "ở", "o", 1);
        add_assoc_string(*arr, "ỡ", "o", 1);
        add_assoc_string(*arr, "ợ", "o", 1);
        add_assoc_string(*arr, "ế", "e", 1);
        add_assoc_string(*arr, "ề", "e", 1);
        add_assoc_string(*arr, "ể", "e", 1);
        add_assoc_string(*arr, "ễ", "e", 1);
        add_assoc_string(*arr, "ệ", "e", 1);
	Z_ADDREF_P(*arr);
}


void yii_validators_validator_built_in_validators(zval **arr) {
	if (Z_TYPE_P(*arr) != IS_ARRAY) {
		array_init(*arr);
	}

	add_assoc_string(*arr, "boolean", "yii\\validators\\BooleanValidator", 1);
	add_assoc_string(*arr, "captcha", "yii\\captcha\\CaptchaValidator", 1);
	add_assoc_string(*arr, "compare", "yii\\validators\\CompareValidator", 1);
	add_assoc_string(*arr, "date", "yii\\validators\\DateValidator", 1);
	add_assoc_string(*arr, "default", "yii\\validators\\DefaultValueValidator", 1);
	add_assoc_string(*arr, "double", "yii\\validators\\NumberValidator", 1);
	add_assoc_string(*arr, "email", "yii\\validators\\EmailValidator", 1);
	add_assoc_string(*arr, "exist", "yii\\validators\\ExistValidator", 1);
	add_assoc_string(*arr, "file", "yii\\validators\\FileValidator", 1);
	add_assoc_string(*arr, "filter", "yii\\validators\\FilterValidator", 1);
	add_assoc_string(*arr, "image", "yii\\validators\\ImageValidator", 1);
	add_assoc_string(*arr, "in", "yii\\validators\\RangeValidator", 1);

	zval *integer_val = NULL;
	MAKE_STD_ZVAL(integer_val);
	array_init(integer_val);
	add_assoc_string(integer_val, "class", "yii\\validators\\NumberValidator", 1);
	add_assoc_bool(integer_val, "integerOnly", 1);
	add_assoc_zval(*arr, "integer", integer_val);

	add_assoc_string(*arr, "match", "yii\\validators\\RegularExpressionValidator", 1);
	add_assoc_string(*arr, "number", "yii\\validators\\NumberValidator", 1);
	add_assoc_string(*arr, "required", "yii\\validators\\RequiredValidator", 1);
	add_assoc_string(*arr, "safe", "yii\\validators\\SafeValidator", 1);
	add_assoc_string(*arr, "string", "yii\\validators\\StringValidator", 1);

	zval *trim_val = NULL;
	MAKE_STD_ZVAL(trim_val);
	array_init(trim_val);
	add_assoc_string(trim_val, "class", "yii\\validators\\FilterValidator", 1);
	add_assoc_string(trim_val, "filter", "trim", 1);
	add_assoc_zval(*arr, "trim", trim_val);

	add_assoc_string(*arr, "unique", "yii\\validators\\UniqueValidator", 1);
	add_assoc_string(*arr, "url", "yii\\validators\\UrlValidator", 1);
}

ZEND_API void zend_trait_use(zend_class_entry *class_entry TSRMLS_DC, int num_trait, ...) /* {{{ */
{
	//ZEND_API void zend_do_implement_trait(zend_class_entry *ce, zend_class_entry *trait TSRMLS_DC)
	zend_class_entry *trait_entry;
	va_list trait_list;
	va_start(trait_list, num_trait);
	
	while (num_trait--) {
		trait_entry = va_arg(trait_list, zend_class_entry *);
		if (!((trait_entry->ce_flags & ZEND_ACC_TRAIT) == ZEND_ACC_TRAIT)) {
			zend_error(E_COMPILE_ERROR, "Class %s is not a trait.", trait_entry->name);
		}
		zend_do_implement_trait(class_entry, trait_entry TSRMLS_CC);
	}
	
	va_end(trait_list);
	zend_do_bind_traits(class_entry);
}
