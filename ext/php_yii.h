
/* This file was generated automatically by Zephir do not modify it! */

#ifndef PHP_YII_H
#define PHP_YII_H 1

#define ZEPHIR_RELEASE 1

#include "kernel/globals.h"

#define PHP_YII_NAME        "cyii2"
#define PHP_YII_VERSION     "0.0.1"
#define PHP_YII_EXTNAME     "yii"
#define PHP_YII_AUTHOR      "syang"
#define PHP_YII_ZEPVERSION  "0.4.1a"
#define PHP_YII_DESCRIPTION "php extension for yii framework 2.0"



ZEND_BEGIN_MODULE_GLOBALS(yii)

	/* Memory */
	zephir_memory_entry *start_memory; /**< The first preallocated frame */
	zephir_memory_entry *end_memory; /**< The last preallocate frame */
	zephir_memory_entry *active_memory; /**< The current memory frame */

	/* Virtual Symbol Tables */
	zephir_symbol_table *active_symbol_table;

	/** Function cache */
	HashTable *fcache;

	/* Max recursion control */
	unsigned int recursive_lock;

	/* Global constants */
	zval *global_true;
	zval *global_false;
	zval *global_null;
	
ZEND_END_MODULE_GLOBALS(yii)

#ifdef ZTS
#include "TSRM.h"
#endif

ZEND_EXTERN_MODULE_GLOBALS(yii)

#ifdef ZTS
	#define ZEPHIR_GLOBAL(v) TSRMG(yii_globals_id, zend_yii_globals *, v)
#else
	#define ZEPHIR_GLOBAL(v) (yii_globals.v)
#endif

#ifdef ZTS
	#define ZEPHIR_VGLOBAL ((zend_yii_globals *) (*((void ***) tsrm_ls))[TSRM_UNSHUFFLE_RSRC_ID(yii_globals_id)])
#else
	#define ZEPHIR_VGLOBAL &(yii_globals)
#endif

#define zephir_globals_def yii_globals
#define zend_zephir_globals_def zend_yii_globals

extern zend_module_entry yii_module_entry;
#define phpext_yii_ptr &yii_module_entry

#endif
