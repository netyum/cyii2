PHP_ARG_ENABLE(yii, whether to enable yii, [ --enable-yii   Enable Yii])

if test "$PHP_YII" = "yes"; then
	AC_DEFINE(HAVE_YII, 1, [Whether you have Yii])
	yii_sources="yii.c kernel/main.c kernel/memory.c kernel/exception.c kernel/hash.c kernel/debug.c kernel/backtrace.c kernel/object.c kernel/array.c kernel/extended/array.c kernel/string.c kernel/fcall.c kernel/require.c kernel/file.c kernel/operators.c kernel/concat.c kernel/variables.c kernel/filter.c kernel/iterator.c kernel/exit.c yii/baseyii.c yii/base/action.c yii/base/actionevent.c yii/base/actionfilter.c yii/base/application.c yii/base/arrayaccesstrait.c yii/base/arrayable.c yii/base/arrayabletrait.c yii/base/behavior.c yii/base/bootstrapinterface.c yii/base/component.c yii/base/controller.c yii/base/errorexception.c yii/base/errorhandler.c yii/base/event.c yii/base/exception.c yii/base/exitexception.c yii/base/inlineaction.c yii/base/invalidcallexception.c yii/base/invalidconfigexception.c yii/base/invalidparamexception.c yii/base/invalidrouteexception.c yii/base/mailevent.c yii/base/model.c yii/base/modelevent.c yii/base/module.c yii/base/notsupportedexception.c yii/base/object.c yii/base/request.c yii/base/response.c yii/base/unknownclassexception.c yii/base/unknownmethodexception.c yii/base/unknownpropertyexception.c yii/base/userexception.c yii/base/viewcontextinterface.c yii/base/viewevent.c yii/base/viewrenderer.c yii/base/widget.c yii/console/application.c yii/di/container.c yii/di/instance.c yii/di/servicelocator.c yii/helpers/arrayhelper.c yii/helpers/inflector.c yii/helpers/stringhelper.c yii/helpers/url.c yii/log/logger.c yii/validators/requiredvalidator.c yii/validators/validationasset.c yii/validators/validator.c yii/web/application.c yii/web/assetbundle.c yii/web/badrequesthttpexception.c yii/web/compositeurlrule.c yii/web/controller.c yii/web/cookie.c yii/web/erroraction.c yii/web/groupurlrule.c yii/web/httpexception.c yii/web/link.c yii/web/linkable.c yii/web/test.c yii/web/urlruleinterface.c extra/extra.c"
	PHP_NEW_EXTENSION(yii, $yii_sources, $ext_shared)

	old_CPPFLAGS=$CPPFLAGS
	CPPFLAGS="$CPPFLAGS $INCLUDES"

	AC_CHECK_DECL(
		[HAVE_BUNDLED_PCRE],
		[
			AC_CHECK_HEADERS(
				[ext/pcre/php_pcre.h],
				[
					PHP_ADD_EXTENSION_DEP([yii], [pcre])
					AC_DEFINE([ZEPHIR_USE_PHP_PCRE], [1], [Whether PHP pcre extension is present at compile time])
				],
				,
				[[#include "main/php.h"]]
			)
		],
		,
		[[#include "php_config.h"]]
	)

	AC_CHECK_DECL(
		[HAVE_JSON],
		[
			AC_CHECK_HEADERS(
				[ext/json/php_json.h],
				[
					PHP_ADD_EXTENSION_DEP([yii], [json])
					AC_DEFINE([ZEPHIR_USE_PHP_JSON], [1], [Whether PHP json extension is present at compile time])
				],
				,
				[[#include "main/php.h"]]
			)
		],
		,
		[[#include "php_config.h"]]
	)

	CPPFLAGS=$old_CPPFLAGS
fi
