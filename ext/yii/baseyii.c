
#ifdef HAVE_CONFIG_H
#include "../ext_config.h"
#endif

#include <php.h>
#include "../php_ext.h"
#include "../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/string.h"
#include "kernel/array.h"
#include "kernel/concat.h"
#include "kernel/hash.h"
#include "kernel/exception.h"
#include "kernel/require.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * BaseYii is the core helper class for the Yii framework.
 *
 * Do not use BaseYii directly. Instead, use its child class [[\Yii]] which you can replace to
 * customize methods of BaseYii.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_BaseYii) {

	ZEPHIR_REGISTER_CLASS(yii, BaseYii, yii, baseyii, yii_baseyii_method_entry, 0);

	/**
	 * @var array class map used by the Yii autoloading mechanism.
	 * The array keys are the class names (without leading backslashes), and the array values
	 * are the corresponding class file paths (or path aliases). This property mainly affects
	 * how [[autoload()]] works.
	 * @see autoload()
	 */
	zend_declare_property_null(yii_baseyii_ce, SL("classMap"), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	/**
	 * @var \yii\console\Application|\yii\web\Application the application instance
	 */
	zend_declare_property_null(yii_baseyii_ce, SL("app"), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	/**
	 * @var array registered path aliases
	 * @see getAlias()
	 * @see setAlias()
	 */
	zend_declare_property_null(yii_baseyii_ce, SL("aliases"), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	/**
	 * @var Container the dependency injection (DI) container used by [[createObject()]].
	 * You may use [[Container::set()]] to set up the needed dependencies of classes and
	 * their initial property values.
	 * @see createObject()
	 * @see Container
	 */
	zend_declare_property_null(yii_baseyii_ce, SL("container"), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	zend_declare_property_null(yii_baseyii_ce, SL("_logger"), ZEND_ACC_PROTECTED|ZEND_ACC_STATIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Returns a string representing the current version of the Yii framework.
 * @return string the version of Yii framework
 */
PHP_METHOD(yii_BaseYii, getVersion) {


	RETURN_STRING("cyii 2.0.0-dev", 1);

}

/**
 * Translates a path alias into an actual path.
 *
 * The translation is done according to the following procedure:
 *
 * 1. If the given alias does not start with '@', it is returned back without change;
 * 2. Otherwise, look for the longest registered alias that matches the beginning part
 *    of the given alias. If it exists, replace the matching part of the given alias with
 *    the corresponding registered path.
 * 3. Throw an exception or return false, depending on the `$throwException` parameter.
 *
 * For example, by default '@yii' is registered as the alias to the Yii framework directory,
 * say '/path/to/yii'. The alias '@yii/web' would then be translated into '/path/to/yii/web'.
 *
 * If you have registered two aliases '@foo' and '@foo/bar'. Then translating '@foo/bar/config'
 * would replace the part '@foo/bar' (instead of '@foo') with the corresponding registered path.
 * This is because the longest alias takes precedence.
 *
 * However, if the alias to be translated is '@foo/barbar/config', then '@foo' will be replaced
 * instead of '@foo/bar', because '/' serves as the boundary character.
 *
 * Note, this method does not check if the returned path exists or not.
 *
 * @param string $alias the alias to be translated.
 * @param boolean $throwException whether to throw an exception if the given alias is invalid.
 * If this is false and an invalid alias is given, false will be returned by this method.
 * @return string|boolean the path corresponding to the alias, false if the root alias is not previously registered.
 * @throws InvalidParamException if the alias is invalid while $throwException is true.
 * @see setAlias()
 */
PHP_METHOD(yii_BaseYii, getAlias) {

	HashTable *_8;
	HashPosition _7;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_4 = NULL, *_5 = NULL;
	zend_bool throwException, _12;
	zval *alias_param = NULL, *throwException_param = NULL, *aliases = NULL, *root_alias, *_0, _1 = zval_used_for_init, _2 = zval_used_for_init, *_3 = NULL, *pos, *root = NULL, *_6, *name = NULL, *path = NULL, *new_pos = NULL, **_9, *_11 = NULL, *_13 = NULL;
	zval *alias = NULL, *_10 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &alias_param, &throwException_param);

	zephir_get_strval(alias, alias_param);
	if (!throwException_param) {
		throwException = 1;
	} else {
		throwException = zephir_get_boolval(throwException_param);
	}


	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("aliases") TSRMLS_CC);
	ZEPHIR_CPY_WRT(aliases, _0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, "@", 0);
	ZEPHIR_SINIT_VAR(_2);
	ZVAL_LONG(&_2, 1);
	ZEPHIR_CALL_FUNCTION(&_3, "strncmp", &_4, alias, &_1, &_2);
	zephir_check_call_status();
	if (zephir_is_true(_3)) {
		RETURN_CTOR(alias);
	}
	ZEPHIR_SINIT_NVAR(_1);
	ZVAL_STRING(&_1, "/", 0);
	ZEPHIR_INIT_VAR(pos);
	zephir_fast_strpos(pos, alias, &_1, 0 );
	if (Z_TYPE_P(pos) == IS_BOOL) {
		ZEPHIR_CPY_WRT(root, alias);
	} else {
		ZEPHIR_SINIT_NVAR(_2);
		ZVAL_LONG(&_2, 0);
		ZEPHIR_CALL_FUNCTION(&root, "substr", &_5, alias, &_2, pos);
		zephir_check_call_status();
	}
	ZEPHIR_OBS_VAR(root_alias);
	if (zephir_array_isset_fetch(&root_alias, aliases, root, 0 TSRMLS_CC)) {
		if (Z_TYPE_P(root_alias) == IS_STRING) {
			if (Z_TYPE_P(pos) == IS_BOOL) {
				zephir_array_fetch(&_6, aliases, root, PH_NOISY | PH_READONLY TSRMLS_CC);
				RETURN_CTOR(_6);
			} else {
				zephir_array_fetch(&_6, aliases, root, PH_NOISY | PH_READONLY TSRMLS_CC);
				ZEPHIR_CALL_FUNCTION(&_3, "substr", &_5, alias, pos);
				zephir_check_call_status();
				ZEPHIR_CONCAT_VV(return_value, _6, _3);
				RETURN_MM();
			}
		} else {
			zephir_array_fetch(&_6, aliases, root, PH_NOISY | PH_READONLY TSRMLS_CC);
			zephir_is_iterable(_6, &_8, &_7, 0, 0);
			for (
			  ; zephir_hash_get_current_data_ex(_8, (void**) &_9, &_7) == SUCCESS
			  ; zephir_hash_move_forward_ex(_8, &_7)
			) {
				ZEPHIR_GET_HMKEY(name, _8, _7);
				ZEPHIR_GET_HVALUE(path, _9);
				ZEPHIR_INIT_LNVAR(_10);
				ZEPHIR_CONCAT_VS(_10, alias, "/");
				ZEPHIR_INIT_LNVAR(_11);
				ZEPHIR_CONCAT_VS(_11, name, "/");
				ZEPHIR_INIT_NVAR(new_pos);
				zephir_fast_strpos(new_pos, _10, _11, 0 );
				_12 = Z_TYPE_P(new_pos) != IS_BOOL;
				if (_12) {
					_12 = ZEPHIR_IS_LONG(new_pos, 0);
				}
				if (_12) {
					ZEPHIR_SINIT_NVAR(_2);
					ZVAL_LONG(&_2, zephir_fast_strlen_ev(name));
					ZEPHIR_CALL_FUNCTION(&_13, "substr", &_5, alias, &_2);
					zephir_check_call_status();
					ZEPHIR_CONCAT_VV(return_value, path, _13);
					RETURN_MM();
				}
			}
		}
	}
	if (throwException) {
		ZEPHIR_INIT_LNVAR(_11);
		object_init_ex(_11, yii_base_invalidparamexception_ce);
		ZEPHIR_INIT_LNVAR(_10);
		ZEPHIR_CONCAT_SV(_10, "Invalid path alias: ", alias);
		ZEPHIR_CALL_METHOD(NULL, _11, "__construct", NULL, _10);
		zephir_check_call_status();
		zephir_throw_exception_debug(_11, "yii/BaseYii.zep", 132 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	} else {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the root alias part of a given alias.
 * A root alias is an alias that has been registered via [[setAlias()]] previously.
 * If a given alias matches multiple root aliases, the longest one will be returned.
 * @param string $alias the alias
 * @return string|boolean the root alias, or false if no root alias is found
 */
PHP_METHOD(yii_BaseYii, getRootAlias) {

	zend_bool _13;
	HashTable *_9;
	HashPosition _8;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL;
	zval *alias_param = NULL, *pos, *root = NULL, _0, _1, *_3, *_4, *_5, *name = NULL, *path = NULL, *new_pos = NULL, *_6, *_7, **_10, *_12 = NULL;
	zval *alias = NULL, *_11 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &alias_param);

	zephir_get_strval(alias, alias_param);


	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "/", 0);
	ZEPHIR_INIT_VAR(pos);
	zephir_fast_strpos(pos, alias, &_0, 0 );
	if (Z_TYPE_P(pos) == IS_BOOL) {
		ZEPHIR_CPY_WRT(root, alias);
	} else {
		ZEPHIR_SINIT_VAR(_1);
		ZVAL_LONG(&_1, 0);
		ZEPHIR_CALL_FUNCTION(&root, "substr", &_2, alias, &_1, pos);
		zephir_check_call_status();
	}
	_3 = zephir_fetch_static_property_ce(yii_baseyii_ce, SL("aliases") TSRMLS_CC);
	if (zephir_array_isset(_3, root)) {
		_4 = zephir_fetch_static_property_ce(yii_baseyii_ce, SL("aliases") TSRMLS_CC);
		zephir_array_fetch(&_5, _4, root, PH_NOISY | PH_READONLY TSRMLS_CC);
		if (Z_TYPE_P(_5) == IS_STRING) {
			RETURN_CCTOR(root);
		} else {
			_6 = zephir_fetch_static_property_ce(yii_baseyii_ce, SL("aliases") TSRMLS_CC);
			zephir_array_fetch(&_7, _6, root, PH_NOISY | PH_READONLY TSRMLS_CC);
			zephir_is_iterable(_7, &_9, &_8, 0, 0);
			for (
			  ; zephir_hash_get_current_data_ex(_9, (void**) &_10, &_8) == SUCCESS
			  ; zephir_hash_move_forward_ex(_9, &_8)
			) {
				ZEPHIR_GET_HMKEY(name, _9, _8);
				ZEPHIR_GET_HVALUE(path, _10);
				ZEPHIR_INIT_LNVAR(_11);
				ZEPHIR_CONCAT_VS(_11, alias, "/");
				ZEPHIR_INIT_LNVAR(_12);
				ZEPHIR_CONCAT_VS(_12, name, "/");
				ZEPHIR_INIT_NVAR(new_pos);
				zephir_fast_strpos(new_pos, _11, _12, 0 );
				_13 = Z_TYPE_P(new_pos) != IS_BOOL;
				if (_13) {
					_13 = ZEPHIR_IS_LONG(new_pos, 0);
				}
				if (_13) {
					RETURN_CCTOR(name);
				}
			}
		}
	}
	RETURN_MM_BOOL(0);

}

/**
 * Registers a path alias.
 *
 * A path alias is a short name representing a long path (a file path, a URL, etc.)
 * For example, we use '@yii' as the alias of the path to the Yii framework directory.
 *
 * A path alias must start with the character '@' so that it can be easily differentiated
 * from non-alias paths.
 *
 * Note that this method does not check if the given path exists or not. All it does is
 * to associate the alias with the path.
 *
 * Any trailing '/' and '\' characters in the given path will be trimmed.
 *
 * @param string $alias the alias name (e.g. "@yii"). It must start with a '@' character.
 * It may contain the forward slash '/' which serves as boundary character when performing
 * alias translation by [[getAlias()]].
 * @param string $path the path corresponding to the alias. Trailing '/' and '\' characters
 * will be trimmed. This can be
 *
 * - a directory or a file path (e.g. `/tmp`, `/tmp/main.txt`)
 * - a URL (e.g. `http://www.yiiframework.com`)
 * - a path alias (e.g. `@yii/base`). In this case, the path alias will be converted into the
 *   actual path first by calling [[getAlias()]].
 *
 * @throws InvalidParamException if $path is an invalid alias.
 * @see getAlias()
 */
PHP_METHOD(yii_BaseYii, setAlias) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_4 = NULL, *_6 = NULL, *_10 = NULL;
	zval *alias_param = NULL, *path, *aliases = NULL, *root_alias = NULL, *_0, _1 = zval_used_for_init, _2 = zval_used_for_init, *_3 = NULL, *pos, *root = NULL, *path_ext = NULL, _7, *elements, *_8, *_9;
	zval *alias = NULL, *_5;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &alias_param, &path);

	zephir_get_strval(alias, alias_param);
	ZEPHIR_SEPARATE_PARAM(alias);
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("aliases") TSRMLS_CC);
	ZEPHIR_CPY_WRT(aliases, _0);
	if (Z_TYPE_P(aliases) != IS_ARRAY) {
		ZEPHIR_INIT_VAR(aliases);
		array_init(aliases);
	}
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, "@", 0);
	ZEPHIR_SINIT_VAR(_2);
	ZVAL_LONG(&_2, 1);
	ZEPHIR_CALL_FUNCTION(&_3, "strncmp", &_4, alias, &_1, &_2);
	zephir_check_call_status();
	if (zephir_is_true(_3)) {
		ZEPHIR_INIT_VAR(_5);
		ZEPHIR_CONCAT_SV(_5, "@", alias);
		ZEPHIR_CPY_WRT(alias, _5);
	}
	ZEPHIR_SINIT_NVAR(_1);
	ZVAL_STRING(&_1, "/", 0);
	ZEPHIR_INIT_VAR(pos);
	zephir_fast_strpos(pos, alias, &_1, 0 );
	if (Z_TYPE_P(pos) == IS_BOOL) {
		ZEPHIR_CPY_WRT(root, alias);
	} else {
		ZEPHIR_SINIT_NVAR(_2);
		ZVAL_LONG(&_2, 0);
		ZEPHIR_CALL_FUNCTION(&root, "substr", &_6, alias, &_2, pos);
		zephir_check_call_status();
	}
	if (Z_TYPE_P(path) == IS_STRING) {
		ZEPHIR_SINIT_NVAR(_2);
		ZVAL_STRING(&_2, "@", 0);
		ZEPHIR_SINIT_VAR(_7);
		ZVAL_LONG(&_7, 1);
		ZEPHIR_CALL_FUNCTION(&_3, "strncmp", &_4, path, &_2, &_7);
		zephir_check_call_status();
		if (zephir_is_true(_3)) {
			ZEPHIR_INIT_VAR(path_ext);
			ZEPHIR_SINIT_NVAR(_2);
			ZVAL_STRING(&_2, "\\/", 0);
			zephir_fast_trim(path_ext, path, &_2, ZEPHIR_TRIM_RIGHT TSRMLS_CC);
		} else {
			ZEPHIR_CALL_SELF(&path_ext, "getalias", NULL, path);
			zephir_check_call_status();
		}
		if (!(zephir_array_isset(aliases, root))) {
			if (Z_TYPE_P(pos) == IS_BOOL) {
				zephir_array_update_zval(&aliases, root, &path_ext, PH_COPY | PH_SEPARATE);
			} else {
				zephir_array_update_zval(&elements, alias, &path_ext, PH_COPY | PH_SEPARATE);
				zephir_array_update_zval(&aliases, root, &elements, PH_COPY | PH_SEPARATE);
			}
		} else {
			ZEPHIR_OBS_VAR(_8);
			zephir_array_fetch(&_8, aliases, root, PH_NOISY TSRMLS_CC);
			if (Z_TYPE_P(_8) == IS_STRING) {
				if (Z_TYPE_P(pos) == IS_BOOL) {
					zephir_array_update_zval(&aliases, root, &path_ext, PH_COPY | PH_SEPARATE);
				} else {
					zephir_array_update_zval(&elements, alias, &path_ext, PH_COPY | PH_SEPARATE);
					zephir_array_fetch(&_9, aliases, root, PH_NOISY | PH_READONLY TSRMLS_CC);
					zephir_array_update_zval(&elements, root, &_9, PH_COPY | PH_SEPARATE);
					zephir_array_update_zval(&aliases, root, &elements, PH_COPY | PH_SEPARATE);
				}
			} else {
				ZEPHIR_OBS_VAR(root_alias);
				zephir_array_fetch(&root_alias, aliases, root, PH_NOISY TSRMLS_CC);
				zephir_array_update_zval(&root_alias, alias, &path_ext, PH_COPY | PH_SEPARATE);
				Z_SET_ISREF_P(root_alias);
				ZEPHIR_CALL_FUNCTION(NULL, "krsort", &_10, root_alias);
				Z_UNSET_ISREF_P(root_alias);
				zephir_check_call_status();
				zephir_array_update_zval(&aliases, root, &root_alias, PH_COPY | PH_SEPARATE);
			}
		}
	} else {
		ZEPHIR_OBS_NVAR(root_alias);
		if (zephir_array_isset_fetch(&root_alias, aliases, root, 0 TSRMLS_CC)) {
			if (Z_TYPE_P(root_alias) == IS_ARRAY) {
				zephir_array_unset(&root_alias, alias, PH_SEPARATE);
				zephir_array_update_zval(&aliases, root, &root_alias, PH_COPY | PH_SEPARATE);
			} else {
				if (Z_TYPE_P(pos) == IS_BOOL) {
					zephir_array_unset(&aliases, root, PH_SEPARATE);
				}
			}
		}
	}
	zephir_update_static_property_ce(yii_baseyii_ce, SL("aliases"), aliases TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Class autoload loader.
 * This method is invoked automatically when PHP sees an unknown class.
 * The method will attempt to include the class file according to the following procedure:
 *
 * 1. Search in [[classMap]];
 * 2. If the class is namespaced (e.g. `yii\base\Component`), it will attempt
 *    to include the file associated with the corresponding path alias
 *    (e.g. `@yii/base/Component.php`);
 *
 * This autoloader allows loading classes that follow the [PSR-4 standard](http://www.php-fig.org/psr/psr-4/)
 * and have its top-level namespace or sub-namespaces defined as path aliases.
 *
 * Example: When aliases `@yii` and `@yii/bootstrap` are defined, classes in the `yii\bootstrap` namespace
 * will be loaded using the `@yii/bootstrap` alias which points to the directory where bootstrap extension
 * files are installed and all classes from other `yii` namespaces will be loaded from the yii framework directory.
 *
 * @param string $className the fully qualified class name without a leading backslash "\"
 * @throws UnknownClassException if the class does not exist in the class file
 */
PHP_METHOD(yii_BaseYii, autoload) {

	zend_bool _9, _13, _14;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_4 = NULL, *_7 = NULL, *_10 = NULL, *_11 = NULL, *_15 = NULL;
	zval *className_param = NULL, *classFile = NULL, *_0, _1 = zval_used_for_init, _2 = zval_used_for_init, *_3 = NULL, *_5 = NULL, *pos, _6, *_8 = NULL, *_12, *_16, *_17;
	zval *className = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &className_param);

	zephir_get_strval(className, className_param);


	ZEPHIR_OBS_VAR(classFile);
	_0 = zephir_fetch_static_property_ce(yii_baseyii_ce, SL("classMap") TSRMLS_CC);
	if (zephir_array_isset_fetch(&classFile, _0, className, 0 TSRMLS_CC)) {
		ZEPHIR_SINIT_VAR(_1);
		ZVAL_LONG(&_1, 0);
		ZEPHIR_SINIT_VAR(_2);
		ZVAL_LONG(&_2, 1);
		ZEPHIR_CALL_FUNCTION(&_3, "substr", &_4, classFile, &_1, &_2);
		zephir_check_call_status();
		if (ZEPHIR_IS_STRING(_3, "@")) {
			ZEPHIR_CALL_SELF(&_5, "getalias", NULL, classFile);
			zephir_check_call_status();
			ZEPHIR_CPY_WRT(classFile, _5);
		}
	}
	ZEPHIR_SINIT_NVAR(_1);
	ZVAL_STRING(&_1, "\\", 0);
	ZEPHIR_INIT_VAR(pos);
	zephir_fast_strpos(pos, className, &_1, 0 );
	if (Z_TYPE_P(pos) != IS_BOOL) {
		ZEPHIR_SINIT_NVAR(_2);
		ZVAL_STRING(&_2, "\\", 0);
		ZEPHIR_SINIT_VAR(_6);
		ZVAL_STRING(&_6, "/", 0);
		ZEPHIR_CALL_FUNCTION(&_3, "str_replace", &_7, &_2, &_6, className);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(_8);
		ZEPHIR_CONCAT_SVS(_8, "@", _3, ".php");
		ZEPHIR_CALL_SELF(&classFile, "getalias", NULL, _8, ZEPHIR_GLOBAL(global_false));
		zephir_check_call_status();
		_9 = ZEPHIR_IS_FALSE(classFile);
		if (!(_9)) {
			ZEPHIR_CALL_FUNCTION(&_5, "is_file", &_10, classFile);
			zephir_check_call_status();
			_9 = !zephir_is_true(_5);
		}
		if (_9) {
			RETURN_MM_NULL();
		}
	} else {
		RETURN_MM_NULL();
	}
	if (zephir_require_zval(classFile TSRMLS_CC) == FAILURE) {
		RETURN_MM_NULL();
	}
	ZEPHIR_INIT_LNVAR(_8);
	ZEPHIR_CONCAT_VS(_8, classFile, "\n");
	ZEPHIR_SINIT_NVAR(_1);
	ZVAL_STRING(&_1, "/tmp/load.log", 0);
	ZEPHIR_SINIT_NVAR(_2);
	ZVAL_LONG(&_2, 8);
	ZEPHIR_CALL_FUNCTION(NULL, "file_put_contents", &_11, &_1, _8, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_12);
	ZEPHIR_GET_CONSTANT(_12, "YII_DEBUG");
	_9 = zephir_is_true(_12);
	if (_9) {
		_9 = !zephir_class_exists(className, zephir_is_true(ZEPHIR_GLOBAL(global_false))  TSRMLS_CC);
	}
	_13 = _9;
	if (_13) {
		_13 = !zephir_interface_exists(className, zephir_is_true(ZEPHIR_GLOBAL(global_false))  TSRMLS_CC);
	}
	_14 = _13;
	if (_14) {
		ZEPHIR_CALL_FUNCTION(&_3, "trait_exists", &_15, className, ZEPHIR_GLOBAL(global_false));
		zephir_check_call_status();
		_14 = !zephir_is_true(_3);
	}
	if (_14) {
		ZEPHIR_INIT_VAR(_16);
		object_init_ex(_16, yii_base_unknownclassexception_ce);
		ZEPHIR_INIT_VAR(_17);
		ZEPHIR_CONCAT_SVSVS(_17, "Unable to find \'", className, "\' in file: ", classFile, " Namespace missing?");
		ZEPHIR_CALL_METHOD(NULL, _16, "__construct", NULL, _17);
		zephir_check_call_status();
		zephir_throw_exception_debug(_16, "yii/BaseYii.zep", 319 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Creates a new object using the given configuration.
 *
 * You may view this method as an enhanced version of the `new` operator.
 * The method supports creating an object based on a class name, a configuration array or
 * an anonymous function.
 *
 * Below are some usage examples:
 *
 * ```php
 * // create an object using a class name
 * $object = Yii::createObject('yii\db\Connection');
 *
 * // create an object using a configuration array
 * $object = Yii::createObject([
 *     'class' => 'yii\db\Connection',
 *     'dsn' => 'mysql:host=127.0.0.1;dbname=demo',
 *     'username' => 'root',
 *     'password' => '',
 *     'charset' => 'utf8',
 * ]);
 *
 * // create an object with two constructor parameters
 * $object = \Yii::createObject('MyClass', [$param1, $param2]);
 * ```
 *
 * Using [[\yii\di\Container|dependency injection container]], this method can also identify
 * dependent objects, instantiate them and inject them into the newly created object.
 *
 * @param string|array|callable $type the object type. This can be specified in one of the following forms:
 *
 * - a string: representing the class name of the object to be created
 * - a configuration array: the array must contain a `class` element which is treated as the object class,
 *   and the rest of the name-value pairs will be used to initialize the corresponding object properties
 * - a PHP callable: either an anonymous function or an array representing a class method (`[$class or $object, $method]`).
 *   The callable should return a new instance of the object being created.
 *
 * @param array $params the constructor parameters
 * @return object the created object
 * @throws InvalidConfigException if the configuration is invalid.
 * @see \yii\di\Container
 */
PHP_METHOD(yii_BaseYii, createObject) {

	zval *text, *_6;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL, *_4 = NULL;
	zend_bool _1;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *params = NULL;
	zval *type, *params_param = NULL, *_0, *class, *object = NULL, *_2 = NULL, *_5 = NULL, *_7, *_8;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &type, &params_param);

	ZEPHIR_SEPARATE_PARAM(type);
	if (!params_param) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	} else {
		zephir_get_arrval(params, params_param);
	}
	ZEPHIR_INIT_VAR(text);
	ZVAL_STRING(text, "class", 1);


	if (Z_TYPE_P(type) == IS_STRING) {
		_0 = zephir_fetch_static_property_ce(yii_baseyii_ce, SL("container") TSRMLS_CC);
		ZEPHIR_RETURN_CALL_METHOD(_0, "get", NULL, type, params);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		_1 = Z_TYPE_P(type) == IS_ARRAY;
		if (_1) {
			_1 = zephir_array_isset_string(type, SS("class"));
		}
		if (_1) {
			ZEPHIR_OBS_VAR(class);
			zephir_array_fetch_string(&class, type, SL("class"), PH_NOISY TSRMLS_CC);
			zephir_array_unset_string(&type, SS("class"), PH_SEPARATE);
			_0 = zephir_fetch_static_property_ce(yii_baseyii_ce, SL("container") TSRMLS_CC);
			ZEPHIR_CALL_METHOD(&object, _0, "get", NULL, class, params, type);
			zephir_check_call_status();
			RETURN_CCTOR(object);
		} else {
			ZEPHIR_CALL_FUNCTION(&_2, "is_callable", &_3, type, ZEPHIR_GLOBAL(global_true));
			zephir_check_call_status();
			if (zephir_is_true(_2)) {
				ZEPHIR_RETURN_CALL_FUNCTION("call_user_func", &_4, type, params);
				zephir_check_call_status();
				RETURN_MM();
			} else {
				if (Z_TYPE_P(type) == IS_ARRAY) {
					ZEPHIR_INIT_VAR(_5);
					object_init_ex(_5, yii_base_invalidconfigexception_ce);
					ZEPHIR_INIT_VAR(_6);
					ZEPHIR_CONCAT_SVS(_6, "Object configuration must be an array containing a \"", text, "\" element.");
					ZEPHIR_CALL_METHOD(NULL, _5, "__construct", NULL, _6);
					zephir_check_call_status();
					zephir_throw_exception_debug(_5, "yii/BaseYii.zep", 385 TSRMLS_CC);
					ZEPHIR_MM_RESTORE();
					return;
				} else {
					ZEPHIR_INIT_LNVAR(_5);
					object_init_ex(_5, yii_base_invalidconfigexception_ce);
					ZEPHIR_INIT_VAR(_7);
					zephir_gettype(_7, type TSRMLS_CC);
					ZEPHIR_INIT_VAR(_8);
					ZEPHIR_CONCAT_SV(_8, "Unsupported configuration type: ", _7);
					ZEPHIR_CALL_METHOD(NULL, _5, "__construct", NULL, _8);
					zephir_check_call_status();
					zephir_throw_exception_debug(_5, "yii/BaseYii.zep", 387 TSRMLS_CC);
					ZEPHIR_MM_RESTORE();
					return;
				}
			}
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * @return Logger message logger
 */
PHP_METHOD(yii_BaseYii, getLogger) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *_0, *_1, *_2 = NULL, *_3;

	ZEPHIR_MM_GROW();

	_0 = zephir_fetch_static_property_ce(yii_baseyii_ce, SL("_logger") TSRMLS_CC);
	if (Z_TYPE_P(_0) != IS_NULL) {
		_1 = zephir_fetch_static_property_ce(yii_baseyii_ce, SL("_logger") TSRMLS_CC);
		RETURN_CTOR(_1);
	} else {
		ZEPHIR_INIT_VAR(_3);
		ZVAL_STRING(_3, "yii\\log\\Logger", 0);
		ZEPHIR_CALL_SELF(&_2, "createobject", NULL, _3);
		zephir_check_temp_parameter(_3);
		zephir_check_call_status();
		zephir_update_static_property_ce(yii_baseyii_ce, SL("_logger"), _2 TSRMLS_CC);
		_1 = zephir_fetch_static_property_ce(yii_baseyii_ce, SL("_logger") TSRMLS_CC);
		RETURN_CTOR(_1);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Sets the logger object.
 * @param Logger $logger the logger object.
 */
PHP_METHOD(yii_BaseYii, setLogger) {

	zval *logger;

	zephir_fetch_params(0, 1, 0, &logger);



	zephir_update_static_property_ce(yii_baseyii_ce, SL("_logger"), logger TSRMLS_CC);

}

/**
 * Logs a trace message.
 * Trace messages are logged mainly for development purpose to see
 * the execution work flow of some code.
 * @param string $message the message to be logged.
 * @param string $category the category of the message.
 */
PHP_METHOD(yii_BaseYii, trace) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *message_param = NULL, *category_param = NULL, *_0, *_1 = NULL;
	zval *message = NULL, *category = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &message_param, &category_param);

	zephir_get_strval(message, message_param);
	if (!category_param) {
		ZEPHIR_INIT_VAR(category);
		ZVAL_STRING(category, "application", 1);
	} else {
		zephir_get_strval(category, category_param);
	}


	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_GET_CONSTANT(_0, "YII_DEBUG");
	if (zephir_is_true(_0)) {
		ZEPHIR_CALL_SELF(&_1, "getlogger", NULL);
		zephir_check_call_status();
		ZEPHIR_INIT_BNVAR(_0);
		ZVAL_LONG(_0, 0x08);
		ZEPHIR_CALL_METHOD(NULL, _1, "log", NULL, message, _0, category);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Logs an error message.
 * An error message is typically logged when an unrecoverable error occurs
 * during the execution of an application.
 * @param string $message the message to be logged.
 * @param string $category the category of the message.
 */
PHP_METHOD(yii_BaseYii, error) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *message_param = NULL, *category_param = NULL, *_0 = NULL, *_1;
	zval *message = NULL, *category = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &message_param, &category_param);

	zephir_get_strval(message, message_param);
	if (!category_param) {
		ZEPHIR_INIT_VAR(category);
		ZVAL_STRING(category, "application", 1);
	} else {
		zephir_get_strval(category, category_param);
	}


	ZEPHIR_CALL_SELF(&_0, "getlogger", NULL);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_1);
	ZVAL_LONG(_1, 0x01);
	ZEPHIR_CALL_METHOD(NULL, _0, "log", NULL, message, _1, category);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Logs a warning message.
 * A warning message is typically logged when an error occurs while the execution
 * can still continue.
 * @param string $message the message to be logged.
 * @param string $category the category of the message.
 */
PHP_METHOD(yii_BaseYii, warning) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *message_param = NULL, *category_param = NULL, *_0 = NULL, *_1;
	zval *message = NULL, *category = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &message_param, &category_param);

	zephir_get_strval(message, message_param);
	if (!category_param) {
		ZEPHIR_INIT_VAR(category);
		ZVAL_STRING(category, "application", 1);
	} else {
		zephir_get_strval(category, category_param);
	}


	ZEPHIR_CALL_SELF(&_0, "getlogger", NULL);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_1);
	ZVAL_LONG(_1, 0x02);
	ZEPHIR_CALL_METHOD(NULL, _0, "log", NULL, message, _1, category);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Logs an informative message.
 * An informative message is typically logged by an application to keep record of
 * something important (e.g. an administrator logs in).
 * @param string $message the message to be logged.
 * @param string $category the category of the message.
 */
PHP_METHOD(yii_BaseYii, info) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *message_param = NULL, *category_param = NULL, *_0 = NULL, *_1;
	zval *message = NULL, *category = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &message_param, &category_param);

	zephir_get_strval(message, message_param);
	if (!category_param) {
		ZEPHIR_INIT_VAR(category);
		ZVAL_STRING(category, "application", 1);
	} else {
		zephir_get_strval(category, category_param);
	}


	ZEPHIR_CALL_SELF(&_0, "getlogger", NULL);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_1);
	ZVAL_LONG(_1, 0x04);
	ZEPHIR_CALL_METHOD(NULL, _0, "log", NULL, message, _1, category);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Marks the beginning of a code block for profiling.
 * This has to be matched with a call to [[endProfile]] with the same category name.
 * The begin- and end- calls must also be properly nested. For example,
 *
 * ~~~
 * \Yii::beginProfile('block1');
 * // some code to be profiled
 *     \Yii::beginProfile('block2');
 *     // some other code to be profiled
 *     \Yii::endProfile('block2');
 * \Yii::endProfile('block1');
 * ~~~
 * @param string $token token for the code block
 * @param string $category the category of this log message
 * @see endProfile()
 */
PHP_METHOD(yii_BaseYii, beginProfile) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *token_param = NULL, *category_param = NULL, *_0 = NULL, *_1;
	zval *token = NULL, *category = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &token_param, &category_param);

	zephir_get_strval(token, token_param);
	if (!category_param) {
		ZEPHIR_INIT_VAR(category);
		ZVAL_STRING(category, "application", 1);
	} else {
		zephir_get_strval(category, category_param);
	}


	ZEPHIR_CALL_SELF(&_0, "getlogger", NULL);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_1);
	ZVAL_LONG(_1, 0x50);
	ZEPHIR_CALL_METHOD(NULL, _0, "log", NULL, token, _1, category);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Marks the end of a code block for profiling.
 * This has to be matched with a previous call to [[beginProfile]] with the same category name.
 * @param string $token token for the code block
 * @param string $category the category of this log message
 * @see beginProfile()
 */
PHP_METHOD(yii_BaseYii, endProfile) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *token_param = NULL, *category_param = NULL, *_0 = NULL, *_1;
	zval *token = NULL, *category = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &token_param, &category_param);

	zephir_get_strval(token, token_param);
	if (!category_param) {
		ZEPHIR_INIT_VAR(category);
		ZVAL_STRING(category, "application", 1);
	} else {
		zephir_get_strval(category, category_param);
	}


	ZEPHIR_CALL_SELF(&_0, "getlogger", NULL);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_1);
	ZVAL_LONG(_1, 0x60);
	ZEPHIR_CALL_METHOD(NULL, _0, "log", NULL, token, _1, category);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns an HTML hyperlink that can be displayed on your Web page showing "Powered by Yii Framework" information.
 * @return string an HTML hyperlink that can be displayed on your Web page showing "Powered by Yii Framework" information
 */
PHP_METHOD(yii_BaseYii, powered) {


	RETURN_STRING("Powered by <a href=\"http://www.yiiframework.com/\" rel=\"external\">Yii Framework</a>", 1);

}

/**
 * Translates a message to the specified language.
 *
 * This is a shortcut method of [[\yii\i18n\I18N::translate()]].
 *
 * The translation will be conducted according to the message category and the target language will be used.
 *
 * You can add parameters to a translation message that will be substituted with the corresponding value after
 * translation. The format for this is to use curly brackets around the parameter name as you can see in the following example:
 *
 * ```php
 * $username = 'Alexander';
 * echo \Yii::t('app', 'Hello, {username}!', ['username' => $username]);
 * ```
 *
 * Further formatting of message parameters is supported using the [PHP intl extensions](http://www.php.net/manual/en/intro.intl.php)
 * message formatter. See [[\yii\i18n\I18N::translate()]] for more details.
 *
 * @param string $category the message category.
 * @param string $message the message to be translated.
 * @param array $params the parameters that will be used to replace the corresponding placeholders in the message.
 * @param string $language the language code (e.g. `en-US`, `en`). If this is null, the current
 * [[\yii\base\Application::language|application language]] will be used.
 * @return string the translated message.
 */
PHP_METHOD(yii_BaseYii, t) {

	zephir_nts_static zephir_fcall_cache_entry *_6 = NULL;
	HashTable *_3;
	HashPosition _2;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *category_param = NULL, *message_param = NULL, *params = NULL, *language = NULL, *app = NULL, *i18n = NULL, *_0, *_1, *p, *name = NULL, *value = NULL, **_4, *_5 = NULL;
	zval *category = NULL, *message = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 2, &category_param, &message_param, &params, &language);

	zephir_get_strval(category, category_param);
	zephir_get_strval(message, message_param);
	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}
	if (!language) {
		language = ZEPHIR_GLOBAL(global_null);
	}
	ZEPHIR_INIT_VAR(p);
	array_init(p);


	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _0);
	if (Z_TYPE_P(app) != IS_NULL) {
		ZEPHIR_CALL_METHOD(&i18n, app, "geti18n",  NULL);
		zephir_check_call_status();
		if (Z_TYPE_P(language) == IS_NULL) {
			ZEPHIR_OBS_VAR(_1);
			zephir_read_property(&_1, app, SL("language"), PH_NOISY_CC);
			ZEPHIR_RETURN_CALL_METHOD(i18n, "translate", NULL, category, message, params, _1);
			zephir_check_call_status();
			RETURN_MM();
		} else {
			ZEPHIR_RETURN_CALL_METHOD(i18n, "translate", NULL, category, message, params, language);
			zephir_check_call_status();
			RETURN_MM();
		}
	} else {
		zephir_is_iterable(params, &_3, &_2, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_3, (void**) &_4, &_2) == SUCCESS
		  ; zephir_hash_move_forward_ex(_3, &_2)
		) {
			ZEPHIR_GET_HMKEY(name, _3, _2);
			ZEPHIR_GET_HVALUE(value, _4);
			ZEPHIR_INIT_LNVAR(_5);
			ZEPHIR_CONCAT_SVS(_5, "{", name, "}");
			zephir_array_update_zval(&p, _5, &value, PH_COPY | PH_SEPARATE);
		}
		if (zephir_fast_count_int(p TSRMLS_CC) == 0) {
			RETURN_CTOR(message);
		} else {
			ZEPHIR_RETURN_CALL_FUNCTION("strtr", &_6, message, p);
			zephir_check_call_status();
			RETURN_MM();
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Configures an object with the initial property values.
 * @param object $object the object to be configured
 * @param array $properties the property initial values given in terms of name-value pairs.
 * @return object the object itself
 */
PHP_METHOD(yii_BaseYii, configure) {

	HashTable *_1;
	HashPosition _0;
	zval *object, *properties, *name = NULL, *value = NULL, **_2, *tmp = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &object, &properties);

	ZEPHIR_SEPARATE_PARAM(object);


	zephir_is_iterable(properties, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(name, _1, _0);
		ZEPHIR_GET_HVALUE(value, _2);
		ZEPHIR_CPY_WRT(tmp, value);
		zephir_update_property_zval_zval(object, name, tmp TSRMLS_CC);
	}
	RETURN_CCTOR(object);

}

/**
 * Returns the public member variables of an object.
 * This method is provided such that we can get the public member variables of an object.
 * It is different from "get_object_vars()" because the latter will return private
 * and protected variables if it is called within the object itself.
 * @param object $object the object to be handled
 * @return array the public member variables of the object
 */
PHP_METHOD(yii_BaseYii, getObjectVars) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;
	zval *object;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &object);



	ZEPHIR_RETURN_CALL_FUNCTION("get_object_vars", &_0, object);
	zephir_check_call_status();
	RETURN_MM();

}

