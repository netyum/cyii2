
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"
#include "kernel/array.h"
#include "kernel/object.h"
#include "kernel/operators.h"
#include "kernel/hash.h"
#include "kernel/string.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Logger records logged messages in memory and sends them to different targets if [[dispatcher]] is set.
 *
 * Logger can be accessed via `Yii::getLogger()`. You can call the method [[log()]] to record a single log message.
 * For convenience, a set of shortcut methods are provided for logging messages of various severity levels
 * via the [[Yii]] class:
 *
 * - [[Yii::trace()]]
 * - [[Yii::error()]]
 * - [[Yii::warning()]]
 * - [[Yii::info()]]
 * - [[Yii::beginProfile()]]
 * - [[Yii::endProfile()]]
 *
 * When the application ends or [[flushInterval]] is reached, Logger will call [[flush()]]
 * to send logged messages to different log targets, such as file, email, Web, with the help of [[dispatcher]].
 *
 * @property array $dbProfiling The first element indicates the number of SQL statements executed, and the
 * second element the total time spent in SQL execution. This property is read-only.
 * @property float $elapsedTime The total elapsed time in seconds for current request. This property is
 * read-only.
 * @property array $profiling The profiling results. Each element is an array consisting of these elements:
 * `info`, `category`, `timestamp`, `trace`, `level`, `duration`. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_log_Logger) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\log, Logger, yii, log_logger, yii_base_component_ce, yii_log_logger_method_entry, 0);

	/**
	 * @var array logged messages. This property is managed by [[log()]] and [[flush()]].
	 * Each log message is of the following structure:
	 *
	 * ~~~
	 * [
	 *   [0] => message (mixed, can be a string or some complex data, such as an exception object)
	 *   [1] => level (integer)
	 *   [2] => category (string)
	 *   [3] => timestamp (float, obtained by microtime(true))
	 *   [4] => traces (array, debug backtrace, contains the application code call stacks)
	 * ]
	 * ~~~
	 */
	zend_declare_property_null(yii_log_logger_ce, SL("messages"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var integer how many messages should be logged before they are flushed from memory and sent to targets.
	 * Defaults to 1000, meaning the [[flush]] method will be invoked once every 1000 messages logged.
	 * Set this property to be 0 if you don't want to flush messages until the application terminates.
	 * This property mainly affects how much memory will be taken by the logged messages.
	 * A smaller value means less memory, but will increase the execution time due to the overhead of [[flush()]].
	 */
	zend_declare_property_long(yii_log_logger_ce, SL("flushInterval"), 1000, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var integer how much call stack information (file name and line number) should be logged for each message.
	 * If it is greater than 0, at most that number of call stacks will be logged. Note that only application
	 * call stacks are counted.
	 */
	zend_declare_property_long(yii_log_logger_ce, SL("traceLevel"), 0, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var Dispatcher the message dispatcher
	 */
	zend_declare_property_null(yii_log_logger_ce, SL("dispatcher"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * Error message level. An error message is one that indicates the abnormal termination of the
	 * application and may require developer's handling.
	 */
	zend_declare_class_constant_long(yii_log_logger_ce, SL("LEVEL_ERROR"), 0x01 TSRMLS_CC);

	/**
	 * Warning message level. A warning message is one that indicates some abnormal happens but
	 * the application is able to continue to run. Developers should pay attention to this message.
	 */
	zend_declare_class_constant_long(yii_log_logger_ce, SL("LEVEL_WARNING"), 0x02 TSRMLS_CC);

	/**
	 * Informational message level. An informational message is one that includes certain information
	 * for developers to review.
	 */
	zend_declare_class_constant_long(yii_log_logger_ce, SL("LEVEL_INFO"), 0x04 TSRMLS_CC);

	/**
	 * Tracing message level. An tracing message is one that reveals the code execution flow.
	 */
	zend_declare_class_constant_long(yii_log_logger_ce, SL("LEVEL_TRACE"), 0x08 TSRMLS_CC);

	/**
	 * Profiling message level. This indicates the message is for profiling purpose.
	 */
	zend_declare_class_constant_long(yii_log_logger_ce, SL("LEVEL_PROFILE"), 0x40 TSRMLS_CC);

	/**
	 * Profiling message level. This indicates the message is for profiling purpose. It marks the
	 * beginning of a profiling block.
	 */
	zend_declare_class_constant_long(yii_log_logger_ce, SL("LEVEL_PROFILE_BEGIN"), 0x50 TSRMLS_CC);

	/**
	 * Profiling message level. This indicates the message is for profiling purpose. It marks the
	 * end of a profiling block.
	 */
	zend_declare_class_constant_long(yii_log_logger_ce, SL("LEVEL_PROFILE_END"), 0x60 TSRMLS_CC);

	return SUCCESS;

}

/**
 * Initializes the logger by registering [[flush()]] as a shutdown function.
 */
PHP_METHOD(yii_log_Logger, init) {

	zval *_2;
	zval *_1;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_3 = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_PARENT(NULL, yii_log_logger_ce, this_ptr, "init", &_0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_1);
	array_init_size(_1, 3);
	zephir_array_fast_append(_1, this_ptr);
	ZEPHIR_INIT_VAR(_2);
	ZVAL_STRING(_2, "flush", 1);
	zephir_array_fast_append(_1, _2);
	ZEPHIR_CALL_FUNCTION(NULL, "register_shutdown_function", &_3, _1, ZEPHIR_GLOBAL(global_true));
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Logs a message with the given type and category.
 * If [[traceLevel]] is greater than 0, additional call stack information about
 * the application code will be logged as well.
 * @param string $message the message to be logged.
 * @param integer $level the level of the message. This must be one of the following:
 * `Logger::LEVEL_ERROR`, `Logger::LEVEL_WARNING`, `Logger::LEVEL_INFO`, `Logger::LEVEL_TRACE`,
 * `Logger::LEVEL_PROFILE_BEGIN`, `Logger::LEVEL_PROFILE_END`.
 * @param string $category the category of the message.
 */
PHP_METHOD(yii_log_Logger, log) {

	zval *_9;
	zend_bool _7;
	HashTable *_5;
	HashPosition _4;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_2 = NULL, *_3 = NULL;
	int count, ZEPHIR_LAST_CALL_STATUS;
	zval *message, *level, *category = NULL, *time = NULL, *traces, *ts = NULL, *_1, *trace = NULL, **_6, *_8, *_10, *_11;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &message, &level, &category);

	if (!category) {
		ZEPHIR_INIT_VAR(category);
		ZVAL_STRING(category, "application", 1);
	}
	ZEPHIR_INIT_VAR(traces);
	array_init(traces);


	ZEPHIR_CALL_FUNCTION(&time, "microtime", &_0, ZEPHIR_GLOBAL(global_true));
	zephir_check_call_status();
	_1 = zephir_fetch_nproperty_this(this_ptr, SL("traceLevel"), PH_NOISY_CC);
	if (ZEPHIR_GT_LONG(_1, 0)) {
		count = 0;
		ZEPHIR_CALL_FUNCTION(&ts, "debug_backtrace", &_2);
		zephir_check_call_status();
		Z_SET_ISREF_P(ts);
		ZEPHIR_CALL_FUNCTION(NULL, "array_pop", &_3, ts);
		Z_UNSET_ISREF_P(ts);
		zephir_check_call_status();
		zephir_is_iterable(ts, &_5, &_4, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_5, (void**) &_6, &_4) == SUCCESS
		  ; zephir_hash_move_forward_ex(_5, &_4)
		) {
			ZEPHIR_GET_HVALUE(trace, _6);
			_7 = zephir_array_isset_string(trace, SS("file"));
			if (_7) {
				_7 = zephir_array_isset_string(trace, SS("line"));
			}
			if (_7) {
				zephir_array_unset_string(&trace, SS("object"), PH_SEPARATE);
				zephir_array_unset_string(&trace, SS("args"), PH_SEPARATE);
				zephir_array_append(&traces, trace, PH_SEPARATE);
				_8 = zephir_fetch_nproperty_this(this_ptr, SL("traceLevel"), PH_NOISY_CC);
				if (ZEPHIR_LT_LONG(_8, count)) {
					count += 1;
					break;
				} else {
					count += 1;
				}
			}
		}
	}
	ZEPHIR_INIT_VAR(_9);
	array_init_size(_9, 7);
	zephir_array_fast_append(_9, message);
	zephir_array_fast_append(_9, level);
	zephir_array_fast_append(_9, category);
	zephir_array_fast_append(_9, time);
	zephir_array_fast_append(_9, traces);
	zephir_update_property_array_append(this_ptr, SL("messages"), _9 TSRMLS_CC);
	_8 = zephir_fetch_nproperty_this(this_ptr, SL("flushInterval"), PH_NOISY_CC);
	_7 = ZEPHIR_GT_LONG(_8, 0);
	if (_7) {
		_10 = zephir_fetch_nproperty_this(this_ptr, SL("messages"), PH_NOISY_CC);
		_11 = zephir_fetch_nproperty_this(this_ptr, SL("flushInterval"), PH_NOISY_CC);
		_7 = ZEPHIR_LT_LONG(_11, zephir_fast_count_int(_10 TSRMLS_CC));
	}
	if (_7) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "flush", NULL);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Flushes log messages from memory to targets.
 * @param boolean $final whether this is a final call during a request.
 */
PHP_METHOD(yii_log_Logger, flush) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *final = NULL, *_0, *_1, *_2, *_3;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &final);

	if (!final) {
		final = ZEPHIR_GLOBAL(global_false);
	}


	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("dispatcher"), PH_NOISY_CC);
	if (zephir_is_instance_of(_0, SL("yii\\log\\Dispatcher") TSRMLS_CC)) {
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("dispatcher"), PH_NOISY_CC);
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("messages"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(NULL, _1, "dispatch", NULL, _2, final);
		zephir_check_call_status();
	}
	ZEPHIR_INIT_VAR(_3);
	array_init(_3);
	zephir_update_property_this(this_ptr, SL("messages"), _3 TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the total elapsed time since the start of the current request.
 * This method calculates the difference between now and the timestamp
 * defined by constant `YII_BEGIN_TIME` which is evaluated at the beginning
 * of [[\yii\BaseYii]] class file.
 * @return float the total elapsed time in seconds for current request.
 */
PHP_METHOD(yii_log_Logger, getElapsedTime) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	zval *_0 = NULL, *_2;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_FUNCTION(&_0, "microtime", &_1, ZEPHIR_GLOBAL(global_true));
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_2);
	ZEPHIR_GET_CONSTANT(_2, "YII_BEGIN_TIME");
	zephir_sub_function(return_value, _0, _2 TSRMLS_CC);
	RETURN_MM();

}

/**
 * Returns the profiling results.
 *
 * By default, all profiling results will be returned. You may provide
 * `$categories` and `$excludeCategories` as parameters to retrieve the
 * results that you are interested in.
 *
 * @param array $categories list of categories that you are interested in.
 * You can use an asterisk at the end of a category to do a prefix match.
 * For example, 'yii\db\*' will match categories starting with 'yii\db\',
 * such as 'yii\db\Connection'.
 * @param array $excludeCategories list of categories that you want to exclude
 * @return array the profiling results. Each element is an array consisting of these elements:
 * `info`, `category`, `timestamp`, `trace`, `level`, `duration`.
 */
PHP_METHOD(yii_log_Logger, getProfiling) {

	zephir_nts_static zephir_fcall_cache_entry *_20 = NULL;
	HashTable *_3, *_6, *_15, *_18;
	HashPosition _2, _5, _14, _17;
	zend_bool _1, matched, _11, _13;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *categories = NULL, *excludeCategories = NULL, *timings = NULL, *_0, *i = NULL, *timing = NULL, *prefix = NULL, **_4, *category = NULL, **_7, _8 = zval_used_for_init, *_9, *_10 = NULL, *_12, **_16, **_19;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 2, &categories, &excludeCategories);

	if (!categories) {
		ZEPHIR_INIT_VAR(categories);
		array_init(categories);
	}
	if (!excludeCategories) {
		ZEPHIR_INIT_VAR(excludeCategories);
		array_init(excludeCategories);
	}


	_0 = zephir_fetch_nproperty_this(this_ptr, SL("messages"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&timings, this_ptr, "calculatetimings", NULL, _0);
	zephir_check_call_status();
	_1 = ZEPHIR_IS_EMPTY(categories);
	if (_1) {
		_1 = ZEPHIR_IS_EMPTY(excludeCategories);
	}
	if (_1) {
		RETURN_CCTOR(timings);
	}
	zephir_is_iterable(timings, &_3, &_2, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_3, (void**) &_4, &_2) == SUCCESS
	  ; zephir_hash_move_forward_ex(_3, &_2)
	) {
		ZEPHIR_GET_HMKEY(i, _3, _2);
		ZEPHIR_GET_HVALUE(timing, _4);
		matched = ZEPHIR_IS_EMPTY(categories);
		zephir_is_iterable(categories, &_6, &_5, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_6, (void**) &_7, &_5) == SUCCESS
		  ; zephir_hash_move_forward_ex(_6, &_5)
		) {
			ZEPHIR_GET_HVALUE(category, _7);
			ZEPHIR_INIT_NVAR(prefix);
			ZEPHIR_SINIT_NVAR(_8);
			ZVAL_STRING(&_8, "*", 0);
			zephir_fast_trim(prefix, category, &_8, ZEPHIR_TRIM_RIGHT TSRMLS_CC);
			zephir_array_fetch_string(&_9, timing, SL("category"), PH_NOISY | PH_READONLY TSRMLS_CC);
			ZEPHIR_CALL_METHOD(&_10, _9, "index", NULL, prefix);
			zephir_check_call_status();
			_11 = ZEPHIR_IS_LONG(_10, 0);
			if (_11) {
				zephir_array_fetch_string(&_12, timing, SL("category"), PH_NOISY | PH_READONLY TSRMLS_CC);
				_13 = ZEPHIR_IS_IDENTICAL(_12, category);
				if (!(_13)) {
					_13 = !ZEPHIR_IS_IDENTICAL(prefix, category);
				}
				_11 = _13;
			}
			if (_11) {
				matched = 1;
				break;
			}
		}
		if (matched) {
			zephir_is_iterable(excludeCategories, &_15, &_14, 0, 0);
			for (
			  ; zephir_hash_get_current_data_ex(_15, (void**) &_16, &_14) == SUCCESS
			  ; zephir_hash_move_forward_ex(_15, &_14)
			) {
				ZEPHIR_GET_HVALUE(category, _16);
				ZEPHIR_INIT_NVAR(prefix);
				ZEPHIR_SINIT_NVAR(_8);
				ZVAL_STRING(&_8, "*", 0);
				zephir_fast_trim(prefix, category, &_8, ZEPHIR_TRIM_RIGHT TSRMLS_CC);
				zephir_is_iterable(timings, &_18, &_17, 0, 0);
				for (
				  ; zephir_hash_get_current_data_ex(_18, (void**) &_19, &_17) == SUCCESS
				  ; zephir_hash_move_forward_ex(_18, &_17)
				) {
					ZEPHIR_GET_HVALUE(timing, _19);
					zephir_array_fetch_string(&_9, timing, SL("category"), PH_NOISY | PH_READONLY TSRMLS_CC);
					ZEPHIR_CALL_METHOD(&_10, _9, "index", NULL, prefix);
					zephir_check_call_status();
					_11 = ZEPHIR_IS_LONG(_10, 0);
					if (_11) {
						zephir_array_fetch_string(&_12, timing, SL("category"), PH_NOISY | PH_READONLY TSRMLS_CC);
						_13 = ZEPHIR_IS_IDENTICAL(_12, category);
						if (!(_13)) {
							_13 = !ZEPHIR_IS_IDENTICAL(prefix, category);
						}
						_11 = _13;
					}
					if (_11) {
						matched = 0;
						break;
					}
				}
			}
		}
		if (!(matched)) {
			zephir_array_unset(&timings, i, PH_SEPARATE);
		}
	}
	ZEPHIR_RETURN_CALL_FUNCTION("array_values", &_20, timings);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns the statistical results of DB queries.
 * The results returned include the number of SQL statements executed and
 * the total time spent.
 * @return array the first element indicates the number of SQL statements executed,
 * and the second element the total time spent in SQL execution.
 */
PHP_METHOD(yii_log_Logger, getDbProfiling) {

	HashTable *_3;
	HashPosition _2;
	zval *_0;
	int time, ZEPHIR_LAST_CALL_STATUS;
	zval *count, *timings = NULL, *_1, *timing = NULL, **_4, *_5 = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(_0);
	array_init_size(_0, 3);
	ZEPHIR_INIT_VAR(_1);
	ZVAL_STRING(_1, "yii\\db\\Command::query", 1);
	zephir_array_fast_append(_0, _1);
	ZEPHIR_INIT_BNVAR(_1);
	ZVAL_STRING(_1, "yii\\db\\Command::execute", 1);
	zephir_array_fast_append(_0, _1);
	ZEPHIR_CALL_METHOD(&timings, this_ptr, "getprofiling", NULL, _0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(count);
	ZVAL_LONG(count, zephir_fast_count_int(timings TSRMLS_CC));
	time = 0;
	zephir_is_iterable(timings, &_3, &_2, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_3, (void**) &_4, &_2) == SUCCESS
	  ; zephir_hash_move_forward_ex(_3, &_2)
	) {
		ZEPHIR_GET_HVALUE(timing, _4);
		ZEPHIR_OBS_NVAR(_5);
		zephir_array_fetch_string(&_5, timing, SL("duration"), PH_NOISY TSRMLS_CC);
		time += zephir_get_intval(_5);
	}
	array_init_size(return_value, 3);
	zephir_array_fast_append(return_value, count);
	ZEPHIR_INIT_BNVAR(_1);
	ZVAL_LONG(_1, time);
	zephir_array_fast_append(return_value, _1);
	RETURN_MM();

}

/**
 * Calculates the elapsed time for the given log messages.
 * @param array $messages the log messages obtained from profiling
 * @return array timings. Each element is an array consisting of these elements:
 * `info`, `category`, `timestamp`, `trace`, `level`, `duration`.
 */
PHP_METHOD(yii_log_Logger, calculateTimings) {

	zend_bool _4;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL, *_13 = NULL, *_14 = NULL;
	HashTable *_1;
	HashPosition _0;
	zval *messages, *timings, *stack, *i = NULL, *log = NULL, **_2, *token = NULL, *level = NULL, *category = NULL, *timestamp = NULL, *traces = NULL, *timings_last = NULL, *last = NULL, *last_5 = NULL, *_5, *_6 = NULL, *_7, *_8, *_9, *_10, *_11, *_12 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &messages);

	ZEPHIR_INIT_VAR(timings);
	array_init(timings);
	ZEPHIR_INIT_VAR(stack);
	array_init(stack);


	zephir_is_iterable(messages, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(i, _1, _0);
		ZEPHIR_GET_HVALUE(log, _2);
		ZEPHIR_OBS_NVAR(token);
		zephir_array_fetch_long(&token, log, 0, PH_NOISY TSRMLS_CC);
		ZEPHIR_OBS_NVAR(level);
		zephir_array_fetch_long(&level, log, 1, PH_NOISY TSRMLS_CC);
		ZEPHIR_OBS_NVAR(category);
		zephir_array_fetch_long(&category, log, 2, PH_NOISY TSRMLS_CC);
		ZEPHIR_OBS_NVAR(timestamp);
		zephir_array_fetch_long(&timestamp, log, 3, PH_NOISY TSRMLS_CC);
		ZEPHIR_OBS_NVAR(traces);
		zephir_array_fetch_long(&traces, log, 4, PH_NOISY TSRMLS_CC);
		zephir_array_update_long(&log, 5, &i, PH_COPY | PH_SEPARATE, "yii/log/Logger.zep", 280);
		if (ZEPHIR_IS_LONG(level, 0x50)) {
			zephir_array_append(&stack, log, PH_SEPARATE);
		}
		if (ZEPHIR_IS_LONG(level, 0x60)) {
			Z_SET_ISREF_P(stack);
			ZEPHIR_CALL_FUNCTION(&last, "array_pop", &_3, stack);
			Z_UNSET_ISREF_P(stack);
			zephir_check_call_status();
			_4 = Z_TYPE_P(last) != IS_NULL;
			if (_4) {
				zephir_array_fetch_long(&_5, last, 0, PH_NOISY | PH_READONLY TSRMLS_CC);
				_4 = ZEPHIR_IS_IDENTICAL(_5, token);
			}
			if (_4) {
				ZEPHIR_OBS_NVAR(last_5);
				zephir_array_fetch_long(&last_5, last, 5, PH_NOISY TSRMLS_CC);
				if (!(zephir_array_isset(timings, last_5))) {
					ZEPHIR_INIT_NVAR(_6);
					array_init(_6);
					zephir_array_update_zval(&timings, last_5, &_6, PH_COPY | PH_SEPARATE);
				}
				ZEPHIR_OBS_NVAR(timings_last);
				zephir_array_fetch(&timings_last, timings, last_5, PH_NOISY TSRMLS_CC);
				zephir_array_fetch_long(&_7, last, 0, PH_NOISY | PH_READONLY TSRMLS_CC);
				zephir_array_update_string(&timings_last, SL("info"), &_7, PH_COPY | PH_SEPARATE);
				zephir_array_fetch_long(&_8, last, 2, PH_NOISY | PH_READONLY TSRMLS_CC);
				zephir_array_update_string(&timings_last, SL("category"), &_8, PH_COPY | PH_SEPARATE);
				zephir_array_fetch_long(&_9, last, 3, PH_NOISY | PH_READONLY TSRMLS_CC);
				zephir_array_update_string(&timings_last, SL("timestamp"), &_9, PH_COPY | PH_SEPARATE);
				zephir_array_fetch_long(&_10, last, 4, PH_NOISY | PH_READONLY TSRMLS_CC);
				zephir_array_update_string(&timings_last, SL("trace"), &_10, PH_COPY | PH_SEPARATE);
				ZEPHIR_INIT_NVAR(_6);
				ZVAL_LONG(_6, zephir_fast_count_int(stack TSRMLS_CC));
				zephir_array_update_string(&timings_last, SL("level"), &_6, PH_COPY | PH_SEPARATE);
				zephir_array_fetch_long(&_11, last, 3, PH_NOISY | PH_READONLY TSRMLS_CC);
				ZEPHIR_INIT_LNVAR(_12);
				zephir_sub_function(_12, timestamp, _11 TSRMLS_CC);
				zephir_array_update_string(&timings_last, SL("duration"), &_12, PH_COPY | PH_SEPARATE);
				zephir_array_update_zval(&timings, last_5, &timings_last, PH_COPY | PH_SEPARATE);
			}
		}
	}
	Z_SET_ISREF_P(timings);
	ZEPHIR_CALL_FUNCTION(NULL, "ksort", &_13, timings);
	Z_UNSET_ISREF_P(timings);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_FUNCTION("array_values", &_14, timings);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns the text display of the specified level.
 * @param integer $level the message level, e.g. [[LEVEL_ERROR]], [[LEVEL_WARNING]].
 * @return string the text display of the level
 */
PHP_METHOD(yii_log_Logger, getLevelName) {

	zval *level, *levels, *_0, *_1, *_2, *_3, *_4, *_5, *_6;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &level);

	ZEPHIR_INIT_VAR(levels);
	array_init(levels);


	ZEPHIR_INIT_VAR(_0);
	ZVAL_STRING(_0, "error", 1);
	zephir_array_update_long(&levels, 0x01, &_0, PH_COPY | PH_SEPARATE, "yii/log/Logger.zep", 320);
	ZEPHIR_INIT_VAR(_1);
	ZVAL_STRING(_1, "warning", 1);
	zephir_array_update_long(&levels, 0x02, &_1, PH_COPY | PH_SEPARATE, "yii/log/Logger.zep", 321);
	ZEPHIR_INIT_VAR(_2);
	ZVAL_STRING(_2, "info", 1);
	zephir_array_update_long(&levels, 0x04, &_2, PH_COPY | PH_SEPARATE, "yii/log/Logger.zep", 322);
	ZEPHIR_INIT_VAR(_3);
	ZVAL_STRING(_3, "trace", 1);
	zephir_array_update_long(&levels, 0x08, &_3, PH_COPY | PH_SEPARATE, "yii/log/Logger.zep", 323);
	ZEPHIR_INIT_VAR(_4);
	ZVAL_STRING(_4, "profile begin", 1);
	zephir_array_update_long(&levels, 0x50, &_4, PH_COPY | PH_SEPARATE, "yii/log/Logger.zep", 324);
	ZEPHIR_INIT_VAR(_5);
	ZVAL_STRING(_5, "profile end", 1);
	zephir_array_update_long(&levels, 0x60, &_5, PH_COPY | PH_SEPARATE, "yii/log/Logger.zep", 325);
	ZEPHIR_INIT_VAR(_6);
	if (zephir_array_isset(levels, level)) {
		zephir_array_fetch(&_6, levels, level, PH_NOISY TSRMLS_CC);
	} else {
		ZVAL_STRING(_6, "unknown", 1);
	}
	RETURN_CCTOR(_6);

}

