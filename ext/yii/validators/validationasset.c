
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/fcall.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * This asset bundle provides the javascript files for client validation.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_validators_ValidationAsset) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\validators, ValidationAsset, yii, validators_validationasset, yii_web_assetbundle_ce, yii_validators_validationasset_method_entry, 0);

	zend_declare_property_string(yii_validators_validationasset_ce, SL("sourcePath"), "@yii/assets", ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

PHP_METHOD(yii_validators_ValidationAsset, init) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_4 = NULL;
	zval *_0, *_1 = NULL, *_2, *_3;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("js"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		ZEPHIR_INIT_VAR(_1);
		array_init(_1);
		zephir_update_property_this(this_ptr, SL("js"), _1 TSRMLS_CC);
	}
	ZEPHIR_INIT_ZVAL_NREF(_2);
	ZVAL_STRING(_2, "yii.validation.js", 1);
	zephir_update_property_array_append(this_ptr, SL("js"), _2 TSRMLS_CC);
	ZEPHIR_OBS_VAR(_3);
	zephir_read_property_this(&_3, this_ptr, SL("depends"), PH_NOISY_CC);
	if (Z_TYPE_P(_3) == IS_NULL) {
		ZEPHIR_INIT_NVAR(_1);
		array_init(_1);
		zephir_update_property_this(this_ptr, SL("depends"), _1 TSRMLS_CC);
	}
	ZEPHIR_INIT_ZVAL_NREF(_2);
	ZVAL_STRING(_2, "yii\\web\\YiiAsset", 1);
	zephir_update_property_array_append(this_ptr, SL("depends"), _2 TSRMLS_CC);
	ZEPHIR_CALL_PARENT(NULL, yii_validators_validationasset_ce, this_ptr, "init", &_4);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

