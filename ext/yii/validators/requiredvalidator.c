
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/string.h"
#include "kernel/operators.h"
#include "kernel/array.h"
#include "kernel/concat.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * RequiredValidator validates that the specified attribute does not have null or empty value.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_validators_RequiredValidator) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\validators, RequiredValidator, yii, validators_requiredvalidator, yii_validators_validator_ce, yii_validators_requiredvalidator_method_entry, 0);

	/**
	 * @var boolean whether to skip this validator if the value being validated is empty.
	 */
	zend_declare_property_bool(yii_validators_requiredvalidator_ce, SL("skipOnEmpty"), 0, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var mixed the desired value that the attribute must have.
	 * If this is null, the validator will validate that the specified attribute is not empty.
	 * If this is set as a value that is not null, the validator will validate that
	 * the attribute has a value that is the same as this property value.
	 * Defaults to null.
	 * @see strict
	 */
	zend_declare_property_null(yii_validators_requiredvalidator_ce, SL("requiredValue"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var boolean whether the comparison between the attribute value and [[requiredValue]] is strict.
	 * When this is true, both the values and types must match.
	 * Defaults to false, meaning only the values need to match.
	 * Note that when [[requiredValue]] is null, if this property is true, the validator will check
	 * if the attribute value is null; If this property is false, the validator will call [[isEmpty]]
	 * to check if the attribute value is empty.
	 */
	zend_declare_property_bool(yii_validators_requiredvalidator_ce, SL("strict"), 0, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the user-defined error message. It may contain the following placeholders which
	 * will be replaced accordingly by the validator:
	 *
	 * - `{attribute}`: the label of the attribute being validated
	 * - `{value}`: the value of the attribute being validated
	 * - `{requiredValue}`: the value of [[requiredValue]]
	 */
	zend_declare_property_null(yii_validators_requiredvalidator_ce, SL("message"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_validators_RequiredValidator, init) {

	zval *_1, *_2, *_3 = NULL, *_5 = NULL, *_6 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_4 = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_PARENT(NULL, yii_validators_requiredvalidator_ce, this_ptr, "init", &_0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property_this(&_1, this_ptr, SL("message"), PH_NOISY_CC);
	if (Z_TYPE_P(_1) == IS_NULL) {
		ZEPHIR_OBS_VAR(_2);
		zephir_read_property_this(&_2, this_ptr, SL("requiredValue"), PH_NOISY_CC);
		if (Z_TYPE_P(_2) == IS_NULL) {
			ZEPHIR_INIT_VAR(_5);
			ZVAL_STRING(_5, "yii", 0);
			ZEPHIR_INIT_VAR(_6);
			ZVAL_STRING(_6, "{attribute} cannot be blank.", 0);
			ZEPHIR_CALL_CE_STATIC(&_3, yii_baseyii_ce, "t", &_4, _5, _6);
			zephir_check_temp_parameter(_5);
			zephir_check_temp_parameter(_6);
			zephir_check_call_status();
			zephir_update_property_this(this_ptr, SL("message"), _3 TSRMLS_CC);
		} else {
			ZEPHIR_INIT_NVAR(_5);
			ZVAL_STRING(_5, "yii", 0);
			ZEPHIR_INIT_NVAR(_6);
			ZVAL_STRING(_6, "{attribute} must be \"{requiredValue}\".", 0);
			ZEPHIR_CALL_CE_STATIC(&_3, yii_baseyii_ce, "t", &_4, _5, _6);
			zephir_check_temp_parameter(_5);
			zephir_check_temp_parameter(_6);
			zephir_check_call_status();
			zephir_update_property_this(this_ptr, SL("message"), _3 TSRMLS_CC);
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_validators_RequiredValidator, validateValue) {

	zval *_10;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_bool _2, _5, _9;
	zval *value = NULL, *_0, *_1, *_3 = NULL, *_4, *_6 = NULL, *_7 = NULL, *_8, *elements;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &value);

	ZEPHIR_SEPARATE_PARAM(value);
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("requiredValue"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("strict"), PH_NOISY_CC);
		_2 = zephir_is_true(_1);
		if (_2) {
			_2 = Z_TYPE_P(value) != IS_NULL;
		}
		if (_2) {
			RETURN_MM_NULL();
		} else {
			if (Z_TYPE_P(value) == IS_STRING) {
				ZEPHIR_INIT_VAR(_3);
				zephir_fast_trim(_3, value, NULL , ZEPHIR_TRIM_BOTH TSRMLS_CC);
				ZEPHIR_CPY_WRT(value, _3);
			}
			_4 = zephir_fetch_nproperty_this(this_ptr, SL("strict"), PH_NOISY_CC);
			_5 = !zephir_is_true(_4);
			if (_5) {
				ZEPHIR_CALL_METHOD(&_6, this_ptr, "isempty", NULL, value);
				zephir_check_call_status();
				_5 = !zephir_is_true(_6);
			}
			if (_5) {
				RETURN_MM_NULL();
			}
		}
	} else {
		ZEPHIR_OBS_VAR(_7);
		zephir_read_property_this(&_7, this_ptr, SL("strict"), PH_NOISY_CC);
		_2 = !zephir_is_true(_7);
		if (_2) {
			_1 = zephir_fetch_nproperty_this(this_ptr, SL("requiredValue"), PH_NOISY_CC);
			_2 = ZEPHIR_IS_EQUAL(value, _1);
		}
		_5 = _2;
		if (!(_5)) {
			ZEPHIR_OBS_VAR(_8);
			zephir_read_property_this(&_8, this_ptr, SL("strict"), PH_NOISY_CC);
			_9 = zephir_is_true(_8);
			if (_9) {
				_4 = zephir_fetch_nproperty_this(this_ptr, SL("requiredValue"), PH_NOISY_CC);
				_9 = ZEPHIR_IS_IDENTICAL(value, _4);
			}
			_5 = _9;
		}
		if (_5) {
			RETURN_MM_NULL();
		}
	}
	ZEPHIR_OBS_NVAR(_7);
	zephir_read_property_this(&_7, this_ptr, SL("requiredValue"), PH_NOISY_CC);
	if (Z_TYPE_P(_7) == IS_NULL) {
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("message"), PH_NOISY_CC);
		zephir_array_append(&elements, _1, PH_SEPARATE);
		ZEPHIR_INIT_NVAR(_3);
		array_init(_3);
		zephir_array_append(&elements, _3, PH_SEPARATE);
	} else {
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("message"), PH_NOISY_CC);
		zephir_array_append(&elements, _1, PH_SEPARATE);
		ZEPHIR_INIT_VAR(_10);
		array_init_size(_10, 2);
		ZEPHIR_OBS_NVAR(_7);
		zephir_read_property_this(&_7, this_ptr, SL("requiredValue"), PH_NOISY_CC);
		zephir_array_update_string(&_10, SL("requiredValue"), &_7, PH_COPY | PH_SEPARATE);
		zephir_array_append(&elements, _10, PH_SEPARATE);
	}
	RETURN_CCTOR(elements);

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_validators_RequiredValidator, clientValidateAttribute) {

	zephir_nts_static zephir_fcall_cache_entry *_9 = NULL;
	zval *_3 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *object, *attribute, *view, *options, *app = NULL, *i18n = NULL, *_0, *_1, *_2 = NULL, *_4 = NULL, *_5, *_6 = NULL, *_7, *_8 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 3, 0, &object, &attribute, &view);

	ZEPHIR_INIT_VAR(options);
	array_init(options);


	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _0);
	ZEPHIR_CALL_METHOD(&i18n, app, "geti18n",  NULL);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property_this(&_1, this_ptr, SL("requiredValue"), PH_NOISY_CC);
	if (Z_TYPE_P(_1) != IS_NULL) {
		_0 = zephir_fetch_nproperty_this(this_ptr, SL("message"), PH_NOISY_CC);
		ZEPHIR_INIT_VAR(_3);
		array_init_size(_3, 2);
		ZEPHIR_OBS_VAR(_4);
		zephir_read_property_this(&_4, this_ptr, SL("requiredValue"), PH_NOISY_CC);
		zephir_array_update_string(&_3, SL("requiredValue"), &_4, PH_COPY | PH_SEPARATE);
		ZEPHIR_OBS_NVAR(_4);
		zephir_read_property(&_4, app, SL("language"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(&_2, i18n, "format", NULL, _0, _3, _4);
		zephir_check_call_status();
		zephir_array_update_string(&options, SL("message"), &_2, PH_COPY | PH_SEPARATE);
		_5 = zephir_fetch_nproperty_this(this_ptr, SL("requiredValue"), PH_NOISY_CC);
		zephir_array_update_string(&options, SL("requiredValue"), &_5, PH_COPY | PH_SEPARATE);
	} else {
		_5 = zephir_fetch_nproperty_this(this_ptr, SL("message"), PH_NOISY_CC);
		zephir_array_update_string(&options, SL("message"), &_5, PH_COPY | PH_SEPARATE);
	}
	_5 = zephir_fetch_nproperty_this(this_ptr, SL("strict"), PH_NOISY_CC);
	if (zephir_is_true(_5)) {
		ZEPHIR_INIT_VAR(_6);
		ZVAL_LONG(_6, 1);
		zephir_array_update_string(&options, SL("strict"), &_6, PH_COPY | PH_SEPARATE);
	}
	zephir_array_fetch_string(&_7, options, SL("message"), PH_NOISY | PH_READONLY TSRMLS_CC);
	ZEPHIR_INIT_NVAR(_3);
	array_init_size(_3, 2);
	ZEPHIR_CALL_METHOD(&_8, object, "getattributelabel", NULL, attribute);
	zephir_check_call_status();
	zephir_array_update_string(&_3, SL("attribute"), &_8, PH_COPY | PH_SEPARATE);
	ZEPHIR_OBS_NVAR(_4);
	zephir_read_property(&_4, app, SL("language"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_2, i18n, "format", NULL, _7, _3, _4);
	zephir_check_call_status();
	zephir_array_update_string(&options, SL("message"), &_2, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_CE_STATIC(NULL, yii_validators_validationasset_ce, "register", &_9, view);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(_6);
	zephir_json_encode(_6, &(_6), options, 0  TSRMLS_CC);
	ZEPHIR_CONCAT_SVS(return_value, "yii.validation.required(value, messages, ", _6, ");");
	RETURN_MM();

}

