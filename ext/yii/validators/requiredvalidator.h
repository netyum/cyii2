
extern zend_class_entry *yii_validators_requiredvalidator_ce;

ZEPHIR_INIT_CLASS(yii_validators_RequiredValidator);

PHP_METHOD(yii_validators_RequiredValidator, init);
PHP_METHOD(yii_validators_RequiredValidator, validateValue);
PHP_METHOD(yii_validators_RequiredValidator, clientValidateAttribute);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_validators_requiredvalidator_validatevalue, 0, 0, 1)
	ZEND_ARG_INFO(0, value)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_validators_requiredvalidator_clientvalidateattribute, 0, 0, 3)
	ZEND_ARG_INFO(0, object)
	ZEND_ARG_INFO(0, attribute)
	ZEND_ARG_INFO(0, view)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_validators_requiredvalidator_method_entry) {
	PHP_ME(yii_validators_RequiredValidator, init, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_validators_RequiredValidator, validateValue, arginfo_yii_validators_requiredvalidator_validatevalue, ZEND_ACC_PROTECTED)
	PHP_ME(yii_validators_RequiredValidator, clientValidateAttribute, arginfo_yii_validators_requiredvalidator_clientvalidateattribute, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
