
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/array.h"
#include "Zend/zend_closures.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "extra/extra.h"
#include "kernel/hash.h"
#include "kernel/exception.h"
#include "kernel/concat.h"
#include "kernel/operators.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Validator is the base class for all validators.
 *
 * Child classes should override the [[validateValue()]] and/or [[validateAttribute()]] methods to provide the actual
 * logic of performing data validation. Child classes may also override [[clientValidateAttribute()]]
 * to provide client-side validation support.
 *
 * Validator declares a set of [[builtInValidators|built-in validators] which can
 * be referenced using short names. They are listed as follows:
 *
 * - `boolean`: [[BooleanValidator]]
 * - `captcha`: [[\yii\captcha\CaptchaValidator]]
 * - `compare`: [[CompareValidator]]
 * - `date`: [[DateValidator]]
 * - `default`: [[DefaultValueValidator]]
 * - `double`: [[NumberValidator]]
 * - `email`: [[EmailValidator]]
 * - `exist`: [[ExistValidator]]
 * - `file`: [[FileValidator]]
 * - `filter`: [[FilterValidator]]
 * - `image`: [[ImageValidator]]
 * - `in`: [[RangeValidator]]
 * - `integer`: [[NumberValidator]]
 * - `match`: [[RegularExpressionValidator]]
 * - `required`: [[RequiredValidator]]
 * - `safe`: [[SafeValidator]]
 * - `string`: [[StringValidator]]
 * - `trim`: [[FilterValidator]]
 * - `unique`: [[UniqueValidator]]
 * - `url`: [[UrlValidator]]
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_validators_Validator) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\validators, Validator, yii, validators_validator, yii_base_component_ce, yii_validators_validator_method_entry, 0);

	/**
	 * @var array list of built-in validators (name => class or configuration)
	 */
	zend_declare_property_null(yii_validators_validator_ce, SL("builtInValidators"), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	/**
	 * @var array|string attributes to be validated by this validator. For multiple attributes,
	 * please specify them as an array; for single attribute, you may use either a string or an array.
	 */
	zend_declare_property_null(yii_validators_validator_ce, SL("attributes"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the user-defined error message. It may contain the following placeholders which
	 * will be replaced accordingly by the validator:
	 *
	 * - `{attribute}`: the label of the attribute being validated
	 * - `{value}`: the value of the attribute being validated
	 */
	zend_declare_property_null(yii_validators_validator_ce, SL("message"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array|string scenarios that the validator can be applied to. For multiple scenarios,
	 * please specify them as an array; for single scenario, you may use either a string or an array.
	 */
	zend_declare_property_null(yii_validators_validator_ce, SL("on"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array|string scenarios that the validator should not be applied to. For multiple scenarios,
	 * please specify them as an array; for single scenario, you may use either a string or an array.
	 */
	zend_declare_property_null(yii_validators_validator_ce, SL("except"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var boolean whether this validation rule should be skipped if the attribute being validated
	 * already has some validation error according to some previous rules. Defaults to true.
	 */
	zend_declare_property_bool(yii_validators_validator_ce, SL("skipOnError"), 1, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var boolean whether this validation rule should be skipped if the attribute value
	 * is null or an empty string.
	 */
	zend_declare_property_bool(yii_validators_validator_ce, SL("skipOnEmpty"), 1, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var boolean whether to enable client-side validation for this validator.
	 * The actual client-side validation is done via the JavaScript code returned
	 * by [[clientValidateAttribute()]]. If that method returns null, even if this property
	 * is true, no client-side validation will be done by this validator.
	 */
	zend_declare_property_bool(yii_validators_validator_ce, SL("enableClientValidation"), 1, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var callable a PHP callable that replaces the default implementation of [[isEmpty()]].
	 * If not set, [[isEmpty()]] will be used to check if a value is empty. The signature
	 * of the callable should be `function ($value)` which returns a boolean indicating
	 * whether the value is empty.
	 */
	zend_declare_property_null(yii_validators_validator_ce, SL("isEmpty"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var callable a PHP callable whose return value determines whether this validator should be applied.
	 * The signature of the callable should be `function ($model, $attribute)`, where `$model` and `$attribute`
	 * refer to the model and the attribute currently being validated. The callable should return a boolean value.
	 *
	 * This property is mainly provided to support conditional validation on the server side.
	 * If this property is not set, this validator will be always applied on the server side.
	 *
	 * The following example will enable the validator only when the country currently selected is USA:
	 *
	 * ```php
	 * function ($model) {
	 *     return $model->country == Country::USA;
	 * }
	 * ```
	 *
	 * @see whenClient
	 */
	zend_declare_property_null(yii_validators_validator_ce, SL("when"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string a JavaScript function name whose return value determines whether this validator should be applied
	 * on the client side. The signature of the function should be `function (attribute, value)`, where
	 * `attribute` is the name of the attribute being validated and `value` the current value of the attribute.
	 *
	 * This property is mainly provided to support conditional validation on the client side.
	 * If this property is not set, this validator will be always applied on the client side.
	 *
	 * The following example will enable the validator only when the country currently selected is USA:
	 *
	 * ```php
	 * function (attribute, value) {
	 *     return $('#country').value == 'USA';
	 * }
	 * ```
	 *
	 * @see when
	 */
	zend_declare_property_null(yii_validators_validator_ce, SL("whenClient"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Creates a validator object.
 * @param mixed $type the validator type. This can be a built-in validator name,
 * a method name of the model class, an anonymous function, or a validator class name.
 * @param \yii\base\Model $object the data object to be validated.
 * @param array|string $attributes list of attributes to be validated. This can be either an array of
 * the attribute names or a string of comma-separated attribute names.
 * @param array $params initial values to be applied to the validator properties
 * @return Validator the validator
 */
PHP_METHOD(yii_validators_Validator, createValidator) {

	zephir_nts_static zephir_fcall_cache_entry *_8 = NULL;
	HashTable *_6;
	HashPosition _5;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_bool _0, _1;
	zval *type = NULL, *object, *attributes, *params = NULL, *_2 = NULL, *_3, *builtInValidators = NULL, *_4, *validator, *name = NULL, *value = NULL, **_7;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 3, 1, &type, &object, &attributes, &params);

	ZEPHIR_SEPARATE_PARAM(type);
	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	} else {
		ZEPHIR_SEPARATE_PARAM(params);
	}


	zephir_array_update_string(&params, SL("attributes"), &attributes, PH_COPY | PH_SEPARATE);
	_0 = (Z_TYPE_P(type) == IS_OBJECT);
	if (_0) {
		_0 = (zephir_instance_of_ev(type, zend_ce_closure TSRMLS_CC));
	}
	_1 = _0;
	if (!(_1)) {
		ZEPHIR_CALL_METHOD(&_2, object, "hasmethod", NULL, type);
		zephir_check_call_status();
		_1 = zephir_is_true(_2);
	}
	if (_1) {
		ZEPHIR_INIT_VAR(_3);
		ZVAL_STRING(_3, "yii\\validators\\InlineValidator", 1);
		zephir_array_update_string(&params, SL("class"), &_3, PH_COPY | PH_SEPARATE);
		zephir_array_update_string(&params, SL("method"), &type, PH_COPY | PH_SEPARATE);
	} else {
		zephir_read_static_property_ce(&_4, yii_validators_validator_ce, SL("builtInValidators") TSRMLS_CC);
		ZEPHIR_CPY_WRT(builtInValidators, _4);
		if (Z_TYPE_P(builtInValidators) == IS_NULL) {
			ZEPHIR_INIT_VAR(builtInValidators);
			yii_validators_validator_built_in_validators(&builtInValidators);
			zephir_update_static_property_ce(yii_validators_validator_ce, SL("builtInValidators"), builtInValidators TSRMLS_CC);
		}
		ZEPHIR_OBS_VAR(validator);
		if (zephir_array_isset_fetch(&validator, builtInValidators, type, 0 TSRMLS_CC)) {
			ZEPHIR_CPY_WRT(type, validator);
		}
		if (Z_TYPE_P(type) == IS_ARRAY) {
			zephir_is_iterable(type, &_6, &_5, 0, 0);
			for (
			  ; zephir_hash_get_current_data_ex(_6, (void**) &_7, &_5) == SUCCESS
			  ; zephir_hash_move_forward_ex(_6, &_5)
			) {
				ZEPHIR_GET_HMKEY(name, _6, _5);
				ZEPHIR_GET_HVALUE(value, _7);
				zephir_array_update_zval(&params, name, &value, PH_COPY | PH_SEPARATE);
			}
		} else {
			zephir_array_update_string(&params, SL("class"), &type, PH_COPY | PH_SEPARATE);
		}
	}
	ZEPHIR_RETURN_CALL_CE_STATIC(yii_baseyii_ce, "createobject", &_8, params);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_validators_Validator, init) {

	zval *attributes = NULL, *on = NULL, *except = NULL, *_1;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_PARENT(NULL, yii_validators_validator_ce, this_ptr, "init", &_0);
	zephir_check_call_status();
	_1 = zephir_fetch_nproperty_this(this_ptr, SL("attributes"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(attributes, _1);
	_1 = zephir_fetch_nproperty_this(this_ptr, SL("on"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(on, _1);
	_1 = zephir_fetch_nproperty_this(this_ptr, SL("except"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(except, _1);
	if (Z_TYPE_P(attributes) == IS_NULL) {
		ZEPHIR_INIT_VAR(attributes);
		array_init(attributes);
	}
	if (Z_TYPE_P(on) == IS_NULL) {
		ZEPHIR_INIT_VAR(on);
		array_init(on);
	}
	if (Z_TYPE_P(except) == IS_NULL) {
		ZEPHIR_INIT_VAR(except);
		array_init(except);
	}
        convert_to_array(attributes);
        convert_to_array(on);
        convert_to_array(except);
	zephir_update_property_this(this_ptr, SL("attributes"), attributes TSRMLS_CC);
	zephir_update_property_this(this_ptr, SL("on"), on TSRMLS_CC);
	zephir_update_property_this(this_ptr, SL("except"), except TSRMLS_CC);
	ZEPHIR_MM_RESTORE();
}

/**
 * Validates the specified object.
 * @param \yii\base\Model $object the data object being validated
 * @param array|null $attributes the list of attributes to be validated.
 * Note that if an attribute is not associated with the validator,
 * it will be ignored.
 * If this parameter is null, every attribute listed in [[attributes]] will be validated.
 */
PHP_METHOD(yii_validators_Validator, validateAttributes) {

	zephir_fcall_cache_entry *_11 = NULL, *_13 = NULL;
	HashTable *_4;
	HashPosition _3;
	zend_bool skip = 0, _6, _8;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL, *_12 = NULL;
	zval *object, *attributes = NULL, *_0, *_1 = NULL, *attribute = NULL, **_5, *_7, *_9 = NULL, *_10 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &object, &attributes);

	if (!attributes) {
		ZEPHIR_CPY_WRT(attributes, ZEPHIR_GLOBAL(global_null));
	} else {
		ZEPHIR_SEPARATE_PARAM(attributes);
	}


	if (Z_TYPE_P(attributes) == IS_ARRAY) {
		_0 = zephir_fetch_nproperty_this(this_ptr, SL("attributes"), PH_NOISY_CC);
		ZEPHIR_CALL_FUNCTION(&_1, "array_intersect", &_2, _0, attributes);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(attributes, _1);
	} else {
		_0 = zephir_fetch_nproperty_this(this_ptr, SL("attributes"), PH_NOISY_CC);
		ZEPHIR_CPY_WRT(attributes, _0);
	}
	zephir_is_iterable(attributes, &_4, &_3, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_4, (void**) &_5, &_3) == SUCCESS
	  ; zephir_hash_move_forward_ex(_4, &_3)
	) {
		ZEPHIR_GET_HVALUE(attribute, _5);
		_0 = zephir_fetch_nproperty_this(this_ptr, SL("skipOnError"), PH_NOISY_CC);
		_6 = zephir_is_true(_0);
		if (_6) {
			ZEPHIR_CALL_METHOD(&_1, object, "haserrors", NULL, attribute);
			zephir_check_call_status();
			_6 = zephir_is_true(_1);
		}
		if (_6) {
			skip = 1;
		} else {
			_7 = zephir_fetch_nproperty_this(this_ptr, SL("skipOnEmpty"), PH_NOISY_CC);
			_8 = zephir_is_true(_7);
			if (_8) {
				ZEPHIR_OBS_NVAR(_10);
				zephir_read_property_zval(&_10, object, attribute, PH_NOISY_CC);
				ZEPHIR_CALL_METHOD(&_9, this_ptr, "isempty", &_11, _10);
				zephir_check_call_status();
				_8 = zephir_is_true(_9);
			}
			if (_8) {
				skip = 1;
			}
		}
		if (!(skip)) {
			ZEPHIR_OBS_NVAR(_10);
			zephir_read_property_this(&_10, this_ptr, SL("when"), PH_NOISY_CC);
			_8 = Z_TYPE_P(_10) == IS_NULL;
			if (!(_8)) {
				_7 = zephir_fetch_nproperty_this(this_ptr, SL("when"), PH_NOISY_CC);
				ZEPHIR_CALL_FUNCTION(&_9, "call_user_func", &_12, _7, object, attribute);
				zephir_check_call_status();
				_8 = zephir_is_true(_9);
			}
			if (_8) {
				ZEPHIR_CALL_METHOD(NULL, this_ptr, "validateattribute", &_13, object, attribute);
				zephir_check_call_status();
			}
		}
		skip = 0;
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Validates a single attribute.
 * Child classes must implement this method to provide the actual validation logic.
 * @param \yii\base\Model $object the data object to be validated
 * @param string $attribute the name of the attribute to be validated.
 */
PHP_METHOD(yii_validators_Validator, validateAttribute) {

	zend_bool _1;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *object, *attribute, *result = NULL, *_0, *_2, *_3;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &object, &attribute);

	ZEPHIR_INIT_VAR(result);
	ZVAL_NULL(result);


	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_zval(&_0, object, attribute, PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&result, this_ptr, "validatevalue", NULL, _0);
	zephir_check_call_status();
	_1 = Z_TYPE_P(result) == IS_ARRAY;
	if (_1) {
		_1 = zephir_fast_count_int(result TSRMLS_CC) > 0;
	}
	if (_1) {
		zephir_array_fetch_long(&_2, result, 0, PH_NOISY | PH_READONLY TSRMLS_CC);
		zephir_array_fetch_long(&_3, result, 1, PH_NOISY | PH_READONLY TSRMLS_CC);
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "adderror", NULL, object, attribute, _2, _3);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Validates a given value.
 * You may use this method to validate a value out of the context of a data model.
 * @param mixed $value the data value to be validated.
 * @param string $error the error message to be returned, if the validation fails.
 * @return boolean whether the data is valid.
 */
PHP_METHOD(yii_validators_Validator, validate) {

	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL;
	zend_bool _0, _1;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *value, *error = NULL, *result = NULL, *message, *params, *_2 = NULL, *_4 = NULL, *_5, *app = NULL, *i18n = NULL, *language = NULL, *_6, *_7 = NULL, *_8 = NULL, *_9;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &value, &error);

	if (!error) {
		ZEPHIR_CPY_WRT(error, ZEPHIR_GLOBAL(global_null));
	}/* else {
		ZEPHIR_SEPARATE_PARAM(error);
	}*/
	Z_SET_ISREF_P(error);

	ZEPHIR_INIT_VAR(result);
	ZVAL_NULL(result);


	ZEPHIR_CALL_METHOD(&result, this_ptr, "validatevalue", NULL, value);
	zephir_check_call_status();
	_0 = Z_TYPE_P(result) == IS_NULL;
	if (!(_0)) {
		_1 = Z_TYPE_P(result) == IS_ARRAY;
		if (_1) {
			_1 = zephir_fast_count_int(result TSRMLS_CC) == 0;
		}
		_0 = _1;
	}
	if (_0) {
		Z_UNSET_ISREF_P(error);
		RETURN_MM_BOOL(1);
	} else {
		ZEPHIR_OBS_VAR(message);
		zephir_array_fetch_long(&message, result, 0, PH_NOISY TSRMLS_CC);
		ZEPHIR_OBS_VAR(params);
		zephir_array_fetch_long(&params, result, 1, PH_NOISY TSRMLS_CC);
		ZEPHIR_INIT_VAR(_4);
		ZVAL_STRING(_4, "yii", 0);
		ZEPHIR_INIT_VAR(_5);
		ZVAL_STRING(_5, "the input value", 0);
		ZEPHIR_CALL_CE_STATIC(&_2, yii_baseyii_ce, "t", &_3, _4, _5);
		zephir_check_temp_parameter(_4);
		zephir_check_temp_parameter(_5);
		zephir_check_call_status();
		zephir_array_update_string(&params, SL("attribute"), &_2, PH_COPY | PH_SEPARATE);
		if (Z_TYPE_P(value) == IS_ARRAY) {
			ZEPHIR_INIT_NVAR(_4);
			ZVAL_STRING(_4, "array()", 1);
			zephir_array_update_string(&params, SL("value"), &_4, PH_COPY | PH_SEPARATE);
		} else {
			zephir_array_update_string(&params, SL("value"), &value, PH_COPY | PH_SEPARATE);
		}
		zephir_read_static_property_ce(&_6, yii_baseyii_ce, SL("app") TSRMLS_CC);
		ZEPHIR_CPY_WRT(app, _6);
		ZEPHIR_OBS_VAR(_7);
		zephir_read_property(&_7, app, SL("language"), PH_NOISY_CC);
		ZEPHIR_CPY_WRT(language, _7);
		ZEPHIR_CALL_METHOD(&i18n, app, "geti18n",  NULL);
		zephir_check_call_status();
		zephir_read_static_property_ce(&_6, yii_baseyii_ce, SL("app") TSRMLS_CC);
		ZEPHIR_CALL_METHOD(&_8, _6, "geti18n",  NULL);
		zephir_check_call_status();
		zephir_read_static_property_ce(&_9, yii_baseyii_ce, SL("app") TSRMLS_CC);
		ZEPHIR_OBS_NVAR(_7);
		zephir_read_property(&_7, _9, SL("language"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(&error, _8, "format", NULL, message, params, _7);
		zephir_check_call_status();
		Z_UNSET_ISREF_P(error);
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Validates a value.
 * A validator class can implement this method to support data validation out of the context of a data model.
 * @param mixed $value the data value to be validated.
 * @return array|null the error message and the parameters to be inserted into the error message.
 * Null should be returned if the data is valid.
 * @throws NotSupportedException if the validator does not supporting data validation without a model
 */
PHP_METHOD(yii_validators_Validator, validateValue) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *value, *_0, *_1, *_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &value);



	ZEPHIR_INIT_VAR(_0);
	object_init_ex(_0, yii_base_notsupportedexception_ce);
	ZEPHIR_INIT_VAR(_1);
	zephir_get_class(_1, this_ptr, 0 TSRMLS_CC);
	ZEPHIR_INIT_VAR(_2);
	ZEPHIR_CONCAT_VS(_2, _1, " does not support validateValue().");
	ZEPHIR_CALL_METHOD(NULL, _0, "__construct", NULL, _2);
	zephir_check_call_status();
	zephir_throw_exception_debug(_0, "yii/validators/Validator.zep", 309 TSRMLS_CC);
	ZEPHIR_MM_RESTORE();
	return;

}

/**
 * Returns the JavaScript needed for performing client-side validation.
 *
 * You may override this method to return the JavaScript validation code if
 * the validator can support client-side validation.
 *
 * The following JavaScript variables are predefined and can be used in the validation code:
 *
 * - `attribute`: the name of the attribute being validated.
 * - `value`: the value being validated.
 * - `messages`: an array used to hold the validation error messages for the attribute.
 *
 * @param \yii\base\Model $object the data object being validated
 * @param string $attribute the name of the attribute to be validated.
 * @param \yii\web\View $view the view object that is going to be used to render views or view files
 * containing a model form with this validator applied.
 * @return string the client-side validation script. Null if the validator does not support
 * client-side validation.
 * @see \yii\widgets\ActiveForm::enableClientValidation
 */
PHP_METHOD(yii_validators_Validator, clientValidateAttribute) {

	zval *object, *attribute, *view;

	zephir_fetch_params(0, 3, 0, &object, &attribute, &view);



	RETURN_NULL();

}

/**
 * Returns a value indicating whether the validator is active for the given scenario and attribute.
 *
 * A validator is active if
 *
 * - the validator's `on` property is empty, or
 * - the validator's `on` property contains the specified scenario
 *
 * @param string $scenario scenario name
 * @return boolean whether the validator applies to the specified scenario.
 */
PHP_METHOD(yii_validators_Validator, isActive) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL;
	zval *scenario, *except = NULL, *on = NULL, *_0, *_1 = NULL, *_3, *_4;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &scenario);



	_0 = zephir_fetch_nproperty_this(this_ptr, SL("except"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(except, _0);
	if (Z_TYPE_P(except) == IS_NULL) {
		ZEPHIR_INIT_VAR(except);
		array_init(except);
		zephir_update_property_this(this_ptr, SL("except"), except TSRMLS_CC);
	}
	_0 = zephir_fetch_nproperty_this(this_ptr, SL("on"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(on, _0);
	if (Z_TYPE_P(on) == IS_NULL) {
		ZEPHIR_INIT_VAR(on);
		array_init(on);
		zephir_update_property_this(this_ptr, SL("on"), on TSRMLS_CC);
	}
	_0 = zephir_fetch_nproperty_this(this_ptr, SL("except"), PH_NOISY_CC);
	ZEPHIR_CALL_FUNCTION(&_1, "in_array", &_2, scenario, _0, ZEPHIR_GLOBAL(global_true));
	zephir_check_call_status();
	if (!(zephir_is_true(_1))) {
		_3 = zephir_fetch_nproperty_this(this_ptr, SL("on"), PH_NOISY_CC);
		if (zephir_fast_count_int(_3 TSRMLS_CC) == 0) {
			RETURN_MM_BOOL(1);
		} else {
			_4 = zephir_fetch_nproperty_this(this_ptr, SL("on"), PH_NOISY_CC);
			ZEPHIR_CALL_FUNCTION(&_1, "in_array", &_2, scenario, _4, ZEPHIR_GLOBAL(global_true));
			zephir_check_call_status();
			if (zephir_is_true(_1)) {
				RETURN_MM_BOOL(1);
			}
		}
	}
	RETURN_MM_BOOL(0);

}

/**
 * Adds an error about the specified attribute to the model object.
 * This is a helper method that performs message selection and internationalization.
 * @param \yii\base\Model $object the data object being validated
 * @param string $attribute the attribute being validated
 * @param string $message the error message
 * @param array $params values for the placeholders in the error message
 */
PHP_METHOD(yii_validators_Validator, addError) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *object, *attribute, *message, *params = NULL, *value, *_0 = NULL, *_1, *_2, *_3 = NULL, *_4 = NULL, *_5, *_6;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 3, 1, &object, &attribute, &message, &params);

	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	} else {
		ZEPHIR_SEPARATE_PARAM(params);
	}


	ZEPHIR_OBS_VAR(value);
	zephir_read_property_zval(&value, object, attribute, PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_0, object, "getattributelabel", NULL, attribute);
	zephir_check_call_status();
	zephir_array_update_string(&params, SL("attribute"), &_0, PH_COPY | PH_SEPARATE);
	if (Z_TYPE_P(value) == IS_ARRAY) {
		ZEPHIR_INIT_VAR(_1);
		ZVAL_STRING(_1, "array()", 1);
		zephir_array_update_string(&params, SL("value"), &_1, PH_COPY | PH_SEPARATE);
	} else {
		zephir_array_update_string(&params, SL("value"), &value, PH_COPY | PH_SEPARATE);
	}
	zephir_read_static_property_ce(&_2, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CALL_METHOD(&_3, _2, "geti18n",  NULL);
	zephir_check_call_status();
	zephir_read_static_property_ce(&_5, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_OBS_VAR(_6);
	zephir_read_property(&_6, _5, SL("language"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_4, _3, "format", NULL, message, params, _6);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, object, "adderror", NULL, attribute, _4);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Checks if the given value is empty.
 * A value is considered empty if it is null, an empty array, or the trimmed result is an empty string.
 * Note that this method is different from PHP empty(). It will return false when the value is 0.
 * @param mixed $value the value to be checked
 * @return boolean whether the value is empty
 */
PHP_METHOD(yii_validators_Validator, isEmpty) {

	zend_bool _3, _4;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL;
	zval *value, *_0, *_1;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &value);



	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("isEmpty"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) != IS_NULL) {
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("isEmpty"), PH_NOISY_CC);
		ZEPHIR_RETURN_CALL_FUNCTION("call_user_func", &_2, _1, value);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		if (Z_TYPE_P(value) == IS_NULL) {
			RETURN_MM_BOOL(1);
		} else {
			_3 = Z_TYPE_P(value) == IS_ARRAY;
			if (_3) {
				_3 = zephir_fast_count_int(value TSRMLS_CC) == 0;
			}
			if (_3) {
				RETURN_MM_BOOL(1);
			} else {
				_4 = Z_TYPE_P(value) == IS_STRING;
				if (_4) {
					_4 = ZEPHIR_IS_STRING(value, "");
				}
				if (_4) {
					RETURN_MM_BOOL(1);
				}
			}
		}
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_MM_RESTORE();

}

