
extern zend_class_entry *yii_helpers_url_ce;

ZEPHIR_INIT_CLASS(yii_helpers_Url);

PHP_METHOD(yii_helpers_Url, toRoute);
PHP_METHOD(yii_helpers_Url, normalizeRoute);
PHP_METHOD(yii_helpers_Url, to);
PHP_METHOD(yii_helpers_Url, base);
PHP_METHOD(yii_helpers_Url, remember);
PHP_METHOD(yii_helpers_Url, previous);
PHP_METHOD(yii_helpers_Url, canonical);
PHP_METHOD(yii_helpers_Url, home);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_url_toroute, 0, 0, 1)
	ZEND_ARG_INFO(0, route)
	ZEND_ARG_INFO(0, scheme)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_url_normalizeroute, 0, 0, 1)
	ZEND_ARG_INFO(0, route)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_url_to, 0, 0, 0)
	ZEND_ARG_INFO(0, url)
	ZEND_ARG_INFO(0, scheme)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_url_base, 0, 0, 0)
	ZEND_ARG_INFO(0, scheme)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_url_remember, 0, 0, 0)
	ZEND_ARG_INFO(0, url)
	ZEND_ARG_INFO(0, name)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_url_previous, 0, 0, 0)
	ZEND_ARG_INFO(0, name)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_url_home, 0, 0, 0)
	ZEND_ARG_INFO(0, scheme)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_helpers_url_method_entry) {
	PHP_ME(yii_helpers_Url, toRoute, arginfo_yii_helpers_url_toroute, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Url, normalizeRoute, arginfo_yii_helpers_url_normalizeroute, ZEND_ACC_PROTECTED|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Url, to, arginfo_yii_helpers_url_to, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Url, base, arginfo_yii_helpers_url_base, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Url, remember, arginfo_yii_helpers_url_remember, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Url, previous, arginfo_yii_helpers_url_previous, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Url, canonical, NULL, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Url, home, arginfo_yii_helpers_url_home, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
  PHP_FE_END
};
