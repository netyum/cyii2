
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/string.h"
#include "kernel/object.h"
#include "kernel/concat.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * BaseStringHelper provides concrete implementation for [[StringHelper]].
 *
 * Do not use BaseStringHelper. Use [[StringHelper]] instead.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Alex Makarov <sam@rmcreative.ru>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_helpers_StringHelper) {

	ZEPHIR_REGISTER_CLASS(yii\\helpers, StringHelper, yii, helpers_stringhelper, yii_helpers_stringhelper_method_entry, 0);

	return SUCCESS;

}

/**
 * Returns the number of bytes in the given string.
 * This method ensures the string is treated as a byte array by using `mb_strlen()`.
 * @param string $string the string being measured for length
 * @return integer the number of bytes in the given string.
 */
PHP_METHOD(yii_helpers_StringHelper, byteLength) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *str_param = NULL, _0;
	zval *str = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &str_param);

	zephir_get_strval(str, str_param);


	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "8bit", 0);
	ZEPHIR_RETURN_CALL_FUNCTION("mb_strlen", NULL, str, &_0);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns the portion of string specified by the start and length parameters.
 * This method ensures the string is treated as a byte array by using `mb_substr()`.
 * @param string $string the input string. Must be one character or longer.
 * @param integer $start the starting position
 * @param integer $length the desired portion length
 * @return string the extracted part of string, or FALSE on failure or an empty string.
 * @see http://www.php.net/manual/en/function.substr.php
 */
PHP_METHOD(yii_helpers_StringHelper, byteSubstr) {

	int start, length, ZEPHIR_LAST_CALL_STATUS;
	zval *str_param = NULL, *start_param = NULL, *length_param = NULL, _0, _1, _2;
	zval *str = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 3, 0, &str_param, &start_param, &length_param);

	zephir_get_strval(str, str_param);
	start = zephir_get_intval(start_param);
	length = zephir_get_intval(length_param);


	ZEPHIR_SINIT_VAR(_0);
	ZVAL_LONG(&_0, start);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_LONG(&_1, length);
	ZEPHIR_SINIT_VAR(_2);
	ZVAL_STRING(&_2, "8bit", 0);
	ZEPHIR_RETURN_CALL_FUNCTION("mb_substr", NULL, str, &_0, &_1, &_2);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns the trailing name component of a path.
 * This method is similar to the php function `basename()` except that it will
 * treat both \ and / as directory separators, independent of the operating system.
 * This method was mainly created to work on php namespaces. When working with real
 * file paths, php's `basename()` should work fine for you.
 * Note: this method is not aware of the actual filesystem, or path components such as "..".
 *
 * @param string $path A path string.
 * @param string $suffix If the name component ends in suffix this will also be cut off.
 * @return string the trailing name component of the given path.
 * @see http://www.php.net/manual/en/function.basename.php
 */
PHP_METHOD(yii_helpers_StringHelper, basename) {

	zephir_nts_static zephir_fcall_cache_entry *_5 = NULL;
	zephir_fcall_cache_entry *_1 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *suffix = NULL;
	zval *path = NULL, *suffix_param = NULL, *len = NULL, *negative_len, *pos = NULL, *_0 = NULL, _2 = zval_used_for_init, *_3 = NULL, _4 = zval_used_for_init, *_6;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &path, &suffix_param);

	ZEPHIR_SEPARATE_PARAM(path);
	if (!suffix_param) {
		ZEPHIR_INIT_VAR(suffix);
		ZVAL_STRING(suffix, "", 1);
	} else {
		zephir_get_strval(suffix, suffix_param);
	}


	ZEPHIR_CALL_FUNCTION(&len, "mb_strlen", NULL, suffix);
	zephir_check_call_status();
	if (ZEPHIR_GT_LONG(len, 0)) {
		ZEPHIR_INIT_VAR(negative_len);
		ZVAL_LONG(negative_len, (zephir_get_numberval(len) * -1));
		ZEPHIR_CALL_FUNCTION(&_0, "mb_substr", &_1, path, negative_len);
		zephir_check_call_status();
		if (ZEPHIR_IS_EQUAL(_0, suffix)) {
			ZEPHIR_SINIT_VAR(_2);
			ZVAL_LONG(&_2, 0);
			ZEPHIR_CALL_FUNCTION(&_3, "mb_substr", &_1, path, &_2, negative_len);
			zephir_check_call_status();
			ZEPHIR_CPY_WRT(path, _3);
		}
	}
	ZEPHIR_SINIT_NVAR(_2);
	ZVAL_STRING(&_2, "\\", 0);
	ZEPHIR_SINIT_VAR(_4);
	ZVAL_STRING(&_4, "/", 0);
	ZEPHIR_CALL_FUNCTION(&_0, "str_replace", &_5, &_2, &_4, path);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(path, _0);
	ZEPHIR_INIT_VAR(_6);
	ZEPHIR_SINIT_NVAR(_2);
	ZVAL_STRING(&_2, "/\\", 0);
	zephir_fast_trim(_6, path, &_2, ZEPHIR_TRIM_RIGHT TSRMLS_CC);
	ZEPHIR_CPY_WRT(path, _6);
	ZEPHIR_SINIT_NVAR(_4);
	ZVAL_STRING(&_4, "/", 0);
	ZEPHIR_CALL_FUNCTION(&pos, "mb_strrpos", NULL, path, &_4);
	zephir_check_call_status();
	if (Z_TYPE_P(pos) != IS_BOOL) {
		ZEPHIR_SINIT_NVAR(_4);
		ZVAL_LONG(&_4, (zephir_get_numberval(pos) + 1));
		ZEPHIR_RETURN_CALL_FUNCTION("mb_substr", &_1, path, &_4);
		zephir_check_call_status();
		RETURN_MM();
	}
	RETURN_CCTOR(path);

}

/**
 * Returns parent directory's path.
 * This method is similar to `dirname()` except that it will treat
 * both \ and / as directory separators, independent of the operating system.
 *
 * @param string $path A path string.
 * @return string the parent directory's path.
 * @see http://www.php.net/manual/en/function.basename.php
 */
PHP_METHOD(yii_helpers_StringHelper, dirname) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL;
	zval *path, *pos = NULL, _0 = zval_used_for_init, _1, *_2 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &path);



	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "\\", 0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, "/", 0);
	ZEPHIR_CALL_FUNCTION(&_2, "str_replace", &_3, &_0, &_1, path);
	zephir_check_call_status();
	ZEPHIR_SINIT_NVAR(_0);
	ZVAL_STRING(&_0, "/", 0);
	ZEPHIR_CALL_FUNCTION(&pos, "mb_strrpos", NULL, _2, &_0);
	zephir_check_call_status();
	if (Z_TYPE_P(pos) != IS_BOOL) {
		ZEPHIR_SINIT_NVAR(_0);
		ZVAL_LONG(&_0, 0);
		ZEPHIR_RETURN_CALL_FUNCTION("mb_substr", NULL, path, &_0, pos);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		RETURN_MM_STRING("", 1);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Truncates a string to the number of characters specified.
 *
 * @param string $string The string to truncate.
 * @param integer $length How many characters from original string to include into truncated string.
 * @param string $suffix String to append to the end of truncated string.
 * @param string $encoding The charset to use, defaults to charset currently used by application.
 * @return string the truncated string.
 */
PHP_METHOD(yii_helpers_StringHelper, truncate) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *str_param = NULL, *length, *suffix = NULL, *encoding = NULL, *_0, *_1 = NULL, *_2, _3, *_4 = NULL;
	zval *str = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 2, &str_param, &length, &suffix, &encoding);

	zephir_get_strval(str, str_param);
	if (!suffix) {
		ZEPHIR_INIT_VAR(suffix);
		ZVAL_STRING(suffix, "...", 1);
	}
	if (!encoding) {
		ZEPHIR_CPY_WRT(encoding, ZEPHIR_GLOBAL(global_null));
	} else {
		ZEPHIR_SEPARATE_PARAM(encoding);
	}


	if (Z_TYPE_P(encoding) == IS_NULL) {
		zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
		ZEPHIR_OBS_NVAR(encoding);
		zephir_read_property(&encoding, _0, SL("charset"), PH_NOISY_CC);
	}
	ZEPHIR_CALL_FUNCTION(&_1, "mb_strlen", NULL, str, encoding);
	zephir_check_call_status();
	if (ZEPHIR_GT(_1, length)) {
		ZEPHIR_INIT_VAR(_2);
		ZEPHIR_SINIT_VAR(_3);
		ZVAL_LONG(&_3, 0);
		ZEPHIR_CALL_FUNCTION(&_4, "mb_substr", NULL, str, &_3, length, encoding);
		zephir_check_call_status();
		zephir_fast_trim(_2, _4, NULL , ZEPHIR_TRIM_BOTH TSRMLS_CC);
		ZEPHIR_CONCAT_VV(return_value, _2, suffix);
		RETURN_MM();
	} else {
		RETURN_CTOR(str);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Truncates a string to the number of words specified.
 *
 * @param string $string The string to truncate.
 * @param integer $count How many words from original string to include into truncated string.
 * @param string $suffix String to append to the end of truncated string.
 * @return string the truncated string.
 */
PHP_METHOD(yii_helpers_StringHelper, truncateWords) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL, *_4 = NULL;
	double current_words;
	zval *str_param = NULL, *count_num, *suffix = NULL, *words = NULL, *slice, *temp_words = NULL, *retval = NULL, *_0, _1 = zval_used_for_init, _2, *_5;
	zval *str = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &str_param, &count_num, &suffix);

	zephir_get_strval(str, str_param);
	ZEPHIR_SEPARATE_PARAM(str);
	if (!suffix) {
		ZEPHIR_INIT_VAR(suffix);
		ZVAL_STRING(suffix, "...", 1);
	}


	ZEPHIR_INIT_VAR(_0);
	zephir_fast_trim(_0, str, NULL , ZEPHIR_TRIM_BOTH TSRMLS_CC);
	zephir_get_strval(str, _0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, "/(\\s+)/u", 0);
	ZEPHIR_SINIT_VAR(_2);
	ZVAL_LONG(&_2, 2);
	ZEPHIR_CALL_FUNCTION(&words, "preg_split", &_3, &_1, str, ZEPHIR_GLOBAL(global_null), &_2);
	zephir_check_call_status();
	current_words = (double) ((zephir_fast_count_int(words TSRMLS_CC) / 2));
	ZEPHIR_INIT_VAR(slice);
	ZVAL_LONG(slice, ((zephir_get_numberval(count_num) * 2) - 1));
	if (ZEPHIR_IS_DOUBLE(count_num, current_words)) {
		ZEPHIR_SINIT_NVAR(_1);
		ZVAL_LONG(&_1, 0);
		ZEPHIR_CALL_FUNCTION(&temp_words, "array_slice", &_4, words, &_1, slice);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(retval);
		zephir_fast_join_str(retval, SL(""), temp_words TSRMLS_CC);
		ZEPHIR_INIT_VAR(_5);
		ZEPHIR_CONCAT_VV(_5, retval, suffix);
		ZEPHIR_CPY_WRT(retval, _5);
		RETURN_CCTOR(retval);
	} else {
		RETURN_CTOR(str);
	}
	ZEPHIR_MM_RESTORE();

}

