
extern zend_class_entry *yii_helpers_inflector_ce;

ZEPHIR_INIT_CLASS(yii_helpers_Inflector);

PHP_METHOD(yii_helpers_Inflector, pluralize);
PHP_METHOD(yii_helpers_Inflector, singularize);
PHP_METHOD(yii_helpers_Inflector, titleize);
PHP_METHOD(yii_helpers_Inflector, camelize);
PHP_METHOD(yii_helpers_Inflector, camel2words);
PHP_METHOD(yii_helpers_Inflector, camel2id);
PHP_METHOD(yii_helpers_Inflector, id2camel);
PHP_METHOD(yii_helpers_Inflector, underscore);
PHP_METHOD(yii_helpers_Inflector, humanize);
PHP_METHOD(yii_helpers_Inflector, variablize);
PHP_METHOD(yii_helpers_Inflector, tableize);
PHP_METHOD(yii_helpers_Inflector, slug);
PHP_METHOD(yii_helpers_Inflector, classify);
PHP_METHOD(yii_helpers_Inflector, ordinalize);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_pluralize, 0, 0, 1)
	ZEND_ARG_INFO(0, word)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_singularize, 0, 0, 1)
	ZEND_ARG_INFO(0, word)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_titleize, 0, 0, 1)
	ZEND_ARG_INFO(0, words)
	ZEND_ARG_INFO(0, ucAll)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_camelize, 0, 0, 1)
	ZEND_ARG_INFO(0, word)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_camel2words, 0, 0, 1)
	ZEND_ARG_INFO(0, name)
	ZEND_ARG_INFO(0, ucwords)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_camel2id, 0, 0, 1)
	ZEND_ARG_INFO(0, name)
	ZEND_ARG_INFO(0, separator)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_id2camel, 0, 0, 1)
	ZEND_ARG_INFO(0, id)
	ZEND_ARG_INFO(0, separator)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_underscore, 0, 0, 1)
	ZEND_ARG_INFO(0, words)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_humanize, 0, 0, 1)
	ZEND_ARG_INFO(0, word)
	ZEND_ARG_INFO(0, ucAll)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_variablize, 0, 0, 1)
	ZEND_ARG_INFO(0, word)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_tableize, 0, 0, 1)
	ZEND_ARG_INFO(0, className)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_slug, 0, 0, 1)
	ZEND_ARG_INFO(0, str)
	ZEND_ARG_INFO(0, replacement)
	ZEND_ARG_INFO(0, lowercase)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_classify, 0, 0, 1)
	ZEND_ARG_INFO(0, tableName)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_inflector_ordinalize, 0, 0, 1)
	ZEND_ARG_INFO(0, number)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_helpers_inflector_method_entry) {
	PHP_ME(yii_helpers_Inflector, pluralize, arginfo_yii_helpers_inflector_pluralize, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, singularize, arginfo_yii_helpers_inflector_singularize, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, titleize, arginfo_yii_helpers_inflector_titleize, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, camelize, arginfo_yii_helpers_inflector_camelize, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, camel2words, arginfo_yii_helpers_inflector_camel2words, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, camel2id, arginfo_yii_helpers_inflector_camel2id, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, id2camel, arginfo_yii_helpers_inflector_id2camel, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, underscore, arginfo_yii_helpers_inflector_underscore, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, humanize, arginfo_yii_helpers_inflector_humanize, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, variablize, arginfo_yii_helpers_inflector_variablize, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, tableize, arginfo_yii_helpers_inflector_tableize, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, slug, arginfo_yii_helpers_inflector_slug, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, classify, arginfo_yii_helpers_inflector_classify, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_Inflector, ordinalize, arginfo_yii_helpers_inflector_ordinalize, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
  PHP_FE_END
};
