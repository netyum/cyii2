
extern zend_class_entry *yii_helpers_stringhelper_ce;

ZEPHIR_INIT_CLASS(yii_helpers_StringHelper);

PHP_METHOD(yii_helpers_StringHelper, byteLength);
PHP_METHOD(yii_helpers_StringHelper, byteSubstr);
PHP_METHOD(yii_helpers_StringHelper, basename);
PHP_METHOD(yii_helpers_StringHelper, dirname);
PHP_METHOD(yii_helpers_StringHelper, truncate);
PHP_METHOD(yii_helpers_StringHelper, truncateWords);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_stringhelper_bytelength, 0, 0, 1)
	ZEND_ARG_INFO(0, str)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_stringhelper_bytesubstr, 0, 0, 3)
	ZEND_ARG_INFO(0, str)
	ZEND_ARG_INFO(0, start)
	ZEND_ARG_INFO(0, length)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_stringhelper_basename, 0, 0, 1)
	ZEND_ARG_INFO(0, path)
	ZEND_ARG_INFO(0, suffix)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_stringhelper_dirname, 0, 0, 1)
	ZEND_ARG_INFO(0, path)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_stringhelper_truncate, 0, 0, 2)
	ZEND_ARG_INFO(0, str)
	ZEND_ARG_INFO(0, length)
	ZEND_ARG_INFO(0, suffix)
	ZEND_ARG_INFO(0, encoding)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_helpers_stringhelper_truncatewords, 0, 0, 2)
	ZEND_ARG_INFO(0, str)
	ZEND_ARG_INFO(0, count_num)
	ZEND_ARG_INFO(0, suffix)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_helpers_stringhelper_method_entry) {
	PHP_ME(yii_helpers_StringHelper, byteLength, arginfo_yii_helpers_stringhelper_bytelength, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_StringHelper, byteSubstr, arginfo_yii_helpers_stringhelper_bytesubstr, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_StringHelper, basename, arginfo_yii_helpers_stringhelper_basename, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_StringHelper, dirname, arginfo_yii_helpers_stringhelper_dirname, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_StringHelper, truncate, arginfo_yii_helpers_stringhelper_truncate, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_helpers_StringHelper, truncateWords, arginfo_yii_helpers_stringhelper_truncatewords, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
  PHP_FE_END
};
