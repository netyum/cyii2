
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/array.h"
#include "extra/extra.h"
#include "kernel/hash.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/string.h"
#include "kernel/concat.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * BaseInflector provides concrete implementation for [[Inflector]].
 *
 * Do not use BaseInflector. Use [[Inflector]] instead.
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_helpers_Inflector) {

	ZEPHIR_REGISTER_CLASS(yii\\helpers, Inflector, yii, helpers_inflector, yii_helpers_inflector_method_entry, 0);

	/**
	 * @var array the rules for converting a word into its plural form.
	 * The keys are the regular expressions and the values are the corresponding replacements.
	 */
	zend_declare_property_null(yii_helpers_inflector_ce, SL("plurals"), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	/**
	 * @var array the rules for converting a word into its singular form.
	 * The keys are the regular expressions and the values are the corresponding replacements.
	 */
	zend_declare_property_null(yii_helpers_inflector_ce, SL("singulars"), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	/**
	 * @var array the special rules for converting a word between its plural form and singular form.
	 * The keys are the special words in singular form, and the values are the corresponding plural form.
	 */
	zend_declare_property_null(yii_helpers_inflector_ce, SL("specials"), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	/**
	 * @var array map of special chars and its translation. This is used by [[slug()]].
	 */
	zend_declare_property_null(yii_helpers_inflector_ce, SL("transliteration"), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Converts a word to its plural form.
 * Note that this is for English only!
 * For example, 'apple' will become 'apples', and 'child' will become 'children'.
 * @param string $word the word to be pluralized
 * @return string the pluralized word
 */
PHP_METHOD(yii_helpers_Inflector, pluralize) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_5 = NULL, *_6 = NULL;
	HashTable *_2;
	HashPosition _1;
	zval *word, *specials = NULL, *plurals = NULL, *w, *rule = NULL, *replacement = NULL, *_0, **_3, *_4 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &word);

	ZEPHIR_INIT_VAR(specials);
	ZVAL_NULL(specials);
	ZEPHIR_INIT_VAR(plurals);
	ZVAL_NULL(plurals);


	zephir_read_static_property_ce(&_0, yii_helpers_inflector_ce, SL("specials") TSRMLS_CC);
	ZEPHIR_CPY_WRT(specials, _0);
	if (Z_TYPE_P(specials) == IS_NULL) {
		ZEPHIR_INIT_VAR(specials);
		yii_helpers_inflector_specials(&specials);
		zephir_update_static_property_ce(yii_helpers_inflector_ce, SL("specials"), specials TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(w);
	if (zephir_array_isset_fetch(&w, specials, word, 0 TSRMLS_CC)) {
		RETURN_CCTOR(w);
	}
	zephir_read_static_property_ce(&_0, yii_helpers_inflector_ce, SL("plurals") TSRMLS_CC);
	ZEPHIR_CPY_WRT(plurals, _0);
	if (Z_TYPE_P(plurals) == IS_NULL) {
		ZEPHIR_INIT_VAR(plurals);
		yii_helpers_inflector_plurals(&plurals);
		zephir_update_static_property_ce(yii_helpers_inflector_ce, SL("plurals"), plurals TSRMLS_CC);
	}
	zephir_is_iterable(plurals, &_2, &_1, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_2, (void**) &_3, &_1) == SUCCESS
	  ; zephir_hash_move_forward_ex(_2, &_1)
	) {
		ZEPHIR_GET_HMKEY(rule, _2, _1);
		ZEPHIR_GET_HVALUE(replacement, _3);
		ZEPHIR_CALL_FUNCTION(&_4, "preg_match", &_5, rule, word);
		zephir_check_call_status();
		if (zephir_is_true(_4)) {
			ZEPHIR_RETURN_CALL_FUNCTION("preg_replace", &_6, rule, replacement, word);
			zephir_check_call_status();
			RETURN_MM();
		}
	}
	RETURN_CCTOR(word);

}

/**
 * Returns the singular of the $word
 * @param string $word the english word to singularize
 * @return string Singular noun.
 */
PHP_METHOD(yii_helpers_Inflector, singularize) {

	HashTable *_4;
	HashPosition _3;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL, *_7 = NULL, *_8 = NULL;
	zval *word, *specials = NULL, *result = NULL, *singulars = NULL, *rule = NULL, *replacement = NULL, *_0, *_2, **_5, *_6 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &word);



	zephir_read_static_property_ce(&_0, yii_helpers_inflector_ce, SL("specials") TSRMLS_CC);
	ZEPHIR_CPY_WRT(specials, _0);
	if (Z_TYPE_P(specials) == IS_NULL) {
		ZEPHIR_INIT_VAR(specials);
		yii_helpers_inflector_specials(&specials);
		zephir_update_static_property_ce(yii_helpers_inflector_ce, SL("specials"), specials TSRMLS_CC);
	}
	_0 = zephir_fetch_static_property_ce(yii_helpers_inflector_ce, SL("specials") TSRMLS_CC);
	ZEPHIR_CALL_FUNCTION(&result, "array_search", &_1, word, _0, ZEPHIR_GLOBAL(global_true));
	zephir_check_call_status();
	if (Z_TYPE_P(result) != IS_BOOL) {
		RETURN_CCTOR(result);
	}
	zephir_read_static_property_ce(&_2, yii_helpers_inflector_ce, SL("singulars") TSRMLS_CC);
	ZEPHIR_CPY_WRT(singulars, _2);
	if (Z_TYPE_P(singulars) == IS_NULL) {
		ZEPHIR_INIT_VAR(singulars);
		yii_helpers_inflector_singulars(&singulars);
		zephir_update_static_property_ce(yii_helpers_inflector_ce, SL("singulars"), singulars TSRMLS_CC);
	}
	zephir_is_iterable(singulars, &_4, &_3, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_4, (void**) &_5, &_3) == SUCCESS
	  ; zephir_hash_move_forward_ex(_4, &_3)
	) {
		ZEPHIR_GET_HMKEY(rule, _4, _3);
		ZEPHIR_GET_HVALUE(replacement, _5);
		ZEPHIR_CALL_FUNCTION(&_6, "preg_match", &_7, rule, word);
		zephir_check_call_status();
		if (zephir_is_true(_6)) {
			ZEPHIR_RETURN_CALL_FUNCTION("preg_replace", &_8, rule, replacement, word);
			zephir_check_call_status();
			RETURN_MM();
		}
	}
	RETURN_CCTOR(word);

}

/**
 * Converts an underscored or CamelCase word into a English
 * sentence.
 * @param string $words
 * @param boolean $ucAll whether to set all words to uppercase
 * @return string
 */
PHP_METHOD(yii_helpers_Inflector, titleize) {

	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL, *_2 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *words = NULL, *ucAll = NULL, *_0 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &words, &ucAll);

	ZEPHIR_SEPARATE_PARAM(words);
	if (!ucAll) {
		ucAll = ZEPHIR_GLOBAL(global_false);
	}


	ZEPHIR_CALL_SELF(&_0, "underscore", NULL, words);
	zephir_check_call_status();
	ZEPHIR_CALL_SELF(&words, "humanize", NULL, _0, ucAll);
	zephir_check_call_status();
	if (zephir_is_true(ucAll)) {
		ZEPHIR_RETURN_CALL_FUNCTION("ucwords", &_1, words);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		ZEPHIR_RETURN_CALL_FUNCTION("ucfirst", &_2, words);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns given word as CamelCased
 * Converts a word like "send_email" to "SendEmail". It
 * will remove non alphanumeric character from the word, so
 * "who's online" will be converted to "WhoSOnline"
 * @see variablize()
 * @param string $word the word to CamelCase
 * @return string
 */
PHP_METHOD(yii_helpers_Inflector, camelize) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL, *_5 = NULL, *_6 = NULL;
	zval *word, _0 = zval_used_for_init, _1 = zval_used_for_init, *_2 = NULL, *_4 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &word);



	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "/[^A-Za-z0-9]+/", 0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, " ", 0);
	ZEPHIR_CALL_FUNCTION(&_2, "preg_replace", &_3, &_0, &_1, word);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(&_4, "ucwords", &_5, _2);
	zephir_check_call_status();
	ZEPHIR_SINIT_NVAR(_0);
	ZVAL_STRING(&_0, " ", 0);
	ZEPHIR_SINIT_NVAR(_1);
	ZVAL_STRING(&_1, "", 0);
	ZEPHIR_RETURN_CALL_FUNCTION("str_replace", &_6, &_0, &_1, _4);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Converts a CamelCase name into space-separated words.
 * For example, 'PostTag' will be converted to 'Post Tag'.
 * @param string $name the string to be converted
 * @param boolean $ucwords whether to capitalize the first letter in each word
 * @return string the resulting words
 */
PHP_METHOD(yii_helpers_Inflector, camel2words) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL, *_5 = NULL, *_6 = NULL;
	zval *name, *ucwords = NULL, *label = NULL, *elements, _0 = zval_used_for_init, _1, *_3, *_4 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &name, &ucwords);

	if (!ucwords) {
		ucwords = ZEPHIR_GLOBAL(global_true);
	}
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "/(?<![A-Z])[A-Z]/", 0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, " \\0", 0);
	ZEPHIR_CALL_FUNCTION(&label, "preg_replace", &_2, &_0, &_1, name);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(elements);
	array_init_size(elements, 5);
	ZEPHIR_INIT_VAR(_3);
	ZVAL_STRING(_3, "-", 1);
	zephir_array_fast_append(elements, _3);
	ZEPHIR_INIT_BNVAR(_3);
	ZVAL_STRING(_3, "_", 1);
	zephir_array_fast_append(elements, _3);
	ZEPHIR_INIT_BNVAR(_3);
	ZVAL_STRING(_3, ".", 1);
	zephir_array_fast_append(elements, _3);
	ZEPHIR_SINIT_NVAR(_0);
	ZVAL_STRING(&_0, " ", 0);
	ZEPHIR_CALL_FUNCTION(&_4, "str_replace", &_5, elements, &_0, label);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(label, _4);
	ZEPHIR_INIT_BNVAR(_3);
	zephir_fast_strtolower(_3, label);
	ZEPHIR_CPY_WRT(label, _3);
	ZEPHIR_INIT_BNVAR(_3);
	zephir_fast_trim(_3, label, NULL , ZEPHIR_TRIM_BOTH TSRMLS_CC);
	ZEPHIR_CPY_WRT(label, _3);
	if (zephir_is_true(ucwords)) {
		ZEPHIR_RETURN_CALL_FUNCTION("ucwords", &_6, label);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		RETURN_CCTOR(label);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Converts a CamelCase name into an ID in lowercase.
 * Words in the ID may be concatenated using the specified character (defaults to '-').
 * For example, 'PostTag' will be converted to 'post-tag'.
 * @param string $name the string to be converted
 * @param string $separator the character used to concatenate the words in the ID
 * @return string the resulting ID
 */
PHP_METHOD(yii_helpers_Inflector, camel2id) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_4 = NULL, *_7 = NULL;
	zval *name, *separator = NULL, *_0 = NULL, _1 = zval_used_for_init, _2, *_3 = NULL, *_5, *_6 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &name, &separator);

	if (!separator) {
		ZEPHIR_INIT_VAR(separator);
		ZVAL_STRING(separator, "-", 1);
	}


	if (ZEPHIR_IS_STRING(separator, "_")) {
		ZEPHIR_INIT_VAR(_0);
		ZEPHIR_SINIT_VAR(_1);
		ZVAL_STRING(&_1, "/(?<![A-Z])[A-Z]/", 0);
		ZEPHIR_SINIT_VAR(_2);
		ZVAL_STRING(&_2, "_\\0", 0);
		ZEPHIR_CALL_FUNCTION(&_3, "preg_replace", &_4, &_1, &_2, name);
		zephir_check_call_status();
		zephir_fast_strtolower(_0, _3);
		ZEPHIR_SINIT_NVAR(_1);
		ZVAL_STRING(&_1, "_", 0);
		zephir_fast_trim(return_value, _0, &_1, ZEPHIR_TRIM_BOTH TSRMLS_CC);
		RETURN_MM();
	} else {
		ZEPHIR_INIT_NVAR(_0);
		ZEPHIR_INIT_VAR(_5);
		ZEPHIR_CONCAT_VS(_5, separator, "\\0");
		ZEPHIR_SINIT_NVAR(_1);
		ZVAL_STRING(&_1, "/(?<![A-Z])[A-Z]/", 0);
		ZEPHIR_CALL_FUNCTION(&_3, "preg_replace", &_4, &_1, _5, name);
		zephir_check_call_status();
		ZEPHIR_SINIT_NVAR(_1);
		ZVAL_STRING(&_1, "_", 0);
		ZEPHIR_CALL_FUNCTION(&_6, "str_replace", &_7, &_1, separator, _3);
		zephir_check_call_status();
		zephir_fast_strtolower(_0, _6);
		zephir_fast_trim(return_value, _0, separator, ZEPHIR_TRIM_BOTH TSRMLS_CC);
		RETURN_MM();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Converts an ID into a CamelCase name.
 * Words in the ID separated by `$separator` (defaults to '-') will be concatenated into a CamelCase name.
 * For example, 'post-tag' is converted to 'PostTag'.
 * @param string $id the ID to be converted
 * @param string $separator the character used to separate the words in the ID
 * @return string the resulting CamelCase name
 */
PHP_METHOD(yii_helpers_Inflector, id2camel) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL, *_6 = NULL;
	zval *id, *separator = NULL, *_0, *_1, *_2 = NULL, _4, _5;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &id, &separator);

	if (!separator) {
		ZEPHIR_INIT_VAR(separator);
		ZVAL_STRING(separator, "-", 1);
	}


	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_INIT_VAR(_1);
	zephir_fast_explode(_1, separator, id, LONG_MAX TSRMLS_CC);
	zephir_fast_join_str(_0, SL(" "), _1 TSRMLS_CC);
	ZEPHIR_CALL_FUNCTION(&_2, "ucwords", &_3, _0);
	zephir_check_call_status();
	ZEPHIR_SINIT_VAR(_4);
	ZVAL_STRING(&_4, " ", 0);
	ZEPHIR_SINIT_VAR(_5);
	ZVAL_STRING(&_5, "", 0);
	ZEPHIR_RETURN_CALL_FUNCTION("str_replace", &_6, &_4, &_5, _2);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Converts any "CamelCased" into an "underscored_word".
 * @param string $words the word(s) to underscore
 * @return string
 */
PHP_METHOD(yii_helpers_Inflector, underscore) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL;
	zval *words, _0, _1, *_2 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &words);



	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "/(?<=\\w)([A-Z])/", 0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, "_\\1", 0);
	ZEPHIR_CALL_FUNCTION(&_2, "preg_replace", &_3, &_0, &_1, words);
	zephir_check_call_status();
	zephir_fast_strtolower(return_value, _2);
	RETURN_MM();

}

/**
 * Returns a human-readable string from $word
 * @param string $word the string to humanize
 * @param boolean $ucAll whether to set all words to uppercase or not
 * @return string
 */
PHP_METHOD(yii_helpers_Inflector, humanize) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL, *_4 = NULL, *_5 = NULL, *_6 = NULL;
	zval *word = NULL, *ucAll = NULL, _0 = zval_used_for_init, _1 = zval_used_for_init, *_2 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &word, &ucAll);

	ZEPHIR_SEPARATE_PARAM(word);
	if (!ucAll) {
		ucAll = ZEPHIR_GLOBAL(global_false);
	}


	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "/_id$/", 0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, "", 0);
	ZEPHIR_CALL_FUNCTION(&_2, "preg_replace", &_3, &_0, &_1, word);
	zephir_check_call_status();
	ZEPHIR_SINIT_NVAR(_0);
	ZVAL_STRING(&_0, "_", 0);
	ZEPHIR_SINIT_NVAR(_1);
	ZVAL_STRING(&_1, " ", 0);
	ZEPHIR_CALL_FUNCTION(&word, "str_replace", &_4, &_0, &_1, _2);
	zephir_check_call_status();
	if (zephir_is_true(ucAll)) {
		ZEPHIR_RETURN_CALL_FUNCTION("ucwords", &_5, word);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		ZEPHIR_RETURN_CALL_FUNCTION("ucfirst", &_6, word);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Same as camelize but first char is in lowercase.
 * Converts a word like "send_email" to "sendEmail". It
 * will remove non alphanumeric character from the word, so
 * "who's online" will be converted to "whoSOnline"
 * @param string $word to lowerCamelCase
 * @return string
 */
PHP_METHOD(yii_helpers_Inflector, variablize) {

	zephir_nts_static zephir_fcall_cache_entry *_4 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *word = NULL, *_0 = NULL, *_1, _2 = zval_used_for_init, _3, *_5 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &word);

	ZEPHIR_SEPARATE_PARAM(word);


	ZEPHIR_CALL_SELF(&_0, "camelize", NULL, word);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(word, _0);
	ZEPHIR_INIT_VAR(_1);
	ZEPHIR_SINIT_VAR(_2);
	ZVAL_LONG(&_2, 0);
	ZEPHIR_SINIT_VAR(_3);
	ZVAL_LONG(&_3, 1);
	ZEPHIR_CALL_FUNCTION(&_0, "substr", &_4, word, &_2, &_3);
	zephir_check_call_status();
	zephir_fast_strtolower(_1, _0);
	ZEPHIR_SINIT_NVAR(_2);
	ZVAL_LONG(&_2, 1);
	ZEPHIR_CALL_FUNCTION(&_5, "substr", &_4, word, &_2);
	zephir_check_call_status();
	ZEPHIR_CONCAT_VV(return_value, _1, _5);
	RETURN_MM();

}

/**
 * Converts a class name to its table name (pluralized)
 * naming conventions. For example, converts "Person" to "people"
 * @param string $className the class name for getting related table_name
 * @return string
 */
PHP_METHOD(yii_helpers_Inflector, tableize) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *className, *_0 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &className);



	ZEPHIR_CALL_SELF(&_0, "underscore", NULL, className);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_SELF("pluralize", NULL, _0);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns a string with all spaces converted to given replacement and
 * non word characters removed.  Maps special characters to ASCII using
 * [[$transliteration]] array.
 * @param string $string An arbitrary string to convert
 * @param string $replacement The replacement to use for spaces
 * @param boolean $lowercase whether to return the string in lowercase or not. Defaults to `true`.
 * @return string The converted string.
 */
PHP_METHOD(yii_helpers_Inflector, slug) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL, *_4 = NULL, *_7 = NULL;
	zval *str = NULL, *replacement = NULL, *lowercase = NULL, *options, *transliteration = NULL, _0 = zval_used_for_init, *_1 = NULL, *_3 = NULL, *_5, *_6 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 2, &str, &replacement, &lowercase);

	ZEPHIR_SEPARATE_PARAM(str);
	if (!replacement) {
		ZEPHIR_INIT_VAR(replacement);
		ZVAL_STRING(replacement, "-", 1);
	}
	if (!lowercase) {
		lowercase = ZEPHIR_GLOBAL(global_true);
	}


	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "intl", 0);
	ZEPHIR_CALL_FUNCTION(&_1, "extension_loaded", &_2, &_0);
	zephir_check_call_status();
	if (ZEPHIR_IS_TRUE(_1)) {
		ZEPHIR_INIT_VAR(options);
		ZVAL_STRING(options, "Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove;", 1);
		ZEPHIR_CALL_FUNCTION(&_3, "transliterator_transliterate", NULL, options, str);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(str, _3);
		ZEPHIR_SINIT_NVAR(_0);
		ZVAL_STRING(&_0, "/[-\\s]+/", 0);
		ZEPHIR_CALL_FUNCTION(&_3, "preg_replace", &_4, &_0, replacement, str);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(str, _3);
	} else {
		zephir_read_static_property_ce(&_5, yii_helpers_inflector_ce, SL("transliteration") TSRMLS_CC);
		ZEPHIR_CPY_WRT(transliteration, _5);
		if (Z_TYPE_P(transliteration) == IS_NULL) {
			ZEPHIR_INIT_VAR(transliteration);
			yii_helpers_inflector_transliteration(&transliteration);
			zephir_update_static_property_ce(yii_helpers_inflector_ce, SL("transliteration"), transliteration TSRMLS_CC);
		}
		ZEPHIR_INIT_VAR(_6);
		zephir_array_keys(_6, transliteration TSRMLS_CC);
		ZEPHIR_CALL_FUNCTION(&_3, "str_replace", &_7, _6, transliteration, str);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(str, _3);
		ZEPHIR_SINIT_NVAR(_0);
		ZVAL_STRING(&_0, "/[^\\p{L}\\p{Nd}]+/u", 0);
		ZEPHIR_CALL_FUNCTION(&_3, "preg_replace", &_4, &_0, replacement, str);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(str, _3);
	}
	ZEPHIR_INIT_NVAR(_6);
	zephir_fast_trim(_6, str, replacement, ZEPHIR_TRIM_BOTH TSRMLS_CC);
	ZEPHIR_CPY_WRT(str, _6);
	if (zephir_is_true(lowercase)) {
		zephir_fast_strtolower(return_value, str);
		RETURN_MM();
	} else {
		RETURN_CCTOR(str);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Converts a table name to its class name. For example, converts "people" to "Person"
 * @param string $tableName
 * @return string
 */
PHP_METHOD(yii_helpers_Inflector, classify) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *tableName, *_0 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &tableName);



	ZEPHIR_CALL_SELF(&_0, "singularize", NULL, tableName);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_SELF("camelize", NULL, _0);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Converts number to its ordinal English form. For example, converts 13 to 13th, 2 to 2nd ...
 * @param integer $number the number to get its ordinal value
 * @return string
 */
PHP_METHOD(yii_helpers_Inflector, ordinalize) {

	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL;
	zval *number_param = NULL, *copy_number, _0 = zval_used_for_init, _1, *_2 = NULL;
	int number, remainder, ZEPHIR_LAST_CALL_STATUS, _4;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &number_param);

	number = zephir_get_intval(number_param);


	remainder = (number % 100);
	ZEPHIR_INIT_VAR(copy_number);
	ZVAL_LONG(copy_number, number);
	ZEPHIR_SINIT_VAR(_0);
	ZVAL_LONG(&_0, 11);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_LONG(&_1, 13);
	ZEPHIR_CALL_FUNCTION(&_2, "range", &_3, &_0, &_1);
	zephir_check_call_status();
	ZEPHIR_SINIT_NVAR(_0);
	ZVAL_LONG(&_0, (number % 100));
	if (zephir_fast_in_array(&_0, _2 TSRMLS_CC)) {
		ZEPHIR_CONCAT_VS(return_value, copy_number, "th");
		RETURN_MM();
	}
	do {
		_4 = ((number % 10));
		if (_4 == 1) {
			ZEPHIR_CONCAT_VS(return_value, copy_number, "st");
			RETURN_MM();
		}
		if (_4 == 2) {
			ZEPHIR_CONCAT_VS(return_value, copy_number, "nd");
			RETURN_MM();
		}
		if (_4 == 3) {
			ZEPHIR_CONCAT_VS(return_value, copy_number, "rd");
			RETURN_MM();
		}
		ZEPHIR_CONCAT_VS(return_value, copy_number, "th");
		RETURN_MM();
	} while(0);

	ZEPHIR_MM_RESTORE();

}

