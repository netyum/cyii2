
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/array.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/string.h"
#include "kernel/exception.h"
#include "kernel/concat.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * BaseUrl provides concrete implementation for [[Url]].
 *
 * Do not use BaseUrl. Use [[Url]] instead.
 *
 * @author Alexander Makarov <sam@rmcreative.ru>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_helpers_Url) {

	ZEPHIR_REGISTER_CLASS(yii\\helpers, Url, yii, helpers_url, yii_helpers_url_method_entry, 0);

	return SUCCESS;

}

/**
 * Creates a URL for the given route.
 *
 * This method will use [[\yii\web\UrlManager]] to create a URL.
 *
 * You may specify the route as a string, e.g., `site/index`. You may also use an array
 * if you want to specify additional query parameters for the URL being created. The
 * array format must be:
 *
 * ```php
 * // generates: /index.php?r=site/index&param1=value1&param2=value2
 * ['site/index', 'param1' => 'value1', 'param2' => 'value2']
 * ```
 *
 * If you want to create a URL with an anchor, you can use the array format with a `#` parameter.
 * For example,
 *
 * ```php
 * // generates: /index.php?r=site/index&param1=value1#name
 * ['site/index', 'param1' => 'value1', '#' => 'name']
 * ```
 *
 * A route may be either absolute or relative. An absolute route has a leading slash (e.g. `/site/index`),
 * while a relative route has none (e.g. `site/index` or `index`). A relative route will be converted
 * into an absolute one by the following rules:
 *
 * - If the route is an empty string, the current [[\yii\web\Controller::route|route]] will be used;
 * - If the route contains no slashes at all (e.g. `index`), it is considered to be an action ID
 *   of the current controller and will be prepended with [[\yii\web\Controller::uniqueId]];
 * - If the route has no leading slash (e.g. `site/index`), it is considered to be a route relative
 *   to the current module and will be prepended with the module's [[\yii\base\Module::uniqueId|uniqueId]].
 *
 * Below are some examples of using this method:
 *
 * ```php
 * // /index?r=site/index
 * echo Url::toRoute('site/index');
 *
 * // /index?r=site/index&src=ref1#name
 * echo Url::toRoute(['site/index', 'src' => 'ref1', '#' => 'name']);
 *
 * // http://www.example.com/index.php?r=site/index
 * echo Url::toRoute('site/index', true);
 *
 * // https://www.example.com/index.php?r=site/index
 * echo Url::toRoute('site/index', 'https');
 * ```
 *
 * @param string|array $route use a string to represent a route (e.g. `index`, `site/index`),
 * or an array to represent a route with query parameters (e.g. `['site/index', 'param1' => 'value1']`).
 * @param boolean|string $scheme the URI scheme to use in the generated URL:
 *
 * - `false` (default): generating a relative URL.
 * - `true`: generating an absolute URL whose scheme is the same as the current request.
 * - string: generating an absolute URL with the specified scheme (either `http` or `https`).
 *
 * @return string the generated URL
 * @throws InvalidParamException a relative route is given while there is no active controller
 */
PHP_METHOD(yii_helpers_Url, toRoute) {

	zend_bool _3;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *route, *scheme = NULL, *routes = NULL, *app = NULL, *url_manager = NULL, *retval = NULL, *_0, *_1 = NULL, *_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &route, &scheme);

	if (!scheme) {
		scheme = ZEPHIR_GLOBAL(global_false);
	}


	if (Z_TYPE_P(route) == IS_STRING) {
		ZEPHIR_INIT_VAR(routes);
		array_init(routes);
		zephir_array_append(&routes, route, PH_SEPARATE);
	} else {
		ZEPHIR_CPY_WRT(routes, route);
	}
	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _0);
	ZEPHIR_CALL_METHOD(&url_manager, app, "geturlmanager",  NULL);
	zephir_check_call_status();
	zephir_array_fetch_long(&_2, routes, 0, PH_NOISY | PH_READONLY TSRMLS_CC);
	ZEPHIR_CALL_SELF(&_1, "normalizeroute", NULL, _2);
	zephir_check_call_status();
	zephir_array_update_long(&routes, 0, &_1, PH_COPY | PH_SEPARATE, "yii/helpers/Url.zep", 98);
	_3 = Z_TYPE_P(scheme) == IS_BOOL;
	if (_3) {
		_3 = ZEPHIR_IS_FALSE(scheme);
	}
	if (_3) {
		ZEPHIR_CALL_METHOD(&retval, url_manager, "createurl", NULL, routes);
		zephir_check_call_status();
	} else {
		if (Z_TYPE_P(scheme) == IS_STRING) {
			ZEPHIR_CALL_METHOD(&retval, url_manager, "createabsoluteurl", NULL, routes, scheme);
			zephir_check_call_status();
		} else {
			ZEPHIR_CALL_METHOD(&retval, url_manager, "createabsoluteurl", NULL, routes, ZEPHIR_GLOBAL(global_null));
			zephir_check_call_status();
		}
	}
	RETURN_CCTOR(retval);

}

/**
 * Normalizes route and makes it suitable for UrlManager. Absolute routes are staying as is
 * while relative routes are converted to absolute ones.
 *
 * A relative route is a route without a leading slash, such as "view", "post/view".
 *
 * - If the route is an empty string, the current [[\yii\web\Controller::route|route]] will be used;
 * - If the route contains no slashes at all, it is considered to be an action ID
 *   of the current controller and will be prepended with [[\yii\web\Controller::uniqueId]];
 * - If the route has no leading slash, it is considered to be a route relative
 *   to the current module and will be prepended with the module's uniqueId.
 *
 * @param string $route the route. This can be either an absolute route or a relative route.
 * @return string normalized route suitable for UrlManager
 * @throws InvalidParamException a relative route is given while there is no active controller
 */
PHP_METHOD(yii_helpers_Url, normalizeRoute) {

	zend_bool _3;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL;
	zval *route_param = NULL, *pos = NULL, *app = NULL, *controller = NULL, _0 = zval_used_for_init, _1 = zval_used_for_init, *_4, *_5 = NULL, *_6 = NULL, *_8 = NULL, *module = NULL, _9;
	zval *route = NULL, *_7;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &route_param);

	zephir_get_strval(route, route_param);


	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "/", 0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_LONG(&_1, 1);
	ZEPHIR_CALL_FUNCTION(&pos, "strncmp", &_2, route, &_0, &_1);
	zephir_check_call_status();
	_3 = Z_TYPE_P(pos) != IS_BOOL;
	if (_3) {
		_3 = ZEPHIR_IS_LONG(pos, 0);
	}
	if (_3) {
		ZEPHIR_SINIT_NVAR(_0);
		ZVAL_STRING(&_0, "/", 0);
		zephir_fast_trim(return_value, route, &_0, ZEPHIR_TRIM_LEFT TSRMLS_CC);
		RETURN_MM();
	}
	zephir_read_static_property_ce(&_4, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _4);
	ZEPHIR_OBS_VAR(_5);
	zephir_read_property(&_5, app, SL("controller"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(controller, _5);
	if (Z_TYPE_P(controller) == IS_NULL) {
		ZEPHIR_INIT_VAR(_6);
		object_init_ex(_6, yii_base_invalidparamexception_ce);
		ZEPHIR_INIT_VAR(_7);
		ZEPHIR_CONCAT_SVS(_7, "Unable to resolve the relative route: ", route, ". No active controller is available.");
		ZEPHIR_CALL_METHOD(NULL, _6, "__construct", NULL, _7);
		zephir_check_call_status();
		zephir_throw_exception_debug(_6, "yii/helpers/Url.zep", 143 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_SINIT_NVAR(_1);
	ZVAL_STRING(&_1, "/", 0);
	ZEPHIR_INIT_BNVAR(pos);
	zephir_fast_strpos(pos, route, &_1, 0 );
	if (Z_TYPE_P(pos) == IS_BOOL) {
		if (ZEPHIR_IS_STRING(route, "")) {
			ZEPHIR_RETURN_CALL_METHOD(controller, "getroute", NULL);
			zephir_check_call_status();
			RETURN_MM();
		} else {
			ZEPHIR_CALL_METHOD(&_8, controller, "getuniqueid",  NULL);
			zephir_check_call_status();
			ZEPHIR_CONCAT_VSV(return_value, _8, "/", route);
			RETURN_MM();
		}
	} else {
		ZEPHIR_OBS_NVAR(_5);
		zephir_read_property(&_5, controller, SL("module"), PH_NOISY_CC);
		ZEPHIR_CPY_WRT(module, _5);
		ZEPHIR_CALL_METHOD(&_8, module, "getuniqueid",  NULL);
		zephir_check_call_status();
		ZEPHIR_INIT_LNVAR(_6);
		ZEPHIR_CONCAT_VSV(_6, _8, "/", route);
		ZEPHIR_SINIT_VAR(_9);
		ZVAL_STRING(&_9, "/", 0);
		zephir_fast_trim(return_value, _6, &_9, ZEPHIR_TRIM_LEFT TSRMLS_CC);
		RETURN_MM();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Creates a URL based on the given parameters.
 *
 * This method is very similar to [[toRoute()]]. The only difference is that this method
 * requires a route to be specified as an array only. If a string is given, it will be treated
 * as a URL which will be prefixed with the base URL if it does not start with a slash.
 * In particular, if `$url` is
 *
 * - an array: [[toRoute()]] will be called to generate the URL. For example:
 *   `['site/index']`, `['post/index', 'page' => 2]`. Please refer to [[toRoute()]] for more details
 *   on how to specify a route.
 * - a string with a leading `@`: it is treated as an alias and the corresponding aliased string
 *   will be subject to the following rules.
 * - an empty string: the currently requested URL will be returned;
 * - a string without a leading slash: it will be prefixed with [[\yii\web\Request::baseUrl]].
 * - a string with a leading slash: it will be returned as is.
 *
 * Note that in case `$scheme` is specified (either a string or true), an absolute URL with host info
 * will be returned.
 *
 * Below are some examples of using this method:
 *
 * ```php
 * // /index?r=site/index
 * echo Url::to(['site/index']);
 *
 * // /index?r=site/index&src=ref1#name
 * echo Url::to(['site/index', 'src' => 'ref1', '#' => 'name']);
 *
 * // the currently requested URL
 * echo Url::to();
 *
 * // /images/logo.gif
 * echo Url::to('images/logo.gif');
 *
 * // http://www.example.com/index.php?r=site/index
 * echo Url::to(['site/index'], true);
 *
 * // https://www.example.com/index.php?r=site/index
 * echo Url::to(['site/index'], 'https');
 * ```
 *
 *
 * @param array|string $url the parameter to be used to generate a valid URL
 * @param boolean|string $scheme the URI scheme to use in the generated URL:
 *
 * - `false` (default): generating a relative URL.
 * - `true`: generating an absolute URL whose scheme is the same as the current request.
 * - string: generating an absolute URL with the specified scheme (either `http` or `https`).
 *
 * @return string the generated URL
 * @throws InvalidParamException a relative route is given while there is no active controller
 */
PHP_METHOD(yii_helpers_Url, to) {

	zend_bool _7, _8, _9;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL, *_6 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *url = NULL, *scheme = NULL, *app = NULL, *request = NULL, *pos = NULL, *one = NULL, *_0 = NULL, *_2, _3 = zval_used_for_init, _4 = zval_used_for_init, _5, *_10 = NULL, *_11 = NULL, *_12;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 2, &url, &scheme);

	if (!url) {
		ZEPHIR_INIT_VAR(url);
		ZVAL_STRING(url, "", 1);
	} else {
		ZEPHIR_SEPARATE_PARAM(url);
	}
	if (!scheme) {
		scheme = ZEPHIR_GLOBAL(global_false);
	}


	if (Z_TYPE_P(url) == IS_ARRAY) {
		ZEPHIR_RETURN_CALL_SELF("toroute", NULL, url, scheme);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_CALL_CE_STATIC(&_0, yii_baseyii_ce, "getalias", &_1, url);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(url, _0);
	zephir_read_static_property_ce(&_2, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _2);
	ZEPHIR_CALL_METHOD(&request, app, "getrequest",  NULL);
	zephir_check_call_status();
	if (ZEPHIR_IS_STRING(url, "")) {
		ZEPHIR_CALL_METHOD(&url, request, "geturl",  NULL);
		zephir_check_call_status();
	} else {
		ZEPHIR_SINIT_VAR(_3);
		ZVAL_STRING(&_3, "://", 0);
		ZEPHIR_INIT_VAR(pos);
		zephir_fast_strpos(pos, url, &_3, 0 );
		ZEPHIR_SINIT_VAR(_4);
		ZVAL_LONG(&_4, 0);
		ZEPHIR_SINIT_VAR(_5);
		ZVAL_LONG(&_5, 1);
		ZEPHIR_CALL_FUNCTION(&one, "substr", &_6, url, &_4, &_5);
		zephir_check_call_status();
		_7 = !ZEPHIR_IS_STRING(one, "/");
		if (_7) {
			_7 = !ZEPHIR_IS_STRING(one, "#");
		}
		_8 = _7;
		if (_8) {
			_8 = !ZEPHIR_IS_STRING(one, ".");
		}
		_9 = _8;
		if (_9) {
			_9 = Z_TYPE_P(pos) == IS_BOOL;
		}
		if (_9) {
			ZEPHIR_CALL_METHOD(&_0, request, "getbaseurl",  NULL);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(_10);
			ZEPHIR_CONCAT_VSV(_10, _0, "/", url);
			ZEPHIR_CPY_WRT(url, _10);
		}
	}
	_7 = Z_TYPE_P(scheme) != IS_BOOL;
	if (!(_7)) {
		_7 = ZEPHIR_IS_TRUE(scheme);
	}
	if (_7) {
		ZEPHIR_SINIT_NVAR(_3);
		ZVAL_STRING(&_3, "://", 0);
		ZEPHIR_INIT_NVAR(pos);
		zephir_fast_strpos(pos, url, &_3, 0 );
		if (Z_TYPE_P(pos) == IS_BOOL) {
			ZEPHIR_CALL_METHOD(&_11, request, "gethostinfo",  NULL);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(_12);
			ZEPHIR_SINIT_NVAR(_4);
			ZVAL_STRING(&_4, "/", 0);
			zephir_fast_trim(_12, url, &_4, ZEPHIR_TRIM_LEFT TSRMLS_CC);
			ZEPHIR_INIT_LNVAR(_10);
			ZEPHIR_CONCAT_VSV(_10, _11, "/", _12);
			ZEPHIR_CPY_WRT(url, _10);
		}
		_8 = Z_TYPE_P(scheme) == IS_STRING;
		if (_8) {
			_8 = Z_TYPE_P(scheme) != IS_BOOL;
		}
		if (_8) {
			ZEPHIR_CALL_FUNCTION(&_11, "substr", &_6, url, pos);
			zephir_check_call_status();
			ZEPHIR_INIT_LNVAR(_10);
			ZEPHIR_CONCAT_VV(_10, scheme, _11);
			ZEPHIR_CPY_WRT(url, _10);
		}
	}
	RETURN_CCTOR(url);

}

/**
 * Returns the base URL of the current request.
 * @param boolean|string $scheme the URI scheme to use in the returned base URL:
 *
 * - `false` (default): returning the base URL without host info.
 * - `true`: returning an absolute base URL whose scheme is the same as the current request.
 * - string: returning an absolute base URL with the specified scheme (either `http` or `https`).
 * @return string
 */
PHP_METHOD(yii_helpers_Url, base) {

	zephir_nts_static zephir_fcall_cache_entry *_7 = NULL;
	zend_bool _1, _5;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *scheme = NULL, *app = NULL, *request, *url = NULL, *pos, *_0, *_2 = NULL, *_3 = NULL, _4, *_6 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &scheme);

	if (!scheme) {
		scheme = ZEPHIR_GLOBAL(global_false);
	}


	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _0);
	ZEPHIR_OBS_VAR(request);
	zephir_read_property(&request, app, SL("getRequest"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&url, request, "getbaseurl",  NULL);
	zephir_check_call_status();
	_1 = Z_TYPE_P(scheme) != IS_BOOL;
	if (!(_1)) {
		_1 = ZEPHIR_IS_TRUE(scheme);
	}
	if (_1) {
		ZEPHIR_CALL_METHOD(&_2, request, "gethostinfo",  NULL);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(_3);
		ZEPHIR_CONCAT_VV(_3, _2, url);
		ZEPHIR_CPY_WRT(url, _3);
		ZEPHIR_SINIT_VAR(_4);
		ZVAL_STRING(&_4, "://", 0);
		ZEPHIR_INIT_VAR(pos);
		zephir_fast_strpos(pos, url, &_4, 0 );
		_5 = Z_TYPE_P(scheme) == IS_STRING;
		if (_5) {
			_5 = !ZEPHIR_IS_STRING(pos, "boolean");
		}
		if (_5) {
			ZEPHIR_CALL_FUNCTION(&_6, "substr", &_7, url, pos);
			zephir_check_call_status();
			ZEPHIR_INIT_LNVAR(_3);
			ZEPHIR_CONCAT_VV(_3, scheme, _6);
			ZEPHIR_CPY_WRT(url, _3);
		}
	}
	RETURN_CCTOR(url);

}

/**
 * Remembers the specified URL so that it can be later fetched back by [[previous()]].
 *
 * @param string|array $url the URL to remember. Please refer to [[to()]] for acceptable formats.
 * If this parameter is not specified, the currently requested URL will be used.
 * @param string $name the name associated with the URL to be remembered. This can be used
 * later by [[previous()]]. If not set, it will use [[\yii\web\User::returnUrlParam]].
 * @see previous()
 */
PHP_METHOD(yii_helpers_Url, remember) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *url = NULL, *name = NULL, *app = NULL, *_0, *_1 = NULL, *_2 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 2, &url, &name);

	if (!url) {
		ZEPHIR_INIT_VAR(url);
		ZVAL_STRING(url, "", 1);
	} else {
		ZEPHIR_SEPARATE_PARAM(url);
	}
	if (!name) {
		name = ZEPHIR_GLOBAL(global_null);
	}


	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _0);
	ZEPHIR_CALL_SELF(&_1, "to", NULL, url);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(url, _1);
	if (Z_TYPE_P(name) == IS_NULL) {
		ZEPHIR_CALL_METHOD(&_1, app, "getuser",  NULL);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, _1, "setreturnurl", NULL, url);
		zephir_check_call_status();
	} else {
		ZEPHIR_CALL_METHOD(&_2, app, "getsession",  NULL);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, _2, "set", NULL, name, url);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the URL previously [[remember()|remembered]].
 *
 * @param string $name the named associated with the URL that was remembered previously.
 * If not set, it will use [[\yii\web\User::returnUrlParam]].
 * @return string the URL previously remembered. Null is returned if no URL was remembered with the given name.
 * @see remember()
 */
PHP_METHOD(yii_helpers_Url, previous) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *name = NULL, *app = NULL, *_0, *_1 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &name);

	if (!name) {
		name = ZEPHIR_GLOBAL(global_null);
	}


	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _0);
	if (Z_TYPE_P(name) == IS_NULL) {
		ZEPHIR_CALL_METHOD(&_1, app, "getuser",  NULL);
		zephir_check_call_status();
		ZEPHIR_RETURN_CALL_METHOD(_1, "getreturnurl", NULL);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		ZEPHIR_CALL_METHOD(&_1, app, "getsession",  NULL);
		zephir_check_call_status();
		ZEPHIR_RETURN_CALL_METHOD(_1, "get", NULL, name);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the canonical URL of the currently requested page.
 * The canonical URL is constructed using the current controller's [[\yii\web\Controller::route]] and
 * [[\yii\web\Controller::actionParams]]. You may use the following code in the layout view to add a link tag
 * about canonical URL:
 *
 * ```php
 * $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()]);
 * ```
 *
 * @return string the canonical URL of the currently requested page
 */
PHP_METHOD(yii_helpers_Url, canonical) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *app = NULL, *url_manager = NULL, *controller = NULL, *params, *route = NULL, *temp_params = NULL, *temp_route = NULL, *_0, *_1;

	ZEPHIR_MM_GROW();

	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _0);
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property(&_1, app, SL("controller"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(controller, _1);
	ZEPHIR_CALL_METHOD(&url_manager, app, "geturlmanager",  NULL);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(params);
	zephir_read_property(&params, controller, SL("actionParams"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&route, controller, "getroute",  NULL);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(temp_params, params);
	ZEPHIR_CPY_WRT(temp_route, route);
	if (Z_TYPE_P(params) == IS_ARRAY) {
		zephir_array_update_long(&temp_params, 0, &temp_params, PH_COPY | PH_SEPARATE, "yii/helpers/Url.zep", 349);
	}
	ZEPHIR_RETURN_CALL_METHOD(url_manager, "createabsoluteurl", NULL, temp_params);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns the home URL.
 *
 * @param boolean|string $scheme the URI scheme to use for the returned URL:
 *
 * - `false` (default): returning a relative URL.
 * - `true`: returning an absolute URL whose scheme is the same as the current request.
 * - string: returning an absolute URL with the specified scheme (either `http` or `https`).
 *
 * @return string home URL
 */
PHP_METHOD(yii_helpers_Url, home) {

	zephir_nts_static zephir_fcall_cache_entry *_8 = NULL;
	zend_bool _1, _6;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *scheme = NULL, *url = NULL, *app = NULL, *request = NULL, *pos, *_0, *_2, *_3 = NULL, *_4 = NULL, _5, *_7 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &scheme);

	if (!scheme) {
		scheme = ZEPHIR_GLOBAL(global_false);
	}


	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _0);
	ZEPHIR_CALL_METHOD(&url, app, "gethomeurl",  NULL);
	zephir_check_call_status();
	_1 = Z_TYPE_P(scheme) != IS_BOOL;
	if (!(_1)) {
		_1 = ZEPHIR_IS_TRUE(scheme);
	}
	if (_1) {
		ZEPHIR_OBS_VAR(_2);
		zephir_read_property(&_2, app, SL("request"), PH_NOISY_CC);
		ZEPHIR_CPY_WRT(request, _2);
		ZEPHIR_CALL_METHOD(&_3, request, "gethostinfo",  NULL);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(_4);
		ZEPHIR_CONCAT_VV(_4, _3, url);
		ZEPHIR_CPY_WRT(url, _4);
		ZEPHIR_SINIT_VAR(_5);
		ZVAL_STRING(&_5, "://", 0);
		ZEPHIR_INIT_VAR(pos);
		zephir_fast_strpos(pos, url, &_5, 0 );
		_6 = Z_TYPE_P(scheme) == IS_STRING;
		if (_6) {
			_6 = Z_TYPE_P(pos) != IS_BOOL;
		}
		if (_6) {
			ZEPHIR_CALL_FUNCTION(&_7, "substr", &_8, url, pos);
			zephir_check_call_status();
			ZEPHIR_INIT_LNVAR(_4);
			ZEPHIR_CONCAT_VV(_4, scheme, _7);
			ZEPHIR_CPY_WRT(url, _4);
		}
	}
	RETURN_CCTOR(url);

}

