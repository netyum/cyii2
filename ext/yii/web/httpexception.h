
extern zend_class_entry *yii_web_httpexception_ce;

ZEPHIR_INIT_CLASS(yii_web_HttpException);

PHP_METHOD(yii_web_HttpException, __construct);
PHP_METHOD(yii_web_HttpException, getName);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_httpexception___construct, 0, 0, 1)
	ZEND_ARG_INFO(0, status)
	ZEND_ARG_INFO(0, message)
	ZEND_ARG_INFO(0, code)
	ZEND_ARG_INFO(0, previous)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_web_httpexception_method_entry) {
	PHP_ME(yii_web_HttpException, __construct, arginfo_yii_web_httpexception___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(yii_web_HttpException, getName, NULL, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
