
extern zend_class_entry *yii_web_erroraction_ce;

ZEPHIR_INIT_CLASS(yii_web_ErrorAction);

PHP_METHOD(yii_web_ErrorAction, run);

ZEPHIR_INIT_FUNCS(yii_web_erroraction_method_entry) {
	PHP_ME(yii_web_ErrorAction, run, NULL, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
