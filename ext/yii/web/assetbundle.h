
extern zend_class_entry *yii_web_assetbundle_ce;

ZEPHIR_INIT_CLASS(yii_web_AssetBundle);

PHP_METHOD(yii_web_AssetBundle, register);
PHP_METHOD(yii_web_AssetBundle, init);
PHP_METHOD(yii_web_AssetBundle, registerAssetFiles);
PHP_METHOD(yii_web_AssetBundle, publish);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_assetbundle_register, 0, 0, 1)
	ZEND_ARG_INFO(0, view)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_assetbundle_registerassetfiles, 0, 0, 1)
	ZEND_ARG_INFO(0, view)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_assetbundle_publish, 0, 0, 1)
	ZEND_ARG_INFO(0, am)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_web_assetbundle_method_entry) {
	PHP_ME(yii_web_AssetBundle, register, arginfo_yii_web_assetbundle_register, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_web_AssetBundle, init, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_AssetBundle, registerAssetFiles, arginfo_yii_web_assetbundle_registerassetfiles, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_AssetBundle, publish, arginfo_yii_web_assetbundle_publish, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
