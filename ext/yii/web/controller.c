
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/hash.h"
#include "kernel/array.h"
#include "kernel/operators.h"
#include "kernel/exception.h"
#include "kernel/string.h"
#include "kernel/concat.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Controller is the base class of web controllers.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_web_Controller) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\web, Controller, yii, web_controller, yii_base_controller_ce, yii_web_controller_method_entry, 0);

	/**
	 * @var boolean whether to enable CSRF validation for the actions in this controller.
	 * CSRF validation is enabled only when both this property and [[Request::enableCsrfValidation]] are true.
	 */
	zend_declare_property_bool(yii_web_controller_ce, SL("enableCsrfValidation"), 1, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array the parameters bound to the current action.
	 */
	zend_declare_property_null(yii_web_controller_ce, SL("actionParams"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Renders a view in response to an AJAX request.
 *
 * This method is similar to [[renderPartial()]] except that it will inject into
 * the rendering result with JS/CSS scripts and files which are registered with the view.
 * For this reason, you should use this method instead of [[renderPartial()]] to render
 * a view to respond to an AJAX request.
 *
 * @param string $view the view name. Please refer to [[render()]] on how to specify a view name.
 * @param array $params the parameters (name-value pairs) that should be made available in the view.
 * @return string the rendering result.
 */
PHP_METHOD(yii_web_Controller, renderAjax) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *view, *params = NULL, *_0 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &view, &params);

	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getview",  NULL);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(_0, "renderajax", NULL, view, params, this_ptr);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Binds the parameters to the action.
 * This method is invoked by [[\yii\base\Action]] when it begins to run with the given parameters.
 * This method will check the parameter names that the action requires and return
 * the provided parameters according to the requirement. If there is any missing parameter,
 * an exception will be thrown.
 * @param \yii\base\Action $action the action to be bound with parameters
 * @param array $params the parameters to be bound to the action
 * @return array the valid parameters that the action can run with.
 * @throws HttpException if there are missing or invalid parameters.
 */
PHP_METHOD(yii_web_Controller, bindActionParams) {

	zephir_fcall_cache_entry *_12 = NULL, *_14 = NULL;
	HashTable *_5;
	HashPosition _4;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_class_entry *_0, *_2;
	zval *action, *params, *method, *args, *missing, *actionParams, *parameters = NULL, *param = NULL, *name = NULL, *_1, *_3 = NULL, **_6, *_7 = NULL, *_8, *_9, *_10, *elements, *p, *_11 = NULL, *_13 = NULL, *_15 = NULL, *temp_p, *_16;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &action, &params);

	ZEPHIR_SEPARATE_PARAM(params);
	ZEPHIR_INIT_VAR(args);
	array_init(args);
	ZEPHIR_INIT_VAR(missing);
	array_init(missing);
	ZEPHIR_INIT_VAR(actionParams);
	array_init(actionParams);
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);
	ZEPHIR_INIT_VAR(p);
	array_init(p);
	ZEPHIR_INIT_VAR(temp_p);
	array_init(temp_p);


	ZEPHIR_INIT_VAR(method);
	if (zephir_instance_of_ev(action, yii_base_inlineaction_ce TSRMLS_CC)) {
		_0 = zend_fetch_class(SL("ReflectionMethod"), ZEND_FETCH_CLASS_AUTO TSRMLS_CC);
		object_init_ex(method, _0);
		ZEPHIR_OBS_VAR(_1);
		zephir_read_property(&_1, action, SL("actionMethod"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(NULL, method, "__construct", NULL, this_ptr, _1);
		zephir_check_call_status();
	} else {
		_2 = zend_fetch_class(SL("ReflectionMethod"), ZEND_FETCH_CLASS_AUTO TSRMLS_CC);
		object_init_ex(method, _2);
		ZEPHIR_INIT_VAR(_3);
		ZVAL_STRING(_3, "run", 0);
		ZEPHIR_CALL_METHOD(NULL, method, "__construct", NULL, action, _3);
		zephir_check_temp_parameter(_3);
		zephir_check_call_status();
	}
	ZEPHIR_CALL_METHOD(&parameters, method, "getparameters",  NULL);
	zephir_check_call_status();
	zephir_is_iterable(parameters, &_5, &_4, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_5, (void**) &_6, &_4) == SUCCESS
	  ; zephir_hash_move_forward_ex(_5, &_4)
	) {
		ZEPHIR_GET_HVALUE(param, _6);
		ZEPHIR_CALL_METHOD(&name, param, "getname",  NULL);
		zephir_check_call_status();
		if (zephir_array_key_exists(params, name TSRMLS_CC)) {
			ZEPHIR_CALL_METHOD(&_7, param, "isarray",  NULL);
			zephir_check_call_status();
			if (zephir_is_true(_7)) {
				zephir_array_fetch(&_8, params, name, PH_NOISY | PH_READONLY TSRMLS_CC);
				if (Z_TYPE_P(_8) == IS_ARRAY) {
					zephir_array_fetch(&_9, params, name, PH_NOISY | PH_READONLY TSRMLS_CC);
					zephir_array_append(&args, _9, PH_SEPARATE);
					zephir_array_fetch(&_10, params, name, PH_NOISY | PH_READONLY TSRMLS_CC);
					zephir_array_update_zval(&actionParams, name, &_10, PH_COPY | PH_SEPARATE);
				} else {
					zephir_array_fetch(&_9, params, name, PH_NOISY | PH_READONLY TSRMLS_CC);
					zephir_array_append(&elements, _9, PH_SEPARATE);
					zephir_array_append(&args, elements, PH_SEPARATE);
					zephir_array_update_zval(&actionParams, name, &elements, PH_COPY | PH_SEPARATE);
				}
			} else {
				zephir_array_fetch(&_8, params, name, PH_NOISY | PH_READONLY TSRMLS_CC);
				if (!(Z_TYPE_P(_8) == IS_ARRAY)) {
					zephir_array_fetch(&_9, params, name, PH_NOISY | PH_READONLY TSRMLS_CC);
					zephir_array_append(&args, _9, PH_SEPARATE);
					zephir_array_fetch(&_10, params, name, PH_NOISY | PH_READONLY TSRMLS_CC);
					zephir_array_update_zval(&actionParams, name, &_10, PH_COPY | PH_SEPARATE);
				} else {
					zephir_array_update_string(&p, SL("params"), &name, PH_COPY | PH_SEPARATE);
					ZEPHIR_INIT_LNVAR(_11);
					object_init_ex(_11, yii_web_badrequesthttpexception_ce);
					ZEPHIR_INIT_NVAR(_3);
					ZVAL_STRING(_3, "yii", 0);
					ZEPHIR_INIT_NVAR(_13);
					ZVAL_STRING(_13, "Invalid data received for parameter \"{param}\".", 0);
					ZEPHIR_CALL_CE_STATIC(&_7, yii_baseyii_ce, "t", &_12, _3, _13, name);
					zephir_check_temp_parameter(_3);
					zephir_check_temp_parameter(_13);
					zephir_check_call_status();
					ZEPHIR_CALL_METHOD(NULL, _11, "__construct", &_14, _7);
					zephir_check_call_status();
					zephir_throw_exception_debug(_11, "yii/web/Controller.zep", 90 TSRMLS_CC);
					ZEPHIR_MM_RESTORE();
					return;
				}
				zephir_array_unset(&params, name, PH_SEPARATE);
			}
		} else {
			ZEPHIR_CALL_METHOD(&_7, param, "isdefaultvalueavailable",  NULL);
			zephir_check_call_status();
			if (zephir_is_true(_7)) {
				ZEPHIR_CALL_METHOD(&_7, param, "getdefaultvalue",  NULL);
				zephir_check_call_status();
				zephir_array_append(&args, _7, PH_SEPARATE);
				ZEPHIR_CALL_METHOD(&_15, param, "getdefaultvalue",  NULL);
				zephir_check_call_status();
				zephir_array_update_zval(&actionParams, name, &_15, PH_COPY | PH_SEPARATE);
			} else {
				zephir_array_append(&missing, name, PH_SEPARATE);
			}
		}
	}
	if (zephir_fast_count_int(missing TSRMLS_CC) > 0) {
		ZEPHIR_INIT_NVAR(_3);
		zephir_fast_join_str(_3, SL(", "), missing TSRMLS_CC);
		zephir_array_update_string(&temp_p, SL("params"), &_3, PH_COPY | PH_SEPARATE);
		ZEPHIR_INIT_LNVAR(_11);
		object_init_ex(_11, yii_web_badrequesthttpexception_ce);
		ZEPHIR_INIT_NVAR(_13);
		ZVAL_STRING(_13, "yii", 0);
		ZEPHIR_INIT_VAR(_16);
		ZVAL_STRING(_16, "Missing required parameters: {params}", 0);
		ZEPHIR_CALL_CE_STATIC(&_7, yii_baseyii_ce, "t", &_12, _13, _16, temp_p);
		zephir_check_temp_parameter(_13);
		zephir_check_temp_parameter(_16);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, _11, "__construct", &_14, _7);
		zephir_check_call_status();
		zephir_throw_exception_debug(_11, "yii/web/Controller.zep", 107 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	zephir_update_property_this(this_ptr, SL("actionParams"), actionParams TSRMLS_CC);
	RETURN_CCTOR(args);

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_web_Controller, beforeAction) {

	zend_bool _4, _5;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL, *_8 = NULL;
	zval *action, *app = NULL, *errorHandler = NULL, *request = NULL, *_0 = NULL, *_2, *_3 = NULL, *_6, *_7 = NULL, *_9, *_10;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &action);



	ZEPHIR_CALL_PARENT(&_0, yii_web_controller_ce, this_ptr, "beforeaction", &_1, action);
	zephir_check_call_status();
	if (zephir_is_true(_0)) {
		zephir_read_static_property_ce(&_2, yii_baseyii_ce, SL("app") TSRMLS_CC);
		ZEPHIR_CPY_WRT(app, _2);
		ZEPHIR_OBS_VAR(_3);
		zephir_read_property(&_3, app, SL("errorHandler"), PH_NOISY_CC);
		ZEPHIR_CPY_WRT(errorHandler, _3);
		ZEPHIR_CALL_METHOD(&request, app, "getrequest",  NULL);
		zephir_check_call_status();
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("enableCsrfValidation"), PH_NOISY_CC);
		_4 = zephir_is_true(_2);
		if (_4) {
			ZEPHIR_OBS_NVAR(_3);
			zephir_read_property(&_3, errorHandler, SL("exception"), PH_NOISY_CC);
			_4 = Z_TYPE_P(_3) == IS_NULL;
		}
		_5 = _4;
		if (_5) {
			ZEPHIR_CALL_METHOD(&_0, request, "validatecsrftoken",  NULL);
			zephir_check_call_status();
			_5 = !zephir_is_true(_0);
		}
		if (_5) {
			ZEPHIR_INIT_VAR(_6);
			object_init_ex(_6, yii_web_badrequesthttpexception_ce);
			ZEPHIR_INIT_VAR(_9);
			ZVAL_STRING(_9, "yii", 0);
			ZEPHIR_INIT_VAR(_10);
			ZVAL_STRING(_10, "Unable to verify your data submission.", 0);
			ZEPHIR_CALL_CE_STATIC(&_7, yii_baseyii_ce, "t", &_8, _9, _10);
			zephir_check_temp_parameter(_9);
			zephir_check_temp_parameter(_10);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(NULL, _6, "__construct", NULL, _7);
			zephir_check_call_status();
			zephir_throw_exception_debug(_6, "yii/web/Controller.zep", 126 TSRMLS_CC);
			ZEPHIR_MM_RESTORE();
			return;
		}
		RETURN_MM_BOOL(1);
	} else {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Redirects the browser to the specified URL.
 * This method is a shortcut to [[Response::redirect()]].
 *
 * You can use it in an action by returning the [[Response]] directly:
 *
 * ```php
 * // stop executing this action and redirect to login page
 * return $this->redirect(['login']);
 * ```
 *
 * @param string|array $url the URL to be redirected to. This can be in one of the following formats:
 *
 * - a string representing a URL (e.g. "http://example.com")
 * - a string representing a URL alias (e.g. "@example.com")
 * - an array in the format of `[$route, ...name-value pairs...]` (e.g. `['site/index', 'ref' => 1]`)
 *   [[Url::to()]] will be used to convert the array into a URL.
 *
 * Any relative URL will be converted into an absolute one by prepending it with the host info
 * of the current request.
 *
 * @param integer $statusCode the HTTP status code. Defaults to 302.
 * See <http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html>
 * for details about HTTP status code
 * @return Response the current response object
 */
PHP_METHOD(yii_web_Controller, redirect) {

	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *url, *statusCode = NULL, *_0, *_1 = NULL, *_2 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &url, &statusCode);

	if (!statusCode) {
		ZEPHIR_INIT_VAR(statusCode);
		ZVAL_LONG(statusCode, 302);
	}


	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CALL_METHOD(&_1, _0, "getresponse",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_CE_STATIC(&_2, yii_helpers_url_ce, "to", &_3, url);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(_1, "redirect", NULL, _2, statusCode);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Redirects the browser to the home page.
 *
 * You can use this method in an action by returning the [[Response]] directly:
 *
 * ```php
 * // stop executing this action and redirect to home page
 * return $this->goHome();
 * ```
 *
 * @return Response the current response object
 */
PHP_METHOD(yii_web_Controller, goHome) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *_0, *_1 = NULL, *_2, *_3 = NULL;

	ZEPHIR_MM_GROW();

	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CALL_METHOD(&_1, _0, "getresponse",  NULL);
	zephir_check_call_status();
	zephir_read_static_property_ce(&_2, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CALL_METHOD(&_3, _2, "gethomeurl",  NULL);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(_1, "redirect", NULL, _3);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Redirects the browser to the last visited page.
 *
 * You can use this method in an action by returning the [[Response]] directly:
 *
 * ```php
 * // stop executing this action and redirect to last visited page
 * return $this->goBack();
 * ```
 *
 * @param string|array $defaultUrl the default return URL in case it was not set previously.
 * If this is null and the return URL was not set previously, [[Application::homeUrl]] will be redirected to.
 * Please refer to [[User::setReturnUrl()]] on accepted format of the URL.
 * @return Response the current response object
 * @see User::getReturnUrl()
 */
PHP_METHOD(yii_web_Controller, goBack) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *defaultUrl = NULL, *_0, *_1 = NULL, *_2, *_3 = NULL, *_4 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &defaultUrl);

	if (!defaultUrl) {
		defaultUrl = ZEPHIR_GLOBAL(global_null);
	}


	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CALL_METHOD(&_1, _0, "getresponse",  NULL);
	zephir_check_call_status();
	zephir_read_static_property_ce(&_2, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CALL_METHOD(&_3, _2, "getuser",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_4, _3, "getreturnurl", NULL, defaultUrl);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(_1, "redirect", NULL, _4);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Refreshes the current page.
 * This method is a shortcut to [[Response::refresh()]].
 *
 * You can use it in an action by returning the [[Response]] directly:
 *
 * ```php
 * // stop executing this action and refresh the current page
 * return $this->refresh();
 * ```
 *
 * @param string $anchor the anchor that should be appended to the redirection URL.
 * Defaults to empty. Make sure the anchor starts with '#' if you want to specify it.
 * @return Response the response object itself
 */
PHP_METHOD(yii_web_Controller, refresh) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *anchor = NULL, *_0, *_1 = NULL, *_2, *_3 = NULL, *_4 = NULL, *_5;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &anchor);

	if (!anchor) {
		ZEPHIR_INIT_VAR(anchor);
		ZVAL_STRING(anchor, "", 1);
	}


	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CALL_METHOD(&_1, _0, "getresponse",  NULL);
	zephir_check_call_status();
	zephir_read_static_property_ce(&_2, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CALL_METHOD(&_3, _2, "getrequest",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_4, _3, "geturl",  NULL);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_5);
	ZEPHIR_CONCAT_VV(_5, _4, anchor);
	ZEPHIR_RETURN_CALL_METHOD(_1, "redirect", NULL, _5);
	zephir_check_call_status();
	RETURN_MM();

}

