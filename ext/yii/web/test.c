
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(yii_web_Test) {

	ZEPHIR_REGISTER_CLASS(yii\\web, Test, yii, web_test, yii_web_test_method_entry, 0);

	zend_declare_property_null(yii_web_test_ce, SL("a"), ZEND_ACC_PUBLIC TSRMLS_CC);

	zend_declare_property_null(yii_web_test_ce, SL("c"), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	return SUCCESS;

}

PHP_METHOD(yii_web_Test, makeA) {

	zval *a = NULL, *_0, *_1;

	ZEPHIR_MM_GROW();

	_0 = zephir_fetch_nproperty_this(this_ptr, SL("a"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(a, _0);
	if (Z_TYPE_P(a) == IS_NULL) {
		ZEPHIR_INIT_VAR(a);
		array_init(a);
	}
	ZEPHIR_INIT_VAR(_1);
	ZVAL_STRING(_1, "test", 1);
	zephir_array_update_string(&a, SL("test"), &_1, PH_COPY | PH_SEPARATE);
	zephir_update_property_this(this_ptr, SL("a"), a TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

