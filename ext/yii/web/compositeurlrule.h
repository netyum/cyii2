
extern zend_class_entry *yii_web_compositeurlrule_ce;

ZEPHIR_INIT_CLASS(yii_web_CompositeUrlRule);

PHP_METHOD(yii_web_CompositeUrlRule, createRules);
PHP_METHOD(yii_web_CompositeUrlRule, init);
PHP_METHOD(yii_web_CompositeUrlRule, parseRequest);
PHP_METHOD(yii_web_CompositeUrlRule, createUrl);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_compositeurlrule_parserequest, 0, 0, 2)
	ZEND_ARG_INFO(0, manager)
	ZEND_ARG_INFO(0, request)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_compositeurlrule_createurl, 0, 0, 3)
	ZEND_ARG_INFO(0, manager)
	ZEND_ARG_INFO(0, route)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_web_compositeurlrule_method_entry) {
	PHP_ME(yii_web_CompositeUrlRule, createRules, NULL, ZEND_ACC_ABSTRACT|ZEND_ACC_PROTECTED)
	PHP_ME(yii_web_CompositeUrlRule, init, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_CompositeUrlRule, parseRequest, arginfo_yii_web_compositeurlrule_parserequest, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_CompositeUrlRule, createUrl, arginfo_yii_web_compositeurlrule_createurl, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
