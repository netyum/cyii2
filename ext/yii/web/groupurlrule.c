
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/string.h"
#include "kernel/fcall.h"
#include "kernel/hash.h"
#include "kernel/concat.h"
#include "kernel/array.h"
#include "kernel/exception.h"
#include "kernel/operators.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * GroupUrlRule represents a collection of URL rules sharing the same prefix in their patterns and routes.
 *
 * GroupUrlRule is best used by a module which often uses module ID as the prefix for the URL rules.
 * For example, the following code creates a rule for the `admin` module:
 *
 * ```php
 * new GroupUrlRule([
 *     'prefix' => 'admin',
 *     'rules' => [
 *         'login' => 'user/login',
 *         'logout' => 'user/logout',
 *         'dashboard' => 'default/dashboard',
 *     ],
 * ]);
 *
 * // the above rule is equivalent to the following three rules:
 *
 * [
 *     'admin/login' => 'admin/user/login',
 *     'admin/logout' => 'admin/user/logout',
 *     'admin/dashboard' => 'admin/default/dashboard',
 * ]
 * ```
 *
 * The above example assumes the prefix for patterns and routes are the same. They can be made different
 * by configuring [[prefix]] and [[routePrefix]] separately.
 *
 * Using a GroupUrlRule is more efficient than directly declaring the individual rules it contains.
 * This is because GroupUrlRule can quickly determine if it should process a URL parsing or creation request
 * by simply checking if the prefix matches.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_web_GroupUrlRule) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\web, GroupUrlRule, yii, web_groupurlrule, yii_web_compositeurlrule_ce, yii_web_groupurlrule_method_entry, 0);

	/**
	 * @var array the rules contained within this composite rule. Please refer to [[UrlManager::rules]]
	 * for the format of this property.
	 * @see prefix
	 * @see routePrefix
	 */
	zend_declare_property_null(yii_web_groupurlrule_ce, SL("rules"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the prefix for the pattern part of every rule declared in [[rules]].
	 * The prefix and the pattern will be separated with a slash.
	 */
	zend_declare_property_null(yii_web_groupurlrule_ce, SL("prefix"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the prefix for the route part of every rule declared in [[rules]].
	 * The prefix and the route will be separated with a slash.
	 * If this property is not set, it will take the value of [[prefix]].
	 */
	zend_declare_property_null(yii_web_groupurlrule_ce, SL("routePrefix"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array the default configuration of URL rules. Individual rule configurations
	 * specified via [[rules]] will take precedence when the same property of the rule is configured.
	 */
	zend_declare_property_null(yii_web_groupurlrule_ce, SL("ruleConfig"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_web_GroupUrlRule, init) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_5 = NULL;
	zval *_0, *_1, *slash, *_2, *_3 = NULL, *_4;

	ZEPHIR_MM_GROW();
	ZEPHIR_INIT_VAR(slash);
	ZVAL_STRING(slash, "/", 1);

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("routePrefix"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("prefix"), PH_NOISY_CC);
		zephir_update_property_this(this_ptr, SL("routePrefix"), _1 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(_2);
	zephir_read_property_this(&_2, this_ptr, SL("prefix"), PH_NOISY_CC);
	if (Z_TYPE_P(_2) == IS_STRING) {
		ZEPHIR_INIT_VAR(_3);
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("prefix"), PH_NOISY_CC);
		zephir_fast_trim(_3, _1, slash, ZEPHIR_TRIM_BOTH TSRMLS_CC);
		zephir_update_property_this(this_ptr, SL("prefix"), _3 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(_4);
	zephir_read_property_this(&_4, this_ptr, SL("routePrefix"), PH_NOISY_CC);
	if (Z_TYPE_P(_4) == IS_STRING) {
		ZEPHIR_INIT_NVAR(_3);
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("routePrefix"), PH_NOISY_CC);
		zephir_fast_trim(_3, _1, slash, ZEPHIR_TRIM_BOTH TSRMLS_CC);
		zephir_update_property_this(this_ptr, SL("routePrefix"), _3 TSRMLS_CC);
	}
	ZEPHIR_CALL_PARENT(NULL, yii_web_groupurlrule_ce, this_ptr, "init", &_5);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_web_GroupUrlRule, createRules) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_fcall_cache_entry *_12 = NULL;
	zend_bool _9;
	HashTable *_1;
	HashPosition _0;
	zval *rules, *this_rules, *key = NULL, *rule = NULL, *temp_rule = NULL, *slash, **_2, *_3 = NULL, *_4, *_5 = NULL, *_6 = NULL, *_7, *_8 = NULL, *_10, *_11, *merge_array = NULL;

	ZEPHIR_MM_GROW();
	ZEPHIR_INIT_VAR(rules);
	array_init(rules);

	ZEPHIR_INIT_VAR(slash);
	ZVAL_STRING(slash, "/", 1);
	ZEPHIR_OBS_VAR(this_rules);
	zephir_read_property_this(&this_rules, this_ptr, SL("rules"), PH_NOISY_CC);
	zephir_is_iterable(this_rules, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(key, _1, _0);
		ZEPHIR_GET_HVALUE(temp_rule, _2);
		ZEPHIR_CPY_WRT(rule, temp_rule);
		if (Z_TYPE_P(temp_rule) != IS_ARRAY) {
			ZEPHIR_INIT_NVAR(rule);
			array_init(rule);
			ZEPHIR_INIT_NVAR(_3);
			_4 = zephir_fetch_nproperty_this(this_ptr, SL("prefix"), PH_NOISY_CC);
			ZEPHIR_INIT_LNVAR(_5);
			ZEPHIR_CONCAT_VVV(_5, _4, slash, key);
			zephir_fast_trim(_3, _5, slash, ZEPHIR_TRIM_LEFT TSRMLS_CC);
			zephir_array_update_string(&rule, SL("pattern"), &_3, PH_COPY | PH_SEPARATE);
			ZEPHIR_INIT_NVAR(_6);
			_7 = zephir_fetch_nproperty_this(this_ptr, SL("routePrefix"), PH_NOISY_CC);
			ZEPHIR_INIT_LNVAR(_8);
			ZEPHIR_CONCAT_VVV(_8, _7, slash, temp_rule);
			zephir_fast_trim(_6, _8, slash, ZEPHIR_TRIM_LEFT TSRMLS_CC);
			zephir_array_update_string(&rule, SL("route"), &_6, PH_COPY | PH_SEPARATE);
		} else {
			_9 = zephir_array_isset_string(rule, SS("pattern"));
			if (_9) {
				_9 = zephir_array_isset_string(rule, SS("route"));
			}
			if (_9) {
				ZEPHIR_INIT_NVAR(_3);
				_4 = zephir_fetch_nproperty_this(this_ptr, SL("prefix"), PH_NOISY_CC);
				zephir_array_fetch_string(&_10, rule, SL("pattern"), PH_NOISY | PH_READONLY TSRMLS_CC);
				ZEPHIR_INIT_LNVAR(_5);
				ZEPHIR_CONCAT_VVV(_5, _4, slash, _10);
				zephir_fast_trim(_3, _5, slash, ZEPHIR_TRIM_LEFT TSRMLS_CC);
				zephir_array_update_string(&rule, SL("pattern"), &_3, PH_COPY | PH_SEPARATE);
				ZEPHIR_INIT_NVAR(_6);
				_7 = zephir_fetch_nproperty_this(this_ptr, SL("routePrefix"), PH_NOISY_CC);
				zephir_array_fetch_string(&_11, rule, SL("route"), PH_NOISY | PH_READONLY TSRMLS_CC);
				ZEPHIR_INIT_LNVAR(_8);
				ZEPHIR_CONCAT_VVV(_8, _7, slash, _11);
				zephir_fast_trim(_6, _8, slash, ZEPHIR_TRIM_LEFT TSRMLS_CC);
				zephir_array_update_string(&rule, SL("route"), &_6, PH_COPY | PH_SEPARATE);
			}
		}
		ZEPHIR_INIT_NVAR(merge_array);
		_4 = zephir_fetch_nproperty_this(this_ptr, SL("ruleConfig"), PH_NOISY_CC);
		zephir_fast_array_merge(merge_array, &(_4), &(rule) TSRMLS_CC);
		ZEPHIR_CALL_CE_STATIC(&rule, yii_baseyii_ce, "createobject", &_12, merge_array);
		zephir_check_call_status();
		if (!(zephir_instance_of_ev(rule, yii_web_urlruleinterface_ce TSRMLS_CC))) {
			ZEPHIR_THROW_EXCEPTION_DEBUG_STR(yii_base_invalidconfigexception_ce, "URL rule class must implement UrlRuleInterface.", "yii/web/GroupUrlRule.zep", 119);
			return;
		}
		zephir_array_append(&rules, rule, PH_SEPARATE);
	}
	RETURN_CCTOR(rules);

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_web_GroupUrlRule, parseRequest) {

	zephir_nts_static zephir_fcall_cache_entry *_7 = NULL;
	zend_bool _5, _6;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *manager, *request, *pathInfo = NULL, *slash, *pos, *_0, *_1, *_2, *_3, *_4;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &manager, &request);



	ZEPHIR_INIT_VAR(slash);
	ZVAL_STRING(slash, "/", 1);
	ZEPHIR_CALL_METHOD(&pathInfo, request, "getpathinfo",  NULL);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("prefix"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_STRING) {
		ZEPHIR_INIT_VAR(_1);
		ZEPHIR_CONCAT_VV(_1, pathInfo, slash);
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("prefix"), PH_NOISY_CC);
		ZEPHIR_INIT_VAR(_3);
		ZEPHIR_CONCAT_VV(_3, _2, slash);
		ZEPHIR_INIT_VAR(pos);
		zephir_fast_strpos(pos, _1, _3, 0 );
		_4 = zephir_fetch_nproperty_this(this_ptr, SL("prefix"), PH_NOISY_CC);
		_5 = ZEPHIR_IS_STRING(_4, "");
		if (!(_5)) {
			_6 = Z_TYPE_P(pos) != IS_BOOL;
			if (_6) {
				_6 = ZEPHIR_IS_LONG(pos, 0);
			}
			_5 = _6;
		}
		if (_5) {
			ZEPHIR_RETURN_CALL_PARENT(yii_web_groupurlrule_ce, this_ptr, "parserequest", &_7, manager, request);
			zephir_check_call_status();
			RETURN_MM();
		}
	}
	RETURN_MM_BOOL(0);

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_web_GroupUrlRule, createUrl) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_6 = NULL;
	zend_bool _4, _5;
	zval *manager, *route, *params, *pos, *_0, *_1, *_2, *_3;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 3, 0, &manager, &route, &params);



	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("prefix"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_STRING) {
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("routePrefix"), PH_NOISY_CC);
		ZEPHIR_INIT_VAR(_2);
		ZEPHIR_CONCAT_VS(_2, _1, "/");
		ZEPHIR_INIT_VAR(pos);
		zephir_fast_strpos(pos, route, _2, 0 );
		_3 = zephir_fetch_nproperty_this(this_ptr, SL("routePrefix"), PH_NOISY_CC);
		_4 = ZEPHIR_IS_STRING(_3, "");
		if (!(_4)) {
			_5 = Z_TYPE_P(pos) != IS_BOOL;
			if (_5) {
				_5 = ZEPHIR_IS_LONG(pos, 0);
			}
			_4 = _5;
		}
		if (_4) {
			ZEPHIR_RETURN_CALL_PARENT(yii_web_groupurlrule_ce, this_ptr, "createurl", &_6, manager, route, params);
			zephir_check_call_status();
			RETURN_MM();
		}
	}
	RETURN_MM_BOOL(0);

}

