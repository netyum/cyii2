
extern zend_class_entry *yii_web_badrequesthttpexception_ce;

ZEPHIR_INIT_CLASS(yii_web_BadRequestHttpException);

PHP_METHOD(yii_web_BadRequestHttpException, __construct);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_badrequesthttpexception___construct, 0, 0, 0)
	ZEND_ARG_INFO(0, message)
	ZEND_ARG_INFO(0, code)
	ZEND_ARG_INFO(0, previous)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_web_badrequesthttpexception_method_entry) {
	PHP_ME(yii_web_BadRequestHttpException, __construct, arginfo_yii_web_badrequesthttpexception___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
  PHP_FE_END
};
