
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/hash.h"
#include "kernel/concat.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * CompositeUrlRule is the base class for URL rule classes that consist of multiple simpler rules.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_web_CompositeUrlRule) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\web, CompositeUrlRule, yii, web_compositeurlrule, yii_base_object_ce, yii_web_compositeurlrule_method_entry, ZEND_ACC_EXPLICIT_ABSTRACT_CLASS);

	/**
	 * @var UrlRuleInterface[] the URL rules contained in this composite rule.
	 * This property is set in [[init()]] by the return value of [[createRules()]].
	 */
	zend_declare_property_null(yii_web_compositeurlrule_ce, SL("rules"), ZEND_ACC_PROTECTED TSRMLS_CC);

	zend_class_implements(yii_web_compositeurlrule_ce TSRMLS_CC, 1, yii_web_urlruleinterface_ce);
	return SUCCESS;

}

/**
 * Creates the URL rules that should be contained within this composite rule.
 * @return UrlRuleInterface[] the URL rules
 */
PHP_METHOD(yii_web_CompositeUrlRule, createRules) {

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_web_CompositeUrlRule, init) {

	zval *rules = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_PARENT(NULL, yii_web_compositeurlrule_ce, this_ptr, "init", &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&rules, this_ptr, "createrules",  NULL);
	zephir_check_call_status();
	zephir_update_property_this(this_ptr, SL("rules"), rules TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_web_CompositeUrlRule, parseRequest) {

	zephir_fcall_cache_entry *_4 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	HashTable *_2;
	HashPosition _1;
	zval *manager, *request, *rules = NULL, *rule = NULL, *result = NULL, *_0, **_3, *_5 = NULL, *_6 = NULL, *_7 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &manager, &request);



	_0 = zephir_fetch_nproperty_this(this_ptr, SL("rules"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(rules, _0);
	if (Z_TYPE_P(rules) == IS_ARRAY) {
		zephir_is_iterable(rules, &_2, &_1, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_2, (void**) &_3, &_1) == SUCCESS
		  ; zephir_hash_move_forward_ex(_2, &_1)
		) {
			ZEPHIR_GET_HVALUE(rule, _3);
			ZEPHIR_CALL_METHOD(&result, rule, "parserequest", NULL, manager, request);
			zephir_check_call_status();
			if (Z_TYPE_P(result) == IS_BOOL) {
				ZEPHIR_OBS_NVAR(_5);
				zephir_read_property(&_5, rule, SL("name"), PH_NOISY_CC);
				ZEPHIR_INIT_LNVAR(_6);
				ZEPHIR_CONCAT_SV(_6, "Request parsed with URL rule: ", _5);
				ZEPHIR_INIT_NVAR(_7);
				ZVAL_STRING(_7, "CompositeUrlRule:parseRequest", 0);
				ZEPHIR_CALL_CE_STATIC(NULL, yii_baseyii_ce, "trace", &_4, _6, _7);
				zephir_check_temp_parameter(_7);
				zephir_check_call_status();
				RETURN_CCTOR(result);
			}
		}
	}
	RETURN_MM_BOOL(0);

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_web_CompositeUrlRule, createUrl) {

	int ZEPHIR_LAST_CALL_STATUS;
	HashTable *_2;
	HashPosition _1;
	zval *manager, *route, *params, *rules = NULL, *rule = NULL, *url = NULL, *_0, **_3;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 3, 0, &manager, &route, &params);



	_0 = zephir_fetch_nproperty_this(this_ptr, SL("rules"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(rules, _0);
	zephir_is_iterable(rules, &_2, &_1, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_2, (void**) &_3, &_1) == SUCCESS
	  ; zephir_hash_move_forward_ex(_2, &_1)
	) {
		ZEPHIR_GET_HVALUE(rule, _3);
		ZEPHIR_CALL_METHOD(&url, rule, "createurl", NULL, manager, route, params);
		zephir_check_call_status();
		if (Z_TYPE_P(url) != IS_BOOL) {
			RETURN_CCTOR(url);
		}
	}
	RETURN_MM_BOOL(0);

}

