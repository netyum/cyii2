
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/string.h"
#include "kernel/hash.h"
#include "kernel/operators.h"
#include "kernel/concat.h"
#include "kernel/array.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * AssetBundle represents a collection of asset files, such as CSS, JS, images.
 *
 * Each asset bundle has a unique name that globally identifies it among all asset bundles used in an application.
 * The name is the [fully qualified class name](http://php.net/manual/en/language.namespaces.rules.php)
 * of the class representing it.
 *
 * An asset bundle can depend on other asset bundles. When registering an asset bundle
 * with a view, all its dependent asset bundles will be automatically registered.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_web_AssetBundle) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\web, AssetBundle, yii, web_assetbundle, yii_base_object_ce, yii_web_assetbundle_method_entry, 0);

	/**
	 * @var string the root directory of the source asset files. A source asset file
	 * is a file that is part of your source code repository of your Web application.
	 *
	 * You must set this property if the directory containing the source asset files
	 * is not Web accessible (this is usually the case for extensions).
	 *
	 * By setting this property, the asset manager will publish the source asset files
	 * to a Web-accessible directory [[basePath]].
	 *
	 * You can use either a directory or an alias of the directory.
	 */
	zend_declare_property_null(yii_web_assetbundle_ce, SL("sourcePath"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the Web-accessible directory that contains the asset files in this bundle.
	 *
	 * If [[sourcePath]] is set, this property will be *overwritten* by [[AssetManager]]
	 * when it publishes the asset files from [[sourcePath]].
	 *
	 * If the bundle contains any assets that are specified in terms of relative file path,
	 * then this property must be set either manually or automatically (by [[AssetManager]] via
	 * asset publishing).
	 *
	 * You can use either a directory or an alias of the directory.
	 */
	zend_declare_property_null(yii_web_assetbundle_ce, SL("basePath"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the base URL that will be prefixed to the asset files for them to
	 * be accessed via Web server.
	 *
	 * If [[sourcePath]] is set, this property will be *overwritten* by [[AssetManager]]
	 * when it publishes the asset files from [[sourcePath]].
	 *
	 * If the bundle contains any assets that are specified in terms of relative file path,
	 * then this property must be set either manually or automatically (by asset manager via
	 * asset publishing).
	 *
	 * You can use either a URL or an alias of the URL.
	 */
	zend_declare_property_null(yii_web_assetbundle_ce, SL("baseUrl"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array list of bundle class names that this bundle depends on.
	 *
	 * For example:
	 *
	 * ```php
	 * public $depends = [
	 *    'yii\web\YiiAsset',
	 *    'yii\bootstrap\BootstrapAsset',
	 * ];
	 * ```
	 */
	zend_declare_property_null(yii_web_assetbundle_ce, SL("depends"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array list of JavaScript files that this bundle contains. Each JavaScript file can
	 * be either a file path (without leading slash) relative to [[basePath]] or a URL representing
	 * an external JavaScript file.
	 *
	 * Note that only forward slash "/" can be used as directory separator.
	 */
	zend_declare_property_null(yii_web_assetbundle_ce, SL("js"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array list of CSS files that this bundle contains. Each CSS file can
	 * be either a file path (without leading slash) relative to [[basePath]] or a URL representing
	 * an external CSS file.
	 *
	 * Note that only forward slash "/" can be used as directory separator.
	 */
	zend_declare_property_null(yii_web_assetbundle_ce, SL("css"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array the options that will be passed to [[View::registerJsFile()]]
	 * when registering the JS files in this bundle.
	 */
	zend_declare_property_null(yii_web_assetbundle_ce, SL("jsOptions"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array the options that will be passed to [[View::registerCssFile()]]
	 * when registering the CSS files in this bundle.
	 */
	zend_declare_property_null(yii_web_assetbundle_ce, SL("cssOptions"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array the options to be passed to [[AssetManager::publish()]] when the asset bundle
	 * is being published.
	 */
	zend_declare_property_null(yii_web_assetbundle_ce, SL("publishOptions"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * @param View $view
 * @return static the registered asset bundle instance
 */
PHP_METHOD(yii_web_AssetBundle, register) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *view, *_0;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &view);



	ZEPHIR_INIT_VAR(_0);
	zephir_get_called_class(_0 TSRMLS_CC);
	ZEPHIR_RETURN_CALL_METHOD(view, "registerassetbundle", NULL, _0);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Initializes the bundle.
 * If you override this method, make sure you call the parent implementation in the last.
 */
PHP_METHOD(yii_web_AssetBundle, init) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_4 = NULL;
	zval *_0, *_1 = NULL, *_2, *_3 = NULL, *_5, _6 = zval_used_for_init, *_7, *_8;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("depends"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		ZEPHIR_INIT_VAR(_1);
		array_init(_1);
		zephir_update_property_this(this_ptr, SL("depends"), _1 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(_2);
	zephir_read_property_this(&_2, this_ptr, SL("sourcePath"), PH_NOISY_CC);
	if (Z_TYPE_P(_2) != IS_NULL) {
		ZEPHIR_INIT_NVAR(_1);
		_5 = zephir_fetch_nproperty_this(this_ptr, SL("sourcePath"), PH_NOISY_CC);
		ZEPHIR_CALL_CE_STATIC(&_3, yii_baseyii_ce, "getalias", &_4, _5);
		zephir_check_call_status();
		ZEPHIR_SINIT_VAR(_6);
		ZVAL_STRING(&_6, "/\\", 0);
		zephir_fast_trim(_1, _3, &_6, ZEPHIR_TRIM_RIGHT TSRMLS_CC);
		zephir_update_property_this(this_ptr, SL("sourcePath"), _1 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(_7);
	zephir_read_property_this(&_7, this_ptr, SL("basePath"), PH_NOISY_CC);
	if (Z_TYPE_P(_7) != IS_NULL) {
		ZEPHIR_INIT_NVAR(_1);
		_5 = zephir_fetch_nproperty_this(this_ptr, SL("basePath"), PH_NOISY_CC);
		ZEPHIR_CALL_CE_STATIC(&_3, yii_baseyii_ce, "getalias", &_4, _5);
		zephir_check_call_status();
		ZEPHIR_SINIT_NVAR(_6);
		ZVAL_STRING(&_6, "/\\", 0);
		zephir_fast_trim(_1, _3, &_6, ZEPHIR_TRIM_RIGHT TSRMLS_CC);
		zephir_update_property_this(this_ptr, SL("basePath"), _1 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(_8);
	zephir_read_property_this(&_8, this_ptr, SL("baseUrl"), PH_NOISY_CC);
	if (Z_TYPE_P(_8) != IS_NULL) {
		ZEPHIR_INIT_NVAR(_1);
		_5 = zephir_fetch_nproperty_this(this_ptr, SL("baseUrl"), PH_NOISY_CC);
		ZEPHIR_CALL_CE_STATIC(&_3, yii_baseyii_ce, "getalias", &_4, _5);
		zephir_check_call_status();
		ZEPHIR_SINIT_NVAR(_6);
		ZVAL_STRING(&_6, "/", 0);
		zephir_fast_trim(_1, _3, &_6, ZEPHIR_TRIM_RIGHT TSRMLS_CC);
		zephir_update_property_this(this_ptr, SL("baseUrl"), _1 TSRMLS_CC);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Registers the CSS and JS files with the given view.
 * @param \yii\web\View $view the view that the asset files are to be registered with.
 */
PHP_METHOD(yii_web_AssetBundle, registerAssetFiles) {

	zend_bool _10, _11;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_9 = NULL;
	HashTable *_5, *_18;
	HashPosition _4, _17;
	zval *view, *js = NULL, *css = NULL, *pos = NULL, *first_char = NULL, *js_path = NULL, *css_path = NULL, *_0, *_1 = NULL, *_2, *_3, **_6, _7 = zval_used_for_init, _8 = zval_used_for_init, *_12, *_13, *_14 = NULL, *_15, *_16, **_19, *_20;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &view);



	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("js"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		ZEPHIR_INIT_VAR(_1);
		array_init(_1);
		zephir_update_property_this(this_ptr, SL("js"), _1 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(_2);
	zephir_read_property_this(&_2, this_ptr, SL("jsOptions"), PH_NOISY_CC);
	if (Z_TYPE_P(_2) == IS_NULL) {
		ZEPHIR_INIT_NVAR(_1);
		array_init(_1);
		zephir_update_property_this(this_ptr, SL("jsOptions"), _1 TSRMLS_CC);
	}
	_3 = zephir_fetch_nproperty_this(this_ptr, SL("js"), PH_NOISY_CC);
	zephir_is_iterable(_3, &_5, &_4, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_5, (void**) &_6, &_4) == SUCCESS
	  ; zephir_hash_move_forward_ex(_5, &_4)
	) {
		ZEPHIR_GET_HVALUE(js, _6);
		ZEPHIR_SINIT_NVAR(_7);
		ZVAL_LONG(&_7, 0);
		ZEPHIR_SINIT_NVAR(_8);
		ZVAL_LONG(&_8, 1);
		ZEPHIR_CALL_FUNCTION(&first_char, "substr", &_9, js, &_7, &_8);
		zephir_check_call_status();
		ZEPHIR_SINIT_NVAR(_7);
		ZVAL_STRING(&_7, "://", 0);
		ZEPHIR_INIT_NVAR(pos);
		zephir_fast_strpos(pos, js, &_7, 0 );
		_10 = !ZEPHIR_IS_STRING(first_char, "/");
		if (_10) {
			_10 = !ZEPHIR_IS_STRING(first_char, ".");
		}
		_11 = _10;
		if (_11) {
			_11 = Z_TYPE_P(pos) == IS_BOOL;
		}
		if (_11) {
			_12 = zephir_fetch_nproperty_this(this_ptr, SL("baseUrl"), PH_NOISY_CC);
			ZEPHIR_INIT_NVAR(js_path);
			ZEPHIR_CONCAT_VSV(js_path, _12, "/", js);
			ZEPHIR_INIT_NVAR(_1);
			array_init(_1);
			_13 = zephir_fetch_nproperty_this(this_ptr, SL("jsOptions"), PH_NOISY_CC);
			ZEPHIR_CALL_METHOD(NULL, view, "registerjsfile", NULL, js_path, _1, _13);
			zephir_check_call_status();
		} else {
			ZEPHIR_INIT_NVAR(_14);
			array_init(_14);
			_12 = zephir_fetch_nproperty_this(this_ptr, SL("jsOptions"), PH_NOISY_CC);
			ZEPHIR_CALL_METHOD(NULL, view, "registerjsfile", NULL, js, _14, _12);
			zephir_check_call_status();
		}
	}
	ZEPHIR_OBS_VAR(_15);
	zephir_read_property_this(&_15, this_ptr, SL("css"), PH_NOISY_CC);
	if (Z_TYPE_P(_15) == IS_NULL) {
		ZEPHIR_INIT_NVAR(_1);
		array_init(_1);
		zephir_update_property_this(this_ptr, SL("css"), _1 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(_16);
	zephir_read_property_this(&_16, this_ptr, SL("cssOptions"), PH_NOISY_CC);
	if (Z_TYPE_P(_16) == IS_NULL) {
		ZEPHIR_INIT_NVAR(_1);
		array_init(_1);
		zephir_update_property_this(this_ptr, SL("cssOptions"), _1 TSRMLS_CC);
	}
	_12 = zephir_fetch_nproperty_this(this_ptr, SL("css"), PH_NOISY_CC);
	zephir_is_iterable(_12, &_18, &_17, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_18, (void**) &_19, &_17) == SUCCESS
	  ; zephir_hash_move_forward_ex(_18, &_17)
	) {
		ZEPHIR_GET_HVALUE(css, _19);
		ZEPHIR_SINIT_NVAR(_7);
		ZVAL_LONG(&_7, 0);
		ZEPHIR_SINIT_NVAR(_8);
		ZVAL_LONG(&_8, 1);
		ZEPHIR_CALL_FUNCTION(&first_char, "substr", &_9, css, &_7, &_8);
		zephir_check_call_status();
		ZEPHIR_SINIT_NVAR(_7);
		ZVAL_STRING(&_7, "://", 0);
		ZEPHIR_INIT_NVAR(pos);
		zephir_fast_strpos(pos, css, &_7, 0 );
		_10 = !ZEPHIR_IS_STRING(first_char, "/");
		if (_10) {
			_10 = !ZEPHIR_IS_STRING(first_char, ".");
		}
		_11 = _10;
		if (_11) {
			_11 = Z_TYPE_P(pos) == IS_BOOL;
		}
		if (_11) {
			_13 = zephir_fetch_nproperty_this(this_ptr, SL("baseUrl"), PH_NOISY_CC);
			ZEPHIR_INIT_NVAR(css_path);
			ZEPHIR_CONCAT_VSV(css_path, _13, "/", css);
			ZEPHIR_INIT_NVAR(_1);
			array_init(_1);
			_20 = zephir_fetch_nproperty_this(this_ptr, SL("cssOptions"), PH_NOISY_CC);
			ZEPHIR_CALL_METHOD(NULL, view, "registercssfile", NULL, css_path, _1, _20);
			zephir_check_call_status();
		} else {
			ZEPHIR_INIT_NVAR(_14);
			array_init(_14);
			_12 = zephir_fetch_nproperty_this(this_ptr, SL("cssOptions"), PH_NOISY_CC);
			ZEPHIR_CALL_METHOD(NULL, view, "registercssfile", NULL, css, _14, _12);
			zephir_check_call_status();
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Publishes the asset bundle if its source code is not under Web-accessible directory.
 * It will also try to convert non-CSS or JS files (e.g. LESS, Sass) into the corresponding
 * CSS or JS files using [[AssetManager::converter|asset converter]].
 * @param AssetManager $am the asset manager to perform the asset publishing
 */
PHP_METHOD(yii_web_AssetBundle, publish) {

	HashTable *_14, *_26;
	HashPosition _13, _25;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_bool _5, _7, _18, _19, _21;
	zval *am, *retval = NULL, *converter = NULL, *i = NULL, *js = NULL, *css = NULL, *_0, *_1 = NULL, *_2, *_3, *_4, *_6, *_8, *_9, *_10, *_11, *_12, *pos1 = NULL, *pos2 = NULL, **_15, _16 = zval_used_for_init, _17 = zval_used_for_init, *_20 = NULL, *_22 = NULL, *_23 = NULL, *_24 = NULL, **_27;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &am);



	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("js"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		ZEPHIR_INIT_VAR(_1);
		array_init(_1);
		zephir_update_property_this(this_ptr, SL("js"), _1 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(_2);
	zephir_read_property_this(&_2, this_ptr, SL("css"), PH_NOISY_CC);
	if (Z_TYPE_P(_2) == IS_NULL) {
		ZEPHIR_INIT_NVAR(_1);
		array_init(_1);
		zephir_update_property_this(this_ptr, SL("css"), _1 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(_3);
	zephir_read_property_this(&_3, this_ptr, SL("publishOptions"), PH_NOISY_CC);
	if (Z_TYPE_P(_3) == IS_NULL) {
		ZEPHIR_INIT_NVAR(_1);
		array_init(_1);
		zephir_update_property_this(this_ptr, SL("publishOptions"), _1 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(_4);
	zephir_read_property_this(&_4, this_ptr, SL("sourcePath"), PH_NOISY_CC);
	_5 = Z_TYPE_P(_4) != IS_NULL;
	if (_5) {
		ZEPHIR_OBS_VAR(_6);
		zephir_read_property_this(&_6, this_ptr, SL("basePath"), PH_NOISY_CC);
		_5 = Z_TYPE_P(_6) == IS_NULL;
	}
	_7 = _5;
	if (_7) {
		ZEPHIR_OBS_VAR(_8);
		zephir_read_property_this(&_8, this_ptr, SL("baseUrl"), PH_NOISY_CC);
		_7 = Z_TYPE_P(_8) == IS_NULL;
	}
	if (_7) {
		_9 = zephir_fetch_nproperty_this(this_ptr, SL("sourcePath"), PH_NOISY_CC);
		_10 = zephir_fetch_nproperty_this(this_ptr, SL("publishOptions"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(&retval, am, "publish", NULL, _9, _10);
		zephir_check_call_status();
		zephir_array_fetch_long(&_11, retval, 0, PH_NOISY | PH_READONLY TSRMLS_CC);
		zephir_update_property_this(this_ptr, SL("basePath"), _11 TSRMLS_CC);
		zephir_array_fetch_long(&_12, retval, 1, PH_NOISY | PH_READONLY TSRMLS_CC);
		zephir_update_property_this(this_ptr, SL("baseUrl"), _12 TSRMLS_CC);
	}
	ZEPHIR_CALL_METHOD(&converter, am, "getconverter",  NULL);
	zephir_check_call_status();
	_9 = zephir_fetch_nproperty_this(this_ptr, SL("js"), PH_NOISY_CC);
	zephir_is_iterable(_9, &_14, &_13, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_14, (void**) &_15, &_13) == SUCCESS
	  ; zephir_hash_move_forward_ex(_14, &_13)
	) {
		ZEPHIR_GET_HMKEY(i, _14, _13);
		ZEPHIR_GET_HVALUE(js, _15);
		ZEPHIR_SINIT_NVAR(_16);
		ZVAL_STRING(&_16, "/", 0);
		ZEPHIR_INIT_NVAR(pos1);
		zephir_fast_strpos(pos1, js, &_16, 0 );
		ZEPHIR_SINIT_NVAR(_17);
		ZVAL_STRING(&_17, "://", 0);
		ZEPHIR_INIT_NVAR(pos2);
		zephir_fast_strpos(pos2, js, &_17, 0 );
		_18 = Z_TYPE_P(pos1) == IS_BOOL;
		if (!(_18)) {
			_18 = !ZEPHIR_IS_LONG(pos1, 0);
		}
		_19 = _18;
		if (_19) {
			_19 = Z_TYPE_P(pos2) == IS_BOOL;
		}
		if (_19) {
			ZEPHIR_OBS_NVAR(_20);
			zephir_read_property_this(&_20, this_ptr, SL("basePath"), PH_NOISY_CC);
			_21 = Z_TYPE_P(_20) != IS_NULL;
			if (_21) {
				ZEPHIR_OBS_NVAR(_22);
				zephir_read_property_this(&_22, this_ptr, SL("baseUrl"), PH_NOISY_CC);
				_21 = Z_TYPE_P(_22) != IS_NULL;
			}
			if (_21) {
				_10 = zephir_fetch_nproperty_this(this_ptr, SL("basePath"), PH_NOISY_CC);
				ZEPHIR_CALL_METHOD(&_23, converter, "convert", NULL, js, _10);
				zephir_check_call_status();
				zephir_update_property_array(this_ptr, SL("js"), i, _23 TSRMLS_CC);
			} else {
				ZEPHIR_INIT_LNVAR(_24);
				ZEPHIR_CONCAT_SV(_24, "/", js);
				zephir_update_property_array(this_ptr, SL("js"), i, _24 TSRMLS_CC);
			}
		}
	}
	_9 = zephir_fetch_nproperty_this(this_ptr, SL("css"), PH_NOISY_CC);
	zephir_is_iterable(_9, &_26, &_25, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_26, (void**) &_27, &_25) == SUCCESS
	  ; zephir_hash_move_forward_ex(_26, &_25)
	) {
		ZEPHIR_GET_HMKEY(i, _26, _25);
		ZEPHIR_GET_HVALUE(css, _27);
		ZEPHIR_SINIT_NVAR(_16);
		ZVAL_STRING(&_16, "/", 0);
		ZEPHIR_INIT_NVAR(pos1);
		zephir_fast_strpos(pos1, css, &_16, 0 );
		ZEPHIR_SINIT_NVAR(_17);
		ZVAL_STRING(&_17, "://", 0);
		ZEPHIR_INIT_NVAR(pos2);
		zephir_fast_strpos(pos2, css, &_17, 0 );
		_18 = Z_TYPE_P(pos1) == IS_BOOL;
		if (!(_18)) {
			_18 = !ZEPHIR_IS_LONG(pos1, 0);
		}
		_19 = _18;
		if (_19) {
			_19 = Z_TYPE_P(pos2) == IS_BOOL;
		}
		if (_19) {
			ZEPHIR_OBS_NVAR(_20);
			zephir_read_property_this(&_20, this_ptr, SL("basePath"), PH_NOISY_CC);
			_21 = Z_TYPE_P(_20) != IS_NULL;
			if (_21) {
				ZEPHIR_OBS_NVAR(_22);
				zephir_read_property_this(&_22, this_ptr, SL("baseUrl"), PH_NOISY_CC);
				_21 = Z_TYPE_P(_22) != IS_NULL;
			}
			if (_21) {
				_10 = zephir_fetch_nproperty_this(this_ptr, SL("basePath"), PH_NOISY_CC);
				ZEPHIR_CALL_METHOD(&_23, converter, "convert", NULL, css, _10);
				zephir_check_call_status();
				zephir_update_property_array(this_ptr, SL("css"), i, _23 TSRMLS_CC);
			} else {
				ZEPHIR_INIT_LNVAR(_24);
				ZEPHIR_CONCAT_SV(_24, "/", css);
				zephir_update_property_array(this_ptr, SL("css"), i, _24 TSRMLS_CC);
			}
		}
	}
	ZEPHIR_MM_RESTORE();

}

