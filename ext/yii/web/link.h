
extern zend_class_entry *yii_web_link_ce;

ZEPHIR_INIT_CLASS(yii_web_Link);

PHP_METHOD(yii_web_Link, serialize);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_link_serialize, 0, 0, 1)
	ZEND_ARG_INFO(0, links)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_web_link_method_entry) {
	PHP_ME(yii_web_Link, serialize, arginfo_yii_web_link_serialize, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
  PHP_FE_END
};
