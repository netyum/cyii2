
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/operators.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Cookie represents information related with a cookie, such as [[name]], [[value]], [[domain]], etc.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_web_Cookie) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\web, Cookie, yii, web_cookie, yii_base_object_ce, yii_web_cookie_method_entry, 0);

	/**
	 * @var string name of the cookie
	 */
	zend_declare_property_null(yii_web_cookie_ce, SL("name"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string value of the cookie
	 */
	zend_declare_property_string(yii_web_cookie_ce, SL("value"), "", ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string domain of the cookie
	 */
	zend_declare_property_string(yii_web_cookie_ce, SL("domain"), "", ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var integer the timestamp at which the cookie expires. This is the server timestamp.
	 * Defaults to 0, meaning "until the browser is closed".
	 */
	zend_declare_property_long(yii_web_cookie_ce, SL("expire"), 0, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the path on the server in which the cookie will be available on. The default is '/'.
	 */
	zend_declare_property_string(yii_web_cookie_ce, SL("path"), "/", ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var boolean whether cookie should be sent via secure connection
	 */
	zend_declare_property_bool(yii_web_cookie_ce, SL("secure"), 0, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var boolean whether the cookie should be accessible only through the HTTP protocol.
	 * By setting this property to true, the cookie will not be accessible by scripting languages,
	 * such as JavaScript, which can effectively help to reduce identity theft through XSS attacks.
	 */
	zend_declare_property_bool(yii_web_cookie_ce, SL("httpOnly"), 0, ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Magic method to turn a cookie object into a string without having to explicitly access [[value]].
 *
 * ~~~
 * if (isset($request->cookies['name'])) {
 *     $value = (string) $request->cookies['name'];
 * }
 * ~~~
 *
 * @return string The value of the cookie. If the value property is null, an empty string will be returned.
 */
PHP_METHOD(yii_web_Cookie, __toString) {

	zval *_1 = NULL;
	zval *_0;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("value"), PH_NOISY_CC);
	zephir_get_strval(_1, _0);
	RETURN_CTOR(_1);

}

