
extern zend_class_entry *yii_web_groupurlrule_ce;

ZEPHIR_INIT_CLASS(yii_web_GroupUrlRule);

PHP_METHOD(yii_web_GroupUrlRule, init);
PHP_METHOD(yii_web_GroupUrlRule, createRules);
PHP_METHOD(yii_web_GroupUrlRule, parseRequest);
PHP_METHOD(yii_web_GroupUrlRule, createUrl);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_groupurlrule_parserequest, 0, 0, 2)
	ZEND_ARG_INFO(0, manager)
	ZEND_ARG_INFO(0, request)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_groupurlrule_createurl, 0, 0, 3)
	ZEND_ARG_INFO(0, manager)
	ZEND_ARG_INFO(0, route)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_web_groupurlrule_method_entry) {
	PHP_ME(yii_web_GroupUrlRule, init, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_GroupUrlRule, createRules, NULL, ZEND_ACC_PROTECTED)
	PHP_ME(yii_web_GroupUrlRule, parseRequest, arginfo_yii_web_groupurlrule_parserequest, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_GroupUrlRule, createUrl, arginfo_yii_web_groupurlrule_createurl, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
