
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/concat.h"
#include "kernel/array.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * ErrorAction displays application errors using a specified view.
 *
 * To use ErrorAction, you need to do the following steps:
 *
 * First, declare an action of ErrorAction type in the `actions()` method of your `SiteController`
 * class (or whatever controller you prefer), like the following:
 *
 * ```php
 * public function actions()
 * {
 *     return [
 *         'error' => ['class' => 'yii\web\ErrorAction'],
 *     ];
 * }
 * ```
 *
 * Then, create a view file for this action. If the route of your error action is `site/error`, then
 * the view file should be `views/site/error.php`. In this view file, the following variables are available:
 *
 * - `$name`: the error name
 * - `$message`: the error message
 * - `$exception`: the exception being handled
 *
 * Finally, configure the "errorHandler" application component as follows,
 *
 * ```php
 * 'errorHandler' => [
 *     'errorAction' => 'site/error',
 * ]
 * ```
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_web_ErrorAction) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\web, ErrorAction, yii, web_erroraction, yii_base_action_ce, yii_web_erroraction_method_entry, 0);

	/**
	 * @var string the view file to be rendered. If not set, it will take the value of [[id]].
	 * That means, if you name the action as "error" in "SiteController", then the view name
	 * would be "error", and the corresponding view file would be "views/site/error.php".
	 */
	zend_declare_property_null(yii_web_erroraction_ce, SL("view"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the name of the error when the exception name cannot be determined.
	 * Defaults to "Error".
	 */
	zend_declare_property_null(yii_web_erroraction_ce, SL("defaultName"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the message to be displayed when the exception message contains sensitive information.
	 * Defaults to "An internal server error occurred.".
	 */
	zend_declare_property_null(yii_web_erroraction_ce, SL("defaultMessage"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

PHP_METHOD(yii_web_ErrorAction, run) {

	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *exception = NULL, *app = NULL, *code = NULL, *name = NULL, *message = NULL, *_0, *_1, *_2 = NULL, *_4 = NULL, *_5 = NULL, *_6, *request = NULL, *is_ajax = NULL, *elements, *retval = NULL, *_7, *_8;

	ZEPHIR_MM_GROW();
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);

	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _0);
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property(&_1, app, SL("errorHandler"), PH_NOISY_CC);
	ZEPHIR_OBS_VAR(_2);
	zephir_read_property(&_2, _1, SL("exception"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(exception, _2);
	if (Z_TYPE_P(exception) == IS_NULL) {
		RETURN_MM_STRING("", 1);
	}
	if (zephir_instance_of_ev(exception, yii_web_httpexception_ce TSRMLS_CC)) {
		ZEPHIR_OBS_VAR(code);
		zephir_read_property(&code, exception, SL("statusCode"), PH_NOISY_CC);
	} else {
		ZEPHIR_CALL_METHOD(&code, exception, "getcode",  NULL);
		zephir_check_call_status();
	}
	if (zephir_instance_of_ev(exception, yii_base_exception_ce TSRMLS_CC)) {
		ZEPHIR_CALL_METHOD(&name, exception, "getname",  NULL);
		zephir_check_call_status();
	} else {
		ZEPHIR_OBS_NVAR(name);
		zephir_read_property_this(&name, this_ptr, SL("defaultName"), PH_NOISY_CC);
		if (Z_TYPE_P(name) == IS_NULL) {
			ZEPHIR_INIT_VAR(_4);
			ZVAL_STRING(_4, "yii", 0);
			ZEPHIR_INIT_VAR(_5);
			ZVAL_STRING(_5, "Error", 0);
			ZEPHIR_CALL_CE_STATIC(&name, yii_baseyii_ce, "t", &_3, _4, _5);
			zephir_check_temp_parameter(_4);
			zephir_check_temp_parameter(_5);
			zephir_check_call_status();
		}
	}
	if (!(ZEPHIR_IS_EMPTY(code))) {
		ZEPHIR_INIT_VAR(_6);
		ZEPHIR_CONCAT_SVS(_6, " (#", code, ")");
		zephir_concat_self(&name, _6 TSRMLS_CC);
	}
	if (zephir_instance_of_ev(exception, yii_base_userexception_ce TSRMLS_CC)) {
		ZEPHIR_CALL_METHOD(&message, exception, "getmessage",  NULL);
		zephir_check_call_status();
	} else {
		ZEPHIR_OBS_NVAR(message);
		zephir_read_property_this(&message, this_ptr, SL("defaultMessage"), PH_NOISY_CC);
		if (Z_TYPE_P(message) == IS_NULL) {
			ZEPHIR_INIT_NVAR(_4);
			ZVAL_STRING(_4, "yii", 0);
			ZEPHIR_INIT_NVAR(_5);
			ZVAL_STRING(_5, "An internal server error occurred.", 0);
			ZEPHIR_CALL_CE_STATIC(&message, yii_baseyii_ce, "t", &_3, _4, _5);
			zephir_check_temp_parameter(_4);
			zephir_check_temp_parameter(_5);
			zephir_check_call_status();
		}
	}
	ZEPHIR_CALL_METHOD(&request, app, "getrequest",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&is_ajax, request, "getisajax",  NULL);
	zephir_check_call_status();
	if (ZEPHIR_IS_TRUE(is_ajax)) {
		ZEPHIR_CONCAT_VSV(return_value, name, ": ", message);
		RETURN_MM();
	} else {
		zephir_array_update_string(&elements, SL("name"), &name, PH_COPY | PH_SEPARATE);
		zephir_array_update_string(&elements, SL("message"), &message, PH_COPY | PH_SEPARATE);
		zephir_array_update_string(&elements, SL("exception"), &exception, PH_COPY | PH_SEPARATE);
		ZEPHIR_OBS_NVAR(_2);
		zephir_read_property_this(&_2, this_ptr, SL("view"), PH_NOISY_CC);
		if (Z_TYPE_P(_2) == IS_NULL) {
			_0 = zephir_fetch_nproperty_this(this_ptr, SL("controller"), PH_NOISY_CC);
			_7 = zephir_fetch_nproperty_this(this_ptr, SL("id"), PH_NOISY_CC);
			ZEPHIR_CALL_METHOD(&retval, _0, "render", NULL, _7, elements);
			zephir_check_call_status();
		} else {
			_7 = zephir_fetch_nproperty_this(this_ptr, SL("controller"), PH_NOISY_CC);
			_8 = zephir_fetch_nproperty_this(this_ptr, SL("view"), PH_NOISY_CC);
			ZEPHIR_CALL_METHOD(&retval, _7, "render", NULL, _8, elements);
			zephir_check_call_status();
		}
		RETURN_CCTOR(retval);
	}
	ZEPHIR_MM_RESTORE();

}

