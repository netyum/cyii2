
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/hash.h"
#include "kernel/object.h"
#include "kernel/array.h"
#include "kernel/memory.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Link represents a link object as defined in [JSON Hypermedia API Language](https://tools.ietf.org/html/draft-kelly-json-hal-03).
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_web_Link) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\web, Link, yii, web_link, yii_base_object_ce, yii_web_link_method_entry, 0);

	/**
	 * @var string a URI [RFC3986](https://tools.ietf.org/html/rfc3986) or
	 * URI template [RFC6570](https://tools.ietf.org/html/rfc6570). This property is required.
	 */
	zend_declare_property_null(yii_web_link_ce, SL("href"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string a secondary key for selecting Link Objects which share the same relation type
	 */
	zend_declare_property_null(yii_web_link_ce, SL("name"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string a hint to indicate the media type expected when dereferencing the target resource
	 */
	zend_declare_property_null(yii_web_link_ce, SL("type"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var boolean a value indicating whether [[href]] refers to a URI or URI template.
	 */
	zend_declare_property_bool(yii_web_link_ce, SL("templated"), 0, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string a URI that hints about the profile of the target resource.
	 */
	zend_declare_property_null(yii_web_link_ce, SL("profile"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string a label describing the link
	 */
	zend_declare_property_null(yii_web_link_ce, SL("title"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the language of the target resource
	 */
	zend_declare_property_null(yii_web_link_ce, SL("hreflang"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * The self link.
	 */
	zend_declare_class_constant_string(yii_web_link_ce, SL("REL_SELF"), "self" TSRMLS_CC);

	return SUCCESS;

}

/**
 * Serializes a list of links into proper array format.
 * @param array $links the links to be serialized
 * @return array the proper array representation of the links.
 */
PHP_METHOD(yii_web_Link, serialize) {

	HashTable *_1, *_4;
	HashPosition _0, _3;
	zval *links, *temp_links = NULL, *link = NULL, *rel = NULL, *i = NULL, *l = NULL, *temp_link = NULL, *elements, **_2, **_5, *temp_l = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &links);

	ZEPHIR_SEPARATE_PARAM(links);
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	ZEPHIR_CPY_WRT(temp_links, links);
	zephir_is_iterable(temp_links, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(rel, _1, _0);
		ZEPHIR_GET_HVALUE(temp_link, _2);
		ZEPHIR_CPY_WRT(link, temp_link);
		if (Z_TYPE_P(temp_link) == IS_ARRAY) {
			zephir_is_iterable(temp_link, &_4, &_3, 0, 0);
			for (
			  ; zephir_hash_get_current_data_ex(_4, (void**) &_5, &_3) == SUCCESS
			  ; zephir_hash_move_forward_ex(_4, &_3)
			) {
				ZEPHIR_GET_HMKEY(i, _4, _3);
				ZEPHIR_GET_HVALUE(l, _5);
				if (Z_TYPE_P(l) == IS_OBJECT) {
					if (zephir_instance_of_ev(l, yii_web_link_ce TSRMLS_CC)) {
					} else {
						ZEPHIR_CPY_WRT(temp_l, l);
						zephir_array_update_string(&elements, SL("href"), &l, PH_COPY | PH_SEPARATE);
						zephir_array_update_zval(&link, i, &elements, PH_COPY | PH_SEPARATE);
					}
				}
			}
			zephir_array_update_zval(&links, rel, &link, PH_COPY | PH_SEPARATE);
		} else {
			if (Z_TYPE_P(temp_link) == IS_OBJECT) {
				if (!(zephir_instance_of_ev(temp_link, yii_web_link_ce TSRMLS_CC))) {
					zephir_array_update_string(&elements, SL("href"), &link, PH_COPY | PH_SEPARATE);
					zephir_array_update_zval(&links, rel, &elements, PH_COPY | PH_SEPARATE);
				}
			}
		}
	}
	RETURN_CCTOR(links);

}

