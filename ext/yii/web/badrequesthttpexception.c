
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * BadRequestHttpException represents a "Bad Request" HTTP exception with status code 400.
 *
 * Use this exception to represent a generic client error. In many cases, there
 * may be an HTTP exception that more precisely describes the error. In that
 * case, consider using the more precise exception to provide the user with
 * additional information.
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.1
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_web_BadRequestHttpException) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\web, BadRequestHttpException, yii, web_badrequesthttpexception, yii_web_httpexception_ce, yii_web_badrequesthttpexception_method_entry, 0);

	return SUCCESS;

}

/**
 * Constructor.
 * @param string $message error message
 * @param integer $code error code
 * @param \Exception $previous The previous exception used for the exception chaining.
 */
PHP_METHOD(yii_web_BadRequestHttpException, __construct) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;
	zval *message = NULL, *code = NULL, *previous = NULL, *_1;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 3, &message, &code, &previous);

	if (!message) {
		message = ZEPHIR_GLOBAL(global_null);
	}
	if (!code) {
		ZEPHIR_INIT_VAR(code);
		ZVAL_LONG(code, 0);
	}
	if (!previous) {
		previous = ZEPHIR_GLOBAL(global_null);
	}


	ZEPHIR_INIT_VAR(_1);
	ZVAL_LONG(_1, 400);
	ZEPHIR_CALL_PARENT(NULL, yii_web_badrequesthttpexception_ce, this_ptr, "__construct", &_0, _1, message, code, previous);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

