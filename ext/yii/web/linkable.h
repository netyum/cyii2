
extern zend_class_entry *yii_web_linkable_ce;

ZEPHIR_INIT_CLASS(yii_web_Linkable);

ZEPHIR_INIT_FUNCS(yii_web_linkable_method_entry) {
	PHP_ABSTRACT_ME(yii_web_Linkable, getLinks, NULL)
  PHP_FE_END
};
