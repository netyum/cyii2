
extern zend_class_entry *yii_web_controller_ce;

ZEPHIR_INIT_CLASS(yii_web_Controller);

PHP_METHOD(yii_web_Controller, renderAjax);
PHP_METHOD(yii_web_Controller, bindActionParams);
PHP_METHOD(yii_web_Controller, beforeAction);
PHP_METHOD(yii_web_Controller, redirect);
PHP_METHOD(yii_web_Controller, goHome);
PHP_METHOD(yii_web_Controller, goBack);
PHP_METHOD(yii_web_Controller, refresh);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_controller_renderajax, 0, 0, 1)
	ZEND_ARG_INFO(0, view)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_controller_bindactionparams, 0, 0, 2)
	ZEND_ARG_INFO(0, action)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_controller_beforeaction, 0, 0, 1)
	ZEND_ARG_INFO(0, action)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_controller_redirect, 0, 0, 1)
	ZEND_ARG_INFO(0, url)
	ZEND_ARG_INFO(0, statusCode)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_controller_goback, 0, 0, 0)
	ZEND_ARG_INFO(0, defaultUrl)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_controller_refresh, 0, 0, 0)
	ZEND_ARG_INFO(0, anchor)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_web_controller_method_entry) {
	PHP_ME(yii_web_Controller, renderAjax, arginfo_yii_web_controller_renderajax, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_Controller, bindActionParams, arginfo_yii_web_controller_bindactionparams, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_Controller, beforeAction, arginfo_yii_web_controller_beforeaction, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_Controller, redirect, arginfo_yii_web_controller_redirect, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_Controller, goHome, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_Controller, goBack, arginfo_yii_web_controller_goback, ZEND_ACC_PUBLIC)
	PHP_ME(yii_web_Controller, refresh, arginfo_yii_web_controller_refresh, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
