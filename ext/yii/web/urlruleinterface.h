
extern zend_class_entry *yii_web_urlruleinterface_ce;

ZEPHIR_INIT_CLASS(yii_web_UrlRuleInterface);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_urlruleinterface_parserequest, 0, 0, 2)
	ZEND_ARG_INFO(0, manager)
	ZEND_ARG_INFO(0, request)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_web_urlruleinterface_createurl, 0, 0, 3)
	ZEND_ARG_INFO(0, manager)
	ZEND_ARG_INFO(0, route)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_web_urlruleinterface_method_entry) {
	PHP_ABSTRACT_ME(yii_web_UrlRuleInterface, parseRequest, arginfo_yii_web_urlruleinterface_parserequest)
	PHP_ABSTRACT_ME(yii_web_UrlRuleInterface, createUrl, arginfo_yii_web_urlruleinterface_createurl)
  PHP_FE_END
};
