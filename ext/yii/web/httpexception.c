
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"
#include "kernel/array.h"
#include "extra/extra.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * HttpException represents an exception caused by an improper request of the end-user.
 *
 * HttpException can be differentiated via its [[statusCode]] property value which
 * keeps a standard HTTP status code (e.g. 404, 500). Error handlers may use this status code
 * to decide how to format the error page.
 *
 * Throwing an HttpException like in the following example will result in the 404 page to be displayed.
 *
 * ```php
 * if ($item === null) { // item does not exist
 *     throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
 * }
 * ```
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_web_HttpException) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\web, HttpException, yii, web_httpexception, yii_base_userexception_ce, yii_web_httpexception_method_entry, 0);

	/**
	 * @var integer HTTP status code, such as 403, 404, 500, etc.
	 */
	zend_declare_property_null(yii_web_httpexception_ce, SL("statusCode"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Constructor.
 * @param integer $status HTTP status code, such as 404, 500, etc.
 * @param string $message error message
 * @param integer $code error code
 * @param \Exception $previous The previous exception used for the exception chaining.
 */
PHP_METHOD(yii_web_HttpException, __construct) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *status, *message = NULL, *code = NULL, *previous = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 3, &status, &message, &code, &previous);

	if (!message) {
		message = ZEPHIR_GLOBAL(global_null);
	}
	if (!code) {
		ZEPHIR_INIT_VAR(code);
		ZVAL_LONG(code, 0);
	}
	if (!previous) {
		previous = ZEPHIR_GLOBAL(global_null);
	}


	zephir_update_property_this(this_ptr, SL("statusCode"), status TSRMLS_CC);
	ZEPHIR_CALL_PARENT(NULL, yii_web_httpexception_ce, this_ptr, "__construct", NULL, message, code, previous);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * @return string the user-friendly name of this exception
 */
PHP_METHOD(yii_web_HttpException, getName) {

	zval *httpStatuses, *statusCode, *_0;

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(httpStatuses);
	yii_web_response_http_statuses(&httpStatuses);
	_0 = zephir_fetch_nproperty_this(this_ptr, SL("statusCode"), PH_NOISY_CC);
	if (zephir_array_isset_fetch(&statusCode, httpStatuses, _0, 1 TSRMLS_CC)) {
		RETURN_CTOR(statusCode);
	} else {
		RETURN_MM_STRING("Error", 1);
	}
	ZEPHIR_MM_RESTORE();

}

