
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/array.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/exception.h"
#include "kernel/concat.h"
#include "kernel/string.h"
#include "kernel/hash.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Container implements a [dependency injection](http://en.wikipedia.org/wiki/Dependency_injection) container.
 *
 * A dependency injection (DI) container is an object that knows how to instantiate and configure objects and
 * all their dependent objects. For more information about DI, please refer to
 * [Martin Fowler's article](http://martinfowler.com/articles/injection.html).
 *
 * Container supports constructor injection as well as property injection.
 *
 * To use Container, you first need to set up the class dependencies by calling [[set()]].
 * You then call [[get()]] to create a new class object. Container will automatically instantiate
 * dependent objects, inject them into the object being created, configure and finally return the newly created object.
 *
 * By default, [[\Yii::$container]] refers to a Container instance which is used by [[\Yii::createObject()]]
 * to create new object instances. You may use this method to replace the `new` operator
 * when creating a new object, which gives you the benefit of automatic dependency resolution and default
 * property configuration.
 *
 * Below is an example of using Container:
 *
 * ```php
 * namespace app\models;
 *
 * use yii\base\Object;
 * use yii\db\Connection;
 * use yii\di\Container;
 *
 * interface UserFinderInterface
 * {
 *     function findUser();
 * }
 *
 * class UserFinder extends Object implements UserFinderInterface
 * {
 *     public $db;
 *
 *     public function __construct(Connection $db, $config = [])
 *     {
 *         $this->db = $db;
 *         parent::__construct($config);
 *     }
 *
 *     public function findUser()
 *     {
 *     }
 * }
 *
 * class UserLister extends Object
 * {
 *     public $finder;
 *
 *     public function __construct(UserFinderInterface $finder, $config = [])
 *     {
 *         $this->finder = $finder;
 *         parent::__construct($config);
 *     }
 * }
 *
 * $container = new Container;
 * $container->set('yii\db\Connection', [
 *     'dsn' => '...',
 * ]);
 * $container->set('app\models\UserFinderInterface', [
 *     'class' => 'app\models\UserFinder',
 * ]);
 * $container->set('userLister', 'app\models\UserLister');
 *
 * $lister = $container->get('userLister');
 *
 * // which is equivalent to:
 *
 * $db = new \yii\db\Connection(['dsn' => '...']);
 * $finder = new UserFinder($db);
 * $lister = new UserLister($finder);
 * ```
 *
 * @property array $definitions The list of the object definitions or the loaded shared objects (type or ID =>
 * definition or instance). This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_di_Container) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\di, Container, yii, di_container, yii_base_component_ce, yii_di_container_method_entry, 0);

	/**
	 * @var array singleton objects indexed by their types
	 */
	zend_declare_property_null(yii_di_container_ce, SL("_singletons"), ZEND_ACC_PRIVATE TSRMLS_CC);

	/**
	 * @var array object definitions indexed by their types
	 */
	zend_declare_property_null(yii_di_container_ce, SL("_definitions"), ZEND_ACC_PRIVATE TSRMLS_CC);

	/**
	 * @var array constructor parameters indexed by object types
	 */
	zend_declare_property_null(yii_di_container_ce, SL("_params"), ZEND_ACC_PRIVATE TSRMLS_CC);

	/**
	 * @var array cached ReflectionClass objects indexed by class/interface names
	 */
	zend_declare_property_null(yii_di_container_ce, SL("_reflections"), ZEND_ACC_PRIVATE TSRMLS_CC);

	/**
	 * @var array cached dependencies indexed by class/interface names. Each class name
	 * is associated with a list of constructor parameter types or default values.
	 */
	zend_declare_property_null(yii_di_container_ce, SL("_dependencies"), ZEND_ACC_PRIVATE TSRMLS_CC);

	return SUCCESS;

}

/**
 * Returns an instance of the requested class.
 *
 * You may provide constructor parameters (`$params`) and object configurations (`$config`)
 * that will be used during the creation of the instance.
 *
 * Note that if the class is declared to be singleton by calling [[setSingleton()]],
 * the same instance of the class will be returned each time this method is called.
 * In this case, the constructor parameters and object configurations will be used
 * only if the class is instantiated the first time.
 *
 * @param string $class the class name or an alias name (e.g. `foo`) that was previously registered via [[set()]]
 * or [[setSingleton()]].
 * @param array $params a list of constructor parameter values. The parameters should be provided in the order
 * they appear in the constructor declaration. If you want to skip some parameters, you should index the remaining
 * ones with the integers that represent their positions in the constructor parameter list.
 * @param array $config a list of name-value pairs that will be used to initialize the object properties.
 * @return object an instance of the requested class.
 * @throws InvalidConfigException if the class cannot be recognized or correspond to an invalid definition
 */
PHP_METHOD(yii_di_Container, get) {

	zephir_nts_static zephir_fcall_cache_entry *_4 = NULL, *_5 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *class_param = NULL, *params = NULL, *config = NULL, *singletons, *singleton, *definitions, *_0 = NULL, *_1, *_2, *definition, *object = NULL, *_3 = NULL, *concrete, *_6, *_7;
	zval *class = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 2, &class_param, &params, &config);

	zephir_get_strval(class, class_param);
	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	} else {
		ZEPHIR_SEPARATE_PARAM(params);
	}
	if (!config) {
		ZEPHIR_INIT_VAR(config);
		array_init(config);
	} else {
		ZEPHIR_SEPARATE_PARAM(config);
	}


	ZEPHIR_OBS_VAR(singletons);
	zephir_read_property_this(&singletons, this_ptr, SL("_singletons"), PH_NOISY_CC);
	if (Z_TYPE_P(singletons) != IS_ARRAY) {
		ZEPHIR_INIT_VAR(_0);
		array_init(_0);
		zephir_update_property_this(this_ptr, SL("_singletons"), _0 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(definitions);
	zephir_read_property_this(&definitions, this_ptr, SL("_definitions"), PH_NOISY_CC);
	if (Z_TYPE_P(definitions) != IS_ARRAY) {
		ZEPHIR_INIT_NVAR(_0);
		array_init(_0);
		zephir_update_property_this(this_ptr, SL("_definitions"), _0 TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(singleton);
	_1 = zephir_fetch_nproperty_this(this_ptr, SL("_singletons"), PH_NOISY_CC);
	if (zephir_array_isset_fetch(&singleton, _1, class, 0 TSRMLS_CC)) {
		RETURN_CCTOR(singleton);
	} else {
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("_definitions"), PH_NOISY_CC);
		if (!(zephir_array_isset(_2, class))) {
			ZEPHIR_RETURN_CALL_METHOD(this_ptr, "build", NULL, class, params, config);
			zephir_check_call_status();
			RETURN_MM();
		}
	}
	_2 = zephir_fetch_nproperty_this(this_ptr, SL("_definitions"), PH_NOISY_CC);
	ZEPHIR_OBS_VAR(definition);
	zephir_array_fetch(&definition, _2, class, PH_NOISY TSRMLS_CC);
	ZEPHIR_CALL_FUNCTION(&_3, "is_callable", &_4, definition, ZEPHIR_GLOBAL(global_true));
	zephir_check_call_status();
	if (zephir_is_true(_3)) {
		ZEPHIR_CALL_METHOD(&_3, this_ptr, "mergeparams", NULL, class, params);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(params, _3);
		ZEPHIR_CALL_METHOD(&_3, this_ptr, "resolvedependencies", NULL, params);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(params, _3);
		ZEPHIR_CALL_FUNCTION(&object, "call_user_func", &_5, definition, this_ptr, params, config);
		zephir_check_call_status();
	} else {
		if (Z_TYPE_P(definition) == IS_ARRAY) {
			ZEPHIR_OBS_VAR(concrete);
			zephir_array_fetch_string(&concrete, definition, SL("class"), PH_NOISY TSRMLS_CC);
			zephir_array_unset_string(&definition, SS("class"), PH_SEPARATE);
			ZEPHIR_INIT_NVAR(_0);
			zephir_fast_array_merge(_0, &(definition), &(config) TSRMLS_CC);
			ZEPHIR_CPY_WRT(config, _0);
			ZEPHIR_CALL_METHOD(&_3, this_ptr, "mergeparams", NULL, class, params);
			zephir_check_call_status();
			ZEPHIR_CPY_WRT(params, _3);
			if (ZEPHIR_IS_IDENTICAL(concrete, class)) {
				ZEPHIR_CALL_METHOD(&object, this_ptr, "build", NULL, class, params, config);
				zephir_check_call_status();
			} else {
				ZEPHIR_CALL_METHOD(&object, this_ptr, "get", NULL, concrete, params, config);
				zephir_check_call_status();
			}
		} else {
			if (Z_TYPE_P(definition) == IS_OBJECT) {
				zephir_update_property_array(this_ptr, SL("_singletons"), class, definition TSRMLS_CC);
				RETURN_CCTOR(definition);
			} else {
				ZEPHIR_INIT_VAR(_6);
				object_init_ex(_6, yii_base_invalidconfigexception_ce);
				ZEPHIR_INIT_NVAR(_0);
				zephir_gettype(_0, definition TSRMLS_CC);
				ZEPHIR_INIT_VAR(_7);
				ZEPHIR_CONCAT_SV(_7, "Unexpected object definition type: ", _0);
				ZEPHIR_CALL_METHOD(NULL, _6, "__construct", NULL, _7);
				zephir_check_call_status();
				zephir_throw_exception_debug(_6, "yii/di/Container.zep", 192 TSRMLS_CC);
				ZEPHIR_MM_RESTORE();
				return;
			}
		}
	}
	_2 = zephir_fetch_nproperty_this(this_ptr, SL("_singletons"), PH_NOISY_CC);
	if (zephir_array_key_exists(_2, class TSRMLS_CC)) {
		zephir_update_property_array(this_ptr, SL("_singletons"), class, object TSRMLS_CC);
	}
	RETURN_CCTOR(object);

}

/**
 * Registers a class definition with this container.
 *
 * For example,
 *
 * ```php
 * // register a class name as is. This can be skipped.
 * $container->set('yii\db\Connection');
 *
 * // register an interface
 * // When a class depends on the interface, the corresponding class
 * // will be instantiated as the dependent object
 * $container->set('yii\mail\MailInterface', 'yii\swiftmailer\Mailer');
 *
 * // register an alias name. You can use $container->get('foo')
 * // to create an instance of Connection
 * $container->set('foo', 'yii\db\Connection');
 *
 * // register a class with configuration. The configuration
 * // will be applied when the class is instantiated by get()
 * $container->set('yii\db\Connection', [
 *     'dsn' => 'mysql:host=127.0.0.1;dbname=demo',
 *     'username' => 'root',
 *     'password' => '',
 *     'charset' => 'utf8',
 * ]);
 *
 * // register an alias name with class configuration
 * // In this case, a "class" element is required to specify the class
 * $container->set('db', [
 *     'class' => 'yii\db\Connection',
 *     'dsn' => 'mysql:host=127.0.0.1;dbname=demo',
 *     'username' => 'root',
 *     'password' => '',
 *     'charset' => 'utf8',
 * ]);
 *
 * // register a PHP callable
 * // The callable will be executed when $container->get('db') is called
 * $container->set('db', function ($container, $params, $config) {
 *     return new \yii\db\Connection($config);
 * });
 * ```
 *
 * If a class definition with the same name already exists, it will be overwritten with the new one.
 * You may use [[has()]] to check if a class definition already exists.
 *
 * @param string $class class name, interface name or alias name
 * @param mixed $definition the definition associated with `$class`. It can be one of the followings:
 *
 * - a PHP callable: The callable will be executed when [[get()]] is invoked. The signature of the callable
 *   should be `function ($container, $params, $config)`, where `$params` stands for the list of constructor
 *   parameters, `$config` the object configuration, and `$container` the container object. The return value
 *   of the callable will be returned by [[get()]] as the object instance requested.
 * - a configuration array: the array contains name-value pairs that will be used to initialize the property
 *   values of the newly created object when [[get()]] is called. The `class` element stands for the
 *   the class of the object to be created. If `class` is not specified, `$class` will be used as the class name.
 * - a string: a class name, an interface name or an alias name.
 * @param array $params the list of constructor parameters. The parameters will be passed to the class
 * constructor when [[get()]] is called.
 * @return static the container itself
 */
PHP_METHOD(yii_di_Container, set) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *params = NULL;
	zval *class_param = NULL, *definition = NULL, *params_param = NULL, *_0 = NULL, *_1;
	zval *class = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 2, &class_param, &definition, &params_param);

	zephir_get_strval(class, class_param);
	if (!definition) {
		ZEPHIR_INIT_VAR(definition);
		array_init(definition);
	}
	if (!params_param) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	} else {
		zephir_get_arrval(params, params_param);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "normalizedefinition", NULL, class, definition);
	zephir_check_call_status();
	zephir_update_property_array(this_ptr, SL("_definitions"), class, _0 TSRMLS_CC);
	zephir_update_property_array(this_ptr, SL("_params"), class, params TSRMLS_CC);
	_1 = zephir_fetch_nproperty_this(this_ptr, SL("_singletons"), PH_NOISY_CC);
	zephir_array_unset(&_1, class, PH_SEPARATE);
	RETURN_THIS();

}

/**
 * Registers a class definition with this container and marks the class as a singleton class.
 *
 * This method is similar to [[set()]] except that classes registered via this method will only have one
 * instance. Each time [[get()]] is called, the same instance of the specified class will be returned.
 *
 * @param string $class class name, interface name or alias name
 * @param mixed $definition the definition associated with `$class`. See [[set()]] for more details.
 * @param array $params the list of constructor parameters. The parameters will be passed to the class
 * constructor when [[get()]] is called.
 * @return static the container itself
 * @see set()
 */
PHP_METHOD(yii_di_Container, setSingleton) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *params = NULL;
	zval *class_param = NULL, *definition = NULL, *params_param = NULL, *_0 = NULL;
	zval *class = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 2, &class_param, &definition, &params_param);

	zephir_get_strval(class, class_param);
	if (!definition) {
		ZEPHIR_INIT_VAR(definition);
		array_init(definition);
	}
	if (!params_param) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	} else {
		zephir_get_arrval(params, params_param);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "normalizedefinition", NULL, class, definition);
	zephir_check_call_status();
	zephir_update_property_array(this_ptr, SL("_definitions"), class, _0 TSRMLS_CC);
	zephir_update_property_array(this_ptr, SL("_params"), class, params TSRMLS_CC);
	zephir_update_property_array(this_ptr, SL("_singletons"), class, ZEPHIR_GLOBAL(global_null) TSRMLS_CC);
	RETURN_THIS();

}

/**
 * Returns a value indicating whether the container has the definition of the specified name.
 * @param string $class class name, interface name or alias name
 * @return boolean whether the container has the definition of the specified name..
 * @see set()
 */
PHP_METHOD(yii_di_Container, has) {

	zval *class_param = NULL, *_0;
	zval *class = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &class_param);

	zephir_get_strval(class, class_param);


	_0 = zephir_fetch_nproperty_this(this_ptr, SL("_definitions"), PH_NOISY_CC);
	RETURN_MM_BOOL(zephir_array_isset(_0, class));

}

/**
 * Returns a value indicating whether the given name corresponds to a registered singleton.
 * @param string $class class name, interface name or alias name
 * @param boolean $checkInstance whether to check if the singleton has been instantiated.
 * @return boolean whether the given name corresponds to a registered singleton. If `$checkInstance` is true,
 * the method should return a value indicating whether the singleton has been instantiated.
 */
PHP_METHOD(yii_di_Container, hasSingleton) {

	zend_bool checkInstance;
	zval *class_param = NULL, *checkInstance_param = NULL, *_0, *_1, *_2;
	zval *class = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &class_param, &checkInstance_param);

	zephir_get_strval(class, class_param);
	if (!checkInstance_param) {
		checkInstance = 0;
	} else {
		checkInstance = zephir_get_boolval(checkInstance_param);
	}


	ZEPHIR_INIT_VAR(_0);
	if (checkInstance) {
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("_singletons"), PH_NOISY_CC);
		ZVAL_BOOL(_0, zephir_array_isset(_1, class));
	} else {
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("_singletons"), PH_NOISY_CC);
		ZVAL_BOOL(_0, zephir_array_key_exists(_2, class TSRMLS_CC));
	}
	RETURN_CCTOR(_0);

}

/**
 * Removes the definition for the specified name.
 * @param string $class class name, interface name or alias name
 */
PHP_METHOD(yii_di_Container, clear) {

	zval *class_param = NULL, *_0, *_1;
	zval *class = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &class_param);

	zephir_get_strval(class, class_param);


	_0 = zephir_fetch_nproperty_this(this_ptr, SL("_definitions"), PH_NOISY_CC);
	zephir_array_unset(&_0, class, PH_SEPARATE);
	_1 = zephir_fetch_nproperty_this(this_ptr, SL("_singletons"), PH_NOISY_CC);
	zephir_array_unset(&_1, class, PH_SEPARATE);
	ZEPHIR_MM_RESTORE();

}

/**
 * Normalizes the class definition.
 * @param string $class class name
 * @param string|array|callable $definition the class definition
 * @return array the normalized class definition
 * @throws InvalidConfigException if the definition is invalid.
 */
PHP_METHOD(yii_di_Container, normalizeDefinition) {

	zend_bool _2;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	zval *class_param = NULL, *definition, *_0 = NULL, _3, *_4 = NULL, *_5 = NULL, *_7;
	zval *class = NULL, *text, *_6;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &class_param, &definition);

	zephir_get_strval(class, class_param);
	ZEPHIR_SEPARATE_PARAM(definition);
	ZEPHIR_INIT_VAR(text);
	ZVAL_STRING(text, "class", 1);


	if (ZEPHIR_IS_EMPTY(definition)) {
		array_init_size(return_value, 2);
		zephir_array_update_string(&return_value, SL("class"), &class, PH_COPY | PH_SEPARATE);
		RETURN_MM();
	} else {
		if (Z_TYPE_P(definition) == IS_STRING) {
			array_init_size(return_value, 2);
			zephir_array_update_string(&return_value, SL("class"), &definition, PH_COPY | PH_SEPARATE);
			RETURN_MM();
		} else {
			ZEPHIR_CALL_FUNCTION(&_0, "is_callable", &_1, definition, ZEPHIR_GLOBAL(global_true));
			zephir_check_call_status();
			_2 = zephir_is_true(_0);
			if (!(_2)) {
				_2 = Z_TYPE_P(definition) == IS_OBJECT;
			}
			if (_2) {
				RETURN_CCTOR(definition);
			} else {
				if (Z_TYPE_P(definition) == IS_ARRAY) {
					if (!(zephir_array_isset_string(definition, SS("class")))) {
						ZEPHIR_SINIT_VAR(_3);
						ZVAL_STRING(&_3, "\\", 0);
						ZEPHIR_INIT_VAR(_4);
						zephir_fast_strpos(_4, class, &_3, 0 );
						if (!ZEPHIR_IS_FALSE(_4)) {
							zephir_array_update_string(&definition, SL("class"), &class, PH_COPY | PH_SEPARATE);
						} else {
							ZEPHIR_INIT_VAR(_5);
							object_init_ex(_5, yii_base_invalidconfigexception_ce);
							ZEPHIR_INIT_VAR(_6);
							ZEPHIR_CONCAT_SVS(_6, "A class definition requires a \"", text, "\" member.");
							ZEPHIR_CALL_METHOD(NULL, _5, "__construct", NULL, _6);
							zephir_check_call_status();
							zephir_throw_exception_debug(_5, "yii/di/Container.zep", 356 TSRMLS_CC);
							ZEPHIR_MM_RESTORE();
							return;
						}
					}
					RETURN_CCTOR(definition);
				} else {
					ZEPHIR_INIT_LNVAR(_5);
					object_init_ex(_5, yii_base_invalidconfigexception_ce);
					ZEPHIR_INIT_NVAR(_4);
					zephir_gettype(_4, definition TSRMLS_CC);
					ZEPHIR_INIT_VAR(_7);
					ZEPHIR_CONCAT_SVSV(_7, "Unsupported definition type for \"", class, "\": ", _4);
					ZEPHIR_CALL_METHOD(NULL, _5, "__construct", NULL, _7);
					zephir_check_call_status();
					zephir_throw_exception_debug(_5, "yii/di/Container.zep", 361 TSRMLS_CC);
					ZEPHIR_MM_RESTORE();
					return;
				}
			}
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the list of the object definitions or the loaded shared objects.
 * @return array the list of the object definitions or the loaded shared objects (type or ID => definition or instance).
 */
PHP_METHOD(yii_di_Container, getDefinitions) {


	RETURN_MEMBER(this_ptr, "_definitions");

}

/**
 * Creates an instance of the specified class.
 * This method will resolve dependencies of the specified class, instantiate them, and inject
 * them into the new instance of the specified class.
 * @param string $class the class name
 * @param array $params constructor parameters
 * @param array $config configurations to be applied to the new instance
 * @return object the newly created instance of the specified class
 */
PHP_METHOD(yii_di_Container, build) {

	zephir_nts_static zephir_fcall_cache_entry *_6 = NULL;
	zend_bool _3;
	HashTable *_1, *_10;
	HashPosition _0, _9;
	int ZEPHIR_LAST_CALL_STATUS, _7;
	zval *class_param = NULL, *params, *config, *elements = NULL, *reflection, *dependencies = NULL, *index = NULL, *param = NULL, **_2, _4, *_5 = NULL, *i = NULL, *_8 = NULL, *object = NULL, *name = NULL, *value = NULL, **_11;
	zval *class = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 3, 0, &class_param, &params, &config);

	zephir_get_strval(class, class_param);
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	ZEPHIR_CALL_METHOD(&elements, this_ptr, "getdependencies", NULL, class);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(reflection);
	zephir_array_fetch_long(&reflection, elements, 0, PH_NOISY TSRMLS_CC);
	ZEPHIR_OBS_VAR(dependencies);
	zephir_array_fetch_long(&dependencies, elements, 1, PH_NOISY TSRMLS_CC);
	zephir_is_iterable(params, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(index, _1, _0);
		ZEPHIR_GET_HVALUE(param, _2);
		zephir_array_update_zval(&dependencies, index, &param, PH_COPY | PH_SEPARATE);
	}
	_3 = !ZEPHIR_IS_EMPTY(dependencies);
	if (_3) {
		ZEPHIR_SINIT_VAR(_4);
		ZVAL_STRING(&_4, "yii\\base\\Object", 0);
		ZEPHIR_CALL_FUNCTION(&_5, "is_a", &_6, class, &_4, ZEPHIR_GLOBAL(global_true));
		zephir_check_call_status();
		_3 = zephir_is_true(_5);
	}
	if (_3) {
		if (zephir_fast_count_int(dependencies TSRMLS_CC)) {
			ZEPHIR_INIT_VAR(i);
			ZVAL_LONG(i, zephir_fast_count_int(dependencies TSRMLS_CC));
			_7 = (zephir_get_numberval(i) - 1);
			ZEPHIR_INIT_NVAR(i);
			ZVAL_LONG(i, _7);
			zephir_array_update_zval(&dependencies, i, &config, PH_COPY | PH_SEPARATE);
		}
		ZEPHIR_CALL_METHOD(&_8, this_ptr, "resolvedependencies", NULL, dependencies, reflection);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(dependencies, _8);
		ZEPHIR_RETURN_CALL_METHOD(reflection, "newinstanceargs", NULL, dependencies);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		ZEPHIR_CALL_METHOD(&_8, this_ptr, "resolvedependencies", NULL, dependencies, reflection);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(dependencies, _8);
		ZEPHIR_CALL_METHOD(&object, reflection, "newinstanceargs", NULL, dependencies);
		zephir_check_call_status();
		zephir_is_iterable(config, &_10, &_9, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_10, (void**) &_11, &_9) == SUCCESS
		  ; zephir_hash_move_forward_ex(_10, &_9)
		) {
			ZEPHIR_GET_HMKEY(name, _10, _9);
			ZEPHIR_GET_HVALUE(value, _11);
			zephir_update_property_zval_zval(object, name, value TSRMLS_CC);
		}
		RETURN_CCTOR(object);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Merges the user-specified constructor parameters with the ones registered via [[set()]].
 * @param string $class class name, interface name or alias name
 * @param array $params the constructor parameters
 * @return array the merged parameters
 */
PHP_METHOD(yii_di_Container, mergeParams) {

	HashTable *_5;
	HashPosition _4;
	zval *class_param = NULL, *params, *_0, *_1, *_2, *_3, *ps, *index = NULL, *value = NULL, **_6;
	zval *class = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &class_param, &params);

	zephir_get_strval(class, class_param);


	_0 = zephir_fetch_nproperty_this(this_ptr, SL("_params"), PH_NOISY_CC);
	zephir_array_fetch(&_1, _0, class, PH_NOISY | PH_READONLY TSRMLS_CC);
	if (ZEPHIR_IS_EMPTY(_1)) {
		RETURN_CCTOR(params);
	}
	if (ZEPHIR_IS_EMPTY(params)) {
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("_params"), PH_NOISY_CC);
		zephir_array_fetch(&_3, _2, class, PH_NOISY | PH_READONLY TSRMLS_CC);
		RETURN_CTOR(_3);
	} else {
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("_params"), PH_NOISY_CC);
		ZEPHIR_OBS_VAR(ps);
		zephir_array_fetch(&ps, _2, class, PH_NOISY TSRMLS_CC);
		zephir_is_iterable(params, &_5, &_4, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_5, (void**) &_6, &_4) == SUCCESS
		  ; zephir_hash_move_forward_ex(_5, &_4)
		) {
			ZEPHIR_GET_HMKEY(index, _5, _4);
			ZEPHIR_GET_HVALUE(value, _6);
			zephir_array_update_zval(&ps, index, &value, PH_COPY | PH_SEPARATE);
		}
		RETURN_CCTOR(ps);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the dependencies of the specified class.
 * @param string $class class name, interface name or alias name
 * @return array the dependencies of the specified class.
 */
PHP_METHOD(yii_di_Container, getDependencies) {

	zephir_fcall_cache_entry *_10 = NULL;
	HashTable *_6;
	HashPosition _5;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_class_entry *_3;
	zval *class_param = NULL, *r, *_0, *_1, *_2, *dependencies, *reflection, *constructor = NULL, *param = NULL, *_4 = NULL, **_7, *_8 = NULL, *c = NULL, *_9 = NULL, *_11 = NULL;
	zval *class = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &class_param);

	zephir_get_strval(class, class_param);
	ZEPHIR_INIT_VAR(dependencies);
	array_init(dependencies);


	ZEPHIR_OBS_VAR(r);
	_0 = zephir_fetch_nproperty_this(this_ptr, SL("_reflections"), PH_NOISY_CC);
	if (zephir_array_isset_fetch(&r, _0, class, 0 TSRMLS_CC)) {
		array_init_size(return_value, 3);
		zephir_array_fast_append(return_value, r);
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("_dependencies"), PH_NOISY_CC);
		ZEPHIR_OBS_VAR(_2);
		zephir_array_fetch(&_2, _1, class, PH_NOISY TSRMLS_CC);
		zephir_array_fast_append(return_value, _2);
		RETURN_MM();
	}
	ZEPHIR_INIT_VAR(reflection);
	_3 = zend_fetch_class(SL("ReflectionClass"), ZEND_FETCH_CLASS_AUTO TSRMLS_CC);
	object_init_ex(reflection, _3);
	ZEPHIR_CALL_METHOD(NULL, reflection, "__construct", NULL, class);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&constructor, reflection, "getconstructor",  NULL);
	zephir_check_call_status();
	if (Z_TYPE_P(constructor) != IS_NULL) {
		ZEPHIR_CALL_METHOD(&_4, constructor, "getparameters",  NULL);
		zephir_check_call_status();
		zephir_is_iterable(_4, &_6, &_5, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_6, (void**) &_7, &_5) == SUCCESS
		  ; zephir_hash_move_forward_ex(_6, &_5)
		) {
			ZEPHIR_GET_HVALUE(param, _7);
			ZEPHIR_CALL_METHOD(&_8, param, "isdefaultvalueavailable",  NULL);
			zephir_check_call_status();
			if (zephir_is_true(_8)) {
				ZEPHIR_CALL_METHOD(&_8, param, "getdefaultvalue",  NULL);
				zephir_check_call_status();
				zephir_array_append(&dependencies, _8, PH_SEPARATE);
			} else {
				ZEPHIR_CALL_METHOD(&c, param, "getclass",  NULL);
				zephir_check_call_status();
				if (Z_TYPE_P(c) == IS_NULL) {
					ZEPHIR_CALL_CE_STATIC(&_9, yii_di_instance_ce, "of", &_10, c);
					zephir_check_call_status();
					zephir_array_append(&dependencies, _9, PH_SEPARATE);
				} else {
					ZEPHIR_CALL_METHOD(&_11, c, "getname",  NULL);
					zephir_check_call_status();
					ZEPHIR_CALL_CE_STATIC(&_9, yii_di_instance_ce, "of", &_10, _11);
					zephir_check_call_status();
					zephir_array_append(&dependencies, _9, PH_SEPARATE);
				}
			}
		}
	}
	zephir_update_property_array(this_ptr, SL("_reflections"), class, reflection TSRMLS_CC);
	zephir_update_property_array(this_ptr, SL("_dependencies"), class, dependencies TSRMLS_CC);
	array_init_size(return_value, 3);
	zephir_array_fast_append(return_value, reflection);
	zephir_array_fast_append(return_value, dependencies);
	RETURN_MM();

}

/**
 * Resolves dependencies by replacing them with the actual object instances.
 * @param array $dependencies the dependencies
 * @param ReflectionClass $reflection the class reflection associated with the dependencies
 * @return array the resolved dependencies
 * @throws InvalidConfigException if a dependency cannot be resolved or if a dependency cannot be fulfilled.
 */
PHP_METHOD(yii_di_Container, resolveDependencies) {

	zephir_fcall_cache_entry *_7 = NULL, *_12 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_bool _3;
	HashTable *_1;
	HashPosition _0;
	zval *dependencies, *reflection = NULL, *index = NULL, *dependency = NULL, **_2, *_4 = NULL, *_5 = NULL, *_6 = NULL, *name = NULL, *class = NULL, *_8 = NULL, *_9, *_10 = NULL, *_11 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &dependencies, &reflection);

	ZEPHIR_SEPARATE_PARAM(dependencies);
	if (!reflection) {
		reflection = ZEPHIR_GLOBAL(global_null);
	}


	zephir_is_iterable(dependencies, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(index, _1, _0);
		ZEPHIR_GET_HVALUE(dependency, _2);
		_3 = Z_TYPE_P(dependency) == IS_OBJECT;
		if (_3) {
			_3 = (zephir_instance_of_ev(dependency, yii_di_instance_ce TSRMLS_CC));
		}
		if (_3) {
			ZEPHIR_OBS_NVAR(_4);
			zephir_read_property(&_4, dependency, SL("id"), PH_NOISY_CC);
			if (Z_TYPE_P(_4) != IS_NULL) {
				ZEPHIR_OBS_NVAR(_6);
				zephir_read_property(&_6, dependency, SL("id"), PH_NOISY_CC);
				ZEPHIR_CALL_METHOD(&_5, this_ptr, "get", &_7, _6);
				zephir_check_call_status();
				zephir_array_update_zval(&dependencies, index, &_5, PH_COPY | PH_SEPARATE);
			} else {
				if (Z_TYPE_P(reflection) != IS_NULL) {
					ZEPHIR_CALL_METHOD(&_5, reflection, "getconstructor",  NULL);
					zephir_check_call_status();
					ZEPHIR_CALL_METHOD(&_8, _5, "getparameters",  NULL);
					zephir_check_call_status();
					zephir_array_fetch(&_9, _8, index, PH_NOISY | PH_READONLY TSRMLS_CC);
					ZEPHIR_CALL_METHOD(&name, _9, "getname",  NULL);
					zephir_check_call_status();
					ZEPHIR_CALL_METHOD(&class, reflection, "getname",  NULL);
					zephir_check_call_status();
					ZEPHIR_INIT_LNVAR(_10);
					object_init_ex(_10, yii_base_invalidconfigexception_ce);
					ZEPHIR_INIT_LNVAR(_11);
					ZEPHIR_CONCAT_SVSVS(_11, "Missing required parameter \"", name, "\" when instantiating \"", class, "\".");
					ZEPHIR_CALL_METHOD(NULL, _10, "__construct", &_12, _11);
					zephir_check_call_status();
					zephir_throw_exception_debug(_10, "yii/di/Container.zep", 507 TSRMLS_CC);
					ZEPHIR_MM_RESTORE();
					return;
				}
			}
		}
	}
	RETURN_CCTOR(dependencies);

}

