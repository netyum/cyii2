
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/exception.h"
#include "kernel/concat.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Instance represents a reference to a named object in a dependency injection (DI) container or a service locator.
 *
 * You may use [[get()]] to obtain the actual object referenced by [[id]].
 *
 * Instance is mainly used in two places:
 *
 * - When configuring a dependency injection container, you use Instance to reference a class name, interface name
 *   or alias name. The reference can later be resolved into the actual object by the container.
 * - In classes which use service locator to obtain dependent objects.
 *
 * The following example shows how to configure a DI container with Instance:
 *
 * ```php
 * $container = new \yii\di\Container;
 * $container->set('cache', 'yii\caching\DbCache', Instance::of('db'));
 * $container->set('db', [
 *     'class' => 'yii\db\Connection',
 *     'dsn' => 'sqlite:path/to/file.db',
 * ]);
 * ```
 *
 * And the following example shows how a class retrieves a component from a service locator:
 *
 * ```php
 * class DbCache extends Cache
 * {
 *     public $db = 'db';
 *
 *     public function init()
 *     {
 *         parent::init();
 *         $this->db = Instance::ensure($this->db, 'yii\db\Connection');
 *     }
 * }
 * ```
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_di_Instance) {

	ZEPHIR_REGISTER_CLASS(yii\\di, Instance, yii, di_instance, yii_di_instance_method_entry, 0);

	/**
	 * @var string the component ID, class name, interface name or alias name
	 */
	zend_declare_property_null(yii_di_instance_ce, SL("id"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Constructor.
 * @param string $id the component ID
 */
PHP_METHOD(yii_di_Instance, __construct) {

	zval *id;

	zephir_fetch_params(0, 1, 0, &id);



	zephir_update_property_this(this_ptr, SL("id"), id TSRMLS_CC);

}

/**
 * Creates a new Instance object.
 * @param string $id the component ID
 * @return Instance the new Instance object.
 */
PHP_METHOD(yii_di_Instance, of) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *id;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &id);



	object_init_ex(return_value, yii_di_instance_ce);
	ZEPHIR_CALL_METHOD(NULL, return_value, "__construct", NULL, id);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Resolves the specified reference into the actual object and makes sure it is of the specified type.
 *
 * The reference may be specified as a string or an Instance object. If the former,
 * it will be treated as a component ID, a class/interface name or an alias, depending on the container type.
 *
 * If you do not specify a container, the method will first try `Yii::$app` followed by `Yii::$container`.
 *
 * For example,
 *
 * ```php
 * use yii\db\Connection;
 *
 * // returns Yii::$app->db
 * $db = Instance::ensure('db', Connection::className());
 * // or
 * $instance = Instance::of('db');
 * $db = Instance::ensure($instance, Connection::className());
 * ```
 *
 * @param object|string|static $reference an object or a reference to the desired object.
 * You may specify a reference in terms of a component ID or an Instance object.
 * @param string $type the class/interface name to be checked. If null, type check will not be performed.
 * @param ServiceLocator|Container $container the container. This will be passed to [[get()]].
 * @return object the object referenced by the Instance, or `$reference` itself if it is an object.
 * @throws InvalidConfigException if the reference is invalid
 */
PHP_METHOD(yii_di_Instance, ensure) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *reference = NULL, *type = NULL, *container = NULL, *temp_reference = NULL, *component = NULL, *class_name, *message, *id = NULL, *_0, *_1 = NULL, *_2 = NULL, *valueType;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 2, &reference, &type, &container);

	ZEPHIR_SEPARATE_PARAM(reference);
	if (!type) {
		type = ZEPHIR_GLOBAL(global_null);
	}
	if (!container) {
		container = ZEPHIR_GLOBAL(global_null);
	}
	ZEPHIR_INIT_VAR(temp_reference);
	ZVAL_NULL(temp_reference);


	if (Z_TYPE_P(reference) == IS_OBJECT) {
		if (zephir_is_instance_of(reference, Z_STRVAL_P(type), Z_STRLEN_P(type) TSRMLS_CC)) {
			RETURN_CCTOR(reference);
		}
	} else {
		if (ZEPHIR_IS_EMPTY(reference)) {
			ZEPHIR_THROW_EXCEPTION_DEBUG_STR(yii_base_invalidconfigexception_ce, "The required component is not specified.", "yii/di/Instance.zep", 115);
			return;
		}
	}
	if (Z_TYPE_P(reference) == IS_STRING) {
		ZEPHIR_INIT_VAR(temp_reference);
		object_init_ex(temp_reference, yii_di_instance_ce);
		ZEPHIR_CALL_METHOD(NULL, temp_reference, "__construct", NULL, reference);
		zephir_check_call_status();
	}
	if (Z_TYPE_P(temp_reference) == IS_OBJECT) {
		ZEPHIR_CPY_WRT(reference, temp_reference);
	}
	if (zephir_instance_of_ev(reference, yii_di_instance_ce TSRMLS_CC)) {
		ZEPHIR_CALL_METHOD(&component, reference, "get", NULL, container);
		zephir_check_call_status();
		if (Z_TYPE_P(type) == IS_NULL) {
			RETURN_CCTOR(component);
		}
		if (zephir_is_instance_of(component, Z_STRVAL_P(type), Z_STRLEN_P(type) TSRMLS_CC)) {
			RETURN_CCTOR(component);
		} else {
			ZEPHIR_INIT_VAR(class_name);
			zephir_get_class(class_name, component, 0 TSRMLS_CC);
			_0 = zephir_fetch_nproperty_this(reference, SL("id"), PH_NOISY_CC);
			ZEPHIR_CPY_WRT(id, _0);
			ZEPHIR_INIT_VAR(message);
			ZEPHIR_CONCAT_SVS(message, "\"", id, "\" refers to a ");
			ZEPHIR_INIT_VAR(_1);
			ZEPHIR_CONCAT_VSVS(_1, class_name, " component. ", type, " is expected.");
			zephir_concat_self(&message, _1 TSRMLS_CC);
			ZEPHIR_INIT_VAR(_2);
			object_init_ex(_2, yii_base_invalidconfigexception_ce);
			ZEPHIR_CALL_METHOD(NULL, _2, "__construct", NULL, message);
			zephir_check_call_status();
			zephir_throw_exception_debug(_2, "yii/di/Instance.zep", 142 TSRMLS_CC);
			ZEPHIR_MM_RESTORE();
			return;
		}
	}
	if (Z_TYPE_P(reference) == IS_OBJECT) {
		ZEPHIR_INIT_VAR(valueType);
		zephir_get_class(valueType, reference, 0 TSRMLS_CC);
	} else {
		ZEPHIR_INIT_BNVAR(valueType);
		zephir_gettype(valueType, reference TSRMLS_CC);
	}
	ZEPHIR_INIT_LNVAR(_1);
	object_init_ex(_1, yii_base_invalidconfigexception_ce);
	ZEPHIR_INIT_LNVAR(_2);
	ZEPHIR_CONCAT_SVVS(_2, "Invalid data type: ", valueType, type, " is expected.");
	ZEPHIR_CALL_METHOD(NULL, _1, "__construct", NULL, _2);
	zephir_check_call_status();
	zephir_throw_exception_debug(_1, "yii/di/Instance.zep", 148 TSRMLS_CC);
	ZEPHIR_MM_RESTORE();
	return;

}

/**
 * Returns the actual object referenced by this Instance object.
 * @param ServiceLocator|Container $container the container used to locate the referenced object.
 * If null, the method will first try `Yii::$app` then `Yii::$container`.
 * @return object the actual object referenced by this Instance object.
 */
PHP_METHOD(yii_di_Instance, get) {

	zend_bool _1;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *container = NULL, *_0, *_2, *_3 = NULL, *_4, *_5, *_6;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &container);

	if (!container) {
		container = ZEPHIR_GLOBAL(global_null);
	}


	if (Z_TYPE_P(container) == IS_OBJECT) {
		_0 = zephir_fetch_nproperty_this(this_ptr, SL("id"), PH_NOISY_CC);
		ZEPHIR_RETURN_CALL_METHOD(container, "get", NULL, _0);
		zephir_check_call_status();
		RETURN_MM();
	}
	zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
	_1 = Z_TYPE_P(_0) == IS_OBJECT;
	if (_1) {
		zephir_read_static_property_ce(&_2, yii_baseyii_ce, SL("app") TSRMLS_CC);
		_4 = zephir_fetch_nproperty_this(this_ptr, SL("id"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(&_3, _2, "has", NULL, _4);
		zephir_check_call_status();
		_1 = zephir_is_true(_3);
	}
	if (_1) {
		zephir_read_static_property_ce(&_5, yii_baseyii_ce, SL("app") TSRMLS_CC);
		_6 = zephir_fetch_nproperty_this(this_ptr, SL("id"), PH_NOISY_CC);
		ZEPHIR_RETURN_CALL_METHOD(_5, "get", NULL, _6);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("container") TSRMLS_CC);
		_5 = zephir_fetch_nproperty_this(this_ptr, SL("id"), PH_NOISY_CC);
		ZEPHIR_RETURN_CALL_METHOD(_0, "get", NULL, _5);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_MM_RESTORE();

}

