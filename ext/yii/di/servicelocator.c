
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/object.h"
#include "kernel/array.h"
#include "Zend/zend_closures.h"
#include "kernel/exception.h"
#include "kernel/concat.h"
#include "kernel/hash.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * ServiceLocator implements a [service locator](http://en.wikipedia.org/wiki/Service_locator_pattern).
 *
 * To use ServiceLocator, you first need to register component IDs with the corresponding component
 * definitions with the locator by calling [[set()]] or [[setComponents()]].
 * You can then call [[get()]] to retrieve a component with the specified ID. The locator will automatically
 * instantiate and configure the component according to the definition.
 *
 * For example,
 *
 * ```php
 * $locator = new \yii\di\ServiceLocator;
 * $locator->setComponents([
 *     'db' => [
 *         'class' => 'yii\db\Connection',
 *         'dsn' => 'sqlite:path/to/file.db',
 *     ],
 *     'cache' => [
 *         'class' => 'yii\caching\DbCache',
 *         'db' => 'db',
 *     ],
 * ]);
 *
 * $db = $locator->get('db');  // or $locator->db
 * $cache = $locator->get('cache');  // or $locator->cache
 * ```
 *
 * Because [[\yii\base\Module]] extends from ServiceLocator, modules and the application are all service locators.
 *
 * @property array $components The list of the component definitions or the loaded component instances (ID =>
 * definition or instance). This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_di_ServiceLocator) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\di, ServiceLocator, yii, di_servicelocator, yii_base_component_ce, yii_di_servicelocator_method_entry, 0);

	/**
	 * @var array shared component instances indexed by their IDs
	 */
	zend_declare_property_null(yii_di_servicelocator_ce, SL("_components"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array component definitions indexed by their IDs
	 */
	zend_declare_property_null(yii_di_servicelocator_ce, SL("_definitions"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Getter magic method.
 * This method is overridden to support accessing components like reading properties.
 * @param string $name component or property name
 * @return mixed the named property value
 */
PHP_METHOD(yii_di_ServiceLocator, __get) {

	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *_0 = NULL;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &name_param);

	zephir_get_strval(name, name_param);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "has", NULL, name);
	zephir_check_call_status();
	if (zephir_is_true(_0)) {
		ZEPHIR_RETURN_CALL_METHOD(this_ptr, "get", NULL, name);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		ZEPHIR_RETURN_CALL_PARENT(yii_di_servicelocator_ce, this_ptr, "__get", &_1, name);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Checks if a property value is null.
 * This method overrides the parent implementation by checking if the named component is loaded.
 * @param string $name the property name or the event name
 * @return boolean whether the property value is null
 */
PHP_METHOD(yii_di_ServiceLocator, __isset) {

	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *_0 = NULL;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &name_param);

	zephir_get_strval(name, name_param);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "has", NULL, name, ZEPHIR_GLOBAL(global_true));
	zephir_check_call_status();
	if (zephir_is_true(_0)) {
		RETURN_MM_BOOL(1);
	} else {
		ZEPHIR_RETURN_CALL_PARENT(yii_di_servicelocator_ce, this_ptr, "__isset", &_1, name);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns a value indicating whether the locator has the specified component definition or has instantiated the component.
 * This method may return different results depending on the value of `$checkInstance`.
 *
 * - If `$checkInstance` is false (default), the method will return a value indicating whether the locator has the specified
 *   component definition.
 * - If `$checkInstance` is true, the method will return a value indicating whether the locator has
 *   instantiated the specified component.
 *
 * @param string $id component ID (e.g. `db`).
 * @param boolean $checkInstance whether the method should check if the component is shared and instantiated.
 * @return boolean whether the locator has the specified component definition or has instantiated the component.
 * @see set()
 */
PHP_METHOD(yii_di_ServiceLocator, has) {

	zval *id_param = NULL, *checkInstance = NULL, *components, *_0;
	zval *id = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &id_param, &checkInstance);

	zephir_get_strval(id, id_param);
	if (!checkInstance) {
		checkInstance = ZEPHIR_GLOBAL(global_false);
	}


	ZEPHIR_OBS_VAR(components);
	zephir_read_property_this(&components, this_ptr, SL("_components"), PH_NOISY_CC);
	if (Z_TYPE_P(components) != IS_ARRAY) {
		ZEPHIR_INIT_BNVAR(components);
		array_init(components);
		zephir_update_property_this(this_ptr, SL("_components"), components TSRMLS_CC);
	}
	if (zephir_is_true(checkInstance)) {
		_0 = zephir_fetch_nproperty_this(this_ptr, SL("_components"), PH_NOISY_CC);
		RETURN_MM_BOOL(zephir_array_isset(_0, id));
	} else {
		_0 = zephir_fetch_nproperty_this(this_ptr, SL("_definitions"), PH_NOISY_CC);
		RETURN_MM_BOOL(zephir_array_isset(_0, id));
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the component instance with the specified ID.
 *
 * @param string $id component ID (e.g. `db`).
 * @param boolean $throwException whether to throw an exception if `$id` is not registered with the locator before.
 * @return object|null the component of the specified ID. If `$throwException` is false and `$id`
 * is not registered before, null will be returned.
 * @throws InvalidConfigException if `$id` refers to a nonexistent component ID
 * @see has()
 * @see set()
 */
PHP_METHOD(yii_di_ServiceLocator, get) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL;
	zend_bool _1;
	zval *id_param = NULL, *throwException = NULL, *components, *component, *definition, *copy_definition = NULL, *copy_object = NULL, *object = NULL, *_0, *_3;
	zval *id = NULL, *_4;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &id_param, &throwException);

	zephir_get_strval(id, id_param);
	if (!throwException) {
		throwException = ZEPHIR_GLOBAL(global_true);
	}


	ZEPHIR_OBS_VAR(components);
	zephir_read_property_this(&components, this_ptr, SL("_components"), PH_NOISY_CC);
	if (Z_TYPE_P(components) != IS_ARRAY) {
		ZEPHIR_INIT_BNVAR(components);
		array_init(components);
	}
	ZEPHIR_OBS_VAR(component);
	if (zephir_array_isset_fetch(&component, components, id, 0 TSRMLS_CC)) {
		RETURN_CCTOR(component);
	}
	ZEPHIR_OBS_VAR(definition);
	_0 = zephir_fetch_nproperty_this(this_ptr, SL("_definitions"), PH_NOISY_CC);
	if (zephir_array_isset_fetch(&definition, _0, id, 0 TSRMLS_CC)) {
		ZEPHIR_CPY_WRT(copy_definition, definition);
		_1 = Z_TYPE_P(definition) == IS_OBJECT;
		if (_1) {
			_1 = !(zephir_instance_of_ev(definition, zend_ce_closure TSRMLS_CC));
		}
		if (_1) {
			zephir_array_update_zval(&components, id, &copy_definition, PH_COPY | PH_SEPARATE);
			zephir_update_property_this(this_ptr, SL("_components"), components TSRMLS_CC);
			RETURN_CCTOR(definition);
		} else {
			ZEPHIR_CALL_CE_STATIC(&object, yii_baseyii_ce, "createobject", &_2, copy_definition);
			zephir_check_call_status();
			ZEPHIR_CPY_WRT(copy_object, object);
			zephir_array_update_zval(&components, id, &copy_object, PH_COPY | PH_SEPARATE);
			zephir_update_property_this(this_ptr, SL("_components"), components TSRMLS_CC);
			RETURN_CCTOR(object);
		}
	} else {
		if (zephir_is_true(throwException)) {
			ZEPHIR_INIT_VAR(_3);
			object_init_ex(_3, yii_base_invalidconfigexception_ce);
			ZEPHIR_INIT_VAR(_4);
			ZEPHIR_CONCAT_SV(_4, "Unknown component ID: ", id);
			ZEPHIR_CALL_METHOD(NULL, _3, "__construct", NULL, _4);
			zephir_check_call_status();
			zephir_throw_exception_debug(_3, "yii/di/ServiceLocator.zep", 163 TSRMLS_CC);
			ZEPHIR_MM_RESTORE();
			return;
		} else {
			RETURN_MM_NULL();
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Registers a component definition with this locator.
 *
 * For example,
 *
 * ```php
 * // a class name
 * $locator->set('cache', 'yii\caching\FileCache');
 *
 * // a configuration array
 * $locator->set('db', [
 *     'class' => 'yii\db\Connection',
 *     'dsn' => 'mysql:host=127.0.0.1;dbname=demo',
 *     'username' => 'root',
 *     'password' => '',
 *     'charset' => 'utf8',
 * ]);
 *
 * // an anonymous function
 * $locator->set('cache', function ($params) {
 *     return new \yii\caching\FileCache;
 * });
 *
 * // an instance
 * $locator->set('cache', new \yii\caching\FileCache);
 * ```
 *
 * If a component definition with the same ID already exists, it will be overwritten.
 *
 * @param string $id component ID (e.g. `db`).
 * @param mixed $definition the component definition to be registered with this locator.
 * It can be one of the followings:
 *
 * - a class name
 * - a configuration array: the array contains name-value pairs that will be used to
 *   initialize the property values of the newly created object when [[get()]] is called.
 *   The `class` element is required and stands for the the class of the object to be created.
 * - a PHP callable: either an anonymous function or an array representing a class method (e.g. `['Foo', 'bar']`).
 *   The callable will be called by [[get()]] to return an object associated with the specified component ID.
 * - an object: When [[get()]] is called, this object will be returned.
 *
 * @throws InvalidConfigException if the definition is an invalid configuration array
 */
PHP_METHOD(yii_di_ServiceLocator, set) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL;
	zend_bool _0;
	zval *id_param = NULL, *definition, *components, *definitions, *_1 = NULL, *_3 = NULL, *_5, *_6;
	zval *id = NULL, *text, *_4;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &id_param, &definition);

	zephir_get_strval(id, id_param);
	ZEPHIR_INIT_VAR(text);
	ZVAL_STRING(text, "class", 1);


	ZEPHIR_OBS_VAR(components);
	zephir_read_property_this(&components, this_ptr, SL("_components"), PH_NOISY_CC);
	ZEPHIR_OBS_VAR(definitions);
	zephir_read_property_this(&definitions, this_ptr, SL("_definitions"), PH_NOISY_CC);
	if (Z_TYPE_P(components) != IS_ARRAY) {
		ZEPHIR_INIT_BNVAR(components);
		array_init(components);
	}
	if (Z_TYPE_P(definitions) != IS_ARRAY) {
		ZEPHIR_INIT_BNVAR(definitions);
		array_init(definitions);
	}
	if (Z_TYPE_P(definition) == IS_NULL) {
		zephir_array_unset(&components, id, PH_SEPARATE);
		zephir_array_unset(&definitions, id, PH_SEPARATE);
		zephir_update_property_this(this_ptr, SL("_components"), components TSRMLS_CC);
		zephir_update_property_this(this_ptr, SL("_definitions"), definitions TSRMLS_CC);
		RETURN_MM_NULL();
	}
	_0 = Z_TYPE_P(definition) == IS_OBJECT;
	if (!(_0)) {
		ZEPHIR_CALL_FUNCTION(&_1, "is_callable", &_2, definition, ZEPHIR_GLOBAL(global_true));
		zephir_check_call_status();
		_0 = zephir_is_true(_1);
	}
	if (_0) {
		zephir_array_update_zval(&definitions, id, &definition, PH_COPY | PH_SEPARATE);
		zephir_update_property_this(this_ptr, SL("_definitions"), definitions TSRMLS_CC);
	} else {
		if (Z_TYPE_P(definition) == IS_ARRAY) {
			if (zephir_array_isset_string(definition, SS("class"))) {
				zephir_array_update_zval(&definitions, id, &definition, PH_COPY | PH_SEPARATE);
				zephir_update_property_this(this_ptr, SL("_definitions"), definitions TSRMLS_CC);
			} else {
				ZEPHIR_INIT_VAR(_3);
				object_init_ex(_3, yii_base_invalidconfigexception_ce);
				ZEPHIR_INIT_VAR(_4);
				ZEPHIR_CONCAT_SVSVS(_4, "The configuration for the \"", id, "\" component must contain a \"", text, "\" element.");
				ZEPHIR_CALL_METHOD(NULL, _3, "__construct", NULL, _4);
				zephir_check_call_status();
				zephir_throw_exception_debug(_3, "yii/di/ServiceLocator.zep", 248 TSRMLS_CC);
				ZEPHIR_MM_RESTORE();
				return;
			}
		} else {
			ZEPHIR_INIT_LNVAR(_3);
			object_init_ex(_3, yii_base_invalidconfigexception_ce);
			ZEPHIR_INIT_VAR(_5);
			zephir_gettype(_5, definition TSRMLS_CC);
			ZEPHIR_INIT_VAR(_6);
			ZEPHIR_CONCAT_SVSV(_6, "Unexpected configuration type for the \"", id, "\" component: ", _5);
			ZEPHIR_CALL_METHOD(NULL, _3, "__construct", NULL, _6);
			zephir_check_call_status();
			zephir_throw_exception_debug(_3, "yii/di/ServiceLocator.zep", 251 TSRMLS_CC);
			ZEPHIR_MM_RESTORE();
			return;
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Removes the component from the locator.
 * @param string $id the component ID
 */
PHP_METHOD(yii_di_ServiceLocator, clear) {

	zval *id_param = NULL, *_0, *_1;
	zval *id = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &id_param);

	zephir_get_strval(id, id_param);


	_0 = zephir_fetch_nproperty_this(this_ptr, SL("_definitions"), PH_NOISY_CC);
	zephir_array_unset(&_0, id, PH_SEPARATE);
	_1 = zephir_fetch_nproperty_this(this_ptr, SL("_components"), PH_NOISY_CC);
	zephir_array_unset(&_1, id, PH_SEPARATE);
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the list of the component definitions or the loaded component instances.
 * @param boolean $returnDefinitions whether to return component definitions instead of the loaded component instances.
 * @return array the list of the component definitions or the loaded component instances (ID => definition or instance).
 */
PHP_METHOD(yii_di_ServiceLocator, getComponents) {

	zval *returnDefinitions_param = NULL, *components, *definitions, *_0 = NULL;
	zend_bool returnDefinitions;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &returnDefinitions_param);

	if (!returnDefinitions_param) {
		returnDefinitions = 1;
	} else {
		returnDefinitions = zephir_get_boolval(returnDefinitions_param);
	}


	components = zephir_fetch_nproperty_this(this_ptr, SL("_components"), PH_NOISY_CC);
	definitions = zephir_fetch_nproperty_this(this_ptr, SL("_definitions"), PH_NOISY_CC);
	if (Z_TYPE_P(components) != IS_ARRAY) {
		ZEPHIR_INIT_VAR(_0);
		array_init(_0);
		zephir_update_property_this(this_ptr, SL("_components"), _0 TSRMLS_CC);
	}
	if (Z_TYPE_P(definitions) != IS_ARRAY) {
		ZEPHIR_INIT_NVAR(_0);
		array_init(_0);
		zephir_update_property_this(this_ptr, SL("_definitions"), _0 TSRMLS_CC);
	}
	ZEPHIR_INIT_NVAR(_0);
	if (returnDefinitions) {
		ZEPHIR_CPY_WRT(_0, definitions);
	} else {
		ZEPHIR_CPY_WRT(_0, components);
	}
	RETURN_CCTOR(_0);

}

/**
 * Registers a set of component definitions in this locator.
 *
 * This is the bulk version of [[set()]]. The parameter should be an array
 * whose keys are component IDs and values the corresponding component definitions.
 *
 * For more details on how to specify component IDs and definitions, please refer to [[set()]].
 *
 * If a component definition with the same ID already exists, it will be overwritten.
 *
 * The following is an example for registering two component definitions:
 *
 * ```php
 * [
 *     'db' => [
 *         'class' => 'yii\db\Connection',
 *         'dsn' => 'sqlite:path/to/file.db',
 *     ],
 *     'cache' => [
 *         'class' => 'yii\caching\DbCache',
 *         'db' => 'db',
 *     ],
 * ]
 * ```
 *
 * @param array $components component definitions or instances
 */
PHP_METHOD(yii_di_ServiceLocator, setComponents) {

	zephir_fcall_cache_entry *_3 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	HashTable *_1;
	HashPosition _0;
	zval *components, *id = NULL, *component = NULL, **_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &components);



	zephir_is_iterable(components, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(id, _1, _0);
		ZEPHIR_GET_HVALUE(component, _2);
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "set", &_3, id, component);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

