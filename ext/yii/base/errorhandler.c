
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/array.h"
#include "kernel/object.h"
#include "kernel/operators.h"
#include "kernel/exit.h"
#include "kernel/concat.h"
#include "kernel/variables.h"
#include "kernel/hash.h"
#include "kernel/exception.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * ErrorHandler handles uncaught PHP errors and exceptions.
 *
 * ErrorHandler is configured as an application component in [[\yii\base\Application]] by default.
 * You can access that instance via `Yii::$app->errorHandler`.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 * @author Carsten Brandt <mail@cebe.cc>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_ErrorHandler) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, ErrorHandler, yii, base_errorhandler, yii_base_component_ce, yii_base_errorhandler_method_entry, ZEND_ACC_EXPLICIT_ABSTRACT_CLASS);

	/**
	 * @var boolean whether to discard any existing page output before error display. Defaults to true.
	 */
	zend_declare_property_bool(yii_base_errorhandler_ce, SL("discardExistingOutput"), 1, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var integer the size of the reserved memory. A portion of memory is pre-allocated so that
	 * when an out-of-memory issue occurs, the error handler is able to handle the error with
	 * the help of this reserved memory. If you set this value to be 0, no memory will be reserved.
	 * Defaults to 256KB.
	 */
	zend_declare_property_long(yii_base_errorhandler_ce, SL("memoryReserveSize"), 262144, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var \Exception the exception that is being handled currently.
	 */
	zend_declare_property_null(yii_base_errorhandler_ce, SL("exception"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string Used to reserve memory for fatal error handler.
	 */
	zend_declare_property_null(yii_base_errorhandler_ce, SL("_memoryReserve"), ZEND_ACC_PROTECTED TSRMLS_CC);

	return SUCCESS;

}

/**
 * Register this error handler
 */
PHP_METHOD(yii_base_ErrorHandler, register) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL, *_3 = NULL, *_4 = NULL, *_5 = NULL;
	zval _0, *handleException, *handleError, *handleFatalError, *_2;

	ZEPHIR_MM_GROW();
	ZEPHIR_INIT_VAR(handleException);
	array_init(handleException);
	ZEPHIR_INIT_VAR(handleError);
	array_init(handleError);
	ZEPHIR_INIT_VAR(handleFatalError);
	array_init(handleFatalError);

	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "display_errors", 0);
	ZEPHIR_CALL_FUNCTION(NULL, "ini_set", &_1, &_0, ZEPHIR_GLOBAL(global_false));
	zephir_check_call_status();
	zephir_array_append(&handleException, this_ptr, PH_SEPARATE);
	ZEPHIR_INIT_VAR(_2);
	ZVAL_STRING(_2, "handleException", 1);
	zephir_array_append(&handleException, _2, PH_SEPARATE);
	zephir_array_append(&handleError, this_ptr, PH_SEPARATE);
	ZEPHIR_INIT_BNVAR(_2);
	ZVAL_STRING(_2, "handleError", 1);
	zephir_array_append(&handleError, _2, PH_SEPARATE);
	zephir_array_append(&handleFatalError, this_ptr, PH_SEPARATE);
	ZEPHIR_INIT_BNVAR(_2);
	ZVAL_STRING(_2, "handleFatalError", 1);
	zephir_array_append(&handleFatalError, _2, PH_SEPARATE);
	ZEPHIR_CALL_FUNCTION(NULL, "set_exception_handler", &_3, handleException);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(NULL, "set_error_handler", &_4, handleError);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(NULL, "register_shutdown_function", &_5, handleFatalError);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Handles uncaught PHP exceptions.
 *
 * This method is implemented as a PHP exception handler.
 *
 * @param \Exception $exception the exception that is not caught
 */
PHP_METHOD(yii_base_ErrorHandler, handleException) {

	zval *_5 = NULL, *_6 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_1 = NULL, *_10 = NULL, *_12 = NULL;
	zval *obj_exception, *e = NULL, *msg = NULL, *_2, *_3 = NULL, _4 = zval_used_for_init, *_7 = NULL, *_8, *_9 = NULL, *_11, *_SERVER;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &obj_exception);



	if (zephir_instance_of_ev(obj_exception, yii_base_exitexception_ce TSRMLS_CC)) {
		RETURN_MM_NULL();
	}
	zephir_update_property_this(this_ptr, SL("exception"), obj_exception TSRMLS_CC);
	ZEPHIR_CALL_FUNCTION(NULL, "restore_error_handler", &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(NULL, "restore_exception_handler", &_1);
	zephir_check_call_status();

	/* try_start_1: */

		ZEPHIR_CALL_METHOD(NULL, this_ptr, "logexception", NULL, obj_exception);
		zephir_check_call_status_or_jump(try_end_1);
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("discardExistingOutput"), PH_NOISY_CC);
		if (zephir_is_true(_2)) {
			ZEPHIR_CALL_METHOD(NULL, this_ptr, "clearoutput", NULL);
			zephir_check_call_status_or_jump(try_end_1);
		}
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "renderexception", NULL, obj_exception);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_INIT_VAR(_3);
		ZEPHIR_GET_CONSTANT(_3, "YII_ENV_TEST");
		if (!(zephir_is_true(_3))) {
			ZEPHIR_SINIT_VAR(_4);
			ZVAL_LONG(&_4, 1);
			zephir_exit(&_4);
			ZEPHIR_MM_RESTORE();
		}

	try_end_1:

	ZEPHIR_CPY_WRT(e, EG(exception));
	if (zephir_is_instance_of(e, SL("Exception") TSRMLS_CC)) {
		zend_clear_exception(TSRMLS_C);
		zephir_get_strval(_5, e);
		ZEPHIR_CPY_WRT(msg, _5);
		zephir_concat_self_str(&msg, SL("\nPrevious exception:\n") TSRMLS_CC);
		zephir_get_strval(_6, obj_exception);
		zephir_concat_self(&msg, _6 TSRMLS_CC);
		ZEPHIR_INIT_NVAR(_3);
		ZEPHIR_GET_CONSTANT(_3, "YII_DEBUG");
		if (zephir_is_true(_3)) {
			ZEPHIR_INIT_NVAR(_3);
			ZEPHIR_GET_CONSTANT(_3, "PHP_SAPI");
			if (ZEPHIR_IS_STRING(_3, "cli")) {
				ZEPHIR_INIT_VAR(_7);
				ZEPHIR_CONCAT_VS(_7, msg, "\n");
				zend_print_zval(_7, 0);
			} else {
				zephir_read_static_property_ce(&_2, yii_baseyii_ce, SL("app") TSRMLS_CC);
				ZEPHIR_OBS_VAR(_8);
				zephir_read_property(&_8, _2, SL("charset"), PH_NOISY_CC);
				ZEPHIR_SINIT_NVAR(_4);
				ZVAL_LONG(&_4, 3);
				ZEPHIR_CALL_FUNCTION(&_9, "htmlspecialchars", &_10, msg, &_4, _8);
				zephir_check_call_status();
				ZEPHIR_INIT_LNVAR(_7);
				ZEPHIR_CONCAT_SVS(_7, "<pre>", _9, "</pre>");
				zend_print_zval(_7, 0);
			}
		}
		ZEPHIR_INIT_VAR(_11);
		zephir_get_global(&_SERVER, SS("_SERVER") TSRMLS_CC);
		zephir_var_export_ex(_11, &(_SERVER) TSRMLS_CC);
		ZEPHIR_INIT_LNVAR(_7);
		ZEPHIR_CONCAT_SV(_7, "\n\$_SERVER = ", _11);
		zephir_concat_self(&msg, _7 TSRMLS_CC);
		ZEPHIR_CALL_FUNCTION(NULL, "error_log", &_12, msg);
		zephir_check_call_status();
		ZEPHIR_SINIT_NVAR(_4);
		ZVAL_LONG(&_4, 1);
		zephir_exit(&_4);
		ZEPHIR_MM_RESTORE();
	}
	zephir_update_property_this(this_ptr, SL("exception"), ZEPHIR_GLOBAL(global_null) TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Handles PHP execution errors such as warnings and notices.
 *
 * This method is used as a PHP error handler. It will simply raise an [[ErrorException]].
 *
 * @param integer $code the level of the error raised.
 * @param string $message the error message.
 * @param string $file the filename that the error was raised in.
 * @param integer $line the line number the error was raised at.
 *
 * @throws ErrorException
 */
PHP_METHOD(yii_base_ErrorHandler, handleError) {

	zephir_fcall_cache_entry *_8 = NULL;
	HashTable *_5;
	HashPosition _4;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_2 = NULL, *_3 = NULL;
	zval *code, *message, *file, *line, *error_reporting = NULL, *obj_exception, *trace = NULL, *frame = NULL, _1 = zval_used_for_init, **_6, *_7;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 4, 0, &code, &message, &file, &line);



	ZEPHIR_CALL_FUNCTION(&error_reporting, "error_reporting", &_0);
	zephir_check_call_status();
	ZEPHIR_SINIT_VAR(_1);
	zephir_bitwise_and_function(&_1, error_reporting, code TSRMLS_CC);
	if (zephir_is_true(&_1)) {
		ZEPHIR_INIT_VAR(obj_exception);
		object_init_ex(obj_exception, yii_base_errorexception_ce);
		ZEPHIR_CALL_METHOD(NULL, obj_exception, "__construct", NULL, message, code, code, file, line);
		zephir_check_call_status();
		ZEPHIR_SINIT_NVAR(_1);
		ZVAL_LONG(&_1, 0);
		ZEPHIR_CALL_FUNCTION(&trace, "debug_backtrace", &_2, &_1);
		zephir_check_call_status();
		Z_SET_ISREF_P(trace);
		ZEPHIR_CALL_FUNCTION(NULL, "array_shift", &_3, trace);
		Z_UNSET_ISREF_P(trace);
		zephir_check_call_status();
		zephir_is_iterable(trace, &_5, &_4, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_5, (void**) &_6, &_4) == SUCCESS
		  ; zephir_hash_move_forward_ex(_5, &_4)
		) {
			ZEPHIR_GET_HVALUE(frame, _6);
			zephir_array_fetch_string(&_7, frame, SL("function"), PH_NOISY | PH_READONLY TSRMLS_CC);
			if (ZEPHIR_IS_STRING(_7, "__toString")) {
				ZEPHIR_CALL_METHOD(NULL, this_ptr, "handleexception", &_8, obj_exception);
				zephir_check_call_status();
				ZEPHIR_SINIT_NVAR(_1);
				ZVAL_LONG(&_1, 1);
				zephir_exit(&_1);
				ZEPHIR_MM_RESTORE();
			}
		}
		zephir_throw_exception_debug(obj_exception, "yii/base/ErrorHandler.zep", 147 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Handles fatal PHP errors
 */
PHP_METHOD(yii_base_ErrorHandler, handleFatalError) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_2 = NULL, *_9 = NULL;
	zval *error = NULL, *obj_exception, *_1 = NULL, *_3, *_4, *_5, *_6, *_7, *_8, _10 = zval_used_for_init, *_11;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_FUNCTION(&error, "error_get_last", &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_CE_STATIC(&_1, yii_base_errorexception_ce, "isfatalerror", &_2, error);
	zephir_check_call_status();
	if (zephir_is_true(_1)) {
		ZEPHIR_INIT_VAR(obj_exception);
		object_init_ex(obj_exception, yii_base_errorexception_ce);
		zephir_array_fetch_string(&_3, error, SL("message"), PH_NOISY | PH_READONLY TSRMLS_CC);
		zephir_array_fetch_string(&_4, error, SL("type"), PH_NOISY | PH_READONLY TSRMLS_CC);
		zephir_array_fetch_string(&_5, error, SL("type"), PH_NOISY | PH_READONLY TSRMLS_CC);
		zephir_array_fetch_string(&_6, error, SL("file"), PH_NOISY | PH_READONLY TSRMLS_CC);
		zephir_array_fetch_string(&_7, error, SL("line"), PH_NOISY | PH_READONLY TSRMLS_CC);
		ZEPHIR_CALL_METHOD(NULL, obj_exception, "__construct", NULL, _3, _4, _5, _6, _7);
		zephir_check_call_status();
		zephir_update_property_this(this_ptr, SL("exception"), obj_exception TSRMLS_CC);
		ZEPHIR_INIT_VAR(_8);
		ZEPHIR_GET_CONSTANT(_8, "PHP_SAPI");
		if (ZEPHIR_IS_STRING(_8, "cli")) {
			ZEPHIR_CALL_FUNCTION(NULL, "error_log", &_9, obj_exception);
			zephir_check_call_status();
			ZEPHIR_SINIT_VAR(_10);
			ZVAL_LONG(&_10, 1);
			zephir_exit(&_10);
			ZEPHIR_MM_RESTORE();
		}
		_11 = zephir_fetch_nproperty_this(this_ptr, SL("discardExistingOutput"), PH_NOISY_CC);
		if (zephir_is_true(_11)) {
			ZEPHIR_CALL_METHOD(NULL, this_ptr, "clearoutput", NULL);
			zephir_check_call_status();
		}
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "renderexception", NULL, obj_exception);
		zephir_check_call_status();
		ZEPHIR_SINIT_NVAR(_10);
		ZVAL_LONG(&_10, 1);
		zephir_exit(&_10);
		ZEPHIR_MM_RESTORE();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Renders the exception.
 * @param \Exception $exception the exception to be rendered.
 */
PHP_METHOD(yii_base_ErrorHandler, renderException) {

}

/**
 * Logs the given exception
 * @param \Exception $exception the exception to be logged
 */
PHP_METHOD(yii_base_ErrorHandler, logException) {

	zval *_4 = NULL;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *obj_exception, *category, *_0, *_1 = NULL, *_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &obj_exception);



	ZEPHIR_INIT_VAR(category);
	zephir_get_class(category, obj_exception, 0 TSRMLS_CC);
	if (zephir_is_instance_of(obj_exception, SL("yii\\web\\HttpException") TSRMLS_CC)) {
		ZEPHIR_OBS_VAR(_0);
		zephir_read_property(&_0, obj_exception, SL("statusCode"), PH_NOISY_CC);
		ZEPHIR_INIT_BNVAR(category);
		ZEPHIR_CONCAT_SV(category, "yii\\web\\HttpException:", _0);
	} else {
		if (zephir_instance_of_ev(obj_exception, zend_get_error_exception(TSRMLS_C) TSRMLS_CC)) {
			ZEPHIR_CALL_METHOD(&_1, obj_exception, "getseverity",  NULL);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(_2);
			ZEPHIR_CONCAT_SV(_2, ":", _1);
			zephir_concat_self(&category, _2 TSRMLS_CC);
		}
	}
	zephir_get_strval(_4, obj_exception);
	ZEPHIR_CALL_CE_STATIC(NULL, yii_baseyii_ce, "error", &_3, _4, category);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Removes all output echoed before calling this method.
 */
PHP_METHOD(yii_base_ErrorHandler, clearOutput) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_1 = NULL, *_2 = NULL;
	zval *level = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_FUNCTION(&level, "ob_get_level", &_0);
	zephir_check_call_status();
	while (1) {
		if (!(ZEPHIR_GT_LONG(level, 0))) {
			break;
		}
		ZEPHIR_CALL_FUNCTION(NULL, "ob_clean", &_1);
		zephir_check_call_status();
		ZEPHIR_CALL_FUNCTION(NULL, "ob_end_clean", &_2);
		zephir_check_call_status();
		ZEPHIR_SUB_ASSIGN(level, level);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Converts an exception into a PHP error.
 *
 * This method can be used to convert exceptions inside of methods like `__toString()`
 * to PHP errors because exceptions cannot be thrown inside of them.
 * @param \Exception $exception the exception to convert to a PHP error.
 */
PHP_METHOD(yii_base_ErrorHandler, convertExceptionToError) {

	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *obj_exception, *_0 = NULL, _1;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &obj_exception);



	ZEPHIR_CALL_SELF(&_0, "convertexceptiontostring", NULL, obj_exception);
	zephir_check_call_status();
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_LONG(&_1, 256);
	ZEPHIR_CALL_FUNCTION(NULL, "trigger_error", &_2, _0, &_1);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Converts an exception into a simple string.
 * @param \Exception $exception the exception being converted
 * @return string the string representation of the exception.
 */
PHP_METHOD(yii_base_ErrorHandler, convertExceptionToString) {

	int ZEPHIR_LAST_CALL_STATUS;
	zend_bool _0, _1;
	zval *obj_exception, *message = NULL, *_2, *_3 = NULL, *_4 = NULL, *_5 = NULL, *_6, *_7 = NULL, *_8 = NULL, *_9, *_10 = NULL, *_11;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &obj_exception);



	_0 = (zephir_is_instance_of(obj_exception, SL("yii\base\\Exception") TSRMLS_CC));
	if (_0) {
		_1 = (zephir_is_instance_of(obj_exception, SL("yii\\base\\UserException") TSRMLS_CC));
		if (!(_1)) {
			ZEPHIR_INIT_VAR(_2);
			ZEPHIR_GET_CONSTANT(_2, "YII_DEBUG");
			_1 = !zephir_is_true(_2);
		}
		_0 = _1;
	}
	if (_0) {
		ZEPHIR_CALL_METHOD(&_3, obj_exception, "getname",  NULL);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&_4, obj_exception, "getmessage",  NULL);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(message);
		ZEPHIR_CONCAT_VSV(message, _3, ": ", _4);
	} else {
		ZEPHIR_INIT_VAR(_5);
		ZEPHIR_GET_CONSTANT(_5, "YII_DEBUG");
		if (zephir_is_true(_5)) {
			if (zephir_instance_of_ev(message, yii_base_exception_ce TSRMLS_CC)) {
				ZEPHIR_CALL_METHOD(&_3, obj_exception, "getname",  NULL);
				zephir_check_call_status();
				ZEPHIR_INIT_NVAR(message);
				ZEPHIR_CONCAT_SVS(message, "Exception (", _3, ")");
			} else {
				if (zephir_instance_of_ev(obj_exception, yii_base_errorexception_ce TSRMLS_CC)) {
					ZEPHIR_CALL_METHOD(&message, obj_exception, "getname",  NULL);
					zephir_check_call_status();
				} else {
					ZEPHIR_INIT_NVAR(message);
					ZVAL_STRING(message, "Exception", 1);
				}
			}
			ZEPHIR_INIT_NVAR(_5);
			zephir_get_class(_5, obj_exception, 0 TSRMLS_CC);
			ZEPHIR_CALL_METHOD(&_4, obj_exception, "getmessage",  NULL);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(_6);
			ZEPHIR_CONCAT_SVSVS(_6, " '", _5, "' with message '", _4, "' \n\nin ");
			zephir_concat_self(&message, _6 TSRMLS_CC);
			ZEPHIR_CALL_METHOD(&_7, obj_exception, "getfile",  NULL);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(&_8, obj_exception, "getline",  NULL);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(_9);
			ZEPHIR_CONCAT_VSVS(_9, _7, ":", _8, "\n\n");
			zephir_concat_self(&message, _9 TSRMLS_CC);
			ZEPHIR_CALL_METHOD(&_10, obj_exception, "gettraceasstring",  NULL);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(_11);
			ZEPHIR_CONCAT_SV(_11, "Stack trace:\n", _10);
			zephir_concat_self(&message, _11 TSRMLS_CC);
		} else {
			ZEPHIR_CALL_METHOD(&_7, obj_exception, "getmessage",  NULL);
			zephir_check_call_status();
			ZEPHIR_INIT_NVAR(message);
			ZEPHIR_CONCAT_SV(message, "Error: ", _7);
		}
	}
	RETURN_CCTOR(message);

}

