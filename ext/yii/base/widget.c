
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/array.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/exception.h"
#include "kernel/concat.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Widget is the base class for widgets.
 *
 * @property string $id ID of the widget.
 * @property \yii\web\View $view The view object that can be used to render views or view files. Note that the
 * type of this property differs in getter and setter. See [[getView()]] and [[setView()]] for details.
 * @property string $viewPath The directory containing the view files for this widget. This property is
 * read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_Widget) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, Widget, yii, base_widget, yii_base_component_ce, yii_base_widget_method_entry, 0);

	/**
	 * @var integer a counter used to generate [[id]] for widgets.
	 * @internal
	 */
	zend_declare_property_long(yii_base_widget_ce, SL("counter"), 0, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	/**
	 * @var string the prefix to the automatically generated widget IDs.
	 * @see getId()
	 */
	zend_declare_property_string(yii_base_widget_ce, SL("autoIdPrefix"), "w", ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	/**
	 * @var Widget[] the widgets that are currently being rendered (not ended). This property
	 * is maintained by [[begin()]] and [[end()]] methods.
	 * @internal
	 */
	zend_declare_property_null(yii_base_widget_ce, SL("stack"), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC TSRMLS_CC);

	zend_declare_property_null(yii_base_widget_ce, SL("_id"), ZEND_ACC_PROTECTED TSRMLS_CC);

	zend_declare_property_null(yii_base_widget_ce, SL("_view"), ZEND_ACC_PROTECTED TSRMLS_CC);

	zend_class_implements(yii_base_widget_ce TSRMLS_CC, 1, yii_base_viewcontextinterface_ce);
	return SUCCESS;

}

/**
 * Begins a widget.
 * This method creates an instance of the calling class. It will apply the configuration
 * to the created instance. A matching [[end()]] call should be called later.
 * @param array $config name-value pairs that will be used to initialize the object properties
 * @return static the newly created widget instance
 */
PHP_METHOD(yii_base_Widget, begin) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	zval *config = NULL, *_0, *widget = NULL, *stack = NULL, *_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &config);

	if (!config) {
		ZEPHIR_INIT_VAR(config);
		array_init(config);
	} else {
		ZEPHIR_SEPARATE_PARAM(config);
	}


	ZEPHIR_INIT_VAR(_0);
	zephir_get_called_class(_0 TSRMLS_CC);
	zephir_array_update_string(&config, SL("class"), &_0, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_CE_STATIC(&widget, yii_baseyii_ce, "createobject", &_1, config);
	zephir_check_call_status();
	zephir_read_static_property_ce(&_2, yii_base_widget_ce, SL("stack") TSRMLS_CC);
	ZEPHIR_CPY_WRT(stack, _2);
	if (Z_TYPE_P(stack) == IS_NULL) {
		ZEPHIR_INIT_VAR(stack);
		array_init(stack);
	}
	zephir_array_append(&stack, widget, PH_SEPARATE);
	zephir_update_static_property_ce(yii_base_widget_ce, SL("stack"), stack TSRMLS_CC);
	RETURN_CCTOR(widget);

}

/**
 * Ends a widget.
 * Note that the rendering result of the widget is directly echoed out.
 * @return static the widget instance that is ended.
 * @throws InvalidCallException if [[begin()]] and [[end()]] calls are not properly nested
 */
PHP_METHOD(yii_base_Widget, end) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL;
	zend_bool _1;
	zval *stack = NULL, *get_class, *get_called_class = NULL, *_0, *widget = NULL, *_3 = NULL, *_4 = NULL, *_5 = NULL;

	ZEPHIR_MM_GROW();

	zephir_read_static_property_ce(&_0, yii_base_widget_ce, SL("stack") TSRMLS_CC);
	ZEPHIR_CPY_WRT(stack, _0);
	_1 = Z_TYPE_P(stack) == IS_ARRAY;
	if (_1) {
		_1 = zephir_fast_count_int(stack TSRMLS_CC) > 0;
	}
	if (_1) {
		Z_SET_ISREF_P(stack);
		ZEPHIR_CALL_FUNCTION(&widget, "array_pop", &_2, stack);
		Z_UNSET_ISREF_P(stack);
		zephir_check_call_status();
		zephir_update_static_property_ce(yii_base_widget_ce, SL("stack"), stack TSRMLS_CC);
		ZEPHIR_INIT_VAR(get_class);
		zephir_get_class(get_class, widget, 0 TSRMLS_CC);
		ZEPHIR_INIT_VAR(get_called_class);
		zephir_get_called_class(get_called_class TSRMLS_CC);
		if (ZEPHIR_IS_EQUAL(get_class, get_called_class)) {
			ZEPHIR_CALL_METHOD(&_3, widget, "run",  NULL);
			zephir_check_call_status();
			zend_print_zval(_3, 0);
			RETURN_CCTOR(widget);
		} else {
			ZEPHIR_INIT_VAR(_4);
			object_init_ex(_4, yii_base_invalidcallexception_ce);
			ZEPHIR_INIT_VAR(_5);
			ZEPHIR_CONCAT_SVSV(_5, "Expecting end() of ", get_class, ", found ", get_called_class);
			ZEPHIR_CALL_METHOD(NULL, _4, "__construct", NULL, _5);
			zephir_check_call_status();
			zephir_throw_exception_debug(_4, "yii/base/Widget.zep", 97 TSRMLS_CC);
			ZEPHIR_MM_RESTORE();
			return;
		}
	} else {
		ZEPHIR_INIT_NVAR(get_called_class);
		zephir_get_called_class(get_called_class TSRMLS_CC);
		ZEPHIR_INIT_LNVAR(_4);
		object_init_ex(_4, yii_base_invalidcallexception_ce);
		ZEPHIR_INIT_LNVAR(_5);
		ZEPHIR_CONCAT_SVS(_5, "Unexpected ", get_called_class, "::end() call. A matching begin() is not found.");
		ZEPHIR_CALL_METHOD(NULL, _4, "__construct", NULL, _5);
		zephir_check_call_status();
		zephir_throw_exception_debug(_4, "yii/base/Widget.zep", 101 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Creates a widget instance and runs it.
 * The widget rendering result is returned by this method.
 * @param array $config name-value pairs that will be used to initialize the object properties
 * @return string the rendering result of the widget.
 */
PHP_METHOD(yii_base_Widget, widget) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_1 = NULL, *_3 = NULL, *_4 = NULL;
	zval *config = NULL, *temp_config = NULL, *widget = NULL, *out = NULL, *content = NULL, *_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &config);

	if (!config) {
		ZEPHIR_INIT_VAR(config);
		array_init(config);
	} else {
		ZEPHIR_SEPARATE_PARAM(config);
	}


	ZEPHIR_CALL_FUNCTION(NULL, "ob_start", &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(NULL, "ob_implicit_flush", &_1, ZEPHIR_GLOBAL(global_false));
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_2);
	zephir_get_called_class(_2 TSRMLS_CC);
	zephir_array_update_string(&config, SL("class"), &_2, PH_COPY | PH_SEPARATE);
	ZEPHIR_CPY_WRT(temp_config, config);
	ZEPHIR_CALL_CE_STATIC(&widget, yii_baseyii_ce, "createobject", &_3, temp_config);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&out, widget, "run",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(&content, "ob_get_clean", &_4);
	zephir_check_call_status();
	zephir_concat_self(&content, out TSRMLS_CC);
	RETURN_CCTOR(content);

}

/**
 * Returns the ID of the widget.
 * @param boolean $autoGenerate whether to generate an ID if it is not set previously
 * @return string ID of the widget.
 */
PHP_METHOD(yii_base_Widget, getId) {

	zval *autoGenerate_param = NULL, *id = NULL, *counter = NULL, *_1, *_2;
	zend_bool autoGenerate, _0;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &autoGenerate_param);

	if (!autoGenerate_param) {
		autoGenerate = 1;
	} else {
		autoGenerate = zephir_get_boolval(autoGenerate_param);
	}


	ZEPHIR_OBS_VAR(id);
	zephir_read_property_this(&id, this_ptr, SL("_id"), PH_NOISY_CC);
	_0 = autoGenerate;
	if (_0) {
		_0 = Z_TYPE_P(id) == IS_NULL;
	}
	if (_0) {
		ZEPHIR_OBS_NVAR(id);
		zephir_read_static_property_ce(&id, yii_base_widget_ce, SL("autoIdPrefix") TSRMLS_CC);
		zephir_read_static_property_ce(&_1, yii_base_widget_ce, SL("counter") TSRMLS_CC);
		zephir_concat_self(&id, _1 TSRMLS_CC);
		zephir_read_static_property_ce(&_2, yii_base_widget_ce, SL("counter") TSRMLS_CC);
		ZEPHIR_CPY_WRT(counter, _2);
		ZEPHIR_ADD_ASSIGN(counter, counter);
		zephir_update_static_property_ce(yii_base_widget_ce, SL("counter"), counter TSRMLS_CC);
		zephir_update_property_this(this_ptr, SL("_id"), id TSRMLS_CC);
	}
	RETURN_CCTOR(id);

}

/**
 * Sets the ID of the widget.
 * @param string $value id of the widget.
 */
PHP_METHOD(yii_base_Widget, setId) {

	zval *value_param = NULL;
	zval *value = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &value_param);

	zephir_get_strval(value, value_param);


	zephir_update_property_this(this_ptr, SL("_id"), value TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the view object that can be used to render views or view files.
 * The [[render()]] and [[renderFile()]] methods will use
 * this view object to implement the actual view rendering.
 * If not set, it will default to the "view" application component.
 * @return \yii\web\View the view object that can be used to render views or view files.
 */
PHP_METHOD(yii_base_Widget, getView) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *view = NULL, *app = NULL, *_0;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(view);
	zephir_read_property_this(&view, this_ptr, SL("_view"), PH_NOISY_CC);
	if (Z_TYPE_P(view) == IS_NULL) {
		zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
		ZEPHIR_CPY_WRT(app, _0);
		ZEPHIR_CALL_METHOD(&view, app, "getview",  NULL);
		zephir_check_call_status();
		zephir_update_property_this(this_ptr, SL("_view"), view TSRMLS_CC);
	}
	RETURN_CCTOR(view);

}

/**
 * Sets the view object to be used by this widget.
 * @param View $view the view object that can be used to render views or view files.
 */
PHP_METHOD(yii_base_Widget, setView) {

	zval *view;

	zephir_fetch_params(0, 1, 0, &view);



	zephir_update_property_this(this_ptr, SL("_view"), view TSRMLS_CC);

}

/**
 * Executes the widget.
 * @return string the result of widget execution to be outputted.
 */
PHP_METHOD(yii_base_Widget, run) {



}

/**
 * Renders a view.
 * The view to be rendered can be specified in one of the following formats:
 *
 * - path alias (e.g. "@app/views/site/index");
 * - absolute path within application (e.g. "//site/index"): the view name starts with double slashes.
 *   The actual view file will be looked for under the [[Application::viewPath|view path]] of the application.
 * - absolute path within module (e.g. "/site/index"): the view name starts with a single slash.
 *   The actual view file will be looked for under the [[Module::viewPath|view path]] of the currently
 *   active module.
 * - relative path (e.g. "index"): the actual view file will be looked for under [[viewPath]].
 *
 * If the view name does not contain a file extension, it will use the default one `.php`.
 *
 * @param string $view the view name.
 * @param array $params the parameters (name-value pairs) that should be made available in the view.
 * @return string the rendering result.
 * @throws InvalidParamException if the view file does not exist.
 */
PHP_METHOD(yii_base_Widget, render) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *view_param = NULL, *params = NULL, *_0 = NULL;
	zval *view = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &view_param, &params);

	zephir_get_strval(view, view_param);
	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getview",  NULL);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(_0, "render", NULL, view, params, this_ptr);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Renders a view file.
 * @param string $file the view file to be rendered. This can be either a file path or a path alias.
 * @param array $params the parameters (name-value pairs) that should be made available in the view.
 * @return string the rendering result.
 * @throws InvalidParamException if the view file does not exist.
 */
PHP_METHOD(yii_base_Widget, renderFile) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *file_param = NULL, *params = NULL, *_0 = NULL;
	zval *file = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &file_param, &params);

	zephir_get_strval(file, file_param);
	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getview",  NULL);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(_0, "renderfile", NULL, file, params, this_ptr);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns the directory containing the view files for this widget.
 * The default implementation returns the 'views' subdirectory under the directory containing the widget class file.
 * @return string the directory containing the view files for this widget.
 */
PHP_METHOD(yii_base_Widget, getViewPath) {

	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_class_entry *_0;
	zval *class, *filename = NULL, *_1 = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(class);
	_0 = zend_fetch_class(SL("ReflectionClass"), ZEND_FETCH_CLASS_AUTO TSRMLS_CC);
	object_init_ex(class, _0);
	ZEPHIR_CALL_METHOD(NULL, class, "__construct", NULL, this_ptr);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&filename, class, "getfilename",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(&_1, "dirname", &_2, filename);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(filename, _1);
	ZEPHIR_CONCAT_VSS(return_value, filename, "/", "views");
	RETURN_MM();

}

