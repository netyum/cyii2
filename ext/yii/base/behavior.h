
extern zend_class_entry *yii_base_behavior_ce;

ZEPHIR_INIT_CLASS(yii_base_Behavior);

PHP_METHOD(yii_base_Behavior, events);
PHP_METHOD(yii_base_Behavior, attach);
PHP_METHOD(yii_base_Behavior, detach);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_behavior_attach, 0, 0, 1)
	ZEND_ARG_INFO(0, owner)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_behavior_method_entry) {
	PHP_ME(yii_base_Behavior, events, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Behavior, attach, arginfo_yii_base_behavior_attach, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Behavior, detach, NULL, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
