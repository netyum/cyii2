
extern zend_class_entry *yii_base_bootstrapinterface_ce;

ZEPHIR_INIT_CLASS(yii_base_BootstrapInterface);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_bootstrapinterface_bootstrap, 0, 0, 1)
	ZEND_ARG_INFO(0, app)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_bootstrapinterface_method_entry) {
	PHP_ABSTRACT_ME(yii_base_BootstrapInterface, bootstrap, arginfo_yii_base_bootstrapinterface_bootstrap)
  PHP_FE_END
};
