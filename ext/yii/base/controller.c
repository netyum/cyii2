
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/memory.h"
#include "kernel/exception.h"
#include "kernel/concat.h"
#include "kernel/hash.h"
#include "kernel/string.h"
#include "kernel/array.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Controller is the base class for classes containing controller logic.
 *
 * @property Module[] $modules All ancestor modules that this controller is located within. This property is
 * read-only.
 * @property string $route The route (module ID, controller ID and action ID) of the current request. This
 * property is read-only.
 * @property string $uniqueId The controller ID that is prefixed with the module ID (if any). This property is
 * read-only.
 * @property View|\yii\web\View $view The view object that can be used to render views or view files.
 * @property string $viewPath The directory containing the view files for this controller. This property is
 * read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_Controller) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, Controller, yii, base_controller, yii_base_component_ce, yii_base_controller_method_entry, 0);

	/**
	 * @var string the ID of this controller.
	 */
	zend_declare_property_null(yii_base_controller_ce, SL("id"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var Module $module the module that this controller belongs to.
	 */
	zend_declare_property_null(yii_base_controller_ce, SL("module"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the ID of the action that is used when the action ID is not specified
	 * in the request. Defaults to 'index'.
	 */
	zend_declare_property_string(yii_base_controller_ce, SL("defaultAction"), "index", ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string|boolean the name of the layout to be applied to this controller's views.
	 * This property mainly affects the behavior of [[render()]].
	 * Defaults to null, meaning the actual layout value should inherit that from [[module]]'s layout value.
	 * If false, no layout will be applied.
	 */
	zend_declare_property_null(yii_base_controller_ce, SL("layout"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var Action the action that is currently being executed. This property will be set
	 * by [[run()]] when it is called by [[Application]] to run an action.
	 */
	zend_declare_property_null(yii_base_controller_ce, SL("action"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var View the view object that can be used to render views or view files.
	 */
	zend_declare_property_null(yii_base_controller_ce, SL("_view"), ZEND_ACC_PROTECTED TSRMLS_CC);

	/**
	 * @event ActionEvent an event raised right before executing a controller action.
	 * You may set [[ActionEvent::isValid]] to be false to cancel the action execution.
	 */
	zend_declare_class_constant_string(yii_base_controller_ce, SL("EVENT_BEFORE_ACTION"), "beforeAction" TSRMLS_CC);

	/**
	 * @event ActionEvent an event raised right after executing a controller action.
	 */
	zend_declare_class_constant_string(yii_base_controller_ce, SL("EVENT_AFTER_ACTION"), "afterAction" TSRMLS_CC);

	zend_class_implements(yii_base_controller_ce TSRMLS_CC, 1, yii_base_viewcontextinterface_ce);
	return SUCCESS;

}

/**
 * @param string $id the ID of this controller.
 * @param Module $module the module that this controller belongs to.
 * @param array $config name-value pairs that will be used to initialize the object properties.
 */
PHP_METHOD(yii_base_Controller, __construct) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;
	zval *id_param = NULL, *module, *config = NULL;
	zval *id = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &id_param, &module, &config);

	zephir_get_strval(id, id_param);
	if (!config) {
		ZEPHIR_INIT_VAR(config);
		array_init(config);
	}


	zephir_update_property_this(this_ptr, SL("id"), id TSRMLS_CC);
	zephir_update_property_this(this_ptr, SL("module"), module TSRMLS_CC);
	ZEPHIR_CALL_PARENT(NULL, yii_base_controller_ce, this_ptr, "__construct", &_0, config);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Declares external actions for the controller.
 * This method is meant to be overwritten to declare external actions for the controller.
 * It should return an array, with array keys being action IDs, and array values the corresponding
 * action class names or action configuration arrays. For example,
 *
 * ~~~
 * return [
 *     'action1' => 'app\components\Action1',
 *     'action2' => [
 *         'class' => 'app\components\Action2',
 *         'property1' => 'value1',
 *         'property2' => 'value2',
 *     ],
 * ];
 * ~~~
 *
 * [[\Yii::createObject()]] will be used later to create the requested action
 * using the configuration provided here.
 */
PHP_METHOD(yii_base_Controller, actions) {


	array_init(return_value);
	return;

}

/**
 * Runs an action within this controller with the specified action ID and parameters.
 * If the action ID is empty, the method will use [[defaultAction]].
 * @param string $id the ID of the action to be executed.
 * @param array $params the parameters (name-value pairs) to be passed to the action.
 * @return mixed the result of the action.
 * @throws InvalidRouteException if the requested action ID cannot be resolved into an action successfully.
 * @see createAction()
 */
PHP_METHOD(yii_base_Controller, runAction) {

	HashTable *_8, *_12;
	HashPosition _7, _11;
	zend_bool runAction;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL, *_10 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *id_param = NULL, *params = NULL, *action = NULL, *oldAction, *app = NULL, *_0 = NULL, *_1 = NULL, *_2, *_4, *_5, *_6, *modules, *get_modules = NULL, *module = NULL, **_9, *result = NULL, **_13;
	zval *id = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &id_param, &params);

	zephir_get_strval(id, id_param);
	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}
	ZEPHIR_INIT_VAR(action);
	ZVAL_NULL(action);
	ZEPHIR_INIT_VAR(modules);
	array_init(modules);
	ZEPHIR_INIT_VAR(result);
	ZVAL_NULL(result);


	ZEPHIR_CALL_METHOD(&action, this_ptr, "createaction", NULL, id);
	zephir_check_call_status();
	if (Z_TYPE_P(action) == IS_NULL) {
		ZEPHIR_INIT_VAR(_0);
		object_init_ex(_0, yii_base_invalidrouteexception_ce);
		ZEPHIR_CALL_METHOD(&_1, this_ptr, "getuniqueid",  NULL);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(_2);
		ZEPHIR_CONCAT_SVSV(_2, "Unable to resolve the request: ", _1, "/", id);
		ZEPHIR_CALL_METHOD(NULL, _0, "__construct", NULL, _2);
		zephir_check_call_status();
		zephir_throw_exception_debug(_0, "yii/base/Controller.zep", 120 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_CALL_METHOD(&_1, action, "getuniqueid",  NULL);
	zephir_check_call_status();
	ZEPHIR_INIT_LNVAR(_0);
	ZEPHIR_CONCAT_SV(_0, "Route to run: ", _1);
	ZEPHIR_INIT_VAR(_4);
	ZVAL_STRING(_4, "Controller:runAction", 0);
	ZEPHIR_CALL_CE_STATIC(NULL, yii_baseyii_ce, "trace", &_3, _0, _4);
	zephir_check_temp_parameter(_4);
	zephir_check_call_status();
	zephir_read_static_property_ce(&_5, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _5);
	ZEPHIR_OBS_VAR(_6);
	zephir_read_property(&_6, app, SL("requestedAction"), PH_NOISY_CC);
	if (Z_TYPE_P(_6) == IS_NULL) {
		zephir_update_property_zval(app, SL("requestedAction"), action TSRMLS_CC);
		zephir_update_static_property_ce(yii_baseyii_ce, SL("app"), app TSRMLS_CC);
	}
	ZEPHIR_OBS_VAR(oldAction);
	zephir_read_property_this(&oldAction, this_ptr, SL("action"), PH_NOISY_CC);
	zephir_update_property_this(this_ptr, SL("action"), action TSRMLS_CC);
	runAction = 1;
	ZEPHIR_CALL_METHOD(&get_modules, this_ptr, "getmodules",  NULL);
	zephir_check_call_status();
	zephir_is_iterable(get_modules, &_8, &_7, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_8, (void**) &_9, &_7) == SUCCESS
	  ; zephir_hash_move_forward_ex(_8, &_7)
	) {
		ZEPHIR_GET_HVALUE(module, _9);
		ZEPHIR_CALL_METHOD(&_1, module, "beforeaction", NULL, action);
		zephir_check_call_status();
		if (zephir_is_true(_1)) {
			Z_SET_ISREF_P(modules);
			ZEPHIR_CALL_FUNCTION(NULL, "array_unshift", &_10, modules, module);
			Z_UNSET_ISREF_P(modules);
			zephir_check_call_status();
		} else {
			runAction = 0;
			break;
		}
	}
	if (runAction == 1) {
		ZEPHIR_CALL_METHOD(&_1, this_ptr, "beforeaction", NULL, action);
		zephir_check_call_status();
		if (zephir_is_true(_1)) {
			ZEPHIR_CALL_METHOD(&result, action, "runwithparams", NULL, params);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(&_1, this_ptr, "afteraction", NULL, action, result);
			zephir_check_call_status();
			ZEPHIR_CPY_WRT(result, _1);
		}
	}
	zephir_is_iterable(modules, &_12, &_11, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_12, (void**) &_13, &_11) == SUCCESS
	  ; zephir_hash_move_forward_ex(_12, &_11)
	) {
		ZEPHIR_GET_HVALUE(module, _13);
		ZEPHIR_CALL_METHOD(&_1, module, "afteraction", NULL, action, result);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(result, _1);
	}
	zephir_update_property_this(this_ptr, SL("action"), oldAction TSRMLS_CC);
	RETURN_CCTOR(result);

}

/**
 * Runs a request specified in terms of a route.
 * The route can be either an ID of an action within this controller or a complete route consisting
 * of module IDs, controller ID and action ID. If the route starts with a slash '/', the parsing of
 * the route will start from the application; otherwise, it will start from the parent module of this controller.
 * @param string $route the route to be handled, e.g., 'view', 'comment/view', '/admin/comment/view'.
 * @param array $params the parameters to be passed to the action.
 * @return mixed the result of the action.
 * @see runAction()
 */
PHP_METHOD(yii_base_Controller, run) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *route, *params = NULL, *pos, *app = NULL, _0, *_1, *new_route, _2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &route, &params);

	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}


	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "/", 0);
	ZEPHIR_INIT_VAR(pos);
	zephir_fast_strpos(pos, route, &_0, 0 );
	if (Z_TYPE_P(pos) == IS_BOOL) {
		ZEPHIR_RETURN_CALL_METHOD(this_ptr, "runaction", NULL, route, params);
		zephir_check_call_status();
		RETURN_MM();
	}
	if (ZEPHIR_GT_LONG(pos, 0)) {
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("module"), PH_NOISY_CC);
		ZEPHIR_RETURN_CALL_METHOD(_1, "runaction", NULL, route, params);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		ZEPHIR_INIT_VAR(new_route);
		ZEPHIR_SINIT_VAR(_2);
		ZVAL_STRING(&_2, "/", 0);
		zephir_fast_trim(new_route, route, &_2, ZEPHIR_TRIM_LEFT TSRMLS_CC);
		zephir_read_static_property_ce(&_1, yii_baseyii_ce, SL("app") TSRMLS_CC);
		ZEPHIR_CPY_WRT(app, _1);
		ZEPHIR_RETURN_CALL_METHOD(app, "runaction", NULL, new_route, params);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Binds the parameters to the action.
 * This method is invoked by [[Action]] when it begins to run with the given parameters.
 * @param Action $action the action to be bound with parameters.
 * @param array $params the parameters to be bound to the action.
 * @return array the valid parameters that the action can run with.
 */
PHP_METHOD(yii_base_Controller, bindActionParams) {

	zval *action, *params;

	zephir_fetch_params(0, 2, 0, &action, &params);



	array_init(return_value);
	return;

}

/**
 * Creates an action based on the given action ID.
 * The method first checks if the action ID has been declared in [[actions()]]. If so,
 * it will use the configuration declared there to create the action object.
 * If not, it will look for a controller method whose name is in the format of `actionXyz`
 * where `Xyz` stands for the action ID. If found, an [[InlineAction]] representing that
 * method will be created and returned.
 * @param string $id the action ID.
 * @return Action the newly created action instance. Null if the ID doesn't resolve into any action.
 */
PHP_METHOD(yii_base_Controller, createAction) {

	zend_class_entry *_11;
	zend_bool _5, _6;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_3 = NULL, *_7 = NULL, *_10 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *id = NULL, *actionMap = NULL, *elements, *_1, *pos, *preg_retval = NULL, *trim_retval, _2 = zval_used_for_init, _4, *explodes, *implodes, *ucwords_retval = NULL, *replace_retval = NULL, *methodName, _8, _9, *method, *_12 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &id);

	ZEPHIR_SEPARATE_PARAM(id);
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	if (ZEPHIR_IS_STRING(id, "")) {
		ZEPHIR_OBS_NVAR(id);
		zephir_read_property_this(&id, this_ptr, SL("defaultAction"), PH_NOISY_CC);
	}
	ZEPHIR_CALL_METHOD(&actionMap, this_ptr, "actions",  NULL);
	zephir_check_call_status();
	if (zephir_array_isset(actionMap, id)) {
		zephir_array_append(&elements, id, PH_SEPARATE);
		zephir_array_append(&elements, this_ptr, PH_SEPARATE);
		zephir_array_fetch(&_1, actionMap, id, PH_NOISY | PH_READONLY TSRMLS_CC);
		ZEPHIR_RETURN_CALL_CE_STATIC(yii_baseyii_ce, "createobject", &_0, _1, elements);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		ZEPHIR_SINIT_VAR(_2);
		ZVAL_STRING(&_2, "/^[a-z0-9\\-_]+$/", 0);
		ZEPHIR_CALL_FUNCTION(&preg_retval, "preg_match", &_3, &_2, id);
		zephir_check_call_status();
		ZEPHIR_SINIT_NVAR(_2);
		ZVAL_STRING(&_2, "--", 0);
		ZEPHIR_INIT_VAR(pos);
		zephir_fast_strpos(pos, id, &_2, 0 );
		ZEPHIR_INIT_VAR(trim_retval);
		ZEPHIR_SINIT_VAR(_4);
		ZVAL_STRING(&_4, "-", 0);
		zephir_fast_trim(trim_retval, id, &_4, ZEPHIR_TRIM_BOTH TSRMLS_CC);
		_5 = zephir_is_true(preg_retval);
		if (_5) {
			_5 = Z_TYPE_P(pos) == IS_BOOL;
		}
		_6 = _5;
		if (_6) {
			_6 = ZEPHIR_IS_EQUAL(trim_retval, id);
		}
		if (_6) {
			ZEPHIR_INIT_VAR(explodes);
			zephir_fast_explode_str(explodes, SL("-"), id, LONG_MAX TSRMLS_CC);
			ZEPHIR_INIT_VAR(implodes);
			zephir_fast_join_str(implodes, SL(" "), explodes TSRMLS_CC);
			ZEPHIR_CALL_FUNCTION(&ucwords_retval, "ucwords", &_7, implodes);
			zephir_check_call_status();
			ZEPHIR_SINIT_VAR(_8);
			ZVAL_STRING(&_8, " ", 0);
			ZEPHIR_SINIT_VAR(_9);
			ZVAL_STRING(&_9, "", 0);
			ZEPHIR_CALL_FUNCTION(&replace_retval, "str_replace", &_10, &_8, &_9, ucwords_retval);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(methodName);
			ZEPHIR_CONCAT_SV(methodName, "action", replace_retval);
			if ((zephir_method_exists(this_ptr, methodName TSRMLS_CC)  == SUCCESS)) {
				ZEPHIR_INIT_VAR(method);
				_11 = zend_fetch_class(SL("ReflectionMethod"), ZEND_FETCH_CLASS_AUTO TSRMLS_CC);
				object_init_ex(method, _11);
				ZEPHIR_CALL_METHOD(NULL, method, "__construct", NULL, this_ptr, methodName);
				zephir_check_call_status();
				ZEPHIR_CALL_METHOD(&_12, method, "getname",  NULL);
				zephir_check_call_status();
				if (ZEPHIR_IS_EQUAL(_12, methodName)) {
					object_init_ex(return_value, yii_base_inlineaction_ce);
					ZEPHIR_CALL_METHOD(NULL, return_value, "__construct", NULL, id, this_ptr, methodName);
					zephir_check_call_status();
					RETURN_MM();
				}
			}
		}
	}
	RETURN_MM_NULL();

}

/**
 * This method is invoked right before an action is executed.
 *
 * The method will trigger the [[EVENT_BEFORE_ACTION]] event. The return value of the method
 * will determine whether the action should continue to run.
 *
 * If you override this method, your code should look like the following:
 *
 * ```php
 * public function beforeAction($action)
 * {
 *     if (parent::beforeAction($action)) {
 *         // your custom code here
 *         return true;  // or false if needed
 *     } else {
 *         return false;
 *     }
 * }
 * ```
 *
 * @param Action $action the action to be executed.
 * @return boolean whether the action should continue to run.
 */
PHP_METHOD(yii_base_Controller, beforeAction) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *action, *event, *temp_action = NULL, *_0, *_1;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &action);



	ZEPHIR_CPY_WRT(temp_action, action);
	ZEPHIR_INIT_VAR(event);
	object_init_ex(event, yii_base_actionevent_ce);
	ZEPHIR_CALL_METHOD(NULL, event, "__construct", NULL, action);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_0);
	ZVAL_STRING(_0, "beforeAction", 0);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "trigger", NULL, _0, event);
	zephir_check_temp_parameter(_0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property(&_1, event, SL("isValid"), PH_NOISY_CC);
	RETURN_CCTOR(_1);

}

/**
 * This method is invoked right after an action is executed.
 *
 * The method will trigger the [[EVENT_AFTER_ACTION]] event. The return value of the method
 * will be used as the action return value.
 *
 * If you override this method, your code should look like the following:
 *
 * ```php
 * public function afterAction($action, $result)
 * {
 *     $result = parent::afterAction($action, $result);
 *     // your custom code here
 *     return $result;
 * }
 * ```
 *
 * @param Action $action the action just executed.
 * @param mixed $result the action return result.
 * @return mixed the processed action result.
 */
PHP_METHOD(yii_base_Controller, afterAction) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *action, *result, *event, *temp_action = NULL, *_0, *_1;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &action, &result);



	ZEPHIR_CPY_WRT(temp_action, action);
	ZEPHIR_INIT_VAR(event);
	object_init_ex(event, yii_base_actionevent_ce);
	ZEPHIR_CALL_METHOD(NULL, event, "__construct", NULL, action);
	zephir_check_call_status();
	zephir_update_property_zval(event, SL("result"), result TSRMLS_CC);
	ZEPHIR_INIT_VAR(_0);
	ZVAL_STRING(_0, "afterAction", 0);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "trigger", NULL, _0, event);
	zephir_check_temp_parameter(_0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property(&_1, event, SL("result"), PH_NOISY_CC);
	RETURN_CCTOR(_1);

}

/**
 * Returns all ancestor modules of this controller.
 * The first module in the array is the outermost one (i.e., the application instance),
 * while the last is the innermost one.
 * @return Module[] all ancestor modules that this controller is located within.
 */
PHP_METHOD(yii_base_Controller, getModules) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	zval *modules = NULL, *module = NULL, *this_module, *elements, *module_module = NULL, *_0;

	ZEPHIR_MM_GROW();
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);

	ZEPHIR_OBS_VAR(this_module);
	zephir_read_property_this(&this_module, this_ptr, SL("module"), PH_NOISY_CC);
	zephir_array_append(&elements, this_module, PH_SEPARATE);
	ZEPHIR_CPY_WRT(modules, elements);
	_0 = zephir_fetch_nproperty_this(this_ptr, SL("module"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(module, _0);
	ZEPHIR_OBS_VAR(module_module);
	zephir_read_property(&module_module, module, SL("module"), PH_NOISY_CC);
	while (1) {
		if (!(Z_TYPE_P(module_module) != IS_NULL)) {
			break;
		}
		Z_SET_ISREF_P(modules);
		ZEPHIR_CALL_FUNCTION(NULL, "array_unshift", &_1, modules, module_module);
		Z_UNSET_ISREF_P(modules);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(module, module_module);
		ZEPHIR_OBS_NVAR(module_module);
		zephir_read_property(&module_module, module, SL("module"), PH_NOISY_CC);
	}
	RETURN_CCTOR(modules);

}

/**
 * @return string the controller ID that is prefixed with the module ID (if any).
 */
PHP_METHOD(yii_base_Controller, getUniqueId) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *_0, *_1, *_2, *_3 = NULL, *_4;

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property_this(&_1, this_ptr, SL("module"), PH_NOISY_CC);
	if (zephir_instance_of_ev(_1, yii_base_application_ce TSRMLS_CC)) {
		zephir_read_property_this(&_0, this_ptr, SL("id"), PH_NOISY_CC);
	} else {
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("module"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(&_3, _2, "getuniqueid",  NULL);
		zephir_check_call_status();
		_4 = zephir_fetch_nproperty_this(this_ptr, SL("id"), PH_NOISY_CC);
		ZEPHIR_CONCAT_VSV(_0, _3, "/", _4);
	}
	RETURN_CCTOR(_0);

}

/**
 * Returns the route of the current request.
 * @return string the route (module ID, controller ID and action ID) of the current request.
 */
PHP_METHOD(yii_base_Controller, getRoute) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *_0 = NULL, *_1, *_2;

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property_this(&_1, this_ptr, SL("action"), PH_NOISY_CC);
	if (Z_TYPE_P(_1) == IS_NULL) {
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("action"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(&_0, _2, "getuniqueid",  NULL);
		zephir_check_call_status();
	} else {
		ZEPHIR_CALL_METHOD(&_0, this_ptr, "getuniqueid",  NULL);
		zephir_check_call_status();
	}
	RETURN_CCTOR(_0);

}

/**
 * Renders a view and applies layout if available.
 *
 * The view to be rendered can be specified in one of the following formats:
 *
 * - path alias (e.g. "@app/views/site/index");
 * - absolute path within application (e.g. "//site/index"): the view name starts with double slashes.
 *   The actual view file will be looked for under the [[Application::viewPath|view path]] of the application.
 * - absolute path within module (e.g. "/site/index"): the view name starts with a single slash.
 *   The actual view file will be looked for under the [[Module::viewPath|view path]] of [[module]].
 * - relative path (e.g. "index"): the actual view file will be looked for under [[viewPath]].
 *
 * To determine which layout should be applied, the following two steps are conducted:
 *
 * 1. In the first step, it determines the layout name and the context module:
 *
 * - If [[layout]] is specified as a string, use it as the layout name and [[module]] as the context module;
 * - If [[layout]] is null, search through all ancestor modules of this controller and find the first
 *   module whose [[Module::layout|layout]] is not null. The layout and the corresponding module
 *   are used as the layout name and the context module, respectively. If such a module is not found
 *   or the corresponding layout is not a string, it will return false, meaning no applicable layout.
 *
 * 2. In the second step, it determines the actual layout file according to the previously found layout name
 *    and context module. The layout name can be:
 *
 * - a path alias (e.g. "@app/views/layouts/main");
 * - an absolute path (e.g. "/main"): the layout name starts with a slash. The actual layout file will be
 *   looked for under the [[Application::layoutPath|layout path]] of the application;
 * - a relative path (e.g. "main"): the actual layout layout file will be looked for under the
 *   [[Module::layoutPath|layout path]] of the context module.
 *
 * If the layout name does not contain a file extension, it will use the default one `.php`.
 *
 * @param string $view the view name.
 * @param array $params the parameters (name-value pairs) that should be made available in the view.
 * These parameters will not be available in the layout.
 * @return string the rendering result.
 * @throws InvalidParamException if the view file or the layout file does not exist.
 */
PHP_METHOD(yii_base_Controller, render) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *view, *params = NULL, *output = NULL, *layoutFile = NULL, *_0 = NULL, *_1 = NULL, *elements, *_2 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &view, &params);

	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getview",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&output, _0, "render", NULL, view, params, this_ptr);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getview",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&layoutFile, this_ptr, "findlayoutfile", NULL, _1);
	zephir_check_call_status();
	if (!ZEPHIR_IS_FALSE(layoutFile)) {
		zephir_array_update_string(&elements, SL("content"), &output, PH_COPY | PH_SEPARATE);
		ZEPHIR_CALL_METHOD(&_2, this_ptr, "getview",  NULL);
		zephir_check_call_status();
		ZEPHIR_RETURN_CALL_METHOD(_2, "renderfile", NULL, layoutFile, elements, this_ptr);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		RETURN_CCTOR(output);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Renders a view.
 * This method differs from [[render()]] in that it does not apply any layout.
 * @param string $view the view name. Please refer to [[render()]] on how to specify a view name.
 * @param array $params the parameters (name-value pairs) that should be made available in the view.
 * @return string the rendering result.
 * @throws InvalidParamException if the view file does not exist.
 */
PHP_METHOD(yii_base_Controller, renderPartial) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *view_param = NULL, *params = NULL, *_0 = NULL;
	zval *view = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &view_param, &params);

	zephir_get_strval(view, view_param);
	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getview",  NULL);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(_0, "render", NULL, view, params, this_ptr);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Renders a view file.
 * @param string $file the view file to be rendered. This can be either a file path or a path alias.
 * @param array $params the parameters (name-value pairs) that should be made available in the view.
 * @return string the rendering result.
 * @throws InvalidParamException if the view file does not exist.
 */
PHP_METHOD(yii_base_Controller, renderFile) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *file_param = NULL, *params = NULL, *_0 = NULL;
	zval *file = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &file_param, &params);

	zephir_get_strval(file, file_param);
	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getview",  NULL);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(_0, "renderfile", NULL, file, params, this_ptr);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns the view object that can be used to render views or view files.
 * The [[render()]], [[renderPartial()]] and [[renderFile()]] methods will use
 * this view object to implement the actual view rendering.
 * If not set, it will default to the "view" application component.
 * @return View|\yii\web\View the view object that can be used to render views or view files.
 */
PHP_METHOD(yii_base_Controller, getView) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *view = NULL, *_0, *_1;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("_view"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		zephir_read_static_property_ce(&_1, yii_baseyii_ce, SL("app") TSRMLS_CC);
		ZEPHIR_CALL_METHOD(&view, _1, "getview",  NULL);
		zephir_check_call_status();
		zephir_update_property_this(this_ptr, SL("_view"), view TSRMLS_CC);
	}
	RETURN_MM_MEMBER(this_ptr, "_view");

}

/**
 * Sets the view object to be used by this controller.
 * @param View|\yii\web\View $view the view object that can be used to render views or view files.
 */
PHP_METHOD(yii_base_Controller, setView) {

	zval *view, *temp_view = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &view);



	ZEPHIR_CPY_WRT(temp_view, view);
	zephir_update_property_this(this_ptr, SL("_view"), temp_view TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the directory containing view files for this controller.
 * The default implementation returns the directory named as controller [[id]] under the [[module]]'s
 * [[viewPath]] directory.
 * @return string the directory containing the view files for this controller.
 */
PHP_METHOD(yii_base_Controller, getViewPath) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *_0, *_1 = NULL, *_2;

	ZEPHIR_MM_GROW();

	_0 = zephir_fetch_nproperty_this(this_ptr, SL("module"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_1, _0, "getviewpath",  NULL);
	zephir_check_call_status();
	_2 = zephir_fetch_nproperty_this(this_ptr, SL("id"), PH_NOISY_CC);
	ZEPHIR_CONCAT_VSV(return_value, _1, "/", _2);
	RETURN_MM();

}

/**
 * Finds the applicable layout file.
 * @param View $view the view object to render the layout file.
 * @return string|boolean the layout file path, or false if layout is not needed.
 * Please refer to [[render()]] on how to specify this parameter.
 * @throws InvalidParamException if an invalid path alias is used to specify the layout.
 */
PHP_METHOD(yii_base_Controller, findLayoutFile) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_11 = NULL, *_12 = NULL, *_16 = NULL, *_17 = NULL, *_18 = NULL;
	zend_bool _3, _6;
	zval *view, *module = NULL, *layout = NULL, *_0, *_1, *_2 = NULL, *_4 = NULL, *_5 = NULL, *_7, *file = NULL, _8 = zval_used_for_init, _9 = zval_used_for_init, *_10 = NULL, *_13 = NULL, *_14 = NULL, *_15 = NULL, *path;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &view);

	ZEPHIR_INIT_VAR(layout);
	ZVAL_NULL(layout);


	_0 = zephir_fetch_nproperty_this(this_ptr, SL("module"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(module, _0);
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property_this(&_1, this_ptr, SL("layout"), PH_NOISY_CC);
	if (Z_TYPE_P(_1) == IS_STRING) {
		_0 = zephir_fetch_nproperty_this(this_ptr, SL("layout"), PH_NOISY_CC);
		ZEPHIR_CPY_WRT(layout, _0);
	} else {
		ZEPHIR_OBS_VAR(_2);
		zephir_read_property_this(&_2, this_ptr, SL("layout"), PH_NOISY_CC);
		if (Z_TYPE_P(_2) == IS_NULL) {
			while (1) {
				_3 = Z_TYPE_P(module) != IS_NULL;
				if (_3) {
					ZEPHIR_OBS_NVAR(_4);
					zephir_read_property(&_4, module, SL("layout"), PH_NOISY_CC);
					_3 = Z_TYPE_P(_4) == IS_NULL;
				}
				if (!(_3)) {
					break;
				}
				ZEPHIR_OBS_NVAR(_5);
				zephir_read_property(&_5, module, SL("module"), PH_NOISY_CC);
				ZEPHIR_CPY_WRT(module, _5);
			}
			_6 = Z_TYPE_P(module) != IS_NULL;
			if (_6) {
				ZEPHIR_OBS_NVAR(_5);
				zephir_read_property(&_5, module, SL("layout"), PH_NOISY_CC);
				_6 = Z_TYPE_P(_5) == IS_STRING;
			}
			if (_6) {
				ZEPHIR_OBS_VAR(_7);
				zephir_read_property(&_7, module, SL("layout"), PH_NOISY_CC);
				ZEPHIR_CPY_WRT(layout, _7);
			}
		}
	}
	if (Z_TYPE_P(layout) != IS_STRING) {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_SINIT_VAR(_8);
	ZVAL_STRING(&_8, "@", 0);
	ZEPHIR_SINIT_VAR(_9);
	ZVAL_LONG(&_9, 1);
	ZEPHIR_CALL_FUNCTION(&_10, "strncmp", &_11, layout, &_8, &_9);
	zephir_check_call_status();
	if (ZEPHIR_IS_LONG(_10, 0)) {
		ZEPHIR_CALL_CE_STATIC(&file, yii_baseyii_ce, "getalias", &_12, layout);
		zephir_check_call_status();
	} else {
		ZEPHIR_INIT_NVAR(file);
		ZEPHIR_SINIT_NVAR(_8);
		ZVAL_STRING(&_8, "/", 0);
		ZEPHIR_SINIT_NVAR(_9);
		ZVAL_LONG(&_9, 1);
		ZEPHIR_CALL_FUNCTION(&_13, "strncmp", &_11, layout, &_8, &_9);
		zephir_check_call_status();
		if (ZEPHIR_IS_LONG(_13, 0)) {
			zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
			ZEPHIR_CALL_METHOD(&_14, _0, "getlayoutpath",  NULL);
			zephir_check_call_status();
			ZEPHIR_SINIT_NVAR(_8);
			ZVAL_LONG(&_8, 1);
			ZEPHIR_CALL_FUNCTION(&_15, "substr", &_16, layout, &_8);
			zephir_check_call_status();
			ZEPHIR_CONCAT_VSV(file, _14, "/", _15);
		} else {
			ZEPHIR_CALL_METHOD(&_14, module, "getlayoutpath",  NULL);
			zephir_check_call_status();
			ZEPHIR_CONCAT_VSV(file, _14, "/", layout);
		}
	}
	ZEPHIR_SINIT_NVAR(_8);
	ZVAL_LONG(&_8, 4);
	ZEPHIR_CALL_FUNCTION(&_13, "pathinfo", &_17, file, &_8);
	zephir_check_call_status();
	if (!ZEPHIR_IS_STRING(_13, "")) {
		RETURN_CCTOR(file);
	}
	ZEPHIR_OBS_NVAR(_2);
	zephir_read_property(&_2, view, SL("defaultExtension"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(path);
	ZEPHIR_CONCAT_VSV(path, file, ".", _2);
	ZEPHIR_OBS_NVAR(_4);
	zephir_read_property(&_4, view, SL("defaultExtension"), PH_NOISY_CC);
	_3 = !ZEPHIR_IS_STRING(_4, "php");
	if (_3) {
		ZEPHIR_CALL_FUNCTION(&_13, "is_file", &_18, path);
		zephir_check_call_status();
		_3 = !zephir_is_true(_13);
	}
	if (_3) {
		ZEPHIR_INIT_BNVAR(path);
		ZEPHIR_CONCAT_VS(path, file, ".php");
	}
	RETURN_CCTOR(path);

}

