
extern zend_class_entry *yii_base_viewrenderer_ce;

ZEPHIR_INIT_CLASS(yii_base_ViewRenderer);

PHP_METHOD(yii_base_ViewRenderer, render);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_viewrenderer_render, 0, 0, 3)
	ZEND_ARG_INFO(0, view)
	ZEND_ARG_INFO(0, file)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_viewrenderer_method_entry) {
	PHP_ME(yii_base_ViewRenderer, render, arginfo_yii_base_viewrenderer_render, ZEND_ACC_ABSTRACT|ZEND_ACC_PUBLIC)
  PHP_FE_END
};
