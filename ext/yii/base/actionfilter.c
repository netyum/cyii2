
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"
#include "kernel/array.h"
#include "kernel/operators.h"
#include "kernel/string.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * ActionFilter is the base class for action filters.
 *
 * An action filter will participate in the action execution workflow by responding to
 * the `beforeAction` and `afterAction` events triggered by modules and controllers.
 *
 * Check implementation of [[\yii\filters\AccessControl]], [[\yii\filters\PageCache]] and [[\yii\filters\HttpCache]] as examples on how to use it.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_ActionFilter) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, ActionFilter, yii, base_actionfilter, yii_base_behavior_ce, yii_base_actionfilter_method_entry, 0);

	/**
	 * @var array list of action IDs that this filter should apply to. If this property is not set,
	 * then the filter applies to all actions, unless they are listed in [[except]].
	 * If an action ID appears in both [[only]] and [[except]], this filter will NOT apply to it.
	 *
	 * Note that if the filter is attached to a module, the action IDs should also include child module IDs (if any)
	 * and controller IDs.
	 *
	 * @see except
	 */
	zend_declare_property_null(yii_base_actionfilter_ce, SL("only"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array list of action IDs that this filter should not apply to.
	 * @see only
	 */
	zend_declare_property_null(yii_base_actionfilter_ce, SL("except"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_base_ActionFilter, attach) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *_0;
	zval *owner, *_1;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &owner);



	zephir_update_property_this(this_ptr, SL("owner"), owner TSRMLS_CC);
	ZEPHIR_INIT_VAR(_0);
	array_init_size(_0, 3);
	zephir_array_fast_append(_0, this_ptr);
	ZEPHIR_INIT_VAR(_1);
	ZVAL_STRING(_1, "beforeFilter", 1);
	zephir_array_fast_append(_0, _1);
	ZEPHIR_INIT_BNVAR(_1);
	ZVAL_STRING(_1, "beforeAction", 0);
	ZEPHIR_CALL_METHOD(NULL, owner, "on", NULL, _1, _0);
	zephir_check_temp_parameter(_1);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_base_ActionFilter, detach) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *_2, *_5;
	zval *_0, *_1, *_3 = NULL, *_4;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("owner"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) != IS_NULL) {
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("owner"), PH_NOISY_CC);
		ZEPHIR_INIT_VAR(_2);
		array_init_size(_2, 3);
		zephir_array_fast_append(_2, this_ptr);
		ZEPHIR_INIT_VAR(_3);
		ZVAL_STRING(_3, "beforeFilter", 1);
		zephir_array_fast_append(_2, _3);
		ZEPHIR_INIT_NVAR(_3);
		ZVAL_STRING(_3, "beforeAction", 0);
		ZEPHIR_CALL_METHOD(NULL, _1, "off", NULL, _3, _2);
		zephir_check_temp_parameter(_3);
		zephir_check_call_status();
		_4 = zephir_fetch_nproperty_this(this_ptr, SL("owner"), PH_NOISY_CC);
		ZEPHIR_INIT_VAR(_5);
		array_init_size(_5, 3);
		zephir_array_fast_append(_5, this_ptr);
		ZEPHIR_INIT_NVAR(_3);
		ZVAL_STRING(_3, "afterFilter", 1);
		zephir_array_fast_append(_5, _3);
		ZEPHIR_INIT_NVAR(_3);
		ZVAL_STRING(_3, "afterAction", 0);
		ZEPHIR_CALL_METHOD(NULL, _4, "off", NULL, _3, _5);
		zephir_check_temp_parameter(_3);
		zephir_check_call_status();
		zephir_update_property_this(this_ptr, SL("owner"), ZEPHIR_GLOBAL(global_null) TSRMLS_CC);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * @param ActionEvent $event
 */
PHP_METHOD(yii_base_ActionFilter, beforeFilter) {

	zval *_5;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *event, *_0 = NULL, *_1, *_2, *_3, *_4, *_6 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &event);

	ZEPHIR_SEPARATE_PARAM(event);


	ZEPHIR_OBS_VAR(_1);
	zephir_read_property(&_1, event, SL("action"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "isactive", NULL, _1);
	zephir_check_call_status();
	if (!(zephir_is_true(_0))) {
		RETURN_MM_NULL();
	}
	ZEPHIR_OBS_VAR(_2);
	zephir_read_property(&_2, event, SL("action"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "beforeaction", NULL, _2);
	zephir_check_call_status();
	zephir_update_property_zval(event, SL("isValid"), _0 TSRMLS_CC);
	ZEPHIR_OBS_VAR(_3);
	zephir_read_property(&_3, event, SL("isValid"), PH_NOISY_CC);
	if (zephir_is_true(_3)) {
		_4 = zephir_fetch_nproperty_this(this_ptr, SL("owner"), PH_NOISY_CC);
		ZEPHIR_INIT_VAR(_5);
		array_init_size(_5, 3);
		zephir_array_fast_append(_5, this_ptr);
		ZEPHIR_INIT_VAR(_6);
		ZVAL_STRING(_6, "afterFilter", 1);
		zephir_array_fast_append(_5, _6);
		ZEPHIR_INIT_NVAR(_6);
		ZVAL_STRING(_6, "afterAction", 0);
		ZEPHIR_CALL_METHOD(NULL, _4, "on", NULL, _6, _5, ZEPHIR_GLOBAL(global_null), ZEPHIR_GLOBAL(global_false));
		zephir_check_temp_parameter(_6);
		zephir_check_call_status();
	} else {
		zephir_update_property_zval(event, SL("handled"), (1) ? ZEPHIR_GLOBAL(global_true) : ZEPHIR_GLOBAL(global_false) TSRMLS_CC);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * @param ActionEvent $event
 */
PHP_METHOD(yii_base_ActionFilter, afterFilter) {

	zval *_4;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *event, *_0 = NULL, *_1, *_2, *_3, *_5;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &event);

	ZEPHIR_SEPARATE_PARAM(event);


	ZEPHIR_OBS_VAR(_1);
	zephir_read_property(&_1, event, SL("action"), PH_NOISY_CC);
	ZEPHIR_OBS_VAR(_2);
	zephir_read_property(&_2, event, SL("result"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "afteraction", NULL, _1, _2);
	zephir_check_call_status();
	zephir_update_property_zval(event, SL("result"), _0 TSRMLS_CC);
	_3 = zephir_fetch_nproperty_this(this_ptr, SL("owner"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(_4);
	array_init_size(_4, 3);
	zephir_array_fast_append(_4, this_ptr);
	ZEPHIR_INIT_VAR(_5);
	ZVAL_STRING(_5, "afterFilter", 1);
	zephir_array_fast_append(_4, _5);
	ZEPHIR_INIT_BNVAR(_5);
	ZVAL_STRING(_5, "afterAction", 0);
	ZEPHIR_CALL_METHOD(NULL, _3, "off", NULL, _5, _4);
	zephir_check_temp_parameter(_5);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * This method is invoked right before an action is to be executed (after all possible filters.)
 * You may override this method to do last-minute preparation for the action.
 * @param Action $action the action to be executed.
 * @return boolean whether the action should continue to be executed.
 */
PHP_METHOD(yii_base_ActionFilter, beforeAction) {

	zval *action;

	zephir_fetch_params(0, 1, 0, &action);



	RETURN_BOOL(1);

}

/**
 * This method is invoked right after an action is executed.
 * You may override this method to do some postprocessing for the action.
 * @param Action $action the action just executed.
 * @param mixed $result the action execution result
 * @return mixed the processed action result.
 */
PHP_METHOD(yii_base_ActionFilter, afterAction) {

	zval *action, *result;

	zephir_fetch_params(0, 2, 0, &action, &result);



	RETURN_CCTORW(result);

}

/**
 * Returns a value indicating whether the filer is active for the given action.
 * @param Action $action the action being filtered
 * @return boolean whether the filer is active for the given action.
 */
PHP_METHOD(yii_base_ActionFilter, isActive) {

	zephir_nts_static zephir_fcall_cache_entry *_6 = NULL, *_10 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_bool _1, _2, _3;
	zval *action, *mid = NULL, *id = NULL, *pos, *owner = NULL, *_0, _4, *_5 = NULL, *_7 = NULL, *_8 = NULL, *_9, *_11, *_12;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &action);



	_0 = zephir_fetch_nproperty_this(this_ptr, SL("owner"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(owner, _0);
	_1 = Z_TYPE_P(owner) == IS_OBJECT;
	if (_1) {
		_1 = (zephir_instance_of_ev(owner, yii_base_module_ce TSRMLS_CC));
	}
	if (_1) {
		_0 = zephir_fetch_nproperty_this(this_ptr, SL("owner"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(&mid, _0, "getuniqueid",  NULL);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&id, action, "getuniqueid",  NULL);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(pos);
		zephir_fast_strpos(pos, id, mid, 0 );
		_2 = (!ZEPHIR_IS_STRING(mid, ""));
		if (_2) {
			_2 = (Z_TYPE_P(pos) != IS_BOOL);
		}
		_3 = _2;
		if (_3) {
			_3 = ZEPHIR_IS_LONG(pos, 0);
		}
		if (_3) {
			ZEPHIR_SINIT_VAR(_4);
			ZVAL_LONG(&_4, (zephir_fast_strlen_ev(mid) + 1));
			ZEPHIR_CALL_FUNCTION(&_5, "substr", &_6, id, &_4);
			zephir_check_call_status();
			ZEPHIR_CPY_WRT(id, _5);
		}
	} else {
		ZEPHIR_OBS_VAR(_7);
		zephir_read_property(&_7, action, SL("id"), PH_NOISY_CC);
		ZEPHIR_CPY_WRT(id, _7);
	}
	ZEPHIR_OBS_NVAR(_7);
	zephir_read_property_this(&_7, this_ptr, SL("except"), PH_NOISY_CC);
	if (Z_TYPE_P(_7) == IS_NULL) {
		ZEPHIR_INIT_VAR(_8);
		array_init(_8);
		zephir_update_property_this(this_ptr, SL("except"), _8 TSRMLS_CC);
	}
	ZEPHIR_OBS_NVAR(_7);
	zephir_read_property_this(&_7, this_ptr, SL("only"), PH_NOISY_CC);
	if (Z_TYPE_P(_7) == IS_NULL) {
		ZEPHIR_INIT_NVAR(_8);
		array_init(_8);
		zephir_update_property_this(this_ptr, SL("only"), _8 TSRMLS_CC);
	}
	_9 = zephir_fetch_nproperty_this(this_ptr, SL("except"), PH_NOISY_CC);
	ZEPHIR_CALL_FUNCTION(&_5, "in_array", &_10, id, _9, ZEPHIR_GLOBAL(global_true));
	zephir_check_call_status();
	if (!(zephir_is_true(_5))) {
		_11 = zephir_fetch_nproperty_this(this_ptr, SL("only"), PH_NOISY_CC);
		_2 = zephir_fast_count_int(_11 TSRMLS_CC) == 0;
		if (!(_2)) {
			_12 = zephir_fetch_nproperty_this(this_ptr, SL("only"), PH_NOISY_CC);
			ZEPHIR_CALL_FUNCTION(&_5, "in_array", &_10, id, _12, ZEPHIR_GLOBAL(global_true));
			zephir_check_call_status();
			_2 = zephir_is_true(_5);
		}
		if (_2) {
			RETURN_MM_BOOL(1);
		}
	}
	RETURN_MM_BOOL(0);

}

