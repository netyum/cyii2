
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/memory.h"
#include "kernel/concat.h"
#include "kernel/string.h"
#include "kernel/exception.h"
#include "kernel/hash.h"
#include "kernel/array.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Module is the base class for module and application classes.
 *
 * A module represents a sub-application which contains MVC elements by itself, such as
 * models, views, controllers, etc.
 *
 * A module may consist of [[modules|sub-modules]].
 *
 * [[components|Components]] may be registered with the module so that they are globally
 * accessible within the module.
 *
 * @property array $aliases List of path aliases to be defined. The array keys are alias names (must start
 * with '@') and the array values are the corresponding paths or aliases. See [[setAliases()]] for an example.
 * This property is write-only.
 * @property string $basePath The root directory of the module.
 * @property string $controllerPath The directory that contains the controller classes. This property is
 * read-only.
 * @property string $layoutPath The root directory of layout files. Defaults to "[[viewPath]]/layouts".
 * @property array $modules The modules (indexed by their IDs).
 * @property string $uniqueId The unique ID of the module. This property is read-only.
 * @property string $viewPath The root directory of view files. Defaults to "[[basePath]]/view".
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_Module) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, Module, yii, base_module, yii_di_servicelocator_ce, yii_base_module_method_entry, 0);

	/**
	 * @var array custom module parameters (name => value).
	 */
	zend_declare_property_null(yii_base_module_ce, SL("params"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string an ID that uniquely identifies this module among other modules which have the same [[module|parent]].
	 */
	zend_declare_property_null(yii_base_module_ce, SL("id"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var Module the parent module of this module. Null if this module does not have a parent.
	 */
	zend_declare_property_null(yii_base_module_ce, SL("module"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string|boolean the layout that should be applied for views within this module. This refers to a view name
	 * relative to [[layoutPath]]. If this is not set, it means the layout value of the [[module|parent module]]
	 * will be taken. If this is false, layout will be disabled within this module.
	 */
	zend_declare_property_null(yii_base_module_ce, SL("layout"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var array mapping from controller ID to controller configurations.
	 * Each name-value pair specifies the configuration of a single controller.
	 * A controller configuration can be either a string or an array.
	 * If the former, the string should be the fully qualified class name of the controller.
	 * If the latter, the array must contain a 'class' element which specifies
	 * the controller's fully qualified class name, and the rest of the name-value pairs
	 * in the array are used to initialize the corresponding controller properties. For example,
	 *
	 * ~~~
	 * [
	 *   'account' => 'app\controllers\UserController',
	 *   'article' => [
	 *      'class' => 'app\controllers\PostController',
	 *      'pageTitle' => 'something new',
	 *   ],
	 * ]
	 * ~~~
	 */
	zend_declare_property_null(yii_base_module_ce, SL("controllerMap"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the namespace that controller classes are in. If not set,
	 * it will use the "controllers" sub-namespace under the namespace of this module.
	 * For example, if the namespace of this module is "foo\bar", then the default
	 * controller namespace would be "foo\bar\controllers".
	 */
	zend_declare_property_null(yii_base_module_ce, SL("controllerNamespace"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the default route of this module. Defaults to 'default'.
	 * The route may consist of child module ID, controller ID, and/or action ID.
	 * For example, `help`, `post/create`, `admin/post/create`.
	 * If action ID is not given, it will take the default value as specified in
	 * [[Controller::defaultAction]].
	 */
	zend_declare_property_string(yii_base_module_ce, SL("defaultRoute"), "default", ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var string the root directory of the module.
	 */
	zend_declare_property_null(yii_base_module_ce, SL("_basePath"), ZEND_ACC_PROTECTED TSRMLS_CC);

	/**
	 * @var string the root directory that contains view files for this module
	 */
	zend_declare_property_null(yii_base_module_ce, SL("_viewPath"), ZEND_ACC_PROTECTED TSRMLS_CC);

	/**
	 * @var string the root directory that contains layout view files for this module.
	 */
	zend_declare_property_null(yii_base_module_ce, SL("_layoutPath"), ZEND_ACC_PROTECTED TSRMLS_CC);

	/**
	 * @var array child modules of this module
	 */
	zend_declare_property_null(yii_base_module_ce, SL("_modules"), ZEND_ACC_PROTECTED TSRMLS_CC);

	/**
	 * @event ActionEvent an event raised before executing a controller action.
	 * You may set [[ActionEvent::isValid]] to be false to cancel the action execution.
	 */
	zend_declare_class_constant_string(yii_base_module_ce, SL("EVENT_BEFORE_ACTION"), "beforeAction" TSRMLS_CC);

	/**
	 * @event ActionEvent an event raised after executing a controller action.
	 */
	zend_declare_class_constant_string(yii_base_module_ce, SL("EVENT_AFTER_ACTION"), "afterAction" TSRMLS_CC);

	return SUCCESS;

}

/**
 * Constructor.
 * @param string $id the ID of this module
 * @param Module $parent the parent module (if any)
 * @param array $config name-value pairs that will be used to initialize the object properties
 */
PHP_METHOD(yii_base_Module, __construct) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;
	zval *id_param = NULL, *parent = NULL, *config = NULL;
	zval *id = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 2, &id_param, &parent, &config);

	zephir_get_strval(id, id_param);
	if (!parent) {
		parent = ZEPHIR_GLOBAL(global_null);
	}
	if (!config) {
		ZEPHIR_INIT_VAR(config);
		array_init(config);
	}


	zephir_update_property_this(this_ptr, SL("id"), id TSRMLS_CC);
	if (Z_TYPE_P(parent) != IS_NULL) {
		zephir_update_property_this(this_ptr, SL("module"), parent TSRMLS_CC);
	}
	ZEPHIR_CALL_PARENT(NULL, yii_base_module_ce, this_ptr, "__construct", &_0, config);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Initializes the module.
 *
 * This method is called after the module is created and initialized with property values
 * given in configuration. The default implementation will initialize [[controllerNamespace]]
 * if it is not set.
 *
 * If you override this method, please make sure you call the parent implementation.
 */
PHP_METHOD(yii_base_Module, init) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL, *_4 = NULL;
	zval *class, *pos = NULL, *_0, _1 = zval_used_for_init, *_3 = NULL, *_5;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("controllerNamespace"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		ZEPHIR_INIT_VAR(class);
		zephir_get_class(class, this_ptr, 0 TSRMLS_CC);
		ZEPHIR_SINIT_VAR(_1);
		ZVAL_STRING(&_1, "\\", 0);
		ZEPHIR_CALL_FUNCTION(&pos, "strrpos", &_2, class, &_1);
		zephir_check_call_status();
		if (Z_TYPE_P(pos) != IS_BOOL) {
			ZEPHIR_SINIT_NVAR(_1);
			ZVAL_LONG(&_1, 0);
			ZEPHIR_CALL_FUNCTION(&_3, "substr", &_4, class, &_1, pos);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(_5);
			ZEPHIR_CONCAT_VS(_5, _3, "\\controllers");
			zephir_update_property_this(this_ptr, SL("controllerNamespace"), _5 TSRMLS_CC);
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns an ID that uniquely identifies this module among all modules within the current application.
 * Note that if the module is an application, an empty string will be returned.
 * @return string the unique ID of the module.
 */
PHP_METHOD(yii_base_Module, getUniqueId) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *id, *module = NULL, *this_id, *temp_id, *unique_id = NULL, *slash, *_0;

	ZEPHIR_MM_GROW();

	_0 = zephir_fetch_nproperty_this(this_ptr, SL("module"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(module, _0);
	ZEPHIR_OBS_VAR(this_id);
	zephir_read_property_this(&this_id, this_ptr, SL("id"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(slash);
	ZVAL_STRING(slash, "/", 1);
	if (Z_TYPE_P(module) != IS_NULL) {
		ZEPHIR_CALL_METHOD(&unique_id, module, "getuniqueid",  NULL);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(temp_id);
		ZEPHIR_CONCAT_VVV(temp_id, unique_id, slash, this_id);
		ZEPHIR_INIT_VAR(id);
		zephir_fast_trim(id, temp_id, slash, ZEPHIR_TRIM_LEFT TSRMLS_CC);
		RETURN_CCTOR(id);
	} else {
		RETURN_CCTOR(this_id);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the root directory of the module.
 * It defaults to the directory containing the module class file.
 * @return string the root directory of the module.
 */
PHP_METHOD(yii_base_Module, getBasePath) {

	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_class_entry *_1;
	zval *class, *name = NULL, *_0, *_2 = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("_basePath"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		ZEPHIR_INIT_VAR(class);
		_1 = zend_fetch_class(SL("ReflectionClass"), ZEND_FETCH_CLASS_AUTO TSRMLS_CC);
		object_init_ex(class, _1);
		ZEPHIR_CALL_METHOD(NULL, class, "__construct", NULL, this_ptr);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&name, class, "getfilename",  NULL);
		zephir_check_call_status();
		ZEPHIR_CALL_FUNCTION(&_2, "dirname", &_3, name);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(name, _2);
		zephir_update_property_this(this_ptr, SL("_basePath"), name TSRMLS_CC);
	}
	RETURN_MM_MEMBER(this_ptr, "_basePath");

}

/**
 * Sets the root directory of the module.
 * This method can only be invoked at the beginning of the constructor.
 * @param string $path the root directory of the module. This can be either a directory name or a path alias.
 * @throws InvalidParamException if the directory does not exist.
 */
PHP_METHOD(yii_base_Module, setBasePath) {

	zend_bool _3;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL, *_2 = NULL, *_5 = NULL;
	zval *path_param = NULL, *p = NULL, *_0 = NULL, *_4 = NULL, *_6;
	zval *path = NULL, *_7;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &path_param);

	zephir_get_strval(path, path_param);
	ZEPHIR_SEPARATE_PARAM(path);


	ZEPHIR_CALL_CE_STATIC(&_0, yii_baseyii_ce, "getalias", &_1, path);
	zephir_check_call_status();
	zephir_get_strval(path, _0);
	ZEPHIR_CALL_FUNCTION(&p, "realpath", &_2, path);
	zephir_check_call_status();
	_3 = Z_TYPE_P(p) != IS_BOOL;
	if (_3) {
		ZEPHIR_CALL_FUNCTION(&_4, "is_dir", &_5, p);
		zephir_check_call_status();
		_3 = zephir_is_true(_4);
	}
	if (_3) {
		zephir_update_property_this(this_ptr, SL("_basePath"), p TSRMLS_CC);
	} else {
		ZEPHIR_INIT_VAR(_6);
		object_init_ex(_6, yii_base_invalidparamexception_ce);
		ZEPHIR_INIT_VAR(_7);
		ZEPHIR_CONCAT_SV(_7, "The directory does not exist: ", path);
		ZEPHIR_CALL_METHOD(NULL, _6, "__construct", NULL, _7);
		zephir_check_call_status();
		zephir_throw_exception_debug(_6, "yii/base/Module.zep", 214 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the directory that contains the controller classes according to [[controllerNamespace]].
 * Note that in order for this method to return a value, you must define
 * an alias for the root namespace of [[controllerNamespace]].
 * @return string the directory that contains the controller classes.
 * @throws InvalidParamException if there is no alias defined for the root namespace of [[controllerNamespace]].
 */
PHP_METHOD(yii_base_Module, getControllerPath) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL, *_5 = NULL;
	zval *path = NULL, _0, _1, *_2 = NULL, *_4;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(path);
	zephir_read_property_this(&path, this_ptr, SL("controllerNamespace"), PH_NOISY_CC);
	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "\\", 0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, "/", 0);
	ZEPHIR_CALL_FUNCTION(&_2, "str_replace", &_3, &_0, &_1, path);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(path, _2);
	ZEPHIR_INIT_VAR(_4);
	ZEPHIR_CONCAT_SV(_4, "@", path);
	ZEPHIR_CPY_WRT(path, _4);
	ZEPHIR_RETURN_CALL_CE_STATIC(yii_baseyii_ce, "getalias", &_5, path);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns the directory that contains the view files for this module.
 * @return string the root directory of view files. Defaults to "[[basePath]]/view".
 */
PHP_METHOD(yii_base_Module, getViewPath) {

	zval *_1;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *view_path = NULL, *_0;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("_viewPath"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) != IS_NULL) {
		RETURN_MM_MEMBER(this_ptr, "_viewPath");
	} else {
		ZEPHIR_CALL_METHOD(&view_path, this_ptr, "getbasepath",  NULL);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(_1);
		ZEPHIR_CONCAT_SS(_1, "/", "views");
		zephir_concat_self(&view_path, _1 TSRMLS_CC);
		zephir_update_property_this(this_ptr, SL("_viewPath"), view_path TSRMLS_CC);
		RETURN_CCTOR(view_path);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Sets the directory that contains the view files.
 * @param string $path the root directory of view files.
 * @throws InvalidParamException if the directory is invalid
 */
PHP_METHOD(yii_base_Module, setViewPath) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	zval *path_param = NULL, *_0 = NULL;
	zval *path = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &path_param);

	zephir_get_strval(path, path_param);


	ZEPHIR_CALL_CE_STATIC(&_0, yii_baseyii_ce, "getalias", &_1, path);
	zephir_check_call_status();
	zephir_update_property_this(this_ptr, SL("_viewPath"), _0 TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the directory that contains layout view files for this module.
 * @return string the root directory of layout files. Defaults to "[[viewPath]]/layouts".
 */
PHP_METHOD(yii_base_Module, getLayoutPath) {

	zval *_1;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *layout_path = NULL, *_0;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("_layoutPath"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) != IS_NULL) {
		RETURN_MM_MEMBER(this_ptr, "_layoutPath");
	} else {
		ZEPHIR_CALL_METHOD(&layout_path, this_ptr, "getviewpath",  NULL);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(_1);
		ZEPHIR_CONCAT_SS(_1, "/", "layouts");
		zephir_concat_self(&layout_path, _1 TSRMLS_CC);
		zephir_update_property_this(this_ptr, SL("_layoutPath"), layout_path TSRMLS_CC);
		RETURN_CCTOR(layout_path);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Sets the directory that contains the layout files.
 * @param string $path the root directory of layout files.
 * @throws InvalidParamException if the directory is invalid
 */
PHP_METHOD(yii_base_Module, setLayoutPath) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	zval *path_param = NULL, *_0 = NULL;
	zval *path = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &path_param);

	zephir_get_strval(path, path_param);


	ZEPHIR_CALL_CE_STATIC(&_0, yii_baseyii_ce, "getalias", &_1, path);
	zephir_check_call_status();
	zephir_update_property_this(this_ptr, SL("_layoutPath"), _0 TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Defines path aliases.
 * This method calls [[Yii::setAlias()]] to register the path aliases.
 * This method is provided so that you can define path aliases when configuring a module.
 * @property array list of path aliases to be defined. The array keys are alias names
 * (must start with '@') and the array values are the corresponding paths or aliases.
 * See [[setAliases()]] for an example.
 * @param array $aliases list of path aliases to be defined. The array keys are alias names
 * (must start with '@') and the array values are the corresponding paths or aliases.
 * For example,
 *
 * ~~~
 * [
 *     '@models' => '@app/models', // an existing alias
 *     '@backend' => __DIR__ . '/../backend',  // a directory
 * ]
 * ~~~
 */
PHP_METHOD(yii_base_Module, setAliases) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_fcall_cache_entry *_3 = NULL;
	HashTable *_1;
	HashPosition _0;
	zval *aliases, *temp_aliases = NULL, *alias = NULL, *name = NULL, **_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &aliases);



	ZEPHIR_CPY_WRT(temp_aliases, aliases);
	zephir_is_iterable(temp_aliases, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(name, _1, _0);
		ZEPHIR_GET_HVALUE(alias, _2);
		ZEPHIR_CALL_CE_STATIC(NULL, yii_baseyii_ce, "setalias", &_3, name, alias);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Checks whether the child module of the specified ID exists.
 * This method supports checking the existence of both child and grand child modules.
 * @param string $id module ID. For grand child modules, use ID path relative to this module (e.g. `admin/content`).
 * @return boolean whether the named module exists. Both loaded and unloaded modules
 * are considered.
 */
PHP_METHOD(yii_base_Module, hasModule) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL;
	zval *id_param = NULL, *pos, *sub_id = NULL, *sub_id2 = NULL, *module = NULL, *retval = NULL, _0, _1 = zval_used_for_init, *temp_module;
	zval *id = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &id_param);

	zephir_get_strval(id, id_param);


	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "/", 0);
	ZEPHIR_INIT_VAR(pos);
	zephir_fast_strpos(pos, id, &_0, 0 );
	if (Z_TYPE_P(pos) != IS_BOOL) {
		ZEPHIR_SINIT_VAR(_1);
		ZVAL_LONG(&_1, 0);
		ZEPHIR_CALL_FUNCTION(&sub_id, "substr", &_2, id, &_1, pos);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&module, this_ptr, "getmodule", NULL, sub_id);
		zephir_check_call_status();
		if (Z_TYPE_P(module) == IS_NULL) {
			RETURN_MM_BOOL(0);
		} else {
			ZEPHIR_SINIT_NVAR(_1);
			ZVAL_LONG(&_1, (zephir_get_numberval(pos) + 1));
			ZEPHIR_CALL_FUNCTION(&sub_id2, "substr", &_2, id, &_1);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(&retval, module, "hasmodule", NULL, sub_id2);
			zephir_check_call_status();
			RETURN_CCTOR(retval);
		}
	} else {
		temp_module = zephir_fetch_nproperty_this(this_ptr, SL("_modules"), PH_NOISY_CC);
		if (zephir_array_isset(temp_module, id)) {
			RETURN_MM_BOOL(1);
		} else {
			RETURN_MM_BOOL(0);
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Retrieves the child module of the specified ID.
 * This method supports retrieving both child modules and grand child modules.
 * @param string $id module ID (case-sensitive). To retrieve grand child modules,
 * use ID path relative to this module (e.g. `admin/content`).
 * @param boolean $load whether to load the module if it is not yet loaded.
 * @return Module|null the module instance, null if the module does not exist.
 * @see hasModule()
 */
PHP_METHOD(yii_base_Module, getModule) {

	zend_bool _6;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL, *_3 = NULL, *_7 = NULL;
	zval *id_param = NULL, *load = NULL, *pos, *sub_id = NULL, *sub_id2 = NULL, *module = NULL, *retval = NULL, *modules, *mo, _0, _1 = zval_used_for_init, *_5 = NULL, *elements;
	zval *id = NULL, *_4;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &id_param, &load);

	zephir_get_strval(id, id_param);
	if (!load) {
		load = ZEPHIR_GLOBAL(global_true);
	}
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "/", 0);
	ZEPHIR_INIT_VAR(pos);
	zephir_fast_strpos(pos, id, &_0, 0 );
	if (Z_TYPE_P(pos) != IS_BOOL) {
		ZEPHIR_SINIT_VAR(_1);
		ZVAL_LONG(&_1, 0);
		ZEPHIR_CALL_FUNCTION(&sub_id, "substr", &_2, id, &_1, pos);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&module, this_ptr, "getmodule", NULL, id);
		zephir_check_call_status();
		if (Z_TYPE_P(module) == IS_NULL) {
			RETURN_MM_NULL();
		} else {
			ZEPHIR_SINIT_NVAR(_1);
			ZVAL_LONG(&_1, (zephir_get_numberval(pos) + 1));
			ZEPHIR_CALL_FUNCTION(&sub_id2, "substr", &_2, id, &_1);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(&retval, module, "getmodule", NULL, sub_id2, load);
			zephir_check_call_status();
			RETURN_CCTOR(retval);
		}
	}
	ZEPHIR_OBS_VAR(modules);
	zephir_read_property_this(&modules, this_ptr, SL("_modules"), PH_NOISY_CC);
	ZEPHIR_OBS_VAR(mo);
	if (zephir_array_isset_fetch(&mo, modules, id, 0 TSRMLS_CC)) {
		if (Z_TYPE_P(mo) == IS_OBJECT) {
			if (zephir_instance_of_ev(mo, yii_base_module_ce TSRMLS_CC)) {
				RETURN_CCTOR(mo);
			}
		} else {
			if (ZEPHIR_IS_TRUE(load)) {
				ZEPHIR_INIT_VAR(_4);
				ZEPHIR_CONCAT_SV(_4, "Loading module: ", id);
				ZEPHIR_INIT_VAR(_5);
				ZVAL_STRING(_5, "Module:getModule", 0);
				ZEPHIR_CALL_CE_STATIC(NULL, yii_baseyii_ce, "trace", &_3, _4, _5);
				zephir_check_temp_parameter(_5);
				zephir_check_call_status();
				_6 = Z_TYPE_P(mo) == IS_ARRAY;
				if (_6) {
					_6 = !zephir_array_isset_string(mo, SS("class"));
				}
				if (_6) {
					ZEPHIR_INIT_NVAR(_5);
					ZVAL_STRING(_5, "yii\\base\\Module", 1);
					zephir_array_update_string(&mo, SL("class"), &_5, PH_COPY | PH_SEPARATE);
				}
				zephir_array_append(&elements, id, PH_SEPARATE);
				zephir_array_append(&elements, this_ptr, PH_SEPARATE);
				ZEPHIR_CALL_CE_STATIC(&retval, yii_baseyii_ce, "createobject", &_7, mo, elements);
				zephir_check_call_status();
				zephir_array_update_zval(&modules, id, &retval, PH_COPY | PH_SEPARATE);
				zephir_update_property_this(this_ptr, SL("_modules"), modules TSRMLS_CC);
				RETURN_CCTOR(retval);
			}
		}
	}
	RETURN_MM_NULL();

}

/**
 * Adds a sub-module to this module.
 * @param string $id module ID
 * @param Module|array|null $module the sub-module to be added to this module. This can
 * be one of the followings:
 *
 * - a [[Module]] object
 * - a configuration array: when [[getModule()]] is called initially, the array
 *   will be used to instantiate the sub-module
 * - null: the named sub-module will be removed from this module
 */
PHP_METHOD(yii_base_Module, setModule) {

	zval *id_param = NULL, *module, *modules, *temp_module = NULL;
	zval *id = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &id_param, &module);

	zephir_get_strval(id, id_param);


	ZEPHIR_OBS_VAR(modules);
	zephir_read_property_this(&modules, this_ptr, SL("_modules"), PH_NOISY_CC);
	if (Z_TYPE_P(module) == IS_NULL) {
		if (zephir_array_isset(modules, id)) {
			zephir_array_unset(&modules, id, PH_SEPARATE);
		}
	} else {
		ZEPHIR_CPY_WRT(temp_module, module);
		zephir_array_update_zval(&modules, id, &temp_module, PH_COPY | PH_SEPARATE);
	}
	zephir_update_property_this(this_ptr, SL("_modules"), modules TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the sub-modules in this module.
 * @param boolean $loadedOnly whether to return the loaded sub-modules only. If this is set false,
 * then all sub-modules registered in this module will be returned, whether they are loaded or not.
 * Loaded modules will be returned as objects, while unloaded modules as configuration arrays.
 * @return array the modules (indexed by their IDs)
 */
PHP_METHOD(yii_base_Module, getModules) {

	HashTable *_1;
	HashPosition _0;
	zval *loadedOnly_param = NULL, *new_modules, *modules, *module = NULL, **_2;
	zend_bool loadedOnly;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &loadedOnly_param);

	if (!loadedOnly_param) {
		loadedOnly = 0;
	} else {
		loadedOnly = zephir_get_boolval(loadedOnly_param);
	}


	ZEPHIR_OBS_VAR(modules);
	zephir_read_property_this(&modules, this_ptr, SL("_modules"), PH_NOISY_CC);
	if (Z_TYPE_P(modules) == IS_NULL) {
		ZEPHIR_INIT_BNVAR(modules);
		array_init(modules);
		zephir_update_property_this(this_ptr, SL("_modules"), modules TSRMLS_CC);
	}
	if (loadedOnly) {
		ZEPHIR_INIT_VAR(new_modules);
		array_init(new_modules);
		zephir_is_iterable(modules, &_1, &_0, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
		  ; zephir_hash_move_forward_ex(_1, &_0)
		) {
			ZEPHIR_GET_HVALUE(module, _2);
			if (zephir_instance_of_ev(module, yii_base_module_ce TSRMLS_CC)) {
				zephir_array_append(&new_modules, module, PH_SEPARATE);
			}
		}
		RETURN_CCTOR(new_modules);
	} else {
		RETURN_CCTOR(modules);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Registers sub-modules in the current module.
 *
 * Each sub-module should be specified as a name-value pair, where
 * name refers to the ID of the module and value the module or a configuration
 * array that can be used to create the module. In the latter case, [[Yii::createObject()]]
 * will be used to create the module.
 *
 * If a new sub-module has the same ID as an existing one, the existing one will be overwritten silently.
 *
 * The following is an example for registering two sub-modules:
 *
 * ~~~
 * [
 *     'comment' => [
 *         'class' => 'app\modules\comment\CommentModule',
 *         'db' => 'db',
 *     ],
 *     'booking' => ['class' => 'app\modules\booking\BookingModule'],
 * ]
 * ~~~
 *
 * @param array $modules modules (id => module configuration or instances)
 */
PHP_METHOD(yii_base_Module, setModules) {

	HashTable *_1;
	HashPosition _0;
	zval *modules, *id = NULL, *temp_modules = NULL, *module = NULL, *this_modules, **_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &modules);



	ZEPHIR_OBS_VAR(this_modules);
	zephir_read_property_this(&this_modules, this_ptr, SL("_modules"), PH_NOISY_CC);
	if (Z_TYPE_P(this_modules) != IS_ARRAY) {
		ZEPHIR_INIT_BNVAR(this_modules);
		array_init(this_modules);
	}
	ZEPHIR_CPY_WRT(temp_modules, modules);
	zephir_is_iterable(temp_modules, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(id, _1, _0);
		ZEPHIR_GET_HVALUE(module, _2);
		zephir_array_update_zval(&this_modules, id, &module, PH_COPY | PH_SEPARATE);
	}
	zephir_update_property_this(this_ptr, SL("_modules"), this_modules TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Runs a controller action specified by a route.
 * This method parses the specified route and creates the corresponding child module(s), controller and action
 * instances. It then calls [[Controller::runAction()]] to run the action with the given parameters.
 * If the route is empty, the method will use [[defaultRoute]].
 * @param string $route the route that specifies the action.
 * @param array $params the parameters to be passed to the action
 * @return mixed the result of the action.
 * @throws InvalidRouteException if the requested route cannot be resolved into an action successfully
 */
PHP_METHOD(yii_base_Module, runAction) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *route_param = NULL, *params = NULL, *parts = NULL, *id = NULL, *controller, *actionID, *oldController, *result = NULL, *app = NULL, *_0, *message = NULL, *_1, *_2;
	zval *route = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &route_param, &params);

	zephir_get_strval(route, route_param);
	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}


	ZEPHIR_CALL_METHOD(&parts, this_ptr, "createcontroller", NULL, route);
	zephir_check_call_status();
	if (Z_TYPE_P(parts) == IS_ARRAY) {
		ZEPHIR_OBS_VAR(controller);
		zephir_array_fetch_long(&controller, parts, 0, PH_NOISY TSRMLS_CC);
		ZEPHIR_OBS_VAR(actionID);
		zephir_array_fetch_long(&actionID, parts, 1, PH_NOISY TSRMLS_CC);
		zephir_read_static_property_ce(&_0, yii_baseyii_ce, SL("app") TSRMLS_CC);
		ZEPHIR_CPY_WRT(app, _0);
		ZEPHIR_OBS_VAR(oldController);
		zephir_read_property(&oldController, app, SL("controller"), PH_NOISY_CC);
		zephir_update_property_zval(app, SL("controller"), controller TSRMLS_CC);
		ZEPHIR_CALL_METHOD(&result, controller, "runaction", NULL, actionID, params);
		zephir_check_call_status();
		RETURN_CCTOR(result);
	} else {
		ZEPHIR_CALL_METHOD(&id, this_ptr, "getuniqueid",  NULL);
		zephir_check_call_status();
		if (ZEPHIR_IS_STRING(id, "")) {
			ZEPHIR_CPY_WRT(message, route);
		} else {
			ZEPHIR_INIT_VAR(message);
			ZEPHIR_CONCAT_VSV(message, id, "/", route);
		}
		ZEPHIR_INIT_VAR(_1);
		object_init_ex(_1, yii_base_invalidrouteexception_ce);
		ZEPHIR_INIT_VAR(_2);
		ZEPHIR_CONCAT_SVS(_2, "Unable to resolve the request \"", message, "\".");
		ZEPHIR_CALL_METHOD(NULL, _1, "__construct", NULL, _2);
		zephir_check_call_status();
		zephir_throw_exception_debug(_1, "yii/base/Module.zep", 542 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Creates a controller instance based on the given route.
 *
 * The route should be relative to this module. The method implements the following algorithm
 * to resolve the given route:
 *
 * 1. If the route is empty, use [[defaultRoute]];
 * 2. If the first segment of the route is a valid module ID as declared in [[modules]],
 *    call the module's `createController()` with the rest part of the route;
 * 3. If the first segment of the route is found in [[controllerMap]], create a controller
 *    based on the corresponding configuration found in [[controllerMap]];
 * 4. The given route is in the format of `abc/def/xyz`. Try either `abc\DefController`
 *    or `abc\def\XyzController` class within the [[controllerNamespace|controller namespace]].
 *
 * If any of the above steps resolves into a controller, it is returned together with the rest
 * part of the route which will be treated as the action ID. Otherwise, false will be returned.
 *
 * @param string $route the route consisting of module, controller and action IDs.
 * @return array|boolean If the controller is created successfully, it will be returned together
 * with the requested action ID. Otherwise false will be returned.
 * @throws InvalidConfigException if the controller class and its file do not match.
 */
PHP_METHOD(yii_base_Module, createController) {

	zend_bool _9;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL, *_3 = NULL, *_6 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *route = NULL, *slash, *pos = NULL, *_0, _1, *id = NULL, *explodes, *module = NULL, *controller_map, *controller_id, *controller = NULL, *return_elements, *elements, _4 = zval_used_for_init, *_5 = NULL, *_7 = NULL, *_8 = NULL, *temp_controller = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &route);

	ZEPHIR_SEPARATE_PARAM(route);
	ZEPHIR_INIT_VAR(return_elements);
	array_init(return_elements);
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	if (ZEPHIR_IS_STRING(route, "")) {
		ZEPHIR_OBS_NVAR(route);
		zephir_read_property_this(&route, this_ptr, SL("defaultRoute"), PH_NOISY_CC);
	}
	ZEPHIR_INIT_VAR(slash);
	ZVAL_STRING(slash, "/", 1);
	ZEPHIR_INIT_VAR(_0);
	zephir_fast_trim(_0, route, slash, ZEPHIR_TRIM_BOTH TSRMLS_CC);
	ZEPHIR_CPY_WRT(route, _0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, "//", 0);
	ZEPHIR_INIT_VAR(pos);
	zephir_fast_strpos(pos, route, &_1, 0 );
	if (Z_TYPE_P(pos) != IS_BOOL) {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_INIT_BNVAR(pos);
	zephir_fast_strpos(pos, route, slash, 0 );
	if (Z_TYPE_P(pos) != IS_BOOL) {
		ZEPHIR_INIT_VAR(explodes);
		zephir_fast_explode(explodes, slash, route, 2  TSRMLS_CC);
		ZEPHIR_OBS_VAR(id);
		zephir_array_fetch_long(&id, explodes, 0, PH_NOISY TSRMLS_CC);
		ZEPHIR_OBS_NVAR(route);
		zephir_array_fetch_long(&route, explodes, 1, PH_NOISY TSRMLS_CC);
	} else {
		ZEPHIR_CPY_WRT(id, route);
		ZEPHIR_INIT_NVAR(route);
		ZVAL_STRING(route, "", 1);
	}
	ZEPHIR_CALL_METHOD(&module, this_ptr, "getmodule", NULL, id);
	zephir_check_call_status();
	if (Z_TYPE_P(module) != IS_NULL) {
		ZEPHIR_RETURN_CALL_METHOD(module, "createcontroller", NULL, route);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_OBS_VAR(controller_map);
	zephir_read_property_this(&controller_map, this_ptr, SL("controllerMap"), PH_NOISY_CC);
	ZEPHIR_OBS_VAR(controller_id);
	if (zephir_array_isset_fetch(&controller_id, controller_map, id, 0 TSRMLS_CC)) {
		zephir_array_append(&elements, id, PH_SEPARATE);
		zephir_array_append(&elements, this_ptr, PH_SEPARATE);
		ZEPHIR_CALL_CE_STATIC(&controller, yii_baseyii_ce, "createobject", &_2, controller_id, elements);
		zephir_check_call_status();
		zephir_array_append(&return_elements, controller, PH_SEPARATE);
		zephir_array_append(&return_elements, route, PH_SEPARATE);
		RETURN_CCTOR(return_elements);
	}
	ZEPHIR_CALL_FUNCTION(&pos, "strrpos", &_3, route, slash);
	zephir_check_call_status();
	if (Z_TYPE_P(pos) != IS_BOOL) {
		ZEPHIR_SINIT_VAR(_4);
		ZVAL_LONG(&_4, 0);
		ZEPHIR_CALL_FUNCTION(&_5, "substr", &_6, route, &_4, pos);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(_7);
		ZEPHIR_CONCAT_VV(_7, slash, _5);
		zephir_concat_self(&id, _7 TSRMLS_CC);
		ZEPHIR_SINIT_NVAR(_4);
		ZVAL_LONG(&_4, (zephir_get_numberval(pos) + 1));
		ZEPHIR_CALL_FUNCTION(&_8, "substr", &_6, route, &_4);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(route, _8);
	}
	ZEPHIR_CALL_METHOD(&controller, this_ptr, "createcontrollerbyid", NULL, id);
	zephir_check_call_status();
	_9 = Z_TYPE_P(controller) == IS_NULL;
	if (_9) {
		_9 = !ZEPHIR_IS_STRING(route, "");
	}
	if (_9) {
		ZEPHIR_INIT_LNVAR(_7);
		ZEPHIR_CONCAT_VVV(_7, id, slash, route);
		ZEPHIR_CALL_METHOD(&controller, this_ptr, "createcontrollerbyid", NULL, _7);
		zephir_check_call_status();
		ZEPHIR_INIT_NVAR(route);
		ZVAL_STRING(route, "", 1);
	}
	if (Z_TYPE_P(controller) == IS_NULL) {
		RETURN_MM_BOOL(0);
	} else {
		ZEPHIR_CPY_WRT(temp_controller, controller);
		zephir_array_update_long(&return_elements, 0, &temp_controller, PH_COPY | PH_SEPARATE, "yii/base/Module.zep", 634);
		zephir_array_update_long(&return_elements, 1, &route, PH_COPY | PH_SEPARATE, "yii/base/Module.zep", 635);
		RETURN_CCTOR(return_elements);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Creates a controller based on the given controller ID.
 *
 * The controller ID is relative to this module. The controller class
 * should be namespaced under [[controllerNamespace]].
 *
 * Note that this method does not check [[modules]] or [[controllerMap]].
 *
 * @param string $id the controller ID
 * @return Controller the newly created controller instance, or null if the controller ID is invalid.
 * @throws InvalidConfigException if the controller class and its file name do not match.
 * This exception is only thrown when in debug mode.
 */
PHP_METHOD(yii_base_Module, createControllerByID) {

	zend_bool _11;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL, *_3 = NULL, *_5 = NULL, *_6 = NULL, *_7 = NULL, *_13 = NULL, *_14 = NULL;
	zval *id_param = NULL, _0 = zval_used_for_init, *_1 = NULL, *pos = NULL, *prefix = NULL, *className = NULL, _4 = zval_used_for_init, *_8 = NULL, *_9, *_10, *_12 = NULL, *elements;
	zval *id = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &id_param);

	zephir_get_strval(id, id_param);
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	ZEPHIR_SINIT_VAR(_0);
	ZVAL_STRING(&_0, "%^[a-z0-9\\-_/]+$%", 0);
	ZEPHIR_CALL_FUNCTION(&_1, "preg_match", &_2, &_0, id);
	zephir_check_call_status();
	if (!(zephir_is_true(_1))) {
		RETURN_MM_NULL();
	}
	ZEPHIR_SINIT_NVAR(_0);
	ZVAL_STRING(&_0, "/", 0);
	ZEPHIR_CALL_FUNCTION(&pos, "strrpos", &_3, id, &_0);
	zephir_check_call_status();
	if (Z_TYPE_P(pos) == IS_BOOL) {
		ZEPHIR_INIT_VAR(prefix);
		ZVAL_STRING(prefix, "", 1);
		ZEPHIR_CPY_WRT(className, id);
	} else {
		ZEPHIR_SINIT_NVAR(_0);
		ZVAL_LONG(&_0, 0);
		ZEPHIR_SINIT_VAR(_4);
		ZVAL_LONG(&_4, (zephir_get_numberval(pos) + 1));
		ZEPHIR_CALL_FUNCTION(&prefix, "substr", &_5, id, &_0, &_4);
		zephir_check_call_status();
		ZEPHIR_SINIT_NVAR(_0);
		ZVAL_LONG(&_0, (zephir_get_numberval(pos) + 1));
		ZEPHIR_CALL_FUNCTION(&className, "substr", &_5, id, &_0);
		zephir_check_call_status();
	}
	ZEPHIR_SINIT_NVAR(_0);
	ZVAL_STRING(&_0, "-", 0);
	ZEPHIR_SINIT_NVAR(_4);
	ZVAL_STRING(&_4, " ", 0);
	ZEPHIR_CALL_FUNCTION(&_1, "str_replace", &_6, &_0, &_4, className);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(className, _1);
	ZEPHIR_CALL_FUNCTION(&_1, "ucwords", &_7, className);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(className, _1);
	ZEPHIR_SINIT_NVAR(_0);
	ZVAL_STRING(&_0, " ", 0);
	ZEPHIR_SINIT_NVAR(_4);
	ZVAL_STRING(&_4, "", 0);
	ZEPHIR_CALL_FUNCTION(&_1, "str_replace", &_6, &_0, &_4, className);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(className, _1);
	ZEPHIR_INIT_VAR(_8);
	ZEPHIR_CONCAT_VS(_8, className, "Controller");
	ZEPHIR_CPY_WRT(className, _8);
	ZEPHIR_SINIT_NVAR(_0);
	ZVAL_STRING(&_0, "/", 0);
	ZEPHIR_SINIT_NVAR(_4);
	ZVAL_STRING(&_4, "\\", 0);
	ZEPHIR_CALL_FUNCTION(&_1, "str_replace", &_6, &_0, &_4, prefix);
	zephir_check_call_status();
	ZEPHIR_INIT_LNVAR(_8);
	ZEPHIR_CONCAT_VV(_8, _1, className);
	ZEPHIR_CPY_WRT(className, _8);
	ZEPHIR_INIT_LNVAR(_8);
	ZEPHIR_CONCAT_SV(_8, "\\", className);
	ZEPHIR_CPY_WRT(className, _8);
	_9 = zephir_fetch_nproperty_this(this_ptr, SL("controllerNamespace"), PH_NOISY_CC);
	ZEPHIR_INIT_LNVAR(_8);
	ZEPHIR_CONCAT_VV(_8, _9, className);
	ZEPHIR_CPY_WRT(className, _8);
	ZEPHIR_INIT_VAR(_10);
	ZEPHIR_SINIT_NVAR(_0);
	ZVAL_STRING(&_0, "\\", 0);
	zephir_fast_trim(_10, className, &_0, ZEPHIR_TRIM_LEFT TSRMLS_CC);
	ZEPHIR_CPY_WRT(className, _10);
	ZEPHIR_SINIT_NVAR(_4);
	ZVAL_STRING(&_4, "-", 0);
	ZEPHIR_INIT_BNVAR(pos);
	zephir_fast_strpos(pos, className, &_4, 0 );
	_11 = Z_TYPE_P(pos) != IS_BOOL;
	if (!(_11)) {
		_11 = !zephir_class_exists(className, 1 TSRMLS_CC);
	}
	if (_11) {
		RETURN_MM_NULL();
	}
	ZEPHIR_SINIT_NVAR(_4);
	ZVAL_STRING(&_4, "yii\\base\\Controller", 0);
	ZEPHIR_CALL_FUNCTION(&_12, "is_subclass_of", &_13, className, &_4);
	zephir_check_call_status();
	if (zephir_is_true(_12)) {
		zephir_array_append(&elements, id, PH_SEPARATE);
		zephir_array_append(&elements, this_ptr, PH_SEPARATE);
		ZEPHIR_RETURN_CALL_CE_STATIC(yii_baseyii_ce, "createobject", &_14, className, elements);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		ZEPHIR_INIT_BNVAR(_10);
		ZEPHIR_GET_CONSTANT(_10, "YII_DEBUG");
		if (zephir_is_true(_10)) {
			ZEPHIR_THROW_EXCEPTION_DEBUG_STR(yii_base_invalidconfigexception_ce, "Controller class must extend from \\yii\\base\\Controller.", "yii/base/Module.zep", 693);
			return;
		} else {
			RETURN_MM_NULL();
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * This method is invoked right before an action within this module is executed.
 *
 * The method will trigger the [[EVENT_BEFORE_ACTION]] event. The return value of the method
 * will determine whether the action should continue to run.
 *
 * If you override this method, your code should look like the following:
 *
 * ```php
 * public function beforeAction($action)
 * {
 *     if (parent::beforeAction($action)) {
 *         // your custom code here
 *         return true;  // or false if needed
 *     } else {
 *         return false;
 *     }
 * }
 * ```
 *
 * @param Action $action the action to be executed.
 * @return boolean whether the action should continue to be executed.
 */
PHP_METHOD(yii_base_Module, beforeAction) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *action, *temp_action = NULL, *event, *_0, *_1;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &action);



	ZEPHIR_CPY_WRT(temp_action, action);
	ZEPHIR_INIT_VAR(event);
	object_init_ex(event, yii_base_actionevent_ce);
	ZEPHIR_CALL_METHOD(NULL, event, "__construct", NULL, temp_action);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_0);
	ZVAL_STRING(_0, "beforeAction", 0);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "trigger", NULL, _0, event);
	zephir_check_temp_parameter(_0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property(&_1, event, SL("isValid"), PH_NOISY_CC);
	RETURN_CCTOR(_1);

}

/**
 * This method is invoked right after an action within this module is executed.
 *
 * The method will trigger the [[EVENT_AFTER_ACTION]] event. The return value of the method
 * will be used as the action return value.
 *
 * If you override this method, your code should look like the following:
 *
 * ```php
 * public function afterAction($action, $result)
 * {
 *     $result = parent::afterAction($action, $result);
 *     // your custom code here
 *     return $result;
 * }
 * ```
 *
 * @param Action $action the action just executed.
 * @param mixed $result the action return result.
 * @return mixed the processed action result.
 */
PHP_METHOD(yii_base_Module, afterAction) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *action, *result, *temp_action = NULL, *temp_result = NULL, *event, *_0, *_1;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &action, &result);



	ZEPHIR_CPY_WRT(temp_action, action);
	ZEPHIR_CPY_WRT(temp_result, result);
	ZEPHIR_INIT_VAR(event);
	object_init_ex(event, yii_base_actionevent_ce);
	ZEPHIR_CALL_METHOD(NULL, event, "__construct", NULL, temp_action);
	zephir_check_call_status();
	zephir_update_property_zval(event, SL("result"), temp_result TSRMLS_CC);
	ZEPHIR_INIT_VAR(_0);
	ZVAL_STRING(_0, "afterAction", 0);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "trigger", NULL, _0, event);
	zephir_check_temp_parameter(_0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property(&_1, event, SL("result"), PH_NOISY_CC);
	RETURN_CCTOR(_1);

}

