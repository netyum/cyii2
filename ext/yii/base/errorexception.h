
extern zend_class_entry *yii_base_errorexception_ce;

ZEPHIR_INIT_CLASS(yii_base_ErrorException);

PHP_METHOD(yii_base_ErrorException, __construct);
PHP_METHOD(yii_base_ErrorException, isFatalError);
PHP_METHOD(yii_base_ErrorException, getName);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_errorexception___construct, 0, 0, 0)
	ZEND_ARG_INFO(0, message)
	ZEND_ARG_INFO(0, code)
	ZEND_ARG_INFO(0, severity)
	ZEND_ARG_INFO(0, filename)
	ZEND_ARG_INFO(0, lineno)
	ZEND_ARG_INFO(0, previous)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_errorexception_isfatalerror, 0, 0, 1)
	ZEND_ARG_INFO(0, error)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_errorexception_method_entry) {
	PHP_ME(yii_base_ErrorException, __construct, arginfo_yii_base_errorexception___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(yii_base_ErrorException, isFatalError, arginfo_yii_base_errorexception_isfatalerror, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_base_ErrorException, getName, NULL, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
