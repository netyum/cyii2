
extern zend_class_entry *yii_base_response_ce;

ZEPHIR_INIT_CLASS(yii_base_Response);

PHP_METHOD(yii_base_Response, send);
PHP_METHOD(yii_base_Response, clearOutputBuffers);

ZEPHIR_INIT_FUNCS(yii_base_response_method_entry) {
	PHP_ME(yii_base_Response, send, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Response, clearOutputBuffers, NULL, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
