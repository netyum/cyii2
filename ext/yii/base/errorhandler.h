
extern zend_class_entry *yii_base_errorhandler_ce;

ZEPHIR_INIT_CLASS(yii_base_ErrorHandler);

PHP_METHOD(yii_base_ErrorHandler, register);
PHP_METHOD(yii_base_ErrorHandler, handleException);
PHP_METHOD(yii_base_ErrorHandler, handleError);
PHP_METHOD(yii_base_ErrorHandler, handleFatalError);
PHP_METHOD(yii_base_ErrorHandler, renderException);
PHP_METHOD(yii_base_ErrorHandler, logException);
PHP_METHOD(yii_base_ErrorHandler, clearOutput);
PHP_METHOD(yii_base_ErrorHandler, convertExceptionToError);
PHP_METHOD(yii_base_ErrorHandler, convertExceptionToString);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_errorhandler_handleexception, 0, 0, 1)
	ZEND_ARG_INFO(0, obj_exception)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_errorhandler_handleerror, 0, 0, 4)
	ZEND_ARG_INFO(0, code)
	ZEND_ARG_INFO(0, message)
	ZEND_ARG_INFO(0, file)
	ZEND_ARG_INFO(0, line)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_errorhandler_renderexception, 0, 0, 1)
	ZEND_ARG_INFO(0, obj_exception)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_errorhandler_logexception, 0, 0, 1)
	ZEND_ARG_INFO(0, obj_exception)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_errorhandler_convertexceptiontoerror, 0, 0, 1)
	ZEND_ARG_INFO(0, obj_exception)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_errorhandler_convertexceptiontostring, 0, 0, 1)
	ZEND_ARG_INFO(0, obj_exception)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_errorhandler_method_entry) {
	PHP_ME(yii_base_ErrorHandler, register, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_ErrorHandler, handleException, arginfo_yii_base_errorhandler_handleexception, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_ErrorHandler, handleError, arginfo_yii_base_errorhandler_handleerror, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_ErrorHandler, handleFatalError, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_ErrorHandler, renderException, arginfo_yii_base_errorhandler_renderexception, ZEND_ACC_ABSTRACT|ZEND_ACC_PROTECTED)
	PHP_ME(yii_base_ErrorHandler, logException, arginfo_yii_base_errorhandler_logexception, ZEND_ACC_PROTECTED)
	PHP_ME(yii_base_ErrorHandler, clearOutput, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_ErrorHandler, convertExceptionToError, arginfo_yii_base_errorhandler_convertexceptiontoerror, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_base_ErrorHandler, convertExceptionToString, arginfo_yii_base_errorhandler_convertexceptiontostring, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
  PHP_FE_END
};
