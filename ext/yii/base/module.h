
extern zend_class_entry *yii_base_module_ce;

ZEPHIR_INIT_CLASS(yii_base_Module);

PHP_METHOD(yii_base_Module, __construct);
PHP_METHOD(yii_base_Module, init);
PHP_METHOD(yii_base_Module, getUniqueId);
PHP_METHOD(yii_base_Module, getBasePath);
PHP_METHOD(yii_base_Module, setBasePath);
PHP_METHOD(yii_base_Module, getControllerPath);
PHP_METHOD(yii_base_Module, getViewPath);
PHP_METHOD(yii_base_Module, setViewPath);
PHP_METHOD(yii_base_Module, getLayoutPath);
PHP_METHOD(yii_base_Module, setLayoutPath);
PHP_METHOD(yii_base_Module, setAliases);
PHP_METHOD(yii_base_Module, hasModule);
PHP_METHOD(yii_base_Module, getModule);
PHP_METHOD(yii_base_Module, setModule);
PHP_METHOD(yii_base_Module, getModules);
PHP_METHOD(yii_base_Module, setModules);
PHP_METHOD(yii_base_Module, runAction);
PHP_METHOD(yii_base_Module, createController);
PHP_METHOD(yii_base_Module, createControllerByID);
PHP_METHOD(yii_base_Module, beforeAction);
PHP_METHOD(yii_base_Module, afterAction);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module___construct, 0, 0, 1)
	ZEND_ARG_INFO(0, id)
	ZEND_ARG_INFO(0, parent)
	ZEND_ARG_INFO(0, config)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_setbasepath, 0, 0, 1)
	ZEND_ARG_INFO(0, path)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_setviewpath, 0, 0, 1)
	ZEND_ARG_INFO(0, path)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_setlayoutpath, 0, 0, 1)
	ZEND_ARG_INFO(0, path)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_setaliases, 0, 0, 1)
	ZEND_ARG_INFO(0, aliases)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_hasmodule, 0, 0, 1)
	ZEND_ARG_INFO(0, id)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_getmodule, 0, 0, 1)
	ZEND_ARG_INFO(0, id)
	ZEND_ARG_INFO(0, load)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_setmodule, 0, 0, 2)
	ZEND_ARG_INFO(0, id)
	ZEND_ARG_INFO(0, module)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_getmodules, 0, 0, 0)
	ZEND_ARG_INFO(0, loadedOnly)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_setmodules, 0, 0, 1)
	ZEND_ARG_INFO(0, modules)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_runaction, 0, 0, 1)
	ZEND_ARG_INFO(0, route)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_createcontroller, 0, 0, 1)
	ZEND_ARG_INFO(0, route)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_createcontrollerbyid, 0, 0, 1)
	ZEND_ARG_INFO(0, id)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_beforeaction, 0, 0, 1)
	ZEND_ARG_INFO(0, action)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_module_afteraction, 0, 0, 2)
	ZEND_ARG_INFO(0, action)
	ZEND_ARG_INFO(0, result)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_module_method_entry) {
	PHP_ME(yii_base_Module, __construct, arginfo_yii_base_module___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(yii_base_Module, init, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, getUniqueId, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, getBasePath, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, setBasePath, arginfo_yii_base_module_setbasepath, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, getControllerPath, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, getViewPath, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, setViewPath, arginfo_yii_base_module_setviewpath, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, getLayoutPath, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, setLayoutPath, arginfo_yii_base_module_setlayoutpath, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, setAliases, arginfo_yii_base_module_setaliases, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, hasModule, arginfo_yii_base_module_hasmodule, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, getModule, arginfo_yii_base_module_getmodule, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, setModule, arginfo_yii_base_module_setmodule, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, getModules, arginfo_yii_base_module_getmodules, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, setModules, arginfo_yii_base_module_setmodules, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, runAction, arginfo_yii_base_module_runaction, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, createController, arginfo_yii_base_module_createcontroller, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, createControllerByID, arginfo_yii_base_module_createcontrollerbyid, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, beforeAction, arginfo_yii_base_module_beforeaction, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Module, afterAction, arginfo_yii_base_module_afteraction, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
