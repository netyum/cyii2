
extern zend_class_entry *yii_base_controller_ce;

ZEPHIR_INIT_CLASS(yii_base_Controller);

PHP_METHOD(yii_base_Controller, __construct);
PHP_METHOD(yii_base_Controller, actions);
PHP_METHOD(yii_base_Controller, runAction);
PHP_METHOD(yii_base_Controller, run);
PHP_METHOD(yii_base_Controller, bindActionParams);
PHP_METHOD(yii_base_Controller, createAction);
PHP_METHOD(yii_base_Controller, beforeAction);
PHP_METHOD(yii_base_Controller, afterAction);
PHP_METHOD(yii_base_Controller, getModules);
PHP_METHOD(yii_base_Controller, getUniqueId);
PHP_METHOD(yii_base_Controller, getRoute);
PHP_METHOD(yii_base_Controller, render);
PHP_METHOD(yii_base_Controller, renderPartial);
PHP_METHOD(yii_base_Controller, renderFile);
PHP_METHOD(yii_base_Controller, getView);
PHP_METHOD(yii_base_Controller, setView);
PHP_METHOD(yii_base_Controller, getViewPath);
PHP_METHOD(yii_base_Controller, findLayoutFile);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller___construct, 0, 0, 2)
	ZEND_ARG_INFO(0, id)
	ZEND_ARG_INFO(0, module)
	ZEND_ARG_INFO(0, config)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller_runaction, 0, 0, 1)
	ZEND_ARG_INFO(0, id)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller_run, 0, 0, 1)
	ZEND_ARG_INFO(0, route)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller_bindactionparams, 0, 0, 2)
	ZEND_ARG_INFO(0, action)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller_createaction, 0, 0, 1)
	ZEND_ARG_INFO(0, id)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller_beforeaction, 0, 0, 1)
	ZEND_ARG_INFO(0, action)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller_afteraction, 0, 0, 2)
	ZEND_ARG_INFO(0, action)
	ZEND_ARG_INFO(0, result)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller_render, 0, 0, 1)
	ZEND_ARG_INFO(0, view)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller_renderpartial, 0, 0, 1)
	ZEND_ARG_INFO(0, view)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller_renderfile, 0, 0, 1)
	ZEND_ARG_INFO(0, file)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller_setview, 0, 0, 1)
	ZEND_ARG_INFO(0, view)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_controller_findlayoutfile, 0, 0, 1)
	ZEND_ARG_INFO(0, view)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_controller_method_entry) {
	PHP_ME(yii_base_Controller, __construct, arginfo_yii_base_controller___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(yii_base_Controller, actions, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, runAction, arginfo_yii_base_controller_runaction, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, run, arginfo_yii_base_controller_run, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, bindActionParams, arginfo_yii_base_controller_bindactionparams, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, createAction, arginfo_yii_base_controller_createaction, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, beforeAction, arginfo_yii_base_controller_beforeaction, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, afterAction, arginfo_yii_base_controller_afteraction, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, getModules, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, getUniqueId, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, getRoute, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, render, arginfo_yii_base_controller_render, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, renderPartial, arginfo_yii_base_controller_renderpartial, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, renderFile, arginfo_yii_base_controller_renderfile, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, getView, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, setView, arginfo_yii_base_controller_setview, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, getViewPath, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Controller, findLayoutFile, arginfo_yii_base_controller_findlayoutfile, ZEND_ACC_PROTECTED)
  PHP_FE_END
};
