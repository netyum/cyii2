
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/array.h"
#include "kernel/fcall.h"
#include "kernel/hash.h"
#include "kernel/object.h"
#include "kernel/operators.h"
#include "kernel/exception.h"
#include "kernel/concat.h"
#include "ext/spl/spl_array.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Model is the base class for data models.
 *
 * Model implements the following commonly used features:
 *
 * - attribute declaration: by default, every public class member is considered as
 *   a model attribute
 * - attribute labels: each attribute may be associated with a label for display purpose
 * - massive attribute assignment
 * - scenario-based validation
 *
 * Model also raises the following events when performing data validation:
 *
 * - [[EVENT_BEFORE_VALIDATE]]: an event raised at the beginning of [[validate()]]
 * - [[EVENT_AFTER_VALIDATE]]: an event raised at the end of [[validate()]]
 *
 * You may directly use Model to store model data, or extend it with customization.
 *
 * @property \yii\validators\Validator[] $activeValidators The validators applicable to the current
 * [[scenario]]. This property is read-only.
 * @property array $attributes Attribute values (name => value).
 * @property array $errors An array of errors for all attributes. Empty array is returned if no error. The
 * result is a two-dimensional array. See [[getErrors()]] for detailed description. This property is read-only.
 * @property array $firstErrors The first errors. The array keys are the attribute names, and the array values
 * are the corresponding error messages. An empty array will be returned if there is no error. This property is
 * read-only.
 * @property ArrayIterator $iterator An iterator for traversing the items in the list. This property is
 * read-only.
 * @property string $scenario The scenario that this model is in. Defaults to [[SCENARIO_DEFAULT]].
 * @property ArrayObject|\yii\validators\Validator[] $validators All the validators declared in the model.
 * This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_Model) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, Model, yii, base_model, yii_base_component_ce, yii_base_model_method_entry, 0);

	/**
	 * @var array validation errors (attribute name => array of errors)
	 */
	zend_declare_property_null(yii_base_model_ce, SL("_errors"), ZEND_ACC_PROTECTED TSRMLS_CC);

	/**
	 * @var ArrayObject list of validators
	 */
	zend_declare_property_null(yii_base_model_ce, SL("_validators"), ZEND_ACC_PROTECTED TSRMLS_CC);

	/**
	 * @var string current scenario
	 */
	zend_declare_property_string(yii_base_model_ce, SL("_scenario"), "default", ZEND_ACC_PROTECTED TSRMLS_CC);

	/**
	 * The name of the default scenario.
	 */
	zend_declare_class_constant_string(yii_base_model_ce, SL("SCENARIO_DEFAULT"), "default" TSRMLS_CC);

	/**
	 * @event ModelEvent an event raised at the beginning of [[validate()]]. You may set
	 * [[ModelEvent::isValid]] to be false to stop the validation.
	 */
	zend_declare_class_constant_string(yii_base_model_ce, SL("EVENT_BEFORE_VALIDATE"), "beforeValidate" TSRMLS_CC);

	/**
	 * @event Event an event raised at the end of [[validate()]]
	 */
	zend_declare_class_constant_string(yii_base_model_ce, SL("EVENT_AFTER_VALIDATE"), "afterValidate" TSRMLS_CC);

	zend_class_implements(yii_base_model_ce TSRMLS_CC, 3, zend_ce_aggregate, zend_ce_arrayaccess, yii_base_arrayable_ce);
	zend_trait_use(yii_base_model_ce TSRMLS_CC, 1, yii_base_arrayabletrait_ce);
	return SUCCESS;

}

/**
 * Returns the validation rules for attributes.
 *
 * Validation rules are used by [[validate()]] to check if attribute values are valid.
 * Child classes may override this method to declare different validation rules.
 *
 * Each rule is an array with the following structure:
 *
 * ~~~
 * [
 *     ['attribute1', 'attribute2'],
 *     'validator type',
 *     'on' => ['scenario1', 'scenario2'],
 *     ...other parameters...
 * ]
 * ~~~
 *
 * where
 *
 *  - attribute list: required, specifies the attributes array to be validated, for single attribute you can pass string;
 *  - validator type: required, specifies the validator to be used. It can be a built-in validator name,
 *    a method name of the model class, an anonymous function, or a validator class name.
 *  - on: optional, specifies the [[scenario|scenarios]] array when the validation
 *    rule can be applied. If this option is not set, the rule will apply to all scenarios.
 *  - additional name-value pairs can be specified to initialize the corresponding validator properties.
 *    Please refer to individual validator class API for possible properties.
 *
 * A validator can be either an object of a class extending [[Validator]], or a model class method
 * (called *inline validator*) that has the following signature:
 *
 * ~~~
 * // $params refers to validation parameters given in the rule
 * function validatorName($attribute, $params)
 * ~~~
 *
 * In the above `$attribute` refers to currently validated attribute name while `$params` contains an array of
 * validator configuration options such as `max` in case of `string` validator. Currently validate attribute value
 * can be accessed as `$this->[$attribute]`.
 *
 * Yii also provides a set of [[Validator::builtInValidators|built-in validators]].
 * They each has an alias name which can be used when specifying a validation rule.
 *
 * Below are some examples:
 *
 * ~~~
 * [
 *     // built-in "required" validator
 *     [['username', 'password'], 'required'],
 *     // built-in "string" validator customized with "min" and "max" properties
 *     ['username', 'string', 'min' => 3, 'max' => 12],
 *     // built-in "compare" validator that is used in "register" scenario only
 *     ['password', 'compare', 'compareAttribute' => 'password2', 'on' => 'register'],
 *     // an inline validator defined via the "authenticate()" method in the model class
 *     ['password', 'authenticate', 'on' => 'login'],
 *     // a validator of class "DateRangeValidator"
 *     ['dateRange', 'DateRangeValidator'],
 * ];
 * ~~~
 *
 * Note, in order to inherit rules defined in the parent class, a child class needs to
 * merge the parent rules with child rules using functions such as `array_merge()`.
 *
 * @return array validation rules
 * @see scenarios()
 */
PHP_METHOD(yii_base_Model, rules) {


	array_init(return_value);
	return;

}

/**
 * Returns a list of scenarios and the corresponding active attributes.
 * An active attribute is one that is subject to validation in the current scenario.
 * The returned array should be in the following format:
 *
 * ~~~
 * [
 *     'scenario1' => ['attribute11', 'attribute12', ...],
 *     'scenario2' => ['attribute21', 'attribute22', ...],
 *     ...
 * ]
 * ~~~
 *
 * By default, an active attribute is considered safe and can be massively assigned.
 * If an attribute should NOT be massively assigned (thus considered unsafe),
 * please prefix the attribute with an exclamation character (e.g. '!rank').
 *
 * The default implementation of this method will return all scenarios found in the [[rules()]]
 * declaration. A special scenario named [[SCENARIO_DEFAULT]] will contain all attributes
 * found in the [[rules()]]. Each scenario will be associated with the attributes that
 * are being validated by the validation rules that apply to the scenario.
 *
 * @return array a list of scenarios and the corresponding active attributes.
 */
PHP_METHOD(yii_base_Model, scenarios) {

	zephir_nts_static zephir_fcall_cache_entry *_33 = NULL;
	HashTable *_6, *_11, *_23, *_27, *_30, *_35, *_38, *_41, *_44;
	HashPosition _5, _10, _22, _26, _29, _34, _37, _40, _43;
	zend_bool _1, _13, on_empty = 0, except_empty = 0, _14, _15, _17, _19, _21;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *scenarios, *validators = NULL, *validator = NULL, *scenario = NULL, *on = NULL, *except = NULL, *_0, *iterator = NULL, *_2 = NULL, *_3 = NULL, *_4 = NULL, **_7, *_8 = NULL, *_9 = NULL, **_12, *names, *name = NULL, *attributes = NULL, *attribute = NULL, *scenarios_name = NULL, *_16 = NULL, *_18 = NULL, *_20 = NULL, **_24, *_25 = NULL, **_28, **_31, *_32 = NULL, **_36, **_39, **_42, *temp_scenarios = NULL, **_45;

	ZEPHIR_MM_GROW();
	ZEPHIR_INIT_VAR(scenarios);
	array_init(scenarios);

	ZEPHIR_INIT_VAR(_0);
	array_init(_0);
	zephir_array_update_string(&scenarios, SL("default"), &_0, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&validators, this_ptr, "getvalidators",  NULL);
	zephir_check_call_status();
	_1 = (Z_TYPE_P(validators) == IS_OBJECT);
	if (_1) {
		_1 = (zephir_instance_of_ev(validators, spl_ce_ArrayObject TSRMLS_CC));
	}
	if (_1) {
		ZEPHIR_CALL_METHOD(&iterator, validators, "getiterator",  NULL);
		zephir_check_call_status();
		while (1) {
			ZEPHIR_CALL_METHOD(&_2, iterator, "valid",  NULL);
			zephir_check_call_status();
			if (!(zephir_is_true(_2))) {
				break;
			}
			ZEPHIR_CALL_METHOD(&validator, iterator, "current",  NULL);
			zephir_check_call_status();
			ZEPHIR_OBS_NVAR(_3);
			zephir_read_property(&_3, validator, SL("on"), PH_NOISY_CC);
			if (Z_TYPE_P(_3) == IS_ARRAY) {
				ZEPHIR_OBS_NVAR(_4);
				zephir_read_property(&_4, validator, SL("on"), PH_NOISY_CC);
				ZEPHIR_CPY_WRT(on, _4);
				zephir_is_iterable(on, &_6, &_5, 0, 0);
				for (
				  ; zephir_hash_get_current_data_ex(_6, (void**) &_7, &_5) == SUCCESS
				  ; zephir_hash_move_forward_ex(_6, &_5)
				) {
					ZEPHIR_GET_HVALUE(scenario, _7);
					ZEPHIR_INIT_NVAR(_8);
					array_init(_8);
					zephir_array_update_zval(&scenarios, scenario, &_8, PH_COPY | PH_SEPARATE);
				}
			}
			ZEPHIR_OBS_NVAR(_4);
			zephir_read_property(&_4, validator, SL("except"), PH_NOISY_CC);
			if (Z_TYPE_P(_4) == IS_ARRAY) {
				ZEPHIR_OBS_NVAR(_9);
				zephir_read_property(&_9, validator, SL("except"), PH_NOISY_CC);
				ZEPHIR_CPY_WRT(except, _9);
				zephir_is_iterable(except, &_11, &_10, 0, 0);
				for (
				  ; zephir_hash_get_current_data_ex(_11, (void**) &_12, &_10) == SUCCESS
				  ; zephir_hash_move_forward_ex(_11, &_10)
				) {
					ZEPHIR_GET_HVALUE(scenario, _12);
					ZEPHIR_INIT_NVAR(_8);
					array_init(_8);
					zephir_array_update_zval(&scenarios, scenario, &_8, PH_COPY | PH_SEPARATE);
				}
			}
			ZEPHIR_CALL_METHOD(NULL, iterator, "next", NULL);
			zephir_check_call_status();
		}
	}
	ZEPHIR_INIT_VAR(names);
	zephir_array_keys(names, scenarios TSRMLS_CC);
	_13 = (Z_TYPE_P(validators) == IS_OBJECT);
	if (_13) {
		_13 = (zephir_instance_of_ev(validators, spl_ce_ArrayObject TSRMLS_CC));
	}
	if (_13) {
		ZEPHIR_CALL_METHOD(&iterator, validators, "getiterator",  NULL);
		zephir_check_call_status();
		while (1) {
			ZEPHIR_CALL_METHOD(&_2, iterator, "valid",  NULL);
			zephir_check_call_status();
			if (!(zephir_is_true(_2))) {
				break;
			}
			ZEPHIR_CALL_METHOD(&validator, iterator, "current",  NULL);
			zephir_check_call_status();
			ZEPHIR_OBS_NVAR(_3);
			zephir_read_property(&_3, validator, SL("on"), PH_NOISY_CC);
			_14 = Z_TYPE_P(_3) == IS_NULL;
			if (!(_14)) {
				ZEPHIR_OBS_NVAR(_4);
				zephir_read_property(&_4, validator, SL("on"), PH_NOISY_CC);
				_15 = Z_TYPE_P(_4) == IS_ARRAY;
				if (_15) {
					ZEPHIR_OBS_NVAR(_9);
					zephir_read_property(&_9, validator, SL("on"), PH_NOISY_CC);
					_15 = zephir_fast_count_int(_9 TSRMLS_CC) == 0;
				}
				_14 = _15;
			}
			on_empty = _14;
			ZEPHIR_OBS_NVAR(_16);
			zephir_read_property(&_16, validator, SL("except"), PH_NOISY_CC);
			_17 = Z_TYPE_P(_16) == IS_NULL;
			if (!(_17)) {
				ZEPHIR_OBS_NVAR(_18);
				zephir_read_property(&_18, validator, SL("except"), PH_NOISY_CC);
				_19 = Z_TYPE_P(_18) == IS_ARRAY;
				if (_19) {
					ZEPHIR_OBS_NVAR(_20);
					zephir_read_property(&_20, validator, SL("except"), PH_NOISY_CC);
					_19 = zephir_fast_count_int(_20 TSRMLS_CC) == 0;
				}
				_17 = _19;
			}
			except_empty = _17;
			_21 = on_empty;
			if (_21) {
				_21 = except_empty;
			}
			if (_21) {
				zephir_is_iterable(names, &_23, &_22, 0, 0);
				for (
				  ; zephir_hash_get_current_data_ex(_23, (void**) &_24, &_22) == SUCCESS
				  ; zephir_hash_move_forward_ex(_23, &_22)
				) {
					ZEPHIR_GET_HVALUE(name, _24);
					if (zephir_array_isset(scenarios, name)) {
						ZEPHIR_OBS_NVAR(scenarios_name);
						zephir_array_fetch(&scenarios_name, scenarios, name, PH_NOISY TSRMLS_CC);
					} else {
						ZEPHIR_INIT_NVAR(scenarios_name);
						array_init(scenarios_name);
					}
					ZEPHIR_OBS_NVAR(_25);
					zephir_read_property(&_25, validator, SL("attributes"), PH_NOISY_CC);
					ZEPHIR_CPY_WRT(attributes, _25);
					zephir_is_iterable(attributes, &_27, &_26, 0, 0);
					for (
					  ; zephir_hash_get_current_data_ex(_27, (void**) &_28, &_26) == SUCCESS
					  ; zephir_hash_move_forward_ex(_27, &_26)
					) {
						ZEPHIR_GET_HVALUE(attribute, _28);
						zephir_array_update_zval(&scenarios_name, attribute, &ZEPHIR_GLOBAL(global_true), PH_COPY | PH_SEPARATE);
					}
					zephir_array_update_zval(&scenarios, name, &scenarios_name, PH_COPY | PH_SEPARATE);
				}
			} else {
				if (on_empty) {
					zephir_is_iterable(names, &_30, &_29, 0, 0);
					for (
					  ; zephir_hash_get_current_data_ex(_30, (void**) &_31, &_29) == SUCCESS
					  ; zephir_hash_move_forward_ex(_30, &_29)
					) {
						ZEPHIR_GET_HVALUE(name, _31);
						ZEPHIR_OBS_NVAR(_4);
						zephir_read_property(&_4, validator, SL("except"), PH_NOISY_CC);
						ZEPHIR_CPY_WRT(except, _4);
						if (Z_TYPE_P(except) == IS_NULL) {
							ZEPHIR_INIT_NVAR(except);
							array_init(except);
						}
						ZEPHIR_CALL_FUNCTION(&_32, "in_array", &_33, name, except, ZEPHIR_GLOBAL(global_true));
						zephir_check_call_status();
						if (!(zephir_is_true(_32))) {
							if (zephir_array_isset(scenarios, name)) {
								ZEPHIR_OBS_NVAR(scenarios_name);
								zephir_array_fetch(&scenarios_name, scenarios, name, PH_NOISY TSRMLS_CC);
							} else {
								ZEPHIR_INIT_NVAR(scenarios_name);
								array_init(scenarios_name);
							}
							ZEPHIR_OBS_NVAR(_4);
							zephir_read_property(&_4, validator, SL("attributes"), PH_NOISY_CC);
							ZEPHIR_CPY_WRT(attributes, _4);
							zephir_is_iterable(attributes, &_35, &_34, 0, 0);
							for (
							  ; zephir_hash_get_current_data_ex(_35, (void**) &_36, &_34) == SUCCESS
							  ; zephir_hash_move_forward_ex(_35, &_34)
							) {
								ZEPHIR_GET_HVALUE(attribute, _36);
								zephir_array_update_zval(&scenarios_name, attribute, &ZEPHIR_GLOBAL(global_true), PH_COPY | PH_SEPARATE);
							}
							zephir_array_update_zval(&scenarios, name, &scenarios_name, PH_COPY | PH_SEPARATE);
						}
					}
				} else {
					ZEPHIR_OBS_NVAR(_4);
					zephir_read_property(&_4, validator, SL("on"), PH_NOISY_CC);
					ZEPHIR_CPY_WRT(on, _4);
					ZEPHIR_OBS_NVAR(_4);
					zephir_read_property(&_4, validator, SL("attributes"), PH_NOISY_CC);
					ZEPHIR_CPY_WRT(attributes, _4);
					zephir_is_iterable(on, &_38, &_37, 0, 0);
					for (
					  ; zephir_hash_get_current_data_ex(_38, (void**) &_39, &_37) == SUCCESS
					  ; zephir_hash_move_forward_ex(_38, &_37)
					) {
						ZEPHIR_GET_HVALUE(name, _39);
						if (zephir_array_isset(scenarios, name)) {
							ZEPHIR_OBS_NVAR(scenarios_name);
							zephir_array_fetch(&scenarios_name, scenarios, name, PH_NOISY TSRMLS_CC);
						} else {
							ZEPHIR_INIT_NVAR(scenarios_name);
							array_init(scenarios_name);
						}
						zephir_is_iterable(attributes, &_41, &_40, 0, 0);
						for (
						  ; zephir_hash_get_current_data_ex(_41, (void**) &_42, &_40) == SUCCESS
						  ; zephir_hash_move_forward_ex(_41, &_40)
						) {
							ZEPHIR_GET_HVALUE(attribute, _42);
							zephir_array_update_zval(&scenarios_name, attribute, &ZEPHIR_GLOBAL(global_true), PH_COPY | PH_SEPARATE);
						}
						zephir_array_update_zval(&scenarios, name, &scenarios_name, PH_COPY | PH_SEPARATE);
					}
				}
			}
			ZEPHIR_CALL_METHOD(NULL, iterator, "next", NULL);
			zephir_check_call_status();
		}
	}
	ZEPHIR_CPY_WRT(temp_scenarios, scenarios);
	zephir_is_iterable(temp_scenarios, &_44, &_43, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_44, (void**) &_45, &_43) == SUCCESS
	  ; zephir_hash_move_forward_ex(_44, &_43)
	) {
		ZEPHIR_GET_HMKEY(scenario, _44, _43);
		ZEPHIR_GET_HVALUE(attributes, _45);
		_14 = zephir_fast_count_int(attributes TSRMLS_CC) == 0;
		if (_14) {
			_14 = !ZEPHIR_IS_STRING(scenario, "default");
		}
		if (_14) {
			zephir_array_unset(&scenarios, scenario, PH_SEPARATE);
		} else {
			ZEPHIR_INIT_NVAR(_8);
			zephir_array_keys(_8, attributes TSRMLS_CC);
			zephir_array_update_zval(&scenarios, scenario, &_8, PH_COPY | PH_SEPARATE);
		}
	}
	RETURN_CCTOR(scenarios);

}

/**
 * Returns the form name that this model class should use.
 *
 * The form name is mainly used by [[\yii\widgets\ActiveForm]] to determine how to name
 * the input fields for the attributes in a model. If the form name is "A" and an attribute
 * name is "b", then the corresponding input name would be "A[b]". If the form name is
 * an empty string, then the input name would be "b".
 *
 * By default, this method returns the model class name (without the namespace part)
 * as the form name. You may override it when the model is used in different forms.
 *
 * @return string the form name of this model class.
 */
PHP_METHOD(yii_base_Model, formName) {

	int ZEPHIR_LAST_CALL_STATUS;
	zend_class_entry *_0;
	zval *reflector;

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(reflector);
	_0 = zend_fetch_class(SL("ReflectionClass"), ZEND_FETCH_CLASS_AUTO TSRMLS_CC);
	object_init_ex(reflector, _0);
	ZEPHIR_CALL_METHOD(NULL, reflector, "__construct", NULL, this_ptr);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(reflector, "getshortname", NULL);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns the list of attribute names.
 * By default, this method returns all public non-static properties of the class.
 * You may override this method to change the default behavior.
 * @return array list of attribute names.
 */
PHP_METHOD(yii_base_Model, attributes) {

	HashTable *_3;
	HashPosition _2;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_class_entry *_0;
	zval *class, *names, *property = NULL, *properties = NULL, *_1, **_4, *_5 = NULL;

	ZEPHIR_MM_GROW();
	ZEPHIR_INIT_VAR(names);
	array_init(names);

	ZEPHIR_INIT_VAR(class);
	_0 = zend_fetch_class(SL("ReflectionClass"), ZEND_FETCH_CLASS_AUTO TSRMLS_CC);
	object_init_ex(class, _0);
	ZEPHIR_CALL_METHOD(NULL, class, "__construct", NULL, this_ptr);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_1);
	ZVAL_LONG(_1, 256);
	ZEPHIR_CALL_METHOD(&properties, class, "getproperties", NULL, _1);
	zephir_check_call_status();
	zephir_is_iterable(properties, &_3, &_2, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_3, (void**) &_4, &_2) == SUCCESS
	  ; zephir_hash_move_forward_ex(_3, &_2)
	) {
		ZEPHIR_GET_HVALUE(property, _4);
		ZEPHIR_CALL_METHOD(&_5, property, "isstatic",  NULL);
		zephir_check_call_status();
		if (!(zephir_is_true(_5))) {
			ZEPHIR_CALL_METHOD(&_5, property, "getname",  NULL);
			zephir_check_call_status();
			zephir_array_append(&names, _5, PH_SEPARATE);
		}
	}
	RETURN_CCTOR(names);

}

/**
 * Returns the attribute labels.
 *
 * Attribute labels are mainly used for display purpose. For example, given an attribute
 * `firstName`, we can declare a label `First Name` which is more user-friendly and can
 * be displayed to end users.
 *
 * By default an attribute label is generated using [[generateAttributeLabel()]].
 * This method allows you to explicitly specify attribute labels.
 *
 * Note, in order to inherit labels defined in the parent class, a child class needs to
 * merge the parent labels with child labels using functions such as `array_merge()`.
 *
 * @return array attribute labels (name => label)
 * @see generateAttributeLabel()
 */
PHP_METHOD(yii_base_Model, attributeLabels) {


	array_init(return_value);
	return;

}

/**
 * Performs the data validation.
 *
 * This method executes the validation rules applicable to the current [[scenario]].
 * The following criteria are used to determine whether a rule is currently applicable:
 *
 * - the rule must be associated with the attributes relevant to the current scenario;
 * - the rules must be effective for the current scenario.
 *
 * This method will call [[beforeValidate()]] and [[afterValidate()]] before and
 * after the actual validation, respectively. If [[beforeValidate()]] returns false,
 * the validation will be cancelled and [[afterValidate()]] will not be called.
 *
 * Errors found during the validation can be retrieved via [[getErrors()]],
 * [[getFirstErrors()]] and [[getFirstError()]].
 *
 * @param array $attributeNames list of attribute names that should be validated.
 * If this parameter is empty, it means any attribute listed in the applicable
 * validation rules should be validated.
 * @param boolean $clearErrors whether to call [[clearErrors()]] before performing validation
 * @return boolean whether the validation is successful without any error.
 * @throws InvalidParamException if the current scenario is unknown.
 */
PHP_METHOD(yii_base_Model, validate) {

	HashTable *_4;
	HashPosition _3;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *attributeNames = NULL, *clearErrors = NULL, *scenarios = NULL, *scenario = NULL, *validator = NULL, *active_validators = NULL, *_0, *_1, *_2 = NULL, **_5;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 2, &attributeNames, &clearErrors);

	if (!attributeNames) {
		ZEPHIR_CPY_WRT(attributeNames, ZEPHIR_GLOBAL(global_null));
	} else {
		ZEPHIR_SEPARATE_PARAM(attributeNames);
	}
	if (!clearErrors) {
		clearErrors = ZEPHIR_GLOBAL(global_true);
	}


	ZEPHIR_CALL_METHOD(&scenarios, this_ptr, "scenarios",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&scenario, this_ptr, "getscenario",  NULL);
	zephir_check_call_status();
	if (!(zephir_array_isset(scenarios, scenario))) {
		ZEPHIR_INIT_VAR(_0);
		object_init_ex(_0, yii_base_invalidparamexception_ce);
		ZEPHIR_INIT_VAR(_1);
		ZEPHIR_CONCAT_SV(_1, "Unknown scenario: ", scenario);
		ZEPHIR_CALL_METHOD(NULL, _0, "__construct", NULL, _1);
		zephir_check_call_status();
		zephir_throw_exception_debug(_0, "yii/base/Model.zep", 372 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	if (zephir_is_true(clearErrors)) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "clearerrors", NULL);
		zephir_check_call_status();
	}
	if (Z_TYPE_P(attributeNames) == IS_NULL) {
		ZEPHIR_CALL_METHOD(&attributeNames, this_ptr, "activeattributes",  NULL);
		zephir_check_call_status();
	}
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "beforevalidate",  NULL);
	zephir_check_call_status();
	if (zephir_is_true(_2)) {
		ZEPHIR_CALL_METHOD(&active_validators, this_ptr, "getactivevalidators",  NULL);
		zephir_check_call_status();
		zephir_is_iterable(active_validators, &_4, &_3, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_4, (void**) &_5, &_3) == SUCCESS
		  ; zephir_hash_move_forward_ex(_4, &_3)
		) {
			ZEPHIR_GET_HVALUE(validator, _5);
			ZEPHIR_CALL_METHOD(NULL, validator, "validateattributes", NULL, this_ptr, attributeNames);
			zephir_check_call_status();
		}
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "aftervalidate", NULL);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&_2, this_ptr, "haserrors",  NULL);
		zephir_check_call_status();
		RETURN_MM_BOOL(!zephir_is_true(_2));
	}
	RETURN_MM_BOOL(0);

}

/**
 * This method is invoked before validation starts.
 * The default implementation raises a `beforeValidate` event.
 * You may override this method to do preliminary checks before validation.
 * Make sure the parent implementation is invoked so that the event can be raised.
 * @return boolean whether the validation should be executed. Defaults to true.
 * If false is returned, the validation will stop and the model is considered invalid.
 */
PHP_METHOD(yii_base_Model, beforeValidate) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *event, *_0, *_1;

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(event);
	object_init_ex(event, yii_base_modelevent_ce);
	ZEPHIR_CALL_METHOD(NULL, event, "__construct", NULL);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_0);
	ZVAL_STRING(_0, "beforeValidate", 0);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "trigger", NULL, _0, event);
	zephir_check_temp_parameter(_0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(_1);
	zephir_read_property(&_1, event, SL("isValid"), PH_NOISY_CC);
	RETURN_CCTOR(_1);

}

/**
 * This method is invoked after validation ends.
 * The default implementation raises an `afterValidate` event.
 * You may override this method to do postprocessing after validation.
 * Make sure the parent implementation is invoked so that the event can be raised.
 */
PHP_METHOD(yii_base_Model, afterValidate) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *_0;

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(_0);
	ZVAL_STRING(_0, "afterValidate", 0);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "trigger", NULL, _0);
	zephir_check_temp_parameter(_0);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns all the validators declared in [[rules()]].
 *
 * This method differs from [[getActiveValidators()]] in that the latter
 * only returns the validators applicable to the current [[scenario]].
 *
 * Because this method returns an ArrayObject object, you may
 * manipulate it by inserting or removing validators (useful in model behaviors).
 * For example,
 *
 * ~~~
 * $model->validators[] = $newValidator;
 * ~~~
 *
 * @return ArrayObject|\yii\validators\Validator[] all the validators declared in the model.
 */
PHP_METHOD(yii_base_Model, getValidators) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *_0, *_1 = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("_validators"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		ZEPHIR_CALL_METHOD(&_1, this_ptr, "createvalidators",  NULL);
		zephir_check_call_status();
		zephir_update_property_this(this_ptr, SL("_validators"), _1 TSRMLS_CC);
	}
	RETURN_MM_MEMBER(this_ptr, "_validators");

}

/**
 * Returns the validators applicable to the current [[scenario]].
 * @param string $attribute the name of the attribute whose applicable validators should be returned.
 * If this is null, the validators for ALL attributes in the model will be returned.
 * @return \yii\validators\Validator[] the validators applicable to the current [[scenario]].
 */
PHP_METHOD(yii_base_Model, getActiveValidators) {


	zephir_nts_static zephir_fcall_cache_entry *_7 = NULL;
	zend_bool _0, _3, _4;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *attribute = NULL, *validators, *scenario = NULL, *def_validators = NULL, *validator = NULL, *iterator = NULL, *_1 = NULL, *_2 = NULL, *_5 = NULL, *_6 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &attribute);

	if (!attribute) {
		attribute = ZEPHIR_GLOBAL(global_null);
	}
	ZEPHIR_INIT_VAR(validators);
	array_init(validators);


	ZEPHIR_CALL_METHOD(&scenario, this_ptr, "getscenario",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&def_validators, this_ptr, "getvalidators",  NULL);
	zephir_check_call_status();
	_0 = (Z_TYPE_P(def_validators) == IS_OBJECT);
	if (_0) {
		_0 = (zephir_instance_of_ev(def_validators, spl_ce_ArrayObject TSRMLS_CC));
	}
	if (_0) {
		ZEPHIR_CALL_METHOD(&iterator, def_validators, "getiterator",  NULL);
		zephir_check_call_status();
		while (1) {
			ZEPHIR_CALL_METHOD(&_1, iterator, "valid",  NULL);
			zephir_check_call_status();
			if (!(zephir_is_true(_1))) {
				break;
			}
			ZEPHIR_CALL_METHOD(&validator, iterator, "current",  NULL);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(&_2, validator, "isactive", NULL, scenario);
			zephir_check_call_status();
			_3 = zephir_is_true(_2);
			if (_3) {
				_3 = Z_TYPE_P(attribute) == IS_NULL;
			}
			_4 = _3;
			if (!(_4)) {
				ZEPHIR_OBS_NVAR(_5);
				zephir_read_property(&_5, validator, SL("attributes"), PH_NOISY_CC);
				ZEPHIR_CALL_FUNCTION(&_6, "in_array", &_7, attribute, _5, ZEPHIR_GLOBAL(global_true));
				zephir_check_call_status();
				_4 = zephir_is_true(_6);
			}
			if (_4) {
				zephir_array_append(&validators, validator, PH_SEPARATE);
			}
			ZEPHIR_CALL_METHOD(NULL, iterator, "next", NULL);
			zephir_check_call_status();
		}
	}
	RETURN_CCTOR(validators);
	
}

/**
 * Creates validator objects based on the validation rules specified in [[rules()]].
 * Unlike [[getValidators()]], each time this method is called, a new list of validators will be returned.
 * @return ArrayObject validators
 * @throws InvalidConfigException if any validation rule configuration is invalid
 */
PHP_METHOD(yii_base_Model, createValidators) {

	zephir_nts_static zephir_fcall_cache_entry *_11 = NULL;
	zend_bool _4, _5;
	zephir_fcall_cache_entry *_3 = NULL, *_6 = NULL, *_12 = NULL;
	HashTable *_1;
	HashPosition _0;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *validators, *rules = NULL, *rule = NULL, *validator = NULL, **_2, *_7, *_8, _9 = zval_used_for_init, *_10 = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(validators);
	object_init_ex(validators, spl_ce_ArrayObject);
	ZEPHIR_CALL_METHOD(NULL, validators, "__construct", NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&rules, this_ptr, "rules",  NULL);
	zephir_check_call_status();
	zephir_is_iterable(rules, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HVALUE(rule, _2);
		if (Z_TYPE_P(rule) == IS_OBJECT) {
			if (zephir_instance_of_ev(rule, yii_validators_validator_ce TSRMLS_CC)) {
				ZEPHIR_CALL_METHOD(NULL, validators, "append", &_3, rule);
				zephir_check_call_status();
			}
		} else {
			_4 = Z_TYPE_P(rule) == IS_ARRAY;
			if (_4) {
				_4 = zephir_array_isset_long(rule, 0);
			}
			_5 = _4;
			if (_5) {
				_5 = zephir_array_isset_long(rule, 1);
			}
			if (_5) {
				zephir_array_fetch_long(&_7, rule, 1, PH_NOISY | PH_READONLY TSRMLS_CC);
				zephir_array_fetch_long(&_8, rule, 0, PH_NOISY | PH_READONLY TSRMLS_CC);
				convert_to_array(_8);
				ZEPHIR_SINIT_NVAR(_9);
				ZVAL_LONG(&_9, 2);
				ZEPHIR_CALL_FUNCTION(&_10, "array_slice", &_11, rule, &_9);
				zephir_check_call_status();
				ZEPHIR_CALL_CE_STATIC(&validator, yii_validators_validator_ce, "createvalidator", &_6, _7, this_ptr, _8, _10);
				zephir_check_call_status();
				ZEPHIR_CALL_METHOD(NULL, validators, "append", &_12, validator);
				zephir_check_call_status();
			} else {
				ZEPHIR_THROW_EXCEPTION_DEBUG_STR(yii_base_invalidconfigexception_ce, "Invalid validation rule: a rule must specify both attribute names and validator type.", "yii/base/Model.zep", 492);
				return;
			}
		}
	}
	RETURN_CCTOR(validators);

}

/**
 * Returns a value indicating whether the attribute is required.
 * This is determined by checking if the attribute is associated with a
 * [[\yii\validators\RequiredValidator|required]] validation rule in the
 * current [[scenario]].
 * @param string $attribute attribute name
 * @return boolean whether the attribute is required
 */
PHP_METHOD(yii_base_Model, isAttributeRequired) {

	HashTable *_1;
	HashPosition _0;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *attribute, *validator = NULL, *active_validators = NULL, **_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &attribute);



	ZEPHIR_CALL_METHOD(&active_validators, this_ptr, "getactivevalidators", NULL, attribute);
	zephir_check_call_status();
	zephir_is_iterable(active_validators, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HVALUE(validator, _2);
		if (Z_TYPE_P(validator) == IS_OBJECT) {
			if (zephir_instance_of_ev(validator, yii_validators_requiredvalidator_ce TSRMLS_CC)) {
				RETURN_MM_BOOL(1);
			}
		}
	}
	RETURN_MM_BOOL(0);

}

/**
 * Returns a value indicating whether the attribute is safe for massive assignments.
 * @param string $attribute attribute name
 * @return boolean whether the attribute is safe for massive assignments
 * @see safeAttributes()
 */
PHP_METHOD(yii_base_Model, isAttributeSafe) {

	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *attribute, *safe_attributes = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &attribute);



	ZEPHIR_CALL_METHOD(&safe_attributes, this_ptr, "safeattributes",  NULL);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_FUNCTION("in_array", &_0, attribute, safe_attributes, ZEPHIR_GLOBAL(global_true));
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns a value indicating whether the attribute is active in the current scenario.
 * @param string $attribute attribute name
 * @return boolean whether the attribute is active in the current scenario
 * @see activeAttributes()
 */
PHP_METHOD(yii_base_Model, isAttributeActive) {

	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *attribute, *_0 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &attribute);



	ZEPHIR_CALL_METHOD(&_0, this_ptr, "activeattributes",  NULL);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_FUNCTION("in_array", &_1, attribute, _0, ZEPHIR_GLOBAL(global_true));
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns the text label for the specified attribute.
 * @param string $attribute the attribute name
 * @return string the attribute label
 * @see generateAttributeLabel()
 * @see attributeLabels()
 */
PHP_METHOD(yii_base_Model, getAttributeLabel) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *attribute, *labels = NULL, *_0;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &attribute);



	ZEPHIR_CALL_METHOD(&labels, this_ptr, "attributelabels",  NULL);
	zephir_check_call_status();
	if (zephir_array_isset(labels, attribute)) {
		zephir_array_fetch(&_0, labels, attribute, PH_NOISY | PH_READONLY TSRMLS_CC);
		RETURN_CTOR(_0);
	} else {
		ZEPHIR_RETURN_CALL_METHOD(this_ptr, "generateattributelabel", NULL, attribute);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns a value indicating whether there is any validation error.
 * @param string|null $attribute attribute name. Use null to check all attributes.
 * @return boolean whether there is any error.
 */
PHP_METHOD(yii_base_Model, hasErrors) {

	zval *attribute = NULL, *_0, *_1, *_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &attribute);

	if (!attribute) {
		attribute = ZEPHIR_GLOBAL(global_null);
	}


	ZEPHIR_INIT_VAR(_0);
	if (Z_TYPE_P(attribute) == IS_NULL) {
		ZEPHIR_OBS_VAR(_1);
		zephir_read_property_this(&_1, this_ptr, SL("_errors"), PH_NOISY_CC);
		ZVAL_BOOL(_0, !ZEPHIR_IS_EMPTY(_1));
	} else {
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("_errors"), PH_NOISY_CC);
		ZVAL_BOOL(_0, zephir_array_isset(_2, attribute));
	}
	RETURN_CCTOR(_0);

}

/**
 * Returns the errors for all attribute or a single attribute.
 * @param string $attribute attribute name. Use null to retrieve errors for all attributes.
 * @property array An array of errors for all attributes. Empty array is returned if no error.
 * The result is a two-dimensional array. See [[getErrors()]] for detailed description.
 * @return array errors for all attributes or the specified attribute. Empty array is returned if no error.
 * Note that when returning errors for all attributes, the result is a two-dimensional array, like the following:
 *
 * ~~~
 * [
 *     'username' => [
 *         'Username is required.',
 *         'Username must contain only word characters.',
 *     ],
 *     'email' => [
 *         'Email address is invalid.',
 *     ]
 * ]
 * ~~~
 *
 * @see getFirstErrors()
 * @see getFirstError()
 */
PHP_METHOD(yii_base_Model, getErrors) {

	zval *attribute = NULL, *_0 = NULL, *_1, *_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &attribute);

	if (!attribute) {
		attribute = ZEPHIR_GLOBAL(global_null);
	}


	if (Z_TYPE_P(attribute) == IS_NULL) {
		ZEPHIR_INIT_VAR(_0);
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("_errors"), PH_NOISY_CC);
		if (Z_TYPE_P(_1) == IS_NULL) {
			array_init(_0);
		} else {
			zephir_read_property_this(&_0, this_ptr, SL("_errors"), PH_NOISY_CC);
		}
		RETURN_CCTOR(_0);
	} else {
		ZEPHIR_INIT_NVAR(_0);
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("_errors"), PH_NOISY_CC);
		if (zephir_array_isset(_1, attribute)) {
			_2 = zephir_fetch_nproperty_this(this_ptr, SL("_errors"), PH_NOISY_CC);
			zephir_array_fetch(&_0, _2, attribute, PH_NOISY TSRMLS_CC);
		} else {
			array_init(_0);
		}
		RETURN_CCTOR(_0);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the first error of every attribute in the model.
 * @return array the first errors. The array keys are the attribute names, and the array
 * values are the corresponding error messages. An empty array will be returned if there is no error.
 * @see getErrors()
 * @see getFirstError()
 */
PHP_METHOD(yii_base_Model, getFirstErrors) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_8 = NULL;
	HashTable *_4;
	HashPosition _3;
	zend_bool _1, _6;
	zval *errors, *def_errors, *name = NULL, *es = NULL, *_0, *_2, **_5, *_7 = NULL;

	ZEPHIR_MM_GROW();
	ZEPHIR_INIT_VAR(errors);
	array_init(errors);

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("_errors"), PH_NOISY_CC);
	_1 = Z_TYPE_P(_0) == IS_NULL;
	if (!(_1)) {
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("_errors"), PH_NOISY_CC);
		_1 = zephir_fast_count_int(_2 TSRMLS_CC) == 0;
	}
	if (_1) {
		array_init(return_value);
		RETURN_MM();
	} else {
		ZEPHIR_OBS_VAR(def_errors);
		zephir_read_property_this(&def_errors, this_ptr, SL("_errors"), PH_NOISY_CC);
		zephir_is_iterable(def_errors, &_4, &_3, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_4, (void**) &_5, &_3) == SUCCESS
		  ; zephir_hash_move_forward_ex(_4, &_3)
		) {
			ZEPHIR_GET_HMKEY(name, _4, _3);
			ZEPHIR_GET_HVALUE(es, _5);
			_6 = Z_TYPE_P(es) == IS_ARRAY;
			if (_6) {
				_6 = zephir_fast_count_int(es TSRMLS_CC) > 0;
			}
			if (_6) {
				Z_SET_ISREF_P(es);
				ZEPHIR_CALL_FUNCTION(&_7, "reset", &_8, es);
				Z_UNSET_ISREF_P(es);
				zephir_check_call_status();
				zephir_array_update_zval(&errors, name, &_7, PH_COPY | PH_SEPARATE);
			}
		}
		RETURN_CCTOR(errors);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the first error of the specified attribute.
 * @param string $attribute attribute name.
 * @return string the error message. Null is returned if no error.
 * @see getErrors()
 * @see getFirstErrors()
 */
PHP_METHOD(yii_base_Model, getFirstError) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL;
	zend_bool _0;
	zval *attribute, *errors, *_1;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &attribute);



	ZEPHIR_OBS_VAR(errors);
	zephir_read_property_this(&errors, this_ptr, SL("_errors"), PH_NOISY_CC);
	_0 = Z_TYPE_P(errors) == IS_ARRAY;
	if (_0) {
		_0 = zephir_array_isset(errors, attribute);
	}
	if (_0) {
		zephir_array_fetch(&_1, errors, attribute, PH_NOISY | PH_READONLY TSRMLS_CC);
		Z_SET_ISREF_P(_1);
		ZEPHIR_RETURN_CALL_FUNCTION("reset", &_2, _1);
		Z_UNSET_ISREF_P(_1);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		RETURN_MM_NULL();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Adds a new error to the specified attribute.
 * @param string $attribute attribute name
 * @param string $error new error message
 */
PHP_METHOD(yii_base_Model, addError) {

	zval *attribute, *error = NULL, *errors, *attribute_errors = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &attribute, &error);

	if (!error) {
		ZEPHIR_INIT_VAR(error);
		ZVAL_STRING(error, "", 1);
	}


	ZEPHIR_OBS_VAR(errors);
	zephir_read_property_this(&errors, this_ptr, SL("_errors"), PH_NOISY_CC);
	if (Z_TYPE_P(errors) == IS_NULL) {
		ZEPHIR_INIT_BNVAR(errors);
		array_init(errors);
	}
	if (zephir_array_isset(errors, attribute)) {
		ZEPHIR_OBS_VAR(attribute_errors);
		zephir_array_fetch(&attribute_errors, errors, attribute, PH_NOISY TSRMLS_CC);
	} else {
		ZEPHIR_INIT_NVAR(attribute_errors);
		array_init(attribute_errors);
	}
	zephir_array_append(&attribute_errors, error, PH_SEPARATE);
	zephir_array_update_zval(&errors, attribute, &attribute_errors, PH_COPY | PH_SEPARATE);
	zephir_update_property_this(this_ptr, SL("_errors"), errors TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Removes errors for all attributes or a single attribute.
 * @param string $attribute attribute name. Use null to remove errors for all attribute.
 */
PHP_METHOD(yii_base_Model, clearErrors) {

        zval *attribute = NULL, *errors, *_0;

        ZEPHIR_MM_GROW();
        zephir_fetch_params(1, 0, 1, &attribute);

        if (!attribute) {
                attribute = ZEPHIR_GLOBAL(global_null);
        }


        if (Z_TYPE_P(attribute) == IS_NULL) {
                ZEPHIR_INIT_VAR(_0);
                array_init(_0);
                zephir_update_property_this(this_ptr, SL("_errors"), _0 TSRMLS_CC);
        } else {
                ZEPHIR_OBS_VAR(errors);
                zephir_read_property_this(&errors, this_ptr, SL("_errors"), PH_NOISY_CC);
                zephir_array_unset(&errors, attribute, PH_SEPARATE);
                zephir_update_property_this(this_ptr, SL("_errors"), errors TSRMLS_CC);
        }
        ZEPHIR_MM_RESTORE();

}

/**
 * Generates a user friendly attribute label based on the give attribute name.
 * This is done by replacing underscores, dashes and dots with blanks and
 * changing the first letter of each word to upper case.
 * For example, 'department_name' or 'DepartmentName' will generate 'Department Name'.
 * @param string $name the column name
 * @return string the attribute label
 */
PHP_METHOD(yii_base_Model, generateAttributeLabel) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;
	zval *name;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &name);



	ZEPHIR_RETURN_CALL_CE_STATIC(yii_helpers_inflector_ce, "camel2words", &_0, name, ZEPHIR_GLOBAL(global_true));
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns attribute values.
 * @param array $names list of attributes whose value needs to be returned.
 * Defaults to null, meaning all attributes listed in [[attributes()]] will be returned.
 * If it is an array, only the attributes in the array will be returned.
 * @param array $except list of attributes whose value should NOT be returned.
 * @return array attribute values (name => value).
 */
PHP_METHOD(yii_base_Model, getAttributes) {

	HashTable *_1, *_5;
	HashPosition _0, _4;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *names = NULL, *except = NULL, *values, *name = NULL, **_2, *_3 = NULL, **_6;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 2, &names, &except);

	if (!names) {
		ZEPHIR_CPY_WRT(names, ZEPHIR_GLOBAL(global_null));
	} else {
		ZEPHIR_SEPARATE_PARAM(names);
	}
	if (!except) {
		ZEPHIR_INIT_VAR(except);
		array_init(except);
	}
	ZEPHIR_INIT_VAR(values);
	array_init(values);


	if (Z_TYPE_P(names) == IS_NULL) {
		ZEPHIR_CALL_METHOD(&names, this_ptr, "attributes",  NULL);
		zephir_check_call_status();
	}
	zephir_is_iterable(names, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HVALUE(name, _2);
		ZEPHIR_OBS_NVAR(_3);
		zephir_read_property_zval(&_3, this_ptr, name, PH_NOISY_CC);
		zephir_array_update_zval(&values, name, &_3, PH_COPY | PH_SEPARATE);
	}
	zephir_is_iterable(except, &_5, &_4, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_5, (void**) &_6, &_4) == SUCCESS
	  ; zephir_hash_move_forward_ex(_5, &_4)
	) {
		ZEPHIR_GET_HVALUE(name, _6);
		zephir_array_unset(&values, name, PH_SEPARATE);
	}
	RETURN_CCTOR(values);

}

/**
 * Sets the attribute values in a massive way.
 * @param array $values attribute values (name => value) to be assigned to the model.
 * @param boolean $safeOnly whether the assignments should only be done to the safe attributes.
 * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
 * @see safeAttributes()
 * @see attributes()
 */
PHP_METHOD(yii_base_Model, setAttributes) {

	zephir_fcall_cache_entry *_5 = NULL;
	HashTable *_3;
	HashPosition _2;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *values, *safeOnly = NULL, *attributes = NULL, *name = NULL, *value = NULL, *_0 = NULL, **_4;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &values, &safeOnly);

	if (!safeOnly) {
		safeOnly = ZEPHIR_GLOBAL(global_true);
	}


	if (Z_TYPE_P(values) == IS_ARRAY) {
		if (zephir_is_true(safeOnly)) {
			ZEPHIR_CALL_METHOD(&_0, this_ptr, "safeattributes",  NULL);
			zephir_check_call_status();
			ZEPHIR_CALL_FUNCTION(&attributes, "array_flip", &_1, _0);
			zephir_check_call_status();
		} else {
			ZEPHIR_CALL_METHOD(&_0, this_ptr, "attributes",  NULL);
			zephir_check_call_status();
			ZEPHIR_CALL_FUNCTION(&attributes, "array_flip", &_1, _0);
			zephir_check_call_status();
		}
		zephir_is_iterable(values, &_3, &_2, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_3, (void**) &_4, &_2) == SUCCESS
		  ; zephir_hash_move_forward_ex(_3, &_2)
		) {
			ZEPHIR_GET_HMKEY(name, _3, _2);
			ZEPHIR_GET_HVALUE(value, _4);
			if (zephir_array_isset(attributes, name)) {
				zephir_update_property_zval_zval(this_ptr, name, value TSRMLS_CC);
			} else {
				if (zephir_is_true(safeOnly)) {
					ZEPHIR_CALL_METHOD(NULL, this_ptr, "onunsafeattribute", &_5, name, value);
					zephir_check_call_status();
				}
			}
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * This method is invoked when an unsafe attribute is being massively assigned.
 * The default implementation will log a warning message if YII_DEBUG is on.
 * It does nothing otherwise.
 * @param string $name the unsafe attribute name
 * @param mixed $value the attribute value
 */
PHP_METHOD(yii_base_Model, onUnsafeAttribute) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	zval *name, *value, *_0, *_2, *_3;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &name, &value);



	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_GET_CONSTANT(_0, "YII_DEBUG");
	if (zephir_is_true(_0)) {
		ZEPHIR_INIT_BNVAR(_0);
		zephir_get_class(_0, this_ptr, 0 TSRMLS_CC);
		ZEPHIR_INIT_VAR(_2);
		ZEPHIR_CONCAT_SVSVS(_2, "Failed to set unsafe attribute '", name, "' in '", _0, "'.");
		ZEPHIR_INIT_VAR(_3);
		ZVAL_STRING(_3, "Model:onUnsafeAttribute", 0);
		ZEPHIR_CALL_CE_STATIC(NULL, yii_baseyii_ce, "trace", &_1, _2, _3);
		zephir_check_temp_parameter(_3);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the scenario that this model is used in.
 *
 * Scenario affects how validation is performed and which attributes can
 * be massively assigned.
 *
 * @return string the scenario that this model is in. Defaults to [[SCENARIO_DEFAULT]].
 */
PHP_METHOD(yii_base_Model, getScenario) {


	RETURN_MEMBER(this_ptr, "_scenario");

}

/**
 * Sets the scenario for the model.
 * Note that this method does not check if the scenario exists or not.
 * The method [[validate()]] will perform this check.
 * @param string $value the scenario that this model is in.
 */
PHP_METHOD(yii_base_Model, setScenario) {

	zval *value;

	zephir_fetch_params(0, 1, 0, &value);



	zephir_update_property_this(this_ptr, SL("_scenario"), value TSRMLS_CC);

}

/**
 * Returns the attribute names that are safe to be massively assigned in the current scenario.
 * @return string[] safe attribute names
 */
PHP_METHOD(yii_base_Model, safeAttributes) {

	zephir_nts_static zephir_fcall_cache_entry *_6 = NULL;
	HashTable *_2;
	HashPosition _1;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *scenario = NULL, *scenarios = NULL, *attributes, *attribute = NULL, *first_char = NULL, *_0, **_3, _4 = zval_used_for_init, _5 = zval_used_for_init;

	ZEPHIR_MM_GROW();
	ZEPHIR_INIT_VAR(attributes);
	array_init(attributes);

	ZEPHIR_CALL_METHOD(&scenario, this_ptr, "getscenario",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&scenarios, this_ptr, "scenarios",  NULL);
	zephir_check_call_status();
	if (!(zephir_array_isset(scenarios, scenario))) {
		array_init(return_value);
		RETURN_MM();
	}
	zephir_array_fetch(&_0, scenarios, scenario, PH_NOISY | PH_READONLY TSRMLS_CC);
	zephir_is_iterable(_0, &_2, &_1, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_2, (void**) &_3, &_1) == SUCCESS
	  ; zephir_hash_move_forward_ex(_2, &_1)
	) {
		ZEPHIR_GET_HVALUE(attribute, _3);
		ZEPHIR_SINIT_NVAR(_4);
		ZVAL_LONG(&_4, 0);
		ZEPHIR_SINIT_NVAR(_5);
		ZVAL_LONG(&_5, 1);
		ZEPHIR_CALL_FUNCTION(&first_char, "substr", &_6, attribute, &_4, &_5);
		zephir_check_call_status();
		if (!ZEPHIR_IS_STRING(first_char, "!")) {
			zephir_array_append(&attributes, attribute, PH_SEPARATE);
		}
	}
	RETURN_CCTOR(attributes);

}

/**
 * Returns the attribute names that are subject to validation in the current scenario.
 * @return string[] safe attribute names
 */
PHP_METHOD(yii_base_Model, activeAttributes) {

	zephir_nts_static zephir_fcall_cache_entry *_5 = NULL;
	HashTable *_1;
	HashPosition _0;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *scenario = NULL, *scenarios = NULL, *attributes, *i = NULL, *attribute = NULL, *first_char = NULL, **_2, _3 = zval_used_for_init, _4 = zval_used_for_init, *_6 = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&scenario, this_ptr, "getscenario",  NULL);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&scenarios, this_ptr, "scenarios",  NULL);
	zephir_check_call_status();
	if (!(zephir_array_isset(scenarios, scenario))) {
		array_init(return_value);
		RETURN_MM();
	}
	ZEPHIR_OBS_VAR(attributes);
	zephir_array_fetch(&attributes, scenarios, scenario, PH_NOISY TSRMLS_CC);
	zephir_is_iterable(attributes, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(i, _1, _0);
		ZEPHIR_GET_HVALUE(attribute, _2);
		ZEPHIR_SINIT_NVAR(_3);
		ZVAL_LONG(&_3, 0);
		ZEPHIR_SINIT_NVAR(_4);
		ZVAL_LONG(&_4, 1);
		ZEPHIR_CALL_FUNCTION(&first_char, "substr", &_5, attribute, &_3, &_4);
		zephir_check_call_status();
		if (ZEPHIR_IS_STRING(first_char, "!")) {
			ZEPHIR_SINIT_NVAR(_3);
			ZVAL_LONG(&_3, 1);
			ZEPHIR_CALL_FUNCTION(&_6, "substr", &_5, attribute, &_3);
			zephir_check_call_status();
			zephir_array_update_zval(&attributes, i, &_6, PH_COPY | PH_SEPARATE);
		}
	}
	RETURN_CCTOR(attributes);

}

/**
 * Populates the model with the data from end user.
 * The data to be loaded is `$data[formName]`, where `formName` refers to the value of [[formName()]].
 * If [[formName()]] is empty, the whole `$data` array will be used to populate the model.
 * The data being populated is subject to the safety check by [[setAttributes()]].
 * @param array $data the data array. This is usually `$_POST` or `$_GET`, but can also be any valid array
 * supplied by end user.
 * @param string $formName the form name to be used for loading the data into the model.
 * If not set, [[formName()]] will be used.
 * @return boolean whether the model is successfully populated with some data.
 */
PHP_METHOD(yii_base_Model, load) {

	zend_bool _0;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *data, *formName = NULL, *scope = NULL, *_1;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &data, &formName);

	if (!formName) {
		formName = ZEPHIR_GLOBAL(global_null);
	}


	if (Z_TYPE_P(formName) == IS_NULL) {
		ZEPHIR_CALL_METHOD(&scope, this_ptr, "formname",  NULL);
		zephir_check_call_status();
	} else {
		ZEPHIR_CPY_WRT(scope, formName);
	}
	_0 = ZEPHIR_IS_STRING(scope, "");
	if (_0) {
		_0 = zephir_fast_count_int(data TSRMLS_CC) > 0;
	}
	if (_0) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "setattributes", NULL, data);
		zephir_check_call_status();
		RETURN_MM_BOOL(1);
	} else {
		if (zephir_array_isset(data, scope)) {
			zephir_array_fetch(&_1, data, scope, PH_NOISY | PH_READONLY TSRMLS_CC);
			ZEPHIR_CALL_METHOD(NULL, this_ptr, "setattributes", NULL, _1);
			zephir_check_call_status();
			RETURN_MM_BOOL(1);
		} else {
			RETURN_MM_BOOL(0);
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Populates a set of models with the data from end user.
 * This method is mainly used to collect tabular data input.
 * The data to be loaded for each model is `$data[formName][index]`, where `formName`
 * refers to the value of [[formName()]], and `index` the index of the model in the `$models` array.
 * If [[formName()]] is empty, `$data[index]` will be used to populate each model.
 * The data being populated to each model is subject to the safety check by [[setAttributes()]].
 * @param array $models the models to be populated. Note that all models should have the same class.
 * @param array $data the data array. This is usually `$_POST` or `$_GET`, but can also be any valid array
 * supplied by end user.
 * @return boolean whether the model is successfully populated with some data.
 */
PHP_METHOD(yii_base_Model, loadMultiple) {

	HashTable *_2;
	HashPosition _1;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;
	zend_bool success;
	zval *models, *data, *model = NULL, *scope = NULL, *i = NULL, *new_model = NULL, *temp = NULL, **_3, *_4;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &models, &data);



	Z_SET_ISREF_P(models);
	ZEPHIR_CALL_FUNCTION(&model, "reset", &_0, models);
	Z_UNSET_ISREF_P(models);
	zephir_check_call_status();
	if (Z_TYPE_P(model) == IS_BOOL) {
		RETURN_MM_BOOL(0);
	}
	success = 0;
	ZEPHIR_CALL_METHOD(&scope, model, "formname",  NULL);
	zephir_check_call_status();
	zephir_is_iterable(models, &_2, &_1, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_2, (void**) &_3, &_1) == SUCCESS
	  ; zephir_hash_move_forward_ex(_2, &_1)
	) {
		ZEPHIR_GET_HMKEY(i, _2, _1);
		ZEPHIR_GET_HVALUE(new_model, _3);
		if (ZEPHIR_IS_STRING(scope, "")) {
			if (zephir_array_isset(data, i)) {
				zephir_array_fetch(&_4, data, i, PH_NOISY | PH_READONLY TSRMLS_CC);
				ZEPHIR_CALL_METHOD(NULL, new_model, "setattributes", NULL, _4);
				zephir_check_call_status();
				success = 1;
			}
		} else {
			ZEPHIR_OBS_NVAR(temp);
			zephir_array_fetch(&_4, data, scope, PH_NOISY | PH_READONLY TSRMLS_CC);
			if (zephir_array_isset_fetch(&temp, _4, i, 0 TSRMLS_CC)) {
				ZEPHIR_CALL_METHOD(NULL, model, "setattributes", NULL, temp);
				zephir_check_call_status();
				success = 1;
			}
		}
	}
	RETURN_MM_BOOL(success);

}

/**
 * Validates multiple models.
 * This method will validate every model. The models being validated may
 * be of the same or different types.
 * @param array $models the models to be validated
 * @param array $attributeNames list of attribute names that should be validated.
 * If this parameter is empty, it means any attribute listed in the applicable
 * validation rules should be validated.
 * @return boolean whether all models are valid. False will be returned if one
 * or multiple models have validation error.
 */
PHP_METHOD(yii_base_Model, validateMultiple) {

	int ZEPHIR_LAST_CALL_STATUS;
	HashTable *_1;
	HashPosition _0;
	zend_bool valid, _4;
	zval *models, *attributeNames = NULL, *model = NULL, **_2, *_3 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &models, &attributeNames);

	if (!attributeNames) {
		attributeNames = ZEPHIR_GLOBAL(global_null);
	}


	valid = 1;
	zephir_is_iterable(models, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HVALUE(model, _2);
		ZEPHIR_CALL_METHOD(&_3, model, "validate", NULL, attributeNames);
		zephir_check_call_status();
		_4 = zephir_is_true(_3);
		if (_4) {
			_4 = valid;
		}
		if (_4) {
			valid = 1;
		} else {
			valid = 0;
		}
	}
	RETURN_MM_BOOL(valid);

}

/**
 * Returns the list of fields that should be returned by default by [[toArray()]] when no specific fields are specified.
 *
 * A field is a named element in the returned array by [[toArray()]].
 *
 * This method should return an array of field names or field definitions.
 * If the former, the field name will be treated as an object property name whose value will be used
 * as the field value. If the latter, the array key should be the field name while the array value should be
 * the corresponding field definition which can be either an object property name or a PHP callable
 * returning the corresponding field value. The signature of the callable should be:
 *
 * ```php
 * function ($field, $model) {
 *     // return field value
 * }
 * ```
 *
 * For example, the following code declares four fields:
 *
 * - `email`: the field name is the same as the property name `email`;
 * - `firstName` and `lastName`: the field names are `firstName` and `lastName`, and their
 *   values are obtained from the `first_name` and `last_name` properties;
 * - `fullName`: the field name is `fullName`. Its value is obtained by concatenating `first_name`
 *   and `last_name`.
 *
 * ```php
 * return [
 *     'email',
 *     'firstName' => 'first_name',
 *     'lastName' => 'last_name',
 *     'fullName' => function () {
 *         return $this->first_name . ' ' . $this->last_name;
 *     },
 * ];
 * ```
 *
 * In this method, you may also want to return different lists of fields based on some context
 * information. For example, depending on [[scenario]] or the privilege of the current application user,
 * you may return different sets of visible fields or filter out some fields.
 *
 * The default implementation of this method returns [[attributes()]] indexed by the same attribute names.
 *
 * @return array the list of field names or field definitions.
 * @see toArray()
 */
PHP_METHOD(yii_base_Model, fields) {

	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *fields = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&fields, this_ptr, "attributes",  NULL);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_FUNCTION("array_combine", &_0, fields, fields);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Determines which fields can be returned by [[toArray()]].
 * This method will check the requested fields against those declared in [[fields()]] and [[extraFields()]]
 * to determine which fields can be returned.
 * @param array $fields the fields being requested for exporting
 * @param array $expand the additional fields being requested for exporting
 * @return array the list of fields to be exported. The array keys are the field names, and the array values
 * are the corresponding object property names or PHP callables returning the field values.
 */
PHP_METHOD(yii_base_Model, resolveFields) {

	zephir_nts_static zephir_fcall_cache_entry *_5 = NULL;
	zend_bool _3;
	HashTable *_1, *_7;
	HashPosition _0, _6;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *fields, *expand, *result, *field = NULL, *definition = NULL, *def_fields = NULL, *def_extra_fields = NULL, **_2, *_4 = NULL, **_8;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &fields, &expand);

	ZEPHIR_INIT_VAR(result);
	array_init(result);


	ZEPHIR_CALL_METHOD(&def_fields, this_ptr, "fields",  NULL);
	zephir_check_call_status();
	zephir_is_iterable(def_fields, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(field, _1, _0);
		ZEPHIR_GET_HVALUE(definition, _2);
		if (Z_TYPE_P(field) == IS_LONG) {
			ZEPHIR_CPY_WRT(field, definition);
		}
		_3 = zephir_fast_count_int(fields TSRMLS_CC) == 0;
		if (!(_3)) {
			ZEPHIR_CALL_FUNCTION(&_4, "in_array", &_5, field, fields, ZEPHIR_GLOBAL(global_true));
			zephir_check_call_status();
			_3 = zephir_is_true(_4);
		}
		if (_3) {
			zephir_array_update_zval(&result, field, &definition, PH_COPY | PH_SEPARATE);
		}
	}
	if (zephir_fast_count_int(expand TSRMLS_CC) == 0) {
		RETURN_CCTOR(result);
	}
	ZEPHIR_CALL_METHOD(&def_extra_fields, this_ptr, "extrafields",  NULL);
	zephir_check_call_status();
	zephir_is_iterable(def_extra_fields, &_7, &_6, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_7, (void**) &_8, &_6) == SUCCESS
	  ; zephir_hash_move_forward_ex(_7, &_6)
	) {
		ZEPHIR_GET_HMKEY(field, _7, _6);
		ZEPHIR_GET_HVALUE(definition, _8);
		if (Z_TYPE_P(field) == IS_LONG) {
			ZEPHIR_CPY_WRT(field, definition);
		}
		ZEPHIR_CALL_FUNCTION(&_4, "in_array", &_5, field, expand, ZEPHIR_GLOBAL(global_true));
		zephir_check_call_status();
		if (zephir_is_true(_4)) {
			zephir_array_update_zval(&result, field, &definition, PH_COPY | PH_SEPARATE);
		}
	}
	RETURN_CCTOR(result);

}

/**
 * Returns an iterator for traversing the attributes in the model.
 * This method is required by the interface IteratorAggregate.
 * @return ArrayIterator an iterator for traversing the items in the list.
 */
PHP_METHOD(yii_base_Model, getIterator) {

	zend_class_entry *_0;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *attributes = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&attributes, this_ptr, "getattributes",  NULL);
	zephir_check_call_status();
	_0 = zend_fetch_class(SL("ArrayIterator"), ZEND_FETCH_CLASS_AUTO TSRMLS_CC);
	object_init_ex(return_value, _0);
	ZEPHIR_CALL_METHOD(NULL, return_value, "__construct", NULL, attributes);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Returns whether there is an element at the specified offset.
 * This method is required by the SPL interface `ArrayAccess`.
 * It is implicitly called when you use something like `isset($model[$offset])`.
 * @param mixed $offset the offset to check on
 * @return boolean
 */
PHP_METHOD(yii_base_Model, offsetExists) {

	zval *offset, *_0;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &offset);



	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_zval(&_0, this_ptr, offset, PH_NOISY_CC);
	RETURN_MM_BOOL(Z_TYPE_P(_0) != IS_NULL);

}

/**
 * Returns the element at the specified offset.
 * This method is required by the SPL interface `ArrayAccess`.
 * It is implicitly called when you use something like `$value = $model[$offset];`.
 * @param mixed $offset the offset to retrieve element.
 * @return mixed the element at the offset, null if no element is found at the offset
 */
PHP_METHOD(yii_base_Model, offsetGet) {

	zval *offset, *_0;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &offset);



	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_zval(&_0, this_ptr, offset, PH_NOISY_CC);
	RETURN_CCTOR(_0);

}

/**
 * Sets the element at the specified offset.
 * This method is required by the SPL interface `ArrayAccess`.
 * It is implicitly called when you use something like `$model[$offset] = $item;`.
 * @param integer $offset the offset to set element
 * @param mixed $item the element value
 */
PHP_METHOD(yii_base_Model, offsetSet) {

	zval *offset, *item;

	zephir_fetch_params(0, 2, 0, &offset, &item);



	zephir_update_property_zval_zval(this_ptr, offset, item TSRMLS_CC);

}

/**
 * Sets the element value at the specified offset to null.
 * This method is required by the SPL interface `ArrayAccess`.
 * It is implicitly called when you use something like `unset($model[$offset])`.
 * @param mixed $offset the offset to unset element
 */
PHP_METHOD(yii_base_Model, offsetUnset) {

	zval *offset;

	zephir_fetch_params(0, 1, 0, &offset);



	zephir_update_property_this(this_ptr, Z_STRVAL_P(offset), Z_STRLEN_P(offset), ZEPHIR_GLOBAL(global_null) TSRMLS_CC);

}

