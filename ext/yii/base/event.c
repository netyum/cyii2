
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/string.h"
#include "kernel/object.h"
#include "kernel/array.h"
#include "kernel/operators.h"
#include "kernel/hash.h"
#include "kernel/fcall.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Event is the base class for all event classes.
 *
 * It encapsulates the parameters associated with an event.
 * The [[sender]] property describes who raises the event.
 * And the [[handled]] property indicates if the event is handled.
 * If an event handler sets [[handled]] to be true, the rest of the
 * uninvoked handlers will no longer be called to handle the event.
 *
 * Additionally, when attaching an event handler, extra data may be passed
 * and be available via the [[data]] property when the event handler is invoked.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_Event) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, Event, yii, base_event, yii_base_object_ce, yii_base_event_method_entry, 0);

	/**
	 * @var string the event name. This property is set by [[Component::trigger()]] and [[trigger()]].
	 * Event handlers may use this property to check what event it is handling.
	 */
	zend_declare_property_null(yii_base_event_ce, SL("name"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var object the sender of this event. If not set, this property will be
	 * set as the object whose "trigger()" method is called.
	 * This property may also be a `null` when this event is a
	 * class-level event which is triggered in a static context.
	 */
	zend_declare_property_null(yii_base_event_ce, SL("sender"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var boolean whether the event is handled. Defaults to false.
	 * When a handler sets this to be true, the event processing will stop and
	 * ignore the rest of the uninvoked event handlers.
	 */
	zend_declare_property_bool(yii_base_event_ce, SL("handled"), 0, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var mixed the data that is passed to [[Component::on()]] when attaching an event handler.
	 * Note that this varies according to which event handler is currently executing.
	 */
	zend_declare_property_null(yii_base_event_ce, SL("data"), ZEND_ACC_PUBLIC TSRMLS_CC);

	zend_declare_property_null(yii_base_event_ce, SL("_events"), ZEND_ACC_PROTECTED|ZEND_ACC_STATIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Attaches an event handler to a class-level event.
 *
 * When a class-level event is triggered, event handlers attached
 * to that class and all parent classes will be invoked.
 *
 * For example, the following code attaches an event handler to `ActiveRecord`'s
 * `afterInsert` event:
 *
 * ~~~
 * Event::on(ActiveRecord::className(), ActiveRecord::EVENT_AFTER_INSERT, function ($event) {
 *     Yii::trace(get_class($event->sender) . ' is inserted.');
 * });
 * ~~~
 *
 * The handler will be invoked for EVERY successful ActiveRecord insertion.
 *
 * For more details about how to declare an event handler, please refer to [[Component::on()]].
 *
 * @param string $class the fully qualified class name to which the event handler needs to attach.
 * @param string $name the event name.
 * @param callable $handler the event handler.
 * @param mixed $data the data to be passed to the event handler when the event is triggered.
 * When the event handler is invoked, this data can be accessed via [[Event::data]].
 * @see off()
 */
PHP_METHOD(yii_base_Event, on) {

	zval *name = NULL;
	zval *class = NULL, *name_param = NULL, *handler, *data = NULL, *events, *elements, *event, *event_class, *_0, _1, *_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 3, 1, &class, &name_param, &handler, &data);

	ZEPHIR_SEPARATE_PARAM(class);
	zephir_get_strval(name, name_param);
	if (!data) {
		data = ZEPHIR_GLOBAL(global_null);
	}
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, "\\", 0);
	zephir_fast_trim(_0, class, &_1, ZEPHIR_TRIM_LEFT TSRMLS_CC);
	ZEPHIR_CPY_WRT(class, _0);
	ZEPHIR_OBS_VAR(events);
	zephir_read_static_property_ce(&events, yii_base_event_ce, SL("_events") TSRMLS_CC);
	if (Z_TYPE_P(events) != IS_ARRAY) {
		ZEPHIR_INIT_BNVAR(events);
		array_init(events);
	}
	zephir_array_append(&elements, handler, PH_SEPARATE);
	zephir_array_append(&elements, data, PH_SEPARATE);
	if (!(zephir_array_isset(events, name))) {
		ZEPHIR_INIT_BNVAR(_0);
		array_init(_0);
		zephir_array_update_zval(&events, name, &_0, PH_COPY | PH_SEPARATE);
	}
	ZEPHIR_OBS_VAR(event);
	zephir_array_fetch(&event, events, name, PH_NOISY TSRMLS_CC);
	if (!(zephir_array_isset(event, class))) {
		ZEPHIR_INIT_VAR(_2);
		array_init(_2);
		zephir_array_update_zval(&event, class, &_2, PH_COPY | PH_SEPARATE);
	}
	ZEPHIR_OBS_VAR(event_class);
	zephir_array_fetch(&event_class, event, class, PH_NOISY TSRMLS_CC);
	zephir_array_append(&event_class, elements, PH_SEPARATE);
	zephir_array_update_zval(&event, class, &event_class, PH_COPY | PH_SEPARATE);
	zephir_array_update_zval(&events, name, &event, PH_COPY | PH_SEPARATE);
	zephir_update_static_property_ce(yii_base_event_ce, SL("_events"), events TSRMLS_CC);
	RETURN_CCTOR(elements);

}

/**
 * Detaches an event handler from a class-level event.
 *
 * This method is the opposite of [[on()]].
 *
 * @param string $class the fully qualified class name from which the event handler needs to be detached.
 * @param string $name the event name.
 * @param callable $handler the event handler to be removed.
 * If it is null, all handlers attached to the named event will be removed.
 * @return boolean whether a handler is found and detached.
 * @see on()
 */
PHP_METHOD(yii_base_Event, off) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_16 = NULL;
	HashTable *_12;
	HashPosition _11;
	zend_bool _4, removed, _8;
	zval *name = NULL;
	zval *class = NULL, *name_param = NULL, *handler = NULL, *events = NULL, *event = NULL, *temp_event = NULL, *temp_event2 = NULL, *removed_temp_event = NULL, *_0, _1, *_2, *_3, *_5, *_6, *_7, *i = NULL, *_9, *_10, **_13, *_14, *_15 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &class, &name_param, &handler);

	ZEPHIR_SEPARATE_PARAM(class);
	zephir_get_strval(name, name_param);
	if (!handler) {
		handler = ZEPHIR_GLOBAL(global_null);
	}


	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, "\\", 0);
	zephir_fast_trim(_0, class, &_1, ZEPHIR_TRIM_LEFT TSRMLS_CC);
	ZEPHIR_CPY_WRT(class, _0);
	_2 = zephir_fetch_static_property_ce(yii_base_event_ce, SL("_events") TSRMLS_CC);
	zephir_array_fetch(&_3, _2, name, PH_NOISY | PH_READONLY TSRMLS_CC);
	_4 = !zephir_array_isset(_3, class);
	if (!(_4)) {
		_5 = zephir_fetch_static_property_ce(yii_base_event_ce, SL("_events") TSRMLS_CC);
		zephir_array_fetch(&_6, _5, name, PH_NOISY | PH_READONLY TSRMLS_CC);
		zephir_array_fetch(&_7, _6, class, PH_NOISY | PH_READONLY TSRMLS_CC);
		_4 = ZEPHIR_IS_EMPTY(_7);
	}
	if (_4) {
		RETURN_MM_BOOL(0);
	}
	if (Z_TYPE_P(handler) == IS_NULL) {
		ZEPHIR_OBS_VAR(events);
		zephir_read_static_property_ce(&events, yii_base_event_ce, SL("_events") TSRMLS_CC);
		ZEPHIR_OBS_VAR(event);
		zephir_array_fetch(&event, events, name, PH_NOISY TSRMLS_CC);
		zephir_array_unset(&event, class, PH_SEPARATE);
		zephir_array_update_zval(&events, name, &event, PH_COPY | PH_SEPARATE);
		zephir_update_static_property_ce(yii_base_event_ce, SL("_events"), events TSRMLS_CC);
		RETURN_MM_BOOL(1);
	} else {
		removed = 0;
		ZEPHIR_OBS_NVAR(events);
		zephir_read_static_property_ce(&events, yii_base_event_ce, SL("_events") TSRMLS_CC);
		ZEPHIR_OBS_NVAR(event);
		zephir_array_fetch(&event, events, name, PH_NOISY TSRMLS_CC);
		_8 = zephir_array_isset(event, class);
		if (_8) {
			ZEPHIR_OBS_VAR(_9);
			zephir_array_fetch(&_9, event, class, PH_NOISY TSRMLS_CC);
			_8 = Z_TYPE_P(_9) == IS_ARRAY;
		}
		if (_8) {
			zephir_array_fetch(&_10, event, class, PH_NOISY | PH_READONLY TSRMLS_CC);
			zephir_is_iterable(_10, &_12, &_11, 0, 0);
			for (
			  ; zephir_hash_get_current_data_ex(_12, (void**) &_13, &_11) == SUCCESS
			  ; zephir_hash_move_forward_ex(_12, &_11)
			) {
				ZEPHIR_GET_HMKEY(i, _12, _11);
				ZEPHIR_GET_HVALUE(temp_event, _13);
				zephir_array_fetch_long(&_14, temp_event, 0, PH_NOISY | PH_READONLY TSRMLS_CC);
				if (ZEPHIR_IS_EQUAL(_14, handler)) {
					ZEPHIR_OBS_NVAR(temp_event2);
					zephir_array_fetch(&temp_event2, event, class, PH_NOISY TSRMLS_CC);
					zephir_array_unset(&temp_event2, i, PH_SEPARATE);
					zephir_array_update_zval(&event, class, &temp_event2, PH_COPY | PH_SEPARATE);
					removed = 1;
				}
			}
		}
		if (removed) {
			ZEPHIR_OBS_VAR(removed_temp_event);
			zephir_array_fetch(&removed_temp_event, event, class, PH_NOISY TSRMLS_CC);
			ZEPHIR_CALL_FUNCTION(&_15, "array_values", &_16, removed_temp_event);
			zephir_check_call_status();
			ZEPHIR_CPY_WRT(removed_temp_event, _15);
			zephir_array_update_zval(&event, class, &removed_temp_event, PH_COPY | PH_SEPARATE);
			zephir_array_fetch(&_10, event, class, PH_NOISY | PH_READONLY TSRMLS_CC);
			zephir_array_update_zval(&events, name, &_10, PH_COPY | PH_SEPARATE);
			zephir_update_static_property_ce(yii_base_event_ce, SL("_events"), events TSRMLS_CC);
		}
		RETURN_MM_BOOL(removed);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns a value indicating whether there is any handler attached to the specified class-level event.
 * Note that this method will also check all parent classes to see if there is any handler attached
 * to the named event.
 * @param string|object $class the object or the fully qualified class name specifying the class-level event.
 * @param string $name the event name.
 * @return boolean whether there is any handler attached to the event.
 */
PHP_METHOD(yii_base_Event, hasHandlers) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_13 = NULL;
	zend_bool _1, _8;
	zval *name = NULL;
	zval *class = NULL, *name_param = NULL, *_0, *_2, *_3, *_4 = NULL, _5, *_6, *_7, *_9, *_10, *_11, *_12 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &class, &name_param);

	ZEPHIR_SEPARATE_PARAM(class);
	zephir_get_strval(name, name_param);


	_0 = zephir_fetch_static_property_ce(yii_base_event_ce, SL("_events") TSRMLS_CC);
	_1 = !zephir_array_isset(_0, name);
	if (!(_1)) {
		_2 = zephir_fetch_static_property_ce(yii_base_event_ce, SL("_events") TSRMLS_CC);
		zephir_array_fetch(&_3, _2, name, PH_NOISY | PH_READONLY TSRMLS_CC);
		_1 = ZEPHIR_IS_EMPTY(_3);
	}
	if (_1) {
		RETURN_MM_BOOL(0);
	}
	if (Z_TYPE_P(class) == IS_OBJECT) {
		ZEPHIR_INIT_VAR(_4);
		zephir_get_class(_4, class, 0 TSRMLS_CC);
		ZEPHIR_CPY_WRT(class, _4);
	} else {
		ZEPHIR_INIT_NVAR(_4);
		ZEPHIR_SINIT_VAR(_5);
		ZVAL_STRING(&_5, "\\", 0);
		zephir_fast_trim(_4, class, &_5, ZEPHIR_TRIM_LEFT TSRMLS_CC);
		ZEPHIR_CPY_WRT(class, _4);
	}
	while (1) {
		if (!(Z_TYPE_P(class) != IS_BOOL)) {
			break;
		}
		_6 = zephir_fetch_static_property_ce(yii_base_event_ce, SL("_events") TSRMLS_CC);
		zephir_array_fetch(&_7, _6, name, PH_NOISY | PH_READONLY TSRMLS_CC);
		_8 = zephir_array_isset(_7, class);
		if (_8) {
			_9 = zephir_fetch_static_property_ce(yii_base_event_ce, SL("_events") TSRMLS_CC);
			zephir_array_fetch(&_10, _9, name, PH_NOISY | PH_READONLY TSRMLS_CC);
			zephir_array_fetch(&_11, _10, class, PH_NOISY | PH_READONLY TSRMLS_CC);
			_8 = !ZEPHIR_IS_EMPTY(_11);
		}
		if (_8) {
			RETURN_MM_BOOL(1);
		}
		ZEPHIR_CALL_FUNCTION(&_12, "get_parent_class", &_13, class);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(class, _12);
	}
	RETURN_MM_BOOL(0);

}

/**
 * Triggers a class-level event.
 * This method will cause invocation of event handlers that are attached to the named event
 * for the specified class and all its parent classes.
 * @param string|object $class the object or the fully qualified class name specifying the class-level event.
 * @param string $name the event name.
 * @param Event $event the event parameter. If not set, a default [[Event]] object will be created.
 */
PHP_METHOD(yii_base_Event, trigger) {

	zephir_nts_static zephir_fcall_cache_entry *_13 = NULL, *_16 = NULL;
	HashTable *_11;
	HashPosition _10;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_bool _0, _6;
	zval *name = NULL;
	zval *class = NULL, *name_param = NULL, *event = NULL, *events, *_1, *temp_event = NULL, *_2, *_3 = NULL, _4, *_5, *_7, *_8, *handler = NULL, *data = NULL, *call = NULL, *class_handers = NULL, *_9, **_12, *_14, *_15 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &class, &name_param, &event);

	ZEPHIR_SEPARATE_PARAM(class);
	zephir_get_strval(name, name_param);
	if (!event) {
		event = ZEPHIR_GLOBAL(global_null);
	}
	ZEPHIR_INIT_VAR(temp_event);
	ZVAL_NULL(temp_event);


	ZEPHIR_OBS_VAR(events);
	zephir_read_static_property_ce(&events, yii_base_event_ce, SL("_events") TSRMLS_CC);
	_0 = !zephir_array_isset(events, name);
	if (!(_0)) {
		zephir_array_fetch(&_1, events, name, PH_NOISY | PH_READONLY TSRMLS_CC);
		_0 = ZEPHIR_IS_EMPTY(_1);
	}
	if (_0) {
		RETURN_MM_NULL();
	}
	ZEPHIR_CPY_WRT(temp_event, event);
	if (Z_TYPE_P(temp_event) == IS_NULL) {
		ZEPHIR_INIT_VAR(temp_event);
		object_init_ex(temp_event, yii_base_event_ce);
		ZEPHIR_CALL_METHOD(NULL, temp_event, "__construct", NULL);
		zephir_check_call_status();
	}
	zephir_update_property_zval(temp_event, SL("handled"), (0) ? ZEPHIR_GLOBAL(global_true) : ZEPHIR_GLOBAL(global_false) TSRMLS_CC);
	zephir_update_property_zval(temp_event, SL("name"), name TSRMLS_CC);
	if (Z_TYPE_P(class) == IS_OBJECT) {
		ZEPHIR_OBS_VAR(_2);
		zephir_read_property_this(&_2, temp_event, SL("sender"), PH_NOISY_CC);
		if (Z_TYPE_P(_2) == IS_NULL) {
			zephir_update_property_zval(temp_event, SL("sender"), class TSRMLS_CC);
		}
		ZEPHIR_INIT_VAR(_3);
		zephir_get_class(_3, class, 0 TSRMLS_CC);
		ZEPHIR_CPY_WRT(class, _3);
	} else {
		ZEPHIR_INIT_NVAR(_3);
		ZEPHIR_SINIT_VAR(_4);
		ZVAL_STRING(&_4, "\\", 0);
		zephir_fast_trim(_3, class, &_4, ZEPHIR_TRIM_LEFT TSRMLS_CC);
		ZEPHIR_CPY_WRT(class, _3);
	}
	while (1) {
		if (!(Z_TYPE_P(class) != IS_BOOL)) {
			break;
		}
		zephir_array_fetch(&_5, events, name, PH_NOISY | PH_READONLY TSRMLS_CC);
		_6 = zephir_array_isset(_5, class);
		if (_6) {
			zephir_array_fetch(&_7, events, name, PH_NOISY | PH_READONLY TSRMLS_CC);
			zephir_array_fetch(&_8, _7, class, PH_NOISY | PH_READONLY TSRMLS_CC);
			_6 = !ZEPHIR_IS_EMPTY(_8);
		}
		if (_6) {
			zephir_array_fetch(&_9, events, name, PH_NOISY | PH_READONLY TSRMLS_CC);
			ZEPHIR_OBS_NVAR(class_handers);
			zephir_array_fetch(&class_handers, _9, class, PH_NOISY TSRMLS_CC);
			zephir_is_iterable(class_handers, &_11, &_10, 0, 0);
			for (
			  ; zephir_hash_get_current_data_ex(_11, (void**) &_12, &_10) == SUCCESS
			  ; zephir_hash_move_forward_ex(_11, &_10)
			) {
				ZEPHIR_GET_HVALUE(handler, _12);
				ZEPHIR_OBS_NVAR(data);
				zephir_array_fetch_long(&data, handler, 1, PH_NOISY TSRMLS_CC);
				ZEPHIR_OBS_NVAR(call);
				zephir_array_fetch_long(&call, handler, 0, PH_NOISY TSRMLS_CC);
				zephir_update_property_zval(temp_event, SL("data"), data TSRMLS_CC);
				ZEPHIR_CALL_FUNCTION(NULL, "call_user_func", &_13, call, temp_event);
				zephir_check_call_status();
				_14 = zephir_fetch_nproperty_this(temp_event, SL("handled"), PH_NOISY_CC);
				if (ZEPHIR_IS_TRUE(_14)) {
					RETURN_MM_NULL();
				}
			}
		}
		ZEPHIR_CALL_FUNCTION(&_15, "get_parent_class", &_16, class);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(class, _15);
	}
	ZEPHIR_MM_RESTORE();

}

