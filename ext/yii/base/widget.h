
extern zend_class_entry *yii_base_widget_ce;

ZEPHIR_INIT_CLASS(yii_base_Widget);

PHP_METHOD(yii_base_Widget, begin);
PHP_METHOD(yii_base_Widget, end);
PHP_METHOD(yii_base_Widget, widget);
PHP_METHOD(yii_base_Widget, getId);
PHP_METHOD(yii_base_Widget, setId);
PHP_METHOD(yii_base_Widget, getView);
PHP_METHOD(yii_base_Widget, setView);
PHP_METHOD(yii_base_Widget, run);
PHP_METHOD(yii_base_Widget, render);
PHP_METHOD(yii_base_Widget, renderFile);
PHP_METHOD(yii_base_Widget, getViewPath);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_widget_begin, 0, 0, 0)
	ZEND_ARG_INFO(0, config)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_widget_widget, 0, 0, 0)
	ZEND_ARG_INFO(0, config)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_widget_getid, 0, 0, 0)
	ZEND_ARG_INFO(0, autoGenerate)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_widget_setid, 0, 0, 1)
	ZEND_ARG_INFO(0, value)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_widget_setview, 0, 0, 1)
	ZEND_ARG_INFO(0, view)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_widget_render, 0, 0, 1)
	ZEND_ARG_INFO(0, view)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_widget_renderfile, 0, 0, 1)
	ZEND_ARG_INFO(0, file)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_widget_method_entry) {
	PHP_ME(yii_base_Widget, begin, arginfo_yii_base_widget_begin, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_base_Widget, end, NULL, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_base_Widget, widget, arginfo_yii_base_widget_widget, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_base_Widget, getId, arginfo_yii_base_widget_getid, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Widget, setId, arginfo_yii_base_widget_setid, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Widget, getView, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Widget, setView, arginfo_yii_base_widget_setview, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Widget, run, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Widget, render, arginfo_yii_base_widget_render, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Widget, renderFile, arginfo_yii_base_widget_renderfile, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Widget, getViewPath, NULL, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
