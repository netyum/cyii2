
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/operators.h"
#include "kernel/array.h"
#include "kernel/fcall.h"
#include "kernel/exception.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Request represents a request that is handled by an [[Application]].
 *
 * @property boolean $isConsoleRequest The value indicating whether the current request is made via console.
 * @property string $scriptFile Entry script file path (processed w/ realpath()).
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_Request) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, Request, yii, base_request, yii_base_component_ce, yii_base_request_method_entry, ZEND_ACC_EXPLICIT_ABSTRACT_CLASS);

	zend_declare_property_null(yii_base_request_ce, SL("_scriptFile"), ZEND_ACC_PROTECTED TSRMLS_CC);

	zend_declare_property_null(yii_base_request_ce, SL("_isConsoleRequest"), ZEND_ACC_PROTECTED TSRMLS_CC);

	return SUCCESS;

}

/**
 * Resolves the current request into a route and the associated parameters.
 * @return array the first element is the route, and the second is the associated parameters.
 */
PHP_METHOD(yii_base_Request, resolve) {

}

/**
 * Returns a value indicating whether the current request is made via command line
 * @return boolean the value indicating whether the current request is made via console
 */
PHP_METHOD(yii_base_Request, getIsConsoleRequest) {

	zval *_0, *_1;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("_isConsoleRequest"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) != IS_NULL) {
		RETURN_MM_MEMBER(this_ptr, "_isConsoleRequest");
	} else {
		ZEPHIR_INIT_VAR(_1);
		ZEPHIR_GET_CONSTANT(_1, "PHP_SAPI");
		RETURN_MM_BOOL(ZEPHIR_IS_STRING(_1, "cli"));
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Sets the value indicating whether the current request is made via command line
 * @param boolean $value the value indicating whether the current request is made via command line
 */
PHP_METHOD(yii_base_Request, setIsConsoleRequest) {

	zval *value;

	zephir_fetch_params(0, 1, 0, &value);



	zephir_update_property_this(this_ptr, SL("_isConsoleRequest"), value TSRMLS_CC);

}

/**
 * Returns entry script file path.
 * @return string entry script file path (processed w/ realpath())
 * @throws InvalidConfigException if the entry script file path cannot be determined automatically.
 */
PHP_METHOD(yii_base_Request, getScriptFile) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *scriptFile, *_0, *_SERVER;

	ZEPHIR_MM_GROW();

	_0 = zephir_fetch_nproperty_this(this_ptr, SL("_scriptFile"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		zephir_get_global(&_SERVER, SS("_SERVER") TSRMLS_CC);
		if (zephir_array_isset_string(_SERVER, SS("SCRIPT_FILENAME"))) {
			zephir_get_global(&_SERVER, SS("_SERVER") TSRMLS_CC);
			ZEPHIR_OBS_VAR(scriptFile);
			zephir_array_fetch_string(&scriptFile, _SERVER, SL("SCRIPT_FILENAME"), PH_NOISY TSRMLS_CC);
			ZEPHIR_CALL_METHOD(NULL, this_ptr, "setscriptfile", NULL, scriptFile);
			zephir_check_call_status();
		} else {
			ZEPHIR_THROW_EXCEPTION_DEBUG_STR(yii_base_invalidconfigexception_ce, "Unable to determine the entry script file path.", "yii/base/Request.zep", 67);
			return;
		}
	}
	RETURN_MM_MEMBER(this_ptr, "_scriptFile");

}

/**
 * Sets the entry script file path.
 * The entry script file path can normally be determined based on the `SCRIPT_FILENAME` SERVER variable.
 * However, for some server configurations, this may not be correct or feasible.
 * This setter is provided so that the entry script file path can be manually specified.
 * @param string $value the entry script file path. This can be either a file path or a path alias.
 * @throws InvalidConfigException if the provided entry script file path is invalid.
 */
PHP_METHOD(yii_base_Request, setScriptFile) {

	zend_bool _2;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_1 = NULL, *_4 = NULL;
	zval *value, *path = NULL, *scriptFile = NULL, *_3 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &value);



	ZEPHIR_CALL_CE_STATIC(&path, yii_baseyii_ce, "getalias", &_0, value);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(&scriptFile, "realpath", &_1, path);
	zephir_check_call_status();
	_2 = Z_TYPE_P(scriptFile) != IS_BOOL;
	if (_2) {
		ZEPHIR_CALL_FUNCTION(&_3, "is_file", &_4, scriptFile);
		zephir_check_call_status();
		_2 = zephir_is_true(_3);
	}
	if (_2) {
		zephir_update_property_this(this_ptr, SL("_scriptFile"), scriptFile TSRMLS_CC);
	} else {
		ZEPHIR_THROW_EXCEPTION_DEBUG_STR(yii_base_invalidconfigexception_ce, "Unable to determine the entry script file path.", "yii/base/Request.zep", 90);
		return;
	}
	ZEPHIR_MM_RESTORE();

}

