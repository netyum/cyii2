
extern zend_class_entry *yii_base_request_ce;

ZEPHIR_INIT_CLASS(yii_base_Request);

PHP_METHOD(yii_base_Request, resolve);
PHP_METHOD(yii_base_Request, getIsConsoleRequest);
PHP_METHOD(yii_base_Request, setIsConsoleRequest);
PHP_METHOD(yii_base_Request, getScriptFile);
PHP_METHOD(yii_base_Request, setScriptFile);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_request_setisconsolerequest, 0, 0, 1)
	ZEND_ARG_INFO(0, value)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_request_setscriptfile, 0, 0, 1)
	ZEND_ARG_INFO(0, value)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_request_method_entry) {
	PHP_ME(yii_base_Request, resolve, NULL, ZEND_ACC_ABSTRACT|ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Request, getIsConsoleRequest, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Request, setIsConsoleRequest, arginfo_yii_base_request_setisconsolerequest, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Request, getScriptFile, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Request, setScriptFile, arginfo_yii_base_request_setscriptfile, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
