
extern zend_class_entry *yii_base_inlineaction_ce;

ZEPHIR_INIT_CLASS(yii_base_InlineAction);

PHP_METHOD(yii_base_InlineAction, __construct);
PHP_METHOD(yii_base_InlineAction, runWithParams);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_inlineaction___construct, 0, 0, 3)
	ZEND_ARG_INFO(0, id)
	ZEND_ARG_INFO(0, controller)
	ZEND_ARG_INFO(0, actionMethod)
	ZEND_ARG_INFO(0, config)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_inlineaction_runwithparams, 0, 0, 1)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_inlineaction_method_entry) {
	PHP_ME(yii_base_InlineAction, __construct, arginfo_yii_base_inlineaction___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(yii_base_InlineAction, runWithParams, arginfo_yii_base_inlineaction_runwithparams, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
