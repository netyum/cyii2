
extern zend_class_entry *yii_base_action_ce;

ZEPHIR_INIT_CLASS(yii_base_Action);

PHP_METHOD(yii_base_Action, __construct);
PHP_METHOD(yii_base_Action, getUniqueId);
PHP_METHOD(yii_base_Action, runWithParams);
PHP_METHOD(yii_base_Action, beforeRun);
PHP_METHOD(yii_base_Action, afterRun);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_action___construct, 0, 0, 2)
	ZEND_ARG_INFO(0, id)
	ZEND_ARG_INFO(0, controller)
	ZEND_ARG_INFO(0, config)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_action_runwithparams, 0, 0, 1)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_action_method_entry) {
	PHP_ME(yii_base_Action, __construct, arginfo_yii_base_action___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(yii_base_Action, getUniqueId, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Action, runWithParams, arginfo_yii_base_action_runwithparams, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Action, beforeRun, NULL, ZEND_ACC_PROTECTED)
	PHP_ME(yii_base_Action, afterRun, NULL, ZEND_ACC_PROTECTED)
  PHP_FE_END
};
