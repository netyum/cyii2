
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/memory.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Response represents the response of an [[Application]] to a [[Request]].
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_Response) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, Response, yii, base_response, yii_base_component_ce, yii_base_response_method_entry, 0);

	/**
	 * @var integer the exit status. Exit statuses should be in the range 0 to 254.
	 * The status 0 means the program terminates successfully.
	 */
	zend_declare_property_long(yii_base_response_ce, SL("exitStatus"), 0, ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Sends the response to client.
 */
PHP_METHOD(yii_base_Response, send) {



}

/**
 * Removes all existing output buffers.
 */
PHP_METHOD(yii_base_Response, clearOutputBuffers) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_1 = NULL, *_2 = NULL;
	zval *level = NULL;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_FUNCTION(&level, "ob_get_level", &_0);
	zephir_check_call_status();
	while (1) {
		if (!(ZEPHIR_GT_LONG(level, 0))) {
			break;
		}
		ZEPHIR_CALL_FUNCTION(NULL, "ob_clean", &_1);
		zephir_check_call_status();
		ZEPHIR_CALL_FUNCTION(NULL, "ob_end_clean", &_2);
		zephir_check_call_status();
		ZEPHIR_SUB_ASSIGN(level, level);
	}
	ZEPHIR_MM_RESTORE();

}

