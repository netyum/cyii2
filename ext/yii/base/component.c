
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/concat.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/hash.h"
#include "kernel/operators.h"
#include "kernel/exception.h"
#include "kernel/string.h"
#include "kernel/array.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Component is the base class that implements the *property*, *event* and *behavior* features.
 *
 * Component provides the *event* and *behavior* features, in addition to the *property* feature which is implemented in
 * its parent class [[Object]].
 *
 * Event is a way to "inject" custom code into existing code at certain places. For example, a comment object can trigger
 * an "add" event when the user adds a comment. We can write custom code and attach it to this event so that when the event
 * is triggered (i.e. comment will be added), our custom code will be executed.
 *
 * An event is identified by a name that should be unique within the class it is defined at. Event names are *case-sensitive*.
 *
 * One or multiple PHP callbacks, called *event handlers*, can be attached to an event. You can call [[trigger()]] to
 * raise an event. When an event is raised, the event handlers will be invoked automatically in the order they were
 * attached.
 *
 * To attach an event handler to an event, call [[on()]]:
 *
 * ~~~
 * $post->on('update', function ($event) {
 *     // send email notification
 * });
 * ~~~
 *
 * In the above, an anonymous function is attached to the "update" event of the post. You may attach
 * the following types of event handlers:
 *
 * - anonymous function: `function ($event) { ... }`
 * - object method: `[$object, 'handleAdd']`
 * - static class method: `['Page', 'handleAdd']`
 * - global function: `'handleAdd'`
 *
 * The signature of an event handler should be like the following:
 *
 * ~~~
 * function foo($event)
 * ~~~
 *
 * where `$event` is an [[Event]] object which includes parameters associated with the event.
 *
 * You can also attach a handler to an event when configuring a component with a configuration array.
 * The syntax is like the following:
 *
 * ~~~
 * [
 *     'on add' => function ($event) { ... }
 * ]
 * ~~~
 *
 * where `on add` stands for attaching an event to the `add` event.
 *
 * Sometimes, you may want to associate extra data with an event handler when you attach it to an event
 * and then access it when the handler is invoked. You may do so by
 *
 * ~~~
 * $post->on('update', function ($event) {
 *     // the data can be accessed via $event->data
 * }, $data);
 * ~~~
 *
 * A behavior is an instance of [[Behavior]] or its child class. A component can be attached with one or multiple
 * behaviors. When a behavior is attached to a component, its public properties and methods can be accessed via the
 * component directly, as if the component owns those properties and methods.
 *
 * To attach a behavior to a component, declare it in [[behaviors()]], or explicitly call [[attachBehavior]]. Behaviors
 * declared in [[behaviors()]] are automatically attached to the corresponding component.
 *
 * One can also attach a behavior to a component when configuring it with a configuration array. The syntax is like the
 * following:
 *
 * ~~~
 * [
 *     'as tree' => [
 *         'class' => 'Tree',
 *     ],
 * ]
 * ~~~
 *
 * where `as tree` stands for attaching a behavior named `tree`, and the array will be passed to [[\Yii::createObject()]]
 * to create the behavior object.
 *
 * @property Behavior[] $behaviors List of behaviors attached to this component. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_Component) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, Component, yii, base_component, yii_base_object_ce, yii_base_component_method_entry, 0);

	/**
	 * @var array the attached event handlers (event name => handlers)
	 */
	zend_declare_property_null(yii_base_component_ce, SL("_events"), ZEND_ACC_PROTECTED TSRMLS_CC);

	/**
	 * @var Behavior[] the attached behaviors (behavior name => behavior)
	 */
	zend_declare_property_null(yii_base_component_ce, SL("_behaviors"), ZEND_ACC_PROTECTED TSRMLS_CC);

	return SUCCESS;

}

/**
 * Returns the value of a component property.
 * This method will check in the following order and act accordingly:
 *
 *  - a property defined by a getter: return the getter result
 *  - a property of a behavior: return the behavior property value
 *
 * Do not call this method directly as it is a PHP magic method that
 * will be implicitly called when executing `$value = $component->property;`.
 * @param string $name the property name
 * @return mixed the property value or the value of a behavior's property
 * @throws UnknownPropertyException if the property is not defined
 * @throws InvalidCallException if the property is write-only.
 * @see __set()
 */
PHP_METHOD(yii_base_Component, __get) {

	HashTable *_3;
	HashPosition _2;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *getter = NULL, *setter = NULL, *behavior = NULL, *_1, **_4, *_5 = NULL, *_6 = NULL, *_7 = NULL, *_8 = NULL, *_9 = NULL;
	zval *name = NULL, *_0 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &name_param);

	zephir_get_strval(name, name_param);


	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_CONCAT_SV(_0, "get", name);
	ZEPHIR_CPY_WRT(getter, _0);
	ZEPHIR_INIT_LNVAR(_0);
	ZEPHIR_CONCAT_SV(_0, "set", name);
	ZEPHIR_CPY_WRT(setter, _0);
	if ((zephir_method_exists(this_ptr, getter TSRMLS_CC)  == SUCCESS)) {
		ZEPHIR_RETURN_CALL_METHOD(this_ptr, Z_STRVAL_P(getter), NULL);
		zephir_check_call_status();
		RETURN_MM();
	} else {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
		zephir_check_call_status();
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("_behaviors"), PH_NOISY_CC);
		zephir_is_iterable(_1, &_3, &_2, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_3, (void**) &_4, &_2) == SUCCESS
		  ; zephir_hash_move_forward_ex(_3, &_2)
		) {
			ZEPHIR_GET_HVALUE(behavior, _4);
			ZEPHIR_CALL_METHOD(&_5, behavior, "cangetproperty", NULL, name);
			zephir_check_call_status();
			if (zephir_is_true(_5)) {
				ZEPHIR_OBS_NVAR(_6);
				zephir_read_property_zval(&_6, behavior, name, PH_NOISY_CC);
				RETURN_CCTOR(_6);
			}
		}
	}
	if ((zephir_method_exists(this_ptr, setter TSRMLS_CC)  == SUCCESS)) {
		ZEPHIR_INIT_VAR(_7);
		object_init_ex(_7, yii_base_invalidcallexception_ce);
		ZEPHIR_INIT_VAR(_8);
		zephir_get_class(_8, this_ptr, 0 TSRMLS_CC);
		ZEPHIR_INIT_VAR(_9);
		ZEPHIR_CONCAT_SVSV(_9, "Getting write-only property: ", _8, "::", name);
		ZEPHIR_CALL_METHOD(NULL, _7, "__construct", NULL, _9);
		zephir_check_call_status();
		zephir_throw_exception_debug(_7, "yii/base/Component.zep", 141 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	} else {
		ZEPHIR_INIT_LNVAR(_7);
		object_init_ex(_7, yii_base_unknownpropertyexception_ce);
		ZEPHIR_INIT_NVAR(_8);
		zephir_get_class(_8, this_ptr, 0 TSRMLS_CC);
		ZEPHIR_INIT_LNVAR(_9);
		ZEPHIR_CONCAT_SVSV(_9, "Getting unknown property: ", _8, "::", name);
		ZEPHIR_CALL_METHOD(NULL, _7, "__construct", NULL, _9);
		zephir_check_call_status();
		zephir_throw_exception_debug(_7, "yii/base/Component.zep", 143 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Sets the value of a component property.
 * This method will check in the following order and act accordingly:
 *
 *  - a property defined by a setter: set the property value
 *  - an event in the format of "on xyz": attach the handler to the event "xyz"
 *  - a behavior in the format of "as xyz": attach the behavior named as "xyz"
 *  - a property of a behavior: set the behavior property value
 *
 * Do not call this method directly as it is a PHP magic method that
 * will be implicitly called when executing `$component->property = $value;`.
 * @param string $name the property name or the event name
 * @param mixed $value the property value
 * @throws UnknownPropertyException if the property is not defined
 * @throws InvalidCallException if the property is read-only.
 * @see __get()
 */
PHP_METHOD(yii_base_Component, __set) {

	HashTable *_14;
	HashPosition _13;
	zend_bool _4, _8, _9;
	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL, *_6 = NULL, *_11 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *value, *getter = NULL, *setter = NULL, *cmp_result = NULL, _1 = zval_used_for_init, _2 = zval_used_for_init, *_5 = NULL, *_7 = NULL, *_10 = NULL, *behavior = NULL, *_12, **_15, *_16 = NULL, *_17 = NULL, *_18;
	zval *name = NULL, *_0 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &name_param, &value);

	zephir_get_strval(name, name_param);
	ZEPHIR_SEPARATE_PARAM(name);


	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_CONCAT_SV(_0, "get", name);
	ZEPHIR_CPY_WRT(getter, _0);
	ZEPHIR_INIT_LNVAR(_0);
	ZEPHIR_CONCAT_SV(_0, "set", name);
	ZEPHIR_CPY_WRT(setter, _0);
	if ((zephir_method_exists(this_ptr, setter TSRMLS_CC)  == SUCCESS)) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, Z_STRVAL_P(setter), NULL, value);
		zephir_check_call_status();
		RETURN_MM_NULL();
	} else {
		ZEPHIR_SINIT_VAR(_1);
		ZVAL_STRING(&_1, "on ", 0);
		ZEPHIR_SINIT_VAR(_2);
		ZVAL_LONG(&_2, 3);
		ZEPHIR_CALL_FUNCTION(&cmp_result, "strncmp", &_3, name, &_1, &_2);
		zephir_check_call_status();
		_4 = Z_TYPE_P(cmp_result) != IS_BOOL;
		if (_4) {
			_4 = ZEPHIR_IS_LONG(cmp_result, 0);
		}
		if (_4) {
			ZEPHIR_SINIT_NVAR(_1);
			ZVAL_LONG(&_1, 3);
			ZEPHIR_CALL_FUNCTION(&_5, "substr", &_6, name, &_1);
			zephir_check_call_status();
			zephir_get_strval(name, _5);
			ZEPHIR_INIT_VAR(_7);
			zephir_fast_trim(_7, name, NULL , ZEPHIR_TRIM_BOTH TSRMLS_CC);
			zephir_get_strval(name, _7);
			ZEPHIR_CALL_METHOD(NULL, this_ptr, "on", NULL, name, value);
			zephir_check_call_status();
			RETURN_MM_NULL();
		} else {
			ZEPHIR_SINIT_NVAR(_1);
			ZVAL_STRING(&_1, "as ", 0);
			ZEPHIR_SINIT_NVAR(_2);
			ZVAL_LONG(&_2, 3);
			ZEPHIR_CALL_FUNCTION(&cmp_result, "strncmp", &_3, name, &_1, &_2);
			zephir_check_call_status();
			_8 = Z_TYPE_P(cmp_result) != IS_BOOL;
			if (_8) {
				_8 = ZEPHIR_IS_LONG(cmp_result, 0);
			}
			if (_8) {
				ZEPHIR_SINIT_NVAR(_1);
				ZVAL_LONG(&_1, 3);
				ZEPHIR_CALL_FUNCTION(&_5, "substr", &_6, name, &_1);
				zephir_check_call_status();
				zephir_get_strval(name, _5);
				ZEPHIR_INIT_NVAR(_7);
				zephir_fast_trim(_7, name, NULL , ZEPHIR_TRIM_BOTH TSRMLS_CC);
				zephir_get_strval(name, _7);
				_9 = Z_TYPE_P(value) == IS_OBJECT;
				if (_9) {
					_9 = (zephir_instance_of_ev(value, yii_base_behavior_ce TSRMLS_CC));
				}
				if (_9) {
					ZEPHIR_CALL_METHOD(NULL, this_ptr, "attachbehavior", NULL, name, value);
					zephir_check_call_status();
				} else {
					ZEPHIR_CALL_CE_STATIC(&_10, yii_baseyii_ce, "createobject", &_11, value);
					zephir_check_call_status();
					ZEPHIR_CALL_METHOD(NULL, this_ptr, "attachbehavior", NULL, name, _10);
					zephir_check_call_status();
				}
				RETURN_MM_NULL();
			} else {
				ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
				zephir_check_call_status();
				_12 = zephir_fetch_nproperty_this(this_ptr, SL("_behaviors"), PH_NOISY_CC);
				zephir_is_iterable(_12, &_14, &_13, 0, 0);
				for (
				  ; zephir_hash_get_current_data_ex(_14, (void**) &_15, &_13) == SUCCESS
				  ; zephir_hash_move_forward_ex(_14, &_13)
				) {
					ZEPHIR_GET_HVALUE(behavior, _15);
					ZEPHIR_CALL_METHOD(&_10, behavior, "cansetproperty", NULL, name);
					zephir_check_call_status();
					if (zephir_is_true(_10)) {
						zephir_update_property_zval_zval(behavior, name, value TSRMLS_CC);
						RETURN_MM_NULL();
					}
				}
			}
		}
	}
	if ((zephir_method_exists(this_ptr, getter TSRMLS_CC)  == SUCCESS)) {
		ZEPHIR_INIT_VAR(_16);
		object_init_ex(_16, yii_base_invalidcallexception_ce);
		ZEPHIR_INIT_NVAR(_7);
		zephir_get_class(_7, this_ptr, 0 TSRMLS_CC);
		ZEPHIR_INIT_VAR(_17);
		ZEPHIR_CONCAT_SVSV(_17, "Setting read-only property: ", _7, "::", name);
		ZEPHIR_CALL_METHOD(NULL, _16, "__construct", NULL, _17);
		zephir_check_call_status();
		zephir_throw_exception_debug(_16, "yii/base/Component.zep", 214 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	} else {
		ZEPHIR_INIT_LNVAR(_16);
		object_init_ex(_16, yii_base_unknownpropertyexception_ce);
		ZEPHIR_INIT_VAR(_18);
		zephir_get_class(_18, this_ptr, 0 TSRMLS_CC);
		ZEPHIR_INIT_LNVAR(_17);
		ZEPHIR_CONCAT_SVSV(_17, "Setting unknown property: ", _18, "::", name);
		ZEPHIR_CALL_METHOD(NULL, _16, "__construct", NULL, _17);
		zephir_check_call_status();
		zephir_throw_exception_debug(_16, "yii/base/Component.zep", 216 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Checks if a property value is null.
 * This method will check in the following order and act accordingly:
 *
 *  - a property defined by a setter: return whether the property value is null
 *  - a property of a behavior: return whether the property value is null
 *
 * Do not call this method directly as it is a PHP magic method that
 * will be implicitly called when executing `isset($component->property)`.
 * @param string $name the property name or the event name
 * @return boolean whether the named property is null
 */
PHP_METHOD(yii_base_Component, __isset) {

	HashTable *_4;
	HashPosition _3;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *getter = NULL, *_1 = NULL, *behavior = NULL, *_2, **_5, *_6 = NULL;
	zval *name = NULL, *_0;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &name_param);

	zephir_get_strval(name, name_param);


	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_CONCAT_SV(_0, "get", name);
	ZEPHIR_CPY_WRT(getter, _0);
	if ((zephir_method_exists(this_ptr, getter TSRMLS_CC)  == SUCCESS)) {
		ZEPHIR_CALL_METHOD(&_1, this_ptr, Z_STRVAL_P(getter),  NULL);
		zephir_check_call_status();
		RETURN_MM_BOOL(Z_TYPE_P(_1) != IS_NULL);
	} else {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
		zephir_check_call_status();
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("_behaviors"), PH_NOISY_CC);
		zephir_is_iterable(_2, &_4, &_3, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_4, (void**) &_5, &_3) == SUCCESS
		  ; zephir_hash_move_forward_ex(_4, &_3)
		) {
			ZEPHIR_GET_HVALUE(behavior, _5);
			ZEPHIR_CALL_METHOD(&_1, behavior, "cangetproperty", NULL, name);
			zephir_check_call_status();
			if (zephir_is_true(_1)) {
				ZEPHIR_OBS_NVAR(_6);
				zephir_read_property_zval(&_6, behavior, name, PH_NOISY_CC);
				RETURN_MM_BOOL(Z_TYPE_P(_6) != IS_NULL);
			}
		}
	}
	RETURN_MM_BOOL(0);

}

/**
 * Sets a component property to be null.
 * This method will check in the following order and act accordingly:
 *
 *  - a property defined by a setter: set the property value to be null
 *  - a property of a behavior: set the property value to be null
 *
 * Do not call this method directly as it is a PHP magic method that
 * will be implicitly called when executing `unset($component->property)`.
 * @param string $name the property name
 * @throws InvalidCallException if the property is read only.
 */
PHP_METHOD(yii_base_Component, __unset) {

	HashTable *_3;
	HashPosition _2;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *setter = NULL, *behavior = NULL, *_1, **_4, *_5 = NULL, *_6, *_7, *_8;
	zval *name = NULL, *_0;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &name_param);

	zephir_get_strval(name, name_param);


	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_CONCAT_SV(_0, "set", name);
	ZEPHIR_CPY_WRT(setter, _0);
	if ((zephir_method_exists(this_ptr, setter TSRMLS_CC)  == SUCCESS)) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, Z_STRVAL_P(setter), NULL, ZEPHIR_GLOBAL(global_null));
		zephir_check_call_status();
		RETURN_MM_NULL();
	} else {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
		zephir_check_call_status();
		_1 = zephir_fetch_nproperty_this(this_ptr, SL("_behaviors"), PH_NOISY_CC);
		zephir_is_iterable(_1, &_3, &_2, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_3, (void**) &_4, &_2) == SUCCESS
		  ; zephir_hash_move_forward_ex(_3, &_2)
		) {
			ZEPHIR_GET_HVALUE(behavior, _4);
			ZEPHIR_CALL_METHOD(&_5, behavior, "cansetproperty", NULL, name);
			zephir_check_call_status();
			if (zephir_is_true(_5)) {
				zephir_update_property_zval(behavior, Z_STRVAL_P(name), Z_STRLEN_P(name), ZEPHIR_GLOBAL(global_null) TSRMLS_CC);
				RETURN_MM_NULL();
			}
		}
	}
	ZEPHIR_INIT_VAR(_6);
	object_init_ex(_6, yii_base_invalidcallexception_ce);
	ZEPHIR_INIT_VAR(_7);
	zephir_get_class(_7, this_ptr, 0 TSRMLS_CC);
	ZEPHIR_INIT_VAR(_8);
	ZEPHIR_CONCAT_SVSV(_8, "Unsetting an unknown or read-only property: ", _7, "::", name);
	ZEPHIR_CALL_METHOD(NULL, _6, "__construct", NULL, _8);
	zephir_check_call_status();
	zephir_throw_exception_debug(_6, "yii/base/Component.zep", 285 TSRMLS_CC);
	ZEPHIR_MM_RESTORE();
	return;

}

/**
 * Calls the named method which is not a class method.
 *
 * This method will check if any attached behavior has
 * the named method and will execute it if available.
 *
 * Do not call this method directly as it is a PHP magic method that
 * will be implicitly called when an unknown method is being invoked.
 * @param string $name the method name
 * @param array $params method parameters
 * @return mixed the method return value
 * @throws UnknownMethodException when calling unknown method
 */
PHP_METHOD(yii_base_Component, __call) {

	zval *_5 = NULL;
	HashTable *_2;
	HashPosition _1;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *params, *object = NULL, *_0, **_3, *_4 = NULL, *_6, *_7, *_8;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &name_param, &params);

	zephir_get_strval(name, name_param);


	ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
	zephir_check_call_status();
	_0 = zephir_fetch_nproperty_this(this_ptr, SL("_behaviors"), PH_NOISY_CC);
	zephir_is_iterable(_0, &_2, &_1, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_2, (void**) &_3, &_1) == SUCCESS
	  ; zephir_hash_move_forward_ex(_2, &_1)
	) {
		ZEPHIR_GET_HVALUE(object, _3);
		ZEPHIR_CALL_METHOD(&_4, object, "hasmethod", NULL, name);
		zephir_check_call_status();
		if (zephir_is_true(_4)) {
			ZEPHIR_INIT_NVAR(_5);
			array_init_size(_5, 3);
			zephir_array_fast_append(_5, object);
			zephir_array_fast_append(_5, name);
			ZEPHIR_CALL_USER_FUNC_ARRAY(return_value, _5, params);
			zephir_check_call_status();
			RETURN_MM();
		}
	}
	ZEPHIR_INIT_VAR(_6);
	object_init_ex(_6, yii_base_unknownmethodexception_ce);
	ZEPHIR_INIT_VAR(_7);
	zephir_get_class(_7, this_ptr, 0 TSRMLS_CC);
	ZEPHIR_INIT_VAR(_8);
	ZEPHIR_CONCAT_SVSVS(_8, "Calling unknown method: ", _7, "::", name, "()");
	ZEPHIR_CALL_METHOD(NULL, _6, "__construct", NULL, _8);
	zephir_check_call_status();
	zephir_throw_exception_debug(_6, "yii/base/Component.zep", 311 TSRMLS_CC);
	ZEPHIR_MM_RESTORE();
	return;

}

/**
 * This method is called after the object is created by cloning an existing one.
 * It removes all behaviors because they are attached to the old object.
 */
PHP_METHOD(yii_base_Component, __clone) {

	zval *_0;

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(_0);
	array_init(_0);
	zephir_update_property_this(this_ptr, SL("_events"), _0 TSRMLS_CC);
	zephir_update_property_this(this_ptr, SL("_behaviors"), ZEPHIR_GLOBAL(global_null) TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns a value indicating whether a property is defined for this component.
 * A property is defined if:
 *
 * - the class has a getter or setter method associated with the specified name
 *   (in this case, property name is case-insensitive);
 * - the class has a member variable with the specified name (when `$checkVars` is true);
 * - an attached behavior has a property of the given name (when `$checkBehaviors` is true).
 *
 * @param string $name the property name
 * @param boolean $checkVars whether to treat member variables as properties
 * @param boolean $checkBehaviors whether to treat behaviors' properties as properties of this component
 * @return boolean whether the property is defined
 * @see canGetProperty()
 * @see canSetProperty()
 */
PHP_METHOD(yii_base_Component, hasProperty) {

	int ZEPHIR_LAST_CALL_STATUS;
	zend_bool checkVars, checkBehaviors, _1;
	zval *name_param = NULL, *checkVars_param = NULL, *checkBehaviors_param = NULL, *_0 = NULL, *_2 = NULL;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 2, &name_param, &checkVars_param, &checkBehaviors_param);

	zephir_get_strval(name, name_param);
	if (!checkVars_param) {
		checkVars = 1;
	} else {
		checkVars = zephir_get_boolval(checkVars_param);
	}
	if (!checkBehaviors_param) {
		checkBehaviors = 1;
	} else {
		checkBehaviors = zephir_get_boolval(checkBehaviors_param);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "cangetproperty", NULL, name, (checkVars ? ZEPHIR_GLOBAL(global_true) : ZEPHIR_GLOBAL(global_false)), (checkBehaviors ? ZEPHIR_GLOBAL(global_true) : ZEPHIR_GLOBAL(global_false)));
	zephir_check_call_status();
	_1 = zephir_is_true(_0);
	if (!(_1)) {
		ZEPHIR_CALL_METHOD(&_2, this_ptr, "cansetproperty", NULL, name, ZEPHIR_GLOBAL(global_false), (checkBehaviors ? ZEPHIR_GLOBAL(global_true) : ZEPHIR_GLOBAL(global_false)));
		zephir_check_call_status();
		_1 = zephir_is_true(_2);
	}
	RETURN_MM_BOOL(_1);

}

/**
 * Returns a value indicating whether a property can be read.
 * A property can be read if:
 *
 * - the class has a getter method associated with the specified name
 *   (in this case, property name is case-insensitive);
 * - the class has a member variable with the specified name (when `$checkVars` is true);
 * - an attached behavior has a readable property of the given name (when `$checkBehaviors` is true).
 *
 * @param string $name the property name
 * @param boolean $checkVars whether to treat member variables as properties
 * @param boolean $checkBehaviors whether to treat behaviors' properties as properties of this component
 * @return boolean whether the property can be read
 * @see canSetProperty()
 */
PHP_METHOD(yii_base_Component, canGetProperty) {

	HashTable *_7;
	HashPosition _6;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_4 = NULL;
	zend_bool checkVars, checkBehaviors, _1, _2;
	zval *name_param = NULL, *checkVars_param = NULL, *checkBehaviors_param = NULL, *getter = NULL, *_3 = NULL, *behavior = NULL, *_5, **_8, *_9 = NULL;
	zval *name = NULL, *_0;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 2, &name_param, &checkVars_param, &checkBehaviors_param);

	zephir_get_strval(name, name_param);
	if (!checkVars_param) {
		checkVars = 1;
	} else {
		checkVars = zephir_get_boolval(checkVars_param);
	}
	if (!checkBehaviors_param) {
		checkBehaviors = 1;
	} else {
		checkBehaviors = zephir_get_boolval(checkBehaviors_param);
	}


	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_CONCAT_SV(_0, "get", name);
	ZEPHIR_CPY_WRT(getter, _0);
	_1 = (zephir_method_exists(this_ptr, getter TSRMLS_CC)  == SUCCESS);
	if (!(_1)) {
		_2 = checkVars;
		if (_2) {
			ZEPHIR_CALL_FUNCTION(&_3, "property_exists", &_4, this_ptr, name);
			zephir_check_call_status();
			_2 = zephir_is_true(_3);
		}
		_1 = _2;
	}
	if (_1) {
		RETURN_MM_BOOL(1);
	}
	if (checkBehaviors) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
		zephir_check_call_status();
		_5 = zephir_fetch_nproperty_this(this_ptr, SL("_behaviors"), PH_NOISY_CC);
		zephir_is_iterable(_5, &_7, &_6, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_7, (void**) &_8, &_6) == SUCCESS
		  ; zephir_hash_move_forward_ex(_7, &_6)
		) {
			ZEPHIR_GET_HVALUE(behavior, _8);
			ZEPHIR_CALL_METHOD(&_9, behavior, "cangetproperty", NULL, name, (checkVars ? ZEPHIR_GLOBAL(global_true) : ZEPHIR_GLOBAL(global_false)));
			zephir_check_call_status();
			if (zephir_is_true(_9)) {
				RETURN_MM_BOOL(1);
			}
		}
	}
	RETURN_MM_BOOL(0);

}

/**
 * Returns a value indicating whether a property can be set.
 * A property can be written if:
 *
 * - the class has a setter method associated with the specified name
 *   (in this case, property name is case-insensitive);
 * - the class has a member variable with the specified name (when `$checkVars` is true);
 * - an attached behavior has a writable property of the given name (when `$checkBehaviors` is true).
 *
 * @param string $name the property name
 * @param boolean $checkVars whether to treat member variables as properties
 * @param boolean $checkBehaviors whether to treat behaviors' properties as properties of this component
 * @return boolean whether the property can be written
 * @see canGetProperty()
 */
PHP_METHOD(yii_base_Component, canSetProperty) {

	HashTable *_7;
	HashPosition _6;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_4 = NULL;
	zend_bool checkVars, checkBehaviors, _1, _2;
	zval *name_param = NULL, *checkVars_param = NULL, *checkBehaviors_param = NULL, *setter = NULL, *_3 = NULL, *behavior = NULL, *_5, **_8, *_9 = NULL;
	zval *name = NULL, *_0;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 2, &name_param, &checkVars_param, &checkBehaviors_param);

	zephir_get_strval(name, name_param);
	if (!checkVars_param) {
		checkVars = 1;
	} else {
		checkVars = zephir_get_boolval(checkVars_param);
	}
	if (!checkBehaviors_param) {
		checkBehaviors = 1;
	} else {
		checkBehaviors = zephir_get_boolval(checkBehaviors_param);
	}


	ZEPHIR_INIT_VAR(_0);
	ZEPHIR_CONCAT_SV(_0, "set", name);
	ZEPHIR_CPY_WRT(setter, _0);
	_1 = (zephir_method_exists(this_ptr, setter TSRMLS_CC)  == SUCCESS);
	if (!(_1)) {
		_2 = checkVars;
		if (_2) {
			ZEPHIR_CALL_FUNCTION(&_3, "property_exists", &_4, this_ptr, name);
			zephir_check_call_status();
			_2 = zephir_is_true(_3);
		}
		_1 = _2;
	}
	if (_1) {
		RETURN_MM_BOOL(1);
	}
	if (checkBehaviors) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
		zephir_check_call_status();
		_5 = zephir_fetch_nproperty_this(this_ptr, SL("_behaviors"), PH_NOISY_CC);
		zephir_is_iterable(_5, &_7, &_6, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_7, (void**) &_8, &_6) == SUCCESS
		  ; zephir_hash_move_forward_ex(_7, &_6)
		) {
			ZEPHIR_GET_HVALUE(behavior, _8);
			ZEPHIR_CALL_METHOD(&_9, behavior, "cansetproperty", NULL, name, (checkVars ? ZEPHIR_GLOBAL(global_true) : ZEPHIR_GLOBAL(global_false)));
			zephir_check_call_status();
			if (zephir_is_true(_9)) {
				RETURN_MM_BOOL(1);
			}
		}
	}
	RETURN_MM_BOOL(0);

}

/**
 * Returns a value indicating whether a method is defined.
 * A method is defined if:
 *
 * - the class has a method with the specified name
 * - an attached behavior has a method with the given name (when `$checkBehaviors` is true).
 *
 * @param string $name the property name
 * @param boolean $checkBehaviors whether to treat behaviors' methods as methods of this component
 * @return boolean whether the property is defined
 */
PHP_METHOD(yii_base_Component, hasMethod) {

	HashTable *_2;
	HashPosition _1;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_bool checkBehaviors;
	zval *name_param = NULL, *checkBehaviors_param = NULL, *behavior = NULL, *_0, **_3, *_4 = NULL;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &name_param, &checkBehaviors_param);

	zephir_get_strval(name, name_param);
	if (!checkBehaviors_param) {
		checkBehaviors = 1;
	} else {
		checkBehaviors = zephir_get_boolval(checkBehaviors_param);
	}


	if ((zephir_method_exists(this_ptr, name TSRMLS_CC)  == SUCCESS)) {
		RETURN_MM_BOOL(1);
	}
	if (checkBehaviors) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
		zephir_check_call_status();
		_0 = zephir_fetch_nproperty_this(this_ptr, SL("_behaviors"), PH_NOISY_CC);
		zephir_is_iterable(_0, &_2, &_1, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_2, (void**) &_3, &_1) == SUCCESS
		  ; zephir_hash_move_forward_ex(_2, &_1)
		) {
			ZEPHIR_GET_HVALUE(behavior, _3);
			ZEPHIR_CALL_METHOD(&_4, behavior, "hasmethod", NULL, name);
			zephir_check_call_status();
			if (zephir_is_true(_4)) {
				RETURN_MM_BOOL(1);
			}
		}
	}
	RETURN_MM_BOOL(0);

}

/**
 * Returns a list of behaviors that this component should behave as.
 *
 * Child classes may override this method to specify the behaviors they want to behave as.
 *
 * The return value of this method should be an array of behavior objects or configurations
 * indexed by behavior names. A behavior configuration can be either a string specifying
 * the behavior class or an array of the following structure:
 *
 * ~~~
 * 'behaviorName' => [
 *     'class' => 'BehaviorClass',
 *     'property1' => 'value1',
 *     'property2' => 'value2',
 * ]
 * ~~~
 *
 * Note that a behavior class must extend from [[Behavior]]. Behavior names can be strings
 * or integers. If the former, they uniquely identify the behaviors. If the latter, the corresponding
 * behaviors are anonymous and their properties and methods will NOT be made available via the component
 * (however, the behaviors can still respond to the component's events).
 *
 * Behaviors declared in this method will be attached to the component automatically (on demand).
 *
 * @return array the behavior configurations.
 */
PHP_METHOD(yii_base_Component, behaviors) {


	array_init(return_value);
	return;

}

/**
 * Returns a value indicating whether there is any handler attached to the named event.
 * @param string $name the event name
 * @return boolean whether there is any handler attached to the event.
 */
PHP_METHOD(yii_base_Component, hasEventHandlers) {

	zephir_nts_static zephir_fcall_cache_entry *_4 = NULL;
	zend_bool _1;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *_0, *_2, *_3;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &name_param);

	zephir_get_strval(name, name_param);


	ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
	zephir_check_call_status();
	_0 = zephir_fetch_nproperty_this(this_ptr, SL("_events"), PH_NOISY_CC);
	_1 = zephir_array_isset(_0, name);
	if (_1) {
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("_events"), PH_NOISY_CC);
		zephir_array_fetch(&_3, _2, name, PH_NOISY | PH_READONLY TSRMLS_CC);
		_1 = !ZEPHIR_IS_EMPTY(_3);
	}
	if (_1) {
		RETURN_MM_BOOL(1);
	} else {
		ZEPHIR_RETURN_CALL_CE_STATIC(yii_base_event_ce, "hashandlers", &_4, this_ptr, name);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Attaches an event handler to an event.
 *
 * The event handler must be a valid PHP callback. The followings are
 * some examples:
 *
 * ~~~
 * function ($event) { ... }         // anonymous function
 * [$object, 'handleClick']          // $object->handleClick()
 * ['Page', 'handleClick']           // Page::handleClick()
 * 'handleClick'                     // global function handleClick()
 * ~~~
 *
 * The event handler must be defined with the following signature,
 *
 * ~~~
 * function ($event)
 * ~~~
 *
 * where `$event` is an [[Event]] object which includes parameters associated with the event.
 *
 * @param string $name the event name
 * @param callable $handler the event handler
 * @param mixed $data the data to be passed to the event handler when the event is triggered.
 * When the event handler is invoked, this data can be accessed via [[Event::data]].
 * @param boolean $append whether to append new event handler to the end of the existing
 * handler list. If false, the new handler will be inserted at the beginning of the existing
 * handler list.
 * @see off()
 */
PHP_METHOD(yii_base_Component, on) {

	zephir_nts_static zephir_fcall_cache_entry *_3 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_bool append, _0, _1;
	zval *name_param = NULL, *handler, *data = NULL, *append_param = NULL, *events, *event = NULL, *elements, *_2;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 2, &name_param, &handler, &data, &append_param);

	zephir_get_strval(name, name_param);
	if (!data) {
		data = ZEPHIR_GLOBAL(global_null);
	}
	if (!append_param) {
		append = 1;
	} else {
		append = zephir_get_boolval(append_param);
	}
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(events);
	zephir_read_property_this(&events, this_ptr, SL("_events"), PH_NOISY_CC);
	if (Z_TYPE_P(events) != IS_ARRAY) {
		ZEPHIR_INIT_BNVAR(events);
		array_init(events);
	}
	zephir_array_append(&elements, handler, PH_SEPARATE);
	zephir_array_append(&elements, data, PH_SEPARATE);
	_0 = append == 1;
	if (!(_0)) {
		_0 = !zephir_array_isset(events, name);
	}
	_1 = _0;
	if (!(_1)) {
		zephir_array_fetch(&_2, events, name, PH_NOISY | PH_READONLY TSRMLS_CC);
		_1 = ZEPHIR_IS_EMPTY(_2);
	}
	if (_1) {
		if (zephir_array_isset(events, name)) {
			ZEPHIR_OBS_VAR(event);
			zephir_array_fetch(&event, events, name, PH_NOISY TSRMLS_CC);
		} else {
			ZEPHIR_INIT_NVAR(event);
			array_init(event);
		}
		if (Z_TYPE_P(event) != IS_ARRAY) {
			ZEPHIR_INIT_NVAR(event);
			array_init(event);
		}
		zephir_array_append(&event, elements, PH_SEPARATE);
		zephir_array_update_zval(&events, name, &event, PH_COPY | PH_SEPARATE);
	} else {
		if (zephir_array_isset(events, name)) {
			ZEPHIR_OBS_NVAR(event);
			zephir_array_fetch(&event, events, name, PH_NOISY TSRMLS_CC);
		} else {
			ZEPHIR_INIT_NVAR(event);
			array_init(event);
		}
		if (Z_TYPE_P(event) != IS_ARRAY) {
			ZEPHIR_INIT_NVAR(event);
			array_init(event);
		}
		Z_SET_ISREF_P(event);
		ZEPHIR_CALL_FUNCTION(NULL, "array_unshift", &_3, event, elements);
		Z_UNSET_ISREF_P(event);
		zephir_check_call_status();
		zephir_array_update_zval(&events, name, &event, PH_COPY | PH_SEPARATE);
	}
	zephir_update_property_this(this_ptr, SL("_events"), events TSRMLS_CC);
	ZEPHIR_MM_RESTORE();

}

/**
 * Detaches an existing event handler from this component.
 * This method is the opposite of [[on()]].
 * @param string $name event name
 * @param callable $handler the event handler to be removed.
 * If it is null, all handlers attached to the named event will be removed.
 * @return boolean if a handler is found and detached
 * @see on()
 */
PHP_METHOD(yii_base_Component, off) {

	zephir_nts_static zephir_fcall_cache_entry *_11 = NULL;
	HashTable *_7;
	HashPosition _6;
	int ZEPHIR_LAST_CALL_STATUS;
	zend_bool removed, _1;
	zval *name_param = NULL, *handler = NULL, *events, *temp_event = NULL, *removed_temp_event = NULL, *_0, *_2, *_3, *i = NULL, *event = NULL, *_4, *_5, **_8, *_9, *_10 = NULL;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &name_param, &handler);

	zephir_get_strval(name, name_param);
	if (!handler) {
		handler = ZEPHIR_GLOBAL(global_null);
	}


	ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
	zephir_check_call_status();
	_0 = zephir_fetch_nproperty_this(this_ptr, SL("_events"), PH_NOISY_CC);
	_1 = !zephir_array_isset(_0, name);
	if (!(_1)) {
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("_events"), PH_NOISY_CC);
		zephir_array_fetch(&_3, _2, name, PH_NOISY | PH_READONLY TSRMLS_CC);
		_1 = ZEPHIR_IS_EMPTY(_3);
	}
	if (_1) {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_OBS_VAR(events);
	zephir_read_property_this(&events, this_ptr, SL("_events"), PH_NOISY_CC);
	if (Z_TYPE_P(handler) == IS_NULL) {
		if (zephir_array_isset(events, name)) {
			zephir_array_unset(&events, name, PH_SEPARATE);
		}
		zephir_update_property_this(this_ptr, SL("_events"), events TSRMLS_CC);
		RETURN_MM_BOOL(1);
	} else {
		removed = 0;
		_4 = zephir_fetch_nproperty_this(this_ptr, SL("_events"), PH_NOISY_CC);
		zephir_array_fetch(&_5, _4, name, PH_NOISY | PH_READONLY TSRMLS_CC);
		zephir_is_iterable(_5, &_7, &_6, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_7, (void**) &_8, &_6) == SUCCESS
		  ; zephir_hash_move_forward_ex(_7, &_6)
		) {
			ZEPHIR_GET_HMKEY(i, _7, _6);
			ZEPHIR_GET_HVALUE(event, _8);
			zephir_array_fetch_long(&_9, event, 0, PH_NOISY | PH_READONLY TSRMLS_CC);
			if (ZEPHIR_IS_EQUAL(_9, handler)) {
				ZEPHIR_OBS_NVAR(temp_event);
				zephir_array_fetch(&temp_event, events, name, PH_NOISY TSRMLS_CC);
				zephir_array_unset(&temp_event, i, PH_SEPARATE);
				zephir_array_update_zval(&events, name, &temp_event, PH_COPY | PH_SEPARATE);
				removed = 1;
			}
		}
		if (removed) {
			ZEPHIR_OBS_VAR(removed_temp_event);
			zephir_array_fetch(&removed_temp_event, events, name, PH_NOISY TSRMLS_CC);
			ZEPHIR_CALL_FUNCTION(&_10, "array_values", &_11, removed_temp_event);
			zephir_check_call_status();
			ZEPHIR_CPY_WRT(removed_temp_event, _10);
			zephir_array_update_zval(&events, name, &removed_temp_event, PH_COPY | PH_SEPARATE);
			zephir_update_property_this(this_ptr, SL("_events"), events TSRMLS_CC);
		}
		RETURN_MM_BOOL(removed);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Triggers an event.
 * This method represents the happening of an event. It invokes
 * all attached handlers for the event including class-level handlers.
 * @param string $name the event name
 * @param Event $event the event parameter. If not set, a default [[Event]] object will be created.
 */
PHP_METHOD(yii_base_Component, trigger) {

	zephir_nts_static zephir_fcall_cache_entry *_8 = NULL, *_10 = NULL;
	HashTable *_5;
	HashPosition _4;
	zend_bool _0, _7;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *event = NULL, *events, *temp_event = NULL, *_1, *_2, *handler = NULL, *data = NULL, *call = NULL, *_3, **_6, *_9 = NULL;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &name_param, &event);

	zephir_get_strval(name, name_param);
	if (!event) {
		event = ZEPHIR_GLOBAL(global_null);
	}
	ZEPHIR_INIT_VAR(temp_event);
	ZVAL_NULL(temp_event);


	ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(events);
	zephir_read_property_this(&events, this_ptr, SL("_events"), PH_NOISY_CC);
	if (Z_TYPE_P(events) != IS_ARRAY) {
		ZEPHIR_INIT_BNVAR(events);
		array_init(events);
	}
	_0 = zephir_array_isset(events, name);
	if (_0) {
		zephir_array_fetch(&_1, events, name, PH_NOISY | PH_READONLY TSRMLS_CC);
		_0 = !ZEPHIR_IS_EMPTY(_1);
	}
	if (_0) {
		if (Z_TYPE_P(event) == IS_NULL) {
			ZEPHIR_INIT_VAR(temp_event);
			object_init_ex(temp_event, yii_base_event_ce);
			ZEPHIR_CALL_METHOD(NULL, temp_event, "__construct", NULL);
			zephir_check_call_status();
		} else {
			ZEPHIR_CPY_WRT(temp_event, event);
		}
		ZEPHIR_OBS_VAR(_2);
		zephir_read_property(&_2, temp_event, SL("sender"), PH_NOISY_CC);
		if (Z_TYPE_P(_2) == IS_NULL) {
			zephir_update_property_zval(temp_event, SL("sender"), this_ptr TSRMLS_CC);
		}
		zephir_update_property_zval(temp_event, SL("handled"), (0) ? ZEPHIR_GLOBAL(global_true) : ZEPHIR_GLOBAL(global_false) TSRMLS_CC);
		zephir_update_property_zval(temp_event, SL("name"), name TSRMLS_CC);
		zephir_array_fetch(&_3, events, name, PH_NOISY | PH_READONLY TSRMLS_CC);
		zephir_is_iterable(_3, &_5, &_4, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_5, (void**) &_6, &_4) == SUCCESS
		  ; zephir_hash_move_forward_ex(_5, &_4)
		) {
			ZEPHIR_GET_HVALUE(handler, _6);
			_7 = Z_TYPE_P(handler) == IS_ARRAY;
			if (_7) {
				_7 = zephir_fast_count_int(handler TSRMLS_CC) == 2;
			}
			if (_7) {
				ZEPHIR_OBS_NVAR(data);
				zephir_array_fetch_long(&data, handler, 1, PH_NOISY TSRMLS_CC);
				ZEPHIR_OBS_NVAR(call);
				zephir_array_fetch_long(&call, handler, 0, PH_NOISY TSRMLS_CC);
				zephir_update_property_zval(temp_event, SL("data"), data TSRMLS_CC);
				ZEPHIR_CALL_FUNCTION(NULL, "call_user_func", &_8, call, temp_event);
				zephir_check_call_status();
				ZEPHIR_OBS_NVAR(_9);
				zephir_read_property(&_9, temp_event, SL("handled"), PH_NOISY_CC);
				if (ZEPHIR_IS_TRUE(_9)) {
					RETURN_MM_NULL();
				}
			}
		}
	}
	ZEPHIR_CALL_CE_STATIC(NULL, yii_base_event_ce, "trigger", &_10, this_ptr, name, temp_event);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the named behavior object.
 * @param string $name the behavior name
 * @return Behavior the behavior object, or null if the behavior does not exist
 */
PHP_METHOD(yii_base_Component, getBehavior) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *_0, *_1, *_2;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &name_param);

	zephir_get_strval(name, name_param);


	ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_0);
	_1 = zephir_fetch_nproperty_this(this_ptr, SL("_behaviors"), PH_NOISY_CC);
	if (zephir_array_isset(_1, name)) {
		_2 = zephir_fetch_nproperty_this(this_ptr, SL("_behaviors"), PH_NOISY_CC);
		zephir_array_fetch(&_0, _2, name, PH_NOISY TSRMLS_CC);
	} else {
		ZVAL_NULL(_0);
	}
	RETURN_CCTOR(_0);

}

/**
 * Returns all behaviors attached to this component.
 * @return Behavior[] list of behaviors attached to this component
 */
PHP_METHOD(yii_base_Component, getBehaviors) {

	int ZEPHIR_LAST_CALL_STATUS;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
	zephir_check_call_status();
	RETURN_MM_MEMBER(this_ptr, "_behaviors");

}

/**
 * Attaches a behavior to this component.
 * This method will create the behavior object based on the given
 * configuration. After that, the behavior object will be attached to
 * this component by calling the [[Behavior::attach()]] method.
 * @param string $name the name of the behavior.
 * @param string|array|Behavior $behavior the behavior configuration. This can be one of the following:
 *
 *  - a [[Behavior]] object
 *  - a string specifying the behavior class
 *  - an object configuration array that will be passed to [[Yii::createObject()]] to create the behavior object.
 *
 * @return Behavior the behavior object
 * @see detachBehavior()
 */
PHP_METHOD(yii_base_Component, attachBehavior) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *behavior;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &name_param, &behavior);

	zephir_get_strval(name, name_param);


	ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "attachbehaviorinternal", NULL, name, behavior);
	zephir_check_call_status();
	RETURN_MM();

}

/**
 * Attaches a list of behaviors to the component.
 * Each behavior is indexed by its name and should be a [[Behavior]] object,
 * a string specifying the behavior class, or an configuration array for creating the behavior.
 * @param array $behaviors list of behaviors to be attached to the component
 * @see attachBehavior()
 */
PHP_METHOD(yii_base_Component, attachBehaviors) {

	zephir_fcall_cache_entry *_3 = NULL;
	HashTable *_1;
	HashPosition _0;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *behaviors, *name = NULL, *behavior = NULL, **_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &behaviors);



	ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
	zephir_check_call_status();
	zephir_is_iterable(behaviors, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(name, _1, _0);
		ZEPHIR_GET_HVALUE(behavior, _2);
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "attachbehaviorinternal", &_3, name, behavior);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Detaches a behavior from the component.
 * The behavior's [[Behavior::detach()]] method will be invoked.
 * @param string $name the behavior's name.
 * @return Behavior the detached behavior. Null if the behavior does not exist.
 */
PHP_METHOD(yii_base_Component, detachBehavior) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *name_param = NULL, *behavior, *behaviors;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &name_param);

	zephir_get_strval(name, name_param);


	ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(behaviors);
	zephir_read_property_this(&behaviors, this_ptr, SL("_behaviors"), PH_NOISY_CC);
	ZEPHIR_OBS_VAR(behavior);
	if (zephir_array_isset_fetch(&behavior, behaviors, name, 0 TSRMLS_CC)) {
		zephir_array_unset(&behaviors, name, PH_SEPARATE);
		zephir_update_property_this(this_ptr, SL("_behaviors"), behaviors TSRMLS_CC);
		ZEPHIR_CALL_METHOD(NULL, behavior, "detach", NULL);
		zephir_check_call_status();
		RETURN_CCTOR(behavior);
	} else {
		RETURN_MM_NULL();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Detaches all behaviors from the component.
 */
PHP_METHOD(yii_base_Component, detachBehaviors) {

	zephir_fcall_cache_entry *_3 = NULL;
	HashTable *_1;
	HashPosition _0;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *name = NULL, *behavior = NULL, *behaviors, **_2;

	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(NULL, this_ptr, "ensurebehaviors", NULL);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(behaviors);
	zephir_read_property_this(&behaviors, this_ptr, SL("_behaviors"), PH_NOISY_CC);
	zephir_is_iterable(behaviors, &_1, &_0, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_1, (void**) &_2, &_0) == SUCCESS
	  ; zephir_hash_move_forward_ex(_1, &_0)
	) {
		ZEPHIR_GET_HMKEY(name, _1, _0);
		ZEPHIR_GET_HVALUE(behavior, _2);
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "detachbehavior", &_3, name);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Makes sure that the behaviors declared in [[behaviors()]] are attached to this component.
 */
PHP_METHOD(yii_base_Component, ensureBehaviors) {

	zephir_fcall_cache_entry *_6 = NULL;
	HashTable *_4;
	HashPosition _3;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *_0, *_1, *name = NULL, *behavior = NULL, *_2 = NULL, **_5;

	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(_0);
	zephir_read_property_this(&_0, this_ptr, SL("_behaviors"), PH_NOISY_CC);
	if (Z_TYPE_P(_0) == IS_NULL) {
		ZEPHIR_INIT_VAR(_1);
		array_init(_1);
		zephir_update_property_this(this_ptr, SL("_behaviors"), _1 TSRMLS_CC);
		ZEPHIR_CALL_METHOD(&_2, this_ptr, "behaviors",  NULL);
		zephir_check_call_status();
		zephir_is_iterable(_2, &_4, &_3, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_4, (void**) &_5, &_3) == SUCCESS
		  ; zephir_hash_move_forward_ex(_4, &_3)
		) {
			ZEPHIR_GET_HMKEY(name, _4, _3);
			ZEPHIR_GET_HVALUE(behavior, _5);
			ZEPHIR_CALL_METHOD(NULL, this_ptr, "attachbehaviorinternal", &_6, name, behavior);
			zephir_check_call_status();
		}
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Attaches a behavior to this component.
 * @param string $name the name of the behavior.
 * @param string|array|Behavior $behavior the behavior to be attached
 * @return Behavior the attached behavior.
 */
PHP_METHOD(yii_base_Component, attachBehaviorInternal) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	zval *name_param = NULL, *behavior = NULL, *temp_behavior, *behaviors, *_0 = NULL, *_2;
	zval *name = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &name_param, &behavior);

	zephir_get_strval(name, name_param);
	ZEPHIR_SEPARATE_PARAM(behavior);


	if (Z_TYPE_P(behavior) != IS_OBJECT) {
		ZEPHIR_CALL_CE_STATIC(&_0, yii_baseyii_ce, "createobject", &_1, behavior);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(behavior, _0);
	}
	ZEPHIR_OBS_VAR(temp_behavior);
	_2 = zephir_fetch_nproperty_this(this_ptr, SL("_behaviors"), PH_NOISY_CC);
	if (zephir_array_isset_fetch(&temp_behavior, _2, name, 0 TSRMLS_CC)) {
		ZEPHIR_CALL_METHOD(NULL, temp_behavior, "detach", NULL);
		zephir_check_call_status();
	}
	ZEPHIR_CALL_METHOD(NULL, behavior, "attach", NULL, this_ptr);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(behaviors);
	zephir_read_property_this(&behaviors, this_ptr, SL("_behaviors"), PH_NOISY_CC);
	zephir_array_update_zval(&behaviors, name, &behavior, PH_COPY | PH_SEPARATE);
	zephir_update_property_this(this_ptr, SL("_behaviors"), behaviors TSRMLS_CC);
	RETURN_CCTOR(behavior);

}

