
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/memory.h"
#include "kernel/concat.h"
#include "kernel/exception.h"
#include "kernel/array.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Action is the base class for all controller action classes.
 *
 * Action provides a way to divide a complex controller into
 * smaller actions in separate class files.
 *
 * Derived classes must implement a method named `run()`. This method
 * will be invoked by the controller when the action is requested.
 * The `run()` method can have parameters which will be filled up
 * with user input values automatically according to their names.
 * For example, if the `run()` method is declared as follows:
 *
 * ~~~
 * public function run($id, $type = 'book') { ... }
 * ~~~
 *
 * And the parameters provided for the action are: `['id' => 1]`.
 * Then the `run()` method will be invoked as `run(1)` automatically.
 *
 * @property string $uniqueId The unique ID of this action among the whole application. This property is
 * read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_Action) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, Action, yii, base_action, yii_base_component_ce, yii_base_action_method_entry, 0);

	/**
	 * @var string ID of the action
	 */
	zend_declare_property_null(yii_base_action_ce, SL("id"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var Controller the controller that owns this action
	 */
	zend_declare_property_null(yii_base_action_ce, SL("controller"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Constructor.
 *
 * @param string $id the ID of this action
 * @param Controller $controller the controller that owns this action
 * @param array $config name-value pairs that will be used to initialize the object properties
 */
PHP_METHOD(yii_base_Action, __construct) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;
	zval *id_param = NULL, *controller, *config = NULL;
	zval *id = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &id_param, &controller, &config);

	zephir_get_strval(id, id_param);
	if (!config) {
		ZEPHIR_INIT_VAR(config);
		array_init(config);
	}


	zephir_update_property_this(this_ptr, SL("id"), id TSRMLS_CC);
	zephir_update_property_this(this_ptr, SL("controller"), controller TSRMLS_CC);
	ZEPHIR_CALL_PARENT(NULL, yii_base_action_ce, this_ptr, "__construct", &_0, config);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the unique ID of this action among the whole application.
 *
 * @return string the unique ID of this action among the whole application.
 */
PHP_METHOD(yii_base_Action, getUniqueId) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *_0, *_1 = NULL, *_2;

	ZEPHIR_MM_GROW();

	_0 = zephir_fetch_nproperty_this(this_ptr, SL("controller"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_1, _0, "getuniqueid",  NULL);
	zephir_check_call_status();
	_2 = zephir_fetch_nproperty_this(this_ptr, SL("id"), PH_NOISY_CC);
	ZEPHIR_CONCAT_VSV(return_value, _1, "/", _2);
	RETURN_MM();

}

/**
 * Runs this action with the specified parameters.
 * This method is mainly invoked by the controller.
 *
 * @param array $params the parameters to be bound to the action's run() method.
 * @return mixed the result of the action
 * @throws InvalidConfigException if the action class does not have a run() method
 */
PHP_METHOD(yii_base_Action, runWithParams) {

	zephir_nts_static zephir_fcall_cache_entry *_6 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *params, *args = NULL, *requestedParams = NULL, *_0 = NULL, *_1 = NULL, *_2, *_3, *app = NULL, *result, *_4, *_5, *_7, *_8 = NULL, *elements, *temp_args = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &params);

	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	if (!((zephir_method_exists_ex(this_ptr, SS("run") TSRMLS_CC) == SUCCESS))) {
		ZEPHIR_INIT_VAR(_0);
		object_init_ex(_0, yii_base_invalidconfigexception_ce);
		ZEPHIR_INIT_VAR(_1);
		zephir_get_class(_1, this_ptr, 0 TSRMLS_CC);
		ZEPHIR_INIT_VAR(_2);
		ZEPHIR_CONCAT_VS(_2, _1, " must define a \"run()\" method.");
		ZEPHIR_CALL_METHOD(NULL, _0, "__construct", NULL, _2);
		zephir_check_call_status();
		zephir_throw_exception_debug(_0, "yii/base/Action.zep", 83 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	_3 = zephir_fetch_nproperty_this(this_ptr, SL("controller"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&args, _3, "bindactionparams", NULL, this_ptr, params);
	zephir_check_call_status();
	zephir_read_static_property_ce(&_4, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _4);
	ZEPHIR_OBS_VAR(_5);
	zephir_read_property(&_5, app, SL("requestedParams"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(requestedParams, _5);
	ZEPHIR_INIT_NVAR(_1);
	zephir_get_class(_1, this_ptr, 0 TSRMLS_CC);
	ZEPHIR_INIT_LNVAR(_0);
	ZEPHIR_CONCAT_SVS(_0, "Running action: ", _1, "::run()");
	ZEPHIR_INIT_VAR(_7);
	ZVAL_STRING(_7, "Action:runWithParams", 0);
	ZEPHIR_CALL_CE_STATIC(NULL, yii_baseyii_ce, "trace", &_6, _0, _7);
	zephir_check_temp_parameter(_7);
	zephir_check_call_status();
	if (Z_TYPE_P(requestedParams) == IS_NULL) {
		zephir_update_property_zval(app, SL("requestedParams"), args TSRMLS_CC);
		zephir_update_static_property_ce(yii_baseyii_ce, SL("app"), app TSRMLS_CC);
	}
	ZEPHIR_CALL_METHOD(&_8, this_ptr, "beforerun",  NULL);
	zephir_check_call_status();
	if (zephir_is_true(_8)) {
		zephir_array_append(&elements, this_ptr, PH_SEPARATE);
		ZEPHIR_INIT_NVAR(_1);
		ZVAL_STRING(_1, "run", 1);
		zephir_array_append(&elements, _1, PH_SEPARATE);
		ZEPHIR_CPY_WRT(temp_args, args);
		ZEPHIR_INIT_VAR(result);
		ZEPHIR_CALL_USER_FUNC_ARRAY(result, elements, temp_args);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "afterrun", NULL);
		zephir_check_call_status();
		RETURN_CCTOR(result);
	} else {
		RETURN_MM_NULL();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * This method is called right before `run()` is executed.
 * You may override this method to do preparation work for the action run.
 * If the method returns false, it will cancel the action.
 *
 * @return boolean whether to run the action.
 */
PHP_METHOD(yii_base_Action, beforeRun) {


	RETURN_BOOL(1);

}

/**
 * This method is called right after `run()` is executed.
 * You may override this method to do post-processing work for the action run.
 */
PHP_METHOD(yii_base_Action, afterRun) {



}

