
extern zend_class_entry *yii_base_actionfilter_ce;

ZEPHIR_INIT_CLASS(yii_base_ActionFilter);

PHP_METHOD(yii_base_ActionFilter, attach);
PHP_METHOD(yii_base_ActionFilter, detach);
PHP_METHOD(yii_base_ActionFilter, beforeFilter);
PHP_METHOD(yii_base_ActionFilter, afterFilter);
PHP_METHOD(yii_base_ActionFilter, beforeAction);
PHP_METHOD(yii_base_ActionFilter, afterAction);
PHP_METHOD(yii_base_ActionFilter, isActive);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_actionfilter_attach, 0, 0, 1)
	ZEND_ARG_INFO(0, owner)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_actionfilter_beforefilter, 0, 0, 1)
	ZEND_ARG_INFO(0, event)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_actionfilter_afterfilter, 0, 0, 1)
	ZEND_ARG_INFO(0, event)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_actionfilter_beforeaction, 0, 0, 1)
	ZEND_ARG_INFO(0, action)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_actionfilter_afteraction, 0, 0, 2)
	ZEND_ARG_INFO(0, action)
	ZEND_ARG_INFO(0, result)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_actionfilter_isactive, 0, 0, 1)
	ZEND_ARG_INFO(0, action)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_actionfilter_method_entry) {
	PHP_ME(yii_base_ActionFilter, attach, arginfo_yii_base_actionfilter_attach, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_ActionFilter, detach, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_ActionFilter, beforeFilter, arginfo_yii_base_actionfilter_beforefilter, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_ActionFilter, afterFilter, arginfo_yii_base_actionfilter_afterfilter, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_ActionFilter, beforeAction, arginfo_yii_base_actionfilter_beforeaction, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_ActionFilter, afterAction, arginfo_yii_base_actionfilter_afteraction, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_ActionFilter, isActive, arginfo_yii_base_actionfilter_isactive, ZEND_ACC_PROTECTED)
  PHP_FE_END
};
