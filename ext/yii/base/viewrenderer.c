
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * ViewRenderer is the base class for view renderer classes.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_ViewRenderer) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, ViewRenderer, yii, base_viewrenderer, yii_base_component_ce, yii_base_viewrenderer_method_entry, ZEND_ACC_EXPLICIT_ABSTRACT_CLASS);

	return SUCCESS;

}

/**
 * Renders a view file.
 *
 * This method is invoked by [[View]] whenever it tries to render a view.
 * Child classes must implement this method to render the given view file.
 *
 * @param View $view the view object used for rendering the file.
 * @param string $file the view file.
 * @param array $params the parameters to be passed to the view file.
 * @return string the rendering result
 */
PHP_METHOD(yii_base_ViewRenderer, render) {

}

