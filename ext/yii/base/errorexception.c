
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/hash.h"
#include "kernel/array.h"
#include "kernel/operators.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * ErrorException represents a PHP error.
 *
 * @author Alexander Makarov <sam@rmcreative.ru>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_ErrorException) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, ErrorException, yii, base_errorexception, zend_get_error_exception(TSRMLS_C), yii_base_errorexception_method_entry, 0);

	return SUCCESS;

}

/**
 * Constructs the exception.
 * @link http://php.net/manual/en/errorexception.construct.php
 * @param $message [optional]
 * @param $code [optional]
 * @param $severity [optional]
 * @param $filename [optional]
 * @param $lineno [optional]
 * @param $previous [optional]
 */
PHP_METHOD(yii_base_ErrorException, __construct) {

	zend_class_entry *_14;
	zend_bool _10, _13;
	HashTable *_7;
	HashPosition _6;
	zephir_nts_static zephir_fcall_cache_entry *_2 = NULL, *_5 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *message = NULL, *code = NULL, *severity = NULL, *filename = NULL, *lineno = NULL, *previous = NULL, *trace = NULL, *_0 = NULL, *_1 = NULL, _3, _4, *key = NULL, *frame = NULL, *temp_trace = NULL, *trace_key = NULL, **_8, *_9 = NULL, *_11, *_12, *temp_args = NULL, *ref, *_15;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 6, &message, &code, &severity, &filename, &lineno, &previous);

	if (!message) {
		ZEPHIR_INIT_VAR(message);
		ZVAL_STRING(message, "", 1);
	}
	if (!code) {
		ZEPHIR_INIT_VAR(code);
		ZVAL_LONG(code, 0);
	}
	if (!severity) {
		ZEPHIR_INIT_VAR(severity);
		ZVAL_LONG(severity, 1);
	}
	if (!filename) {
		ZEPHIR_CPY_WRT(filename, ZEPHIR_GLOBAL(global_null));
	} else {
		ZEPHIR_SEPARATE_PARAM(filename);
	}
	if (!lineno) {
		ZEPHIR_CPY_WRT(lineno, ZEPHIR_GLOBAL(global_null));
	} else {
		ZEPHIR_SEPARATE_PARAM(lineno);
	}
	if (!previous) {
		previous = ZEPHIR_GLOBAL(global_null);
	}


	if (Z_TYPE_P(filename) == IS_NULL) {
		ZEPHIR_INIT_NVAR(filename);
		ZVAL_NULL(filename);
	}
	if (Z_TYPE_P(lineno) == IS_NULL) {
		ZEPHIR_INIT_NVAR(lineno);
		ZVAL_NULL(lineno);
	}
	ZEPHIR_CALL_PARENT(NULL, yii_base_errorexception_ce, this_ptr, "__construct", NULL, message, code, severity, filename, lineno, previous);
	zephir_check_call_status();
	if ((zephir_function_exists_ex(SS("xdebug_get_function_stack") TSRMLS_CC) == SUCCESS)) {
		ZEPHIR_CALL_FUNCTION(&_0, "xdebug_get_function_stack", NULL);
		zephir_check_call_status();
		ZEPHIR_CALL_FUNCTION(&_1, "array_reverse", &_2, _0);
		zephir_check_call_status();
		ZEPHIR_SINIT_VAR(_3);
		ZVAL_LONG(&_3, 3);
		ZEPHIR_SINIT_VAR(_4);
		ZVAL_LONG(&_4, -1);
		ZEPHIR_CALL_FUNCTION(&trace, "array_slice", &_5, _1, &_3, &_4);
		zephir_check_call_status();
		ZEPHIR_CPY_WRT(temp_trace, trace);
		zephir_is_iterable(temp_trace, &_7, &_6, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_7, (void**) &_8, &_6) == SUCCESS
		  ; zephir_hash_move_forward_ex(_7, &_6)
		) {
			ZEPHIR_GET_HMKEY(key, _7, _6);
			ZEPHIR_GET_HVALUE(frame, _8);
			if (!(zephir_array_isset(trace, key))) {
				ZEPHIR_INIT_NVAR(_9);
				array_init(_9);
				zephir_array_update_zval(&trace, key, &_9, PH_COPY | PH_SEPARATE);
			}
			ZEPHIR_OBS_NVAR(trace_key);
			zephir_array_fetch(&trace_key, trace, key, PH_NOISY TSRMLS_CC);
			if (!(zephir_array_isset_string(frame, SS("function")))) {
				ZEPHIR_INIT_NVAR(_9);
				ZVAL_STRING(_9, "unknown", 1);
				zephir_array_update_string(&trace_key, SL("funciton"), &_9, PH_COPY | PH_SEPARATE);
			}
			_10 = !zephir_array_isset_string(frame, SS("type"));
			if (!(_10)) {
				zephir_array_fetch_string(&_11, frame, SL("type"), PH_NOISY | PH_READONLY TSRMLS_CC);
				_10 = ZEPHIR_IS_STRING(_11, "static");
			}
			if (_10) {
				ZEPHIR_INIT_NVAR(_9);
				ZVAL_STRING(_9, "::", 1);
				zephir_array_update_string(&trace_key, SL("type"), &_9, PH_COPY | PH_SEPARATE);
			} else {
				zephir_array_fetch_string(&_12, frame, SL("type"), PH_NOISY | PH_READONLY TSRMLS_CC);
				if (ZEPHIR_IS_STRING(_12, "dynamic")) {
					ZEPHIR_INIT_NVAR(_9);
					ZVAL_STRING(_9, "->", 1);
					zephir_array_update_string(&trace_key, SL("type"), &_9, PH_COPY | PH_SEPARATE);
				}
			}
			_13 = zephir_array_isset_string(frame, SS("params"));
			if (_13) {
				_13 = !zephir_array_isset_string(frame, SS("args"));
			}
			if (_13) {
				ZEPHIR_OBS_NVAR(temp_args);
				zephir_array_fetch_string(&temp_args, frame, SL("params"), PH_NOISY TSRMLS_CC);
				zephir_array_update_string(&trace_key, SL("args"), &temp_args, PH_COPY | PH_SEPARATE);
			}
			zephir_array_update_zval(&trace, key, &trace_key, PH_COPY | PH_SEPARATE);
		}
		ZEPHIR_INIT_VAR(ref);
		_14 = zend_fetch_class(SL("ReflectionProperty"), ZEND_FETCH_CLASS_AUTO TSRMLS_CC);
		object_init_ex(ref, _14);
		ZEPHIR_INIT_NVAR(_9);
		ZVAL_STRING(_9, "Exception", 0);
		ZEPHIR_INIT_VAR(_15);
		ZVAL_STRING(_15, "trace", 0);
		ZEPHIR_CALL_METHOD(NULL, ref, "__construct", NULL, _9, _15);
		zephir_check_temp_parameter(_9);
		zephir_check_temp_parameter(_15);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, ref, "setaccessible", NULL, ZEPHIR_GLOBAL(global_true));
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, ref, "setvalue", NULL, this_ptr, trace);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns if error is one of fatal type.
 *
 * @param array $error error got from error_get_last()
 * @return boolean if error is one of fatal type
 */
PHP_METHOD(yii_base_ErrorException, isFatalError) {

	zend_bool _1;
	zval *error, *errors, *_0, *_2;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &error);



	ZEPHIR_INIT_VAR(errors);
	array_init_size(errors, 11);
	ZEPHIR_INIT_VAR(_0);
	ZVAL_LONG(_0, 1);
	zephir_array_fast_append(errors, _0);
	ZEPHIR_INIT_BNVAR(_0);
	ZVAL_LONG(_0, 4);
	zephir_array_fast_append(errors, _0);
	ZEPHIR_INIT_BNVAR(_0);
	ZVAL_LONG(_0, 16);
	zephir_array_fast_append(errors, _0);
	ZEPHIR_INIT_BNVAR(_0);
	ZVAL_LONG(_0, 32);
	zephir_array_fast_append(errors, _0);
	ZEPHIR_INIT_BNVAR(_0);
	ZVAL_LONG(_0, 64);
	zephir_array_fast_append(errors, _0);
	ZEPHIR_INIT_BNVAR(_0);
	ZVAL_LONG(_0, 128);
	zephir_array_fast_append(errors, _0);
	_1 = zephir_array_isset_string(error, SS("type"));
	if (_1) {
		zephir_array_fetch_string(&_2, error, SL("type"), PH_NOISY | PH_READONLY TSRMLS_CC);
		_1 = zephir_fast_in_array(_2, errors TSRMLS_CC);
	}
	RETURN_MM_BOOL(_1);

}

/**
 * @return string the user-friendly name of this exception
 */
PHP_METHOD(yii_base_ErrorException, getName) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *names, *_0, *_1, *_2, *_3, *_4, *_5, *_6, *_7, *_8, *_9, *_10, *_11, *_12, *code = NULL, *_13;

	ZEPHIR_MM_GROW();
	ZEPHIR_INIT_VAR(names);
	array_init(names);

	ZEPHIR_INIT_VAR(_0);
	ZVAL_STRING(_0, "PHP Fatal Error", 1);
	zephir_array_update_long(&names, 1, &_0, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 105);
	ZEPHIR_INIT_VAR(_1);
	ZVAL_STRING(_1, "PHP Parse Error", 1);
	zephir_array_update_long(&names, 4, &_1, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 106);
	ZEPHIR_INIT_VAR(_2);
	ZVAL_STRING(_2, "PHP Core Error", 1);
	zephir_array_update_long(&names, 16, &_2, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 107);
	ZEPHIR_INIT_VAR(_3);
	ZVAL_STRING(_3, "PHP Compile Error", 1);
	zephir_array_update_long(&names, 64, &_3, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 108);
	ZEPHIR_INIT_VAR(_4);
	ZVAL_STRING(_4, "PHP User Error", 1);
	zephir_array_update_long(&names, 256, &_4, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 109);
	ZEPHIR_INIT_VAR(_5);
	ZVAL_STRING(_5, "PHP Warning", 1);
	zephir_array_update_long(&names, 2, &_5, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 110);
	ZEPHIR_INIT_VAR(_6);
	ZVAL_STRING(_6, "PHP Core Warning", 1);
	zephir_array_update_long(&names, 32, &_6, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 111);
	ZEPHIR_INIT_VAR(_7);
	ZVAL_STRING(_7, "PHP Compile Warning", 1);
	zephir_array_update_long(&names, 128, &_7, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 112);
	ZEPHIR_INIT_VAR(_8);
	ZVAL_STRING(_8, "PHP User Warning", 1);
	zephir_array_update_long(&names, 512, &_8, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 113);
	ZEPHIR_INIT_VAR(_9);
	ZVAL_STRING(_9, "PHP Strict Warning", 1);
	zephir_array_update_long(&names, 2048, &_9, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 114);
	ZEPHIR_INIT_VAR(_10);
	ZVAL_STRING(_10, "PHP Notice", 1);
	zephir_array_update_long(&names, 8, &_10, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 115);
	ZEPHIR_INIT_VAR(_11);
	ZVAL_STRING(_11, "PHP Recoverable Error", 1);
	zephir_array_update_long(&names, 4096, &_11, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 116);
	ZEPHIR_INIT_VAR(_12);
	ZVAL_STRING(_12, "PHP Deprecated Warning", 1);
	zephir_array_update_long(&names, 8192, &_12, PH_COPY | PH_SEPARATE, "yii/base/ErrorException.zep", 117);
	ZEPHIR_CALL_METHOD(&code, this_ptr, "getcode",  NULL);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_13);
	if (zephir_array_isset(names, code)) {
		zephir_array_fetch(&_13, names, code, PH_NOISY TSRMLS_CC);
	} else {
		ZVAL_STRING(_13, "Error", 1);
	}
	RETURN_CCTOR(_13);

}

