
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/memory.h"
#include "kernel/concat.h"
#include "kernel/array.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * InlineAction represents an action that is defined as a controller method.
 *
 * The name of the controller method is available via [[actionMethod]] which
 * is set by the [[controller]] who creates this action.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_InlineAction) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, InlineAction, yii, base_inlineaction, yii_base_action_ce, yii_base_inlineaction_method_entry, 0);

	/**
	 * @var string the controller method that this inline action is associated with
	 */
	zend_declare_property_null(yii_base_inlineaction_ce, SL("actionMethod"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * @param string $id the ID of this action
 * @param Controller $controller the controller that owns this action
 * @param string $actionMethod the controller method that this inline action is associated with
 * @param array $config name-value pairs that will be used to initialize the object properties
 */
PHP_METHOD(yii_base_InlineAction, __construct) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;
	zval *id_param = NULL, *controller, *actionMethod_param = NULL, *config = NULL;
	zval *id = NULL, *actionMethod = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 3, 1, &id_param, &controller, &actionMethod_param, &config);

	zephir_get_strval(id, id_param);
	zephir_get_strval(actionMethod, actionMethod_param);
	if (!config) {
		ZEPHIR_INIT_VAR(config);
		array_init(config);
	}


	zephir_update_property_this(this_ptr, SL("actionMethod"), actionMethod TSRMLS_CC);
	ZEPHIR_CALL_PARENT(NULL, yii_base_inlineaction_ce, this_ptr, "__construct", &_0, id, controller, config);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Runs this action with the specified parameters.
 * This method is mainly invoked by the controller.
 * @param array $params action parameters
 * @return mixed the result of the action
 */
PHP_METHOD(yii_base_InlineAction, runWithParams) {

	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *params, *args = NULL, *app = NULL, *requestedParams = NULL, *_0, *_2, *_3, *_4, *_5, *_6, *_7, *_8, *elements, *controller = NULL, *actionMethod = NULL, *temp_args = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &params);

	ZEPHIR_INIT_VAR(elements);
	array_init(elements);


	_0 = zephir_fetch_nproperty_this(this_ptr, SL("controller"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&args, _0, "bindactionparams", NULL, this_ptr, params);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_2);
	_3 = zephir_fetch_nproperty_this(this_ptr, SL("controller"), PH_NOISY_CC);
	zephir_get_class(_2, _3, 0 TSRMLS_CC);
	_4 = zephir_fetch_nproperty_this(this_ptr, SL("actionMethod"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(_5);
	ZEPHIR_CONCAT_SVSVS(_5, "Running action: ", _2, "::", _4, "()");
	ZEPHIR_INIT_VAR(_6);
	ZVAL_STRING(_6, "InlineAction:runWithParams", 0);
	ZEPHIR_CALL_CE_STATIC(NULL, yii_baseyii_ce, "trace", &_1, _5, _6);
	zephir_check_temp_parameter(_6);
	zephir_check_call_status();
	zephir_read_static_property_ce(&_7, yii_baseyii_ce, SL("app") TSRMLS_CC);
	ZEPHIR_CPY_WRT(app, _7);
	ZEPHIR_OBS_VAR(_8);
	zephir_read_property(&_8, app, SL("requestedParams"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(requestedParams, _8);
	if (Z_TYPE_P(requestedParams) == IS_NULL) {
		zephir_update_property_zval(app, SL("requestedParams"), requestedParams TSRMLS_CC);
		zephir_update_static_property_ce(yii_baseyii_ce, SL("app"), app TSRMLS_CC);
	}
	_7 = zephir_fetch_nproperty_this(this_ptr, SL("controller"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(controller, _7);
	_7 = zephir_fetch_nproperty_this(this_ptr, SL("actionMethod"), PH_NOISY_CC);
	ZEPHIR_CPY_WRT(actionMethod, _7);
	ZEPHIR_CPY_WRT(temp_args, args);
	zephir_array_append(&elements, controller, PH_SEPARATE);
	zephir_array_append(&elements, actionMethod, PH_SEPARATE);
	ZEPHIR_CALL_USER_FUNC_ARRAY(return_value, elements, temp_args);
	zephir_check_call_status();
	RETURN_MM();

}

