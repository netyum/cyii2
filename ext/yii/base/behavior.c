
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/hash.h"
#include "kernel/array.h"
#include "kernel/operators.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Behavior is the base class for all behavior classes.
 *
 * A behavior can be used to enhance the functionality of an existing component without modifying its code.
 * In particular, it can "inject" its own methods and properties into the component
 * and make them directly accessible via the component. It can also respond to the events triggered in the component
 * and thus intercept the normal code execution.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_Behavior) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, Behavior, yii, base_behavior, yii_base_object_ce, yii_base_behavior_method_entry, 0);

	/**
	 * @var Component the owner of this behavior
	 */
	zend_declare_property_null(yii_base_behavior_ce, SL("owner"), ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

/**
 * Declares event handlers for the [[owner]]'s events.
 *
 * Child classes may override this method to declare what PHP callbacks should
 * be attached to the events of the [[owner]] component.
 *
 * The callbacks will be attached to the [[owner]]'s events when the behavior is
 * attached to the owner; and they will be detached from the events when
 * the behavior is detached from the component.
 *
 * The callbacks can be any of the followings:
 *
 * - method in this behavior: `'handleClick'`, equivalent to `[$this, 'handleClick']`
 * - object method: `[$object, 'handleClick']`
 * - static method: `['Page', 'handleClick']`
 * - anonymous function: `function ($event) { ... }`
 *
 * The following is an example:
 *
 * ~~~
 * [
 *	 Model::EVENT_BEFORE_VALIDATE => 'myBeforeValidate',
 *	 Model::EVENT_AFTER_VALIDATE => 'myAfterValidate',
 * ]
 * ~~~
 *
 * @return array events (array keys) and the corresponding event handler methods (array values).
 */
PHP_METHOD(yii_base_Behavior, events) {


	array_init(return_value);
	return;

}

/**
 * Attaches the behavior object to the component.
 * The default implementation will set the [[owner]] property
 * and attach event handlers as declared in [[events]].
 * Make sure you call the parent implementation if you override this method.
 * @param Component $owner the component that this behavior is to be attached to.
 */
PHP_METHOD(yii_base_Behavior, attach) {

	HashTable *_2;
	HashPosition _1;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *owner, *event = NULL, *handler = NULL, *_0 = NULL, **_3, *_4 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &owner);



	zephir_update_property_this(this_ptr, SL("owner"), owner TSRMLS_CC);
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "events",  NULL);
	zephir_check_call_status();
	zephir_is_iterable(_0, &_2, &_1, 0, 0);
	for (
	  ; zephir_hash_get_current_data_ex(_2, (void**) &_3, &_1) == SUCCESS
	  ; zephir_hash_move_forward_ex(_2, &_1)
	) {
		ZEPHIR_GET_HMKEY(event, _2, _1);
		ZEPHIR_GET_HVALUE(handler, _3);
		ZEPHIR_INIT_LNVAR(_4);
		if (Z_TYPE_P(handler) == IS_STRING) {
			ZEPHIR_INIT_NVAR(_4);
			array_init_size(_4, 3);
			zephir_array_fast_append(_4, this_ptr);
			zephir_array_fast_append(_4, handler);
		} else {
			ZEPHIR_CPY_WRT(_4, handler);
		}
		ZEPHIR_CALL_METHOD(NULL, owner, "on", NULL, event, _4);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Detaches the behavior object from the component.
 * The default implementation will unset the [[owner]] property
 * and detach event handlers declared in [[events]].
 * Make sure you call the parent implementation if you override this method.
 */
PHP_METHOD(yii_base_Behavior, detach) {

	HashTable *_3;
	HashPosition _2;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *_0, *event = NULL, *handler = NULL, *_1 = NULL, **_4, *_5 = NULL;

	ZEPHIR_MM_GROW();

	_0 = zephir_fetch_nproperty_this(this_ptr, SL("owner"), PH_NOISY_CC);
	if (zephir_is_true(_0)) {
		ZEPHIR_CALL_METHOD(&_1, this_ptr, "events",  NULL);
		zephir_check_call_status();
		zephir_is_iterable(_1, &_3, &_2, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_3, (void**) &_4, &_2) == SUCCESS
		  ; zephir_hash_move_forward_ex(_3, &_2)
		) {
			ZEPHIR_GET_HMKEY(event, _3, _2);
			ZEPHIR_GET_HVALUE(handler, _4);
			_0 = zephir_fetch_nproperty_this(this_ptr, SL("owner"), PH_NOISY_CC);
			ZEPHIR_INIT_LNVAR(_5);
			if (Z_TYPE_P(handler) == IS_STRING) {
				ZEPHIR_INIT_NVAR(_5);
				array_init_size(_5, 3);
				zephir_array_fast_append(_5, this_ptr);
				zephir_array_fast_append(_5, handler);
			} else {
				ZEPHIR_CPY_WRT(_5, handler);
			}
			ZEPHIR_CALL_METHOD(NULL, _0, "off", NULL, event, _5);
			zephir_check_call_status();
		}
		zephir_update_property_this(this_ptr, SL("owner"), ZEPHIR_GLOBAL(global_null) TSRMLS_CC);
	}
	ZEPHIR_MM_RESTORE();

}

