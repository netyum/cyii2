
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * ActionEvent represents the event parameter used for an action event.
 *
 * By setting the [[isValid]] property, one may control whether to continue running the action.
 *
 * @author Mark Jebri <mark.github@yandex.ru>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_base_MailEvent) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\base, MailEvent, yii, base_mailevent, yii_base_event_ce, NULL, 0);

	/**
	 * @var \yii\mail\MessageInterface mail message being send
	 */
	zend_declare_property_null(yii_base_mailevent_ce, SL("message"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var boolean if message send was successful
	 */
	zend_declare_property_null(yii_base_mailevent_ce, SL("isSuccessful"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var boolean whether to continue sending an email. Event handlers of
	 * [[\yii\mail\BaseMailer::EVENT_BEFORE_SEND]] may set this property to decide whether
	 * to continue send or not.
	 */
	zend_declare_property_bool(yii_base_mailevent_ce, SL("isValid"), 1, ZEND_ACC_PUBLIC TSRMLS_CC);

	return SUCCESS;

}

