
extern zend_class_entry *yii_console_application_ce;

ZEPHIR_INIT_CLASS(yii_console_Application);

PHP_METHOD(yii_console_Application, __construct);
PHP_METHOD(yii_console_Application, loadConfig);
PHP_METHOD(yii_console_Application, init);
PHP_METHOD(yii_console_Application, handleRequest);
PHP_METHOD(yii_console_Application, runAction);
PHP_METHOD(yii_console_Application, coreCommands);
PHP_METHOD(yii_console_Application, coreComponents);
PHP_METHOD(yii_console_Application, registerErrorHandler);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_console_application___construct, 0, 0, 0)
	ZEND_ARG_INFO(0, config)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_console_application_loadconfig, 0, 0, 1)
	ZEND_ARG_INFO(0, config)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_console_application_handlerequest, 0, 0, 1)
	ZEND_ARG_INFO(0, request)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_console_application_runaction, 0, 0, 1)
	ZEND_ARG_INFO(0, route)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_console_application_registererrorhandler, 0, 0, 1)
	ZEND_ARG_INFO(0, config)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_console_application_method_entry) {
	PHP_ME(yii_console_Application, __construct, arginfo_yii_console_application___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(yii_console_Application, loadConfig, arginfo_yii_console_application_loadconfig, ZEND_ACC_PROTECTED)
	PHP_ME(yii_console_Application, init, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_console_Application, handleRequest, arginfo_yii_console_application_handlerequest, ZEND_ACC_PUBLIC)
	PHP_ME(yii_console_Application, runAction, arginfo_yii_console_application_runaction, ZEND_ACC_PUBLIC)
	PHP_ME(yii_console_Application, coreCommands, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_console_Application, coreComponents, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_console_Application, registerErrorHandler, arginfo_yii_console_application_registererrorhandler, ZEND_ACC_PROTECTED)
  PHP_FE_END
};
