
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/array.h"
#include "kernel/concat.h"
#include "kernel/hash.h"
#include "kernel/string.h"
#include "kernel/operators.h"
#include "kernel/require.h"
#include "kernel/exit.h"
#include "kernel/object.h"
#include "kernel/exception.h"


/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
/**
 * Application represents a console application.
 *
 * Application extends from [[\yii\base\Application]] by providing functionalities that are
 * specific to console requests. In particular, it deals with console requests
 * through a command-based approach:
 *
 * - A console application consists of one or several possible user commands;
 * - Each user command is implemented as a class extending [[\yii\console\Controller]];
 * - User specifies which command to run on the command line;
 * - The command processes the user request with the specified parameters.
 *
 * The command classes should be under the namespace specified by [[controllerNamespace]].
 * Their naming should follow the same naming convention as controllers. For example, the `help` command
 * is implemented using the `HelpController` class.
 *
 * To run the console application, enter the following on the command line:
 *
 * ~~~
 * yii <route> [--param1=value1 --param2 ...]
 * ~~~
 *
 * where `<route>` refers to a controller route in the form of `ModuleID/ControllerID/ActionID`
 * (e.g. `sitemap/create`), and `param1`, `param2` refers to a set of named parameters that
 * will be used to initialize the controller action (e.g. `--since=0` specifies a `since` parameter
 * whose value is 0 and a corresponding `$since` parameter is passed to the action method).
 *
 * A `help` command is provided by default, which lists available commands and shows their usage.
 * To use this command, simply type:
 *
 * ~~~
 * yii help
 * ~~~
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
ZEPHIR_INIT_CLASS(yii_console_Application) {

	ZEPHIR_REGISTER_CLASS_EX(yii\\console, Application, yii, console_application, yii_base_application_ce, yii_console_application_method_entry, 0);

	/**
	 * @var string the default route of this application. Defaults to 'help',
	 * meaning the `help` command.
	 */
	zend_declare_property_string(yii_console_application_ce, SL("defaultRoute"), "help", ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var boolean whether to enable the commands provided by the core framework.
	 * Defaults to true.
	 */
	zend_declare_property_bool(yii_console_application_ce, SL("enableCoreCommands"), 1, ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * @var Controller the currently active controller instance
	 */
	zend_declare_property_null(yii_console_application_ce, SL("controller"), ZEND_ACC_PUBLIC TSRMLS_CC);

	/**
	 * The option name for specifying the application configuration file path.
	 */
	zend_declare_class_constant_string(yii_console_application_ce, SL("OPTION_APPCONFIG"), "appconfig" TSRMLS_CC);

	return SUCCESS;

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_console_Application, __construct) {

	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	int ZEPHIR_LAST_CALL_STATUS;
	zval *config = NULL, *_0 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &config);

	if (!config) {
		ZEPHIR_INIT_VAR(config);
		array_init(config);
	} else {
		ZEPHIR_SEPARATE_PARAM(config);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "loadconfig", NULL, config);
	zephir_check_call_status();
	ZEPHIR_CPY_WRT(config, _0);
	ZEPHIR_CALL_PARENT(NULL, yii_console_application_ce, this_ptr, "__construct", &_1, config);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

/**
 * Loads the configuration.
 * This method will check if the command line option [[OPTION_APPCONFIG]] is specified.
 * If so, the corresponding file will be loaded as the application configuration.
 * Otherwise, the configuration provided as the parameter will be returned back.
 * @param array $config the configuration provided in the constructor.
 * @return array the actual configuration to be used by the application.
 */
PHP_METHOD(yii_console_Application, loadConfig) {

	zephir_fcall_cache_entry *_8 = NULL;
	zend_bool _7;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_6 = NULL, *_10 = NULL;
	HashTable *_3;
	HashPosition _2;
	zval *_0;
	zval *config, *option = NULL, *param = NULL, *pos = NULL, *path = NULL, *file = NULL, *_SERVER, *_1, **_4, _5 = zval_used_for_init, *_9 = NULL, *_11 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &config);



	zephir_get_global(&_SERVER, SS("_SERVER") TSRMLS_CC);
	if (!(zephir_array_isset_string(_SERVER, SS("argv")))) {
		ZEPHIR_INIT_VAR(_0);
		ZEPHIR_CONCAT_SSS(_0, "--", "appconfig", "=");
		ZEPHIR_CPY_WRT(option, _0);
		zephir_array_fetch_string(&_1, _SERVER, SL("argv"), PH_NOISY | PH_READONLY TSRMLS_CC);
		zephir_is_iterable(_1, &_3, &_2, 0, 0);
		for (
		  ; zephir_hash_get_current_data_ex(_3, (void**) &_4, &_2) == SUCCESS
		  ; zephir_hash_move_forward_ex(_3, &_2)
		) {
			ZEPHIR_GET_HVALUE(param, _4);
			ZEPHIR_INIT_NVAR(pos);
			zephir_fast_strpos(pos, param, option, 0 );
			if (Z_TYPE_P(pos) != IS_BOOL) {
				ZEPHIR_SINIT_NVAR(_5);
				ZVAL_LONG(&_5, zephir_fast_strlen_ev(option));
				ZEPHIR_CALL_FUNCTION(&path, "substr", &_6, param, &_5);
				zephir_check_call_status();
				_7 = Z_TYPE_P(path) == IS_STRING;
				if (_7) {
					_7 = !ZEPHIR_IS_STRING(path, "");
				}
				if (_7) {
					ZEPHIR_CALL_CE_STATIC(&file, yii_baseyii_ce, "getalias", &_8, path);
					zephir_check_call_status();
					ZEPHIR_CALL_FUNCTION(&_9, "is_file", &_10, file);
					zephir_check_call_status();
					if (zephir_is_true(_9)) {
						ZEPHIR_OBSERVE_OR_NULLIFY_PPZV(&_9);
						if (zephir_require_zval_ret(&_9, file TSRMLS_CC) == FAILURE) {
							RETURN_MM_NULL();
						}
						RETURN_CCTOR(_9);
					}
				} else {
					ZEPHIR_INIT_LNVAR(_11);
					ZEPHIR_CONCAT_SVS(_11, "The configuration file does not exist: ", path, "\n");
					zephir_exit(_11);
					ZEPHIR_MM_RESTORE();
				}
			}
		}
	}
	RETURN_CCTOR(config);

}

/**
 * Initialize the application.
 */
PHP_METHOD(yii_console_Application, init) {
        HashTable *_3;
        HashPosition _2;
        zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
        int ZEPHIR_LAST_CALL_STATUS;
        zval *coreCommands = NULL, *id = NULL, *command = NULL, *controllerMap = NULL, *_0, **_4, *_5;

        ZEPHIR_MM_GROW();

        _0 = zephir_fetch_nproperty_this(this_ptr, SL("controllerMap"), PH_NOISY_CC);
        ZEPHIR_CPY_WRT(controllerMap, _0);
        if (Z_TYPE_P(controllerMap) == IS_NULL) {
                ZEPHIR_INIT_VAR(controllerMap);
                array_init(controllerMap);
        }
        ZEPHIR_CALL_METHOD(&coreCommands, this_ptr, "corecommands",  NULL);
        zephir_check_call_status();
        ZEPHIR_CALL_PARENT(NULL, yii_console_application_ce, this_ptr, "init", &_1);
        zephir_check_call_status();
        _0 = zephir_fetch_nproperty_this(this_ptr, SL("enableCoreCommands"), PH_NOISY_CC);
        if (zephir_is_true(_0)) {
                zephir_is_iterable(coreCommands, &_3, &_2, 0, 0);
                for (
                  ; zephir_hash_get_current_data_ex(_3, (void**) &_4, &_2) == SUCCESS
                  ; zephir_hash_move_forward_ex(_3, &_2)
                ) {
                        ZEPHIR_GET_HMKEY(id, _3, _2);
                        ZEPHIR_GET_HVALUE(command, _4);
                        if (!(zephir_array_isset(controllerMap, id))) {
                                zephir_array_update_zval(&controllerMap, id, &command, PH_COPY | PH_SEPARATE);
                        }
                }
        }
        if (!(zephir_array_isset_string(controllerMap, SS("help")))) {
                ZEPHIR_INIT_VAR(_5);
                ZVAL_STRING(_5, "yii\\console\\controllers\\HelpController", 1);
                zephir_array_update_string(&controllerMap, SL("help"), &_5, PH_COPY | PH_SEPARATE);
        }
        zephir_update_property_this(this_ptr, SL("controllerMap"), controllerMap TSRMLS_CC);
        ZEPHIR_MM_RESTORE();
}

/**
 * Handles the specified request.
 * @param Request $request the request to be handled
 * @return Response the resulting response
 */
PHP_METHOD(yii_console_Application, handleRequest) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *request, *requests = NULL, *response = NULL, *route, *params, *result = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &request);



	ZEPHIR_CALL_METHOD(&requests, request, "resolve",  NULL);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(route);
	zephir_array_fetch_long(&route, requests, 0, PH_NOISY TSRMLS_CC);
	ZEPHIR_OBS_VAR(params);
	zephir_array_fetch_long(&params, requests, 1, PH_NOISY TSRMLS_CC);
	zephir_update_property_this(this_ptr, SL("requestedRoute"), route TSRMLS_CC);
	ZEPHIR_CALL_METHOD(&result, this_ptr, "runaction", NULL, route, params);
	zephir_check_call_status();
	if (zephir_is_instance_of(result, SL("yii\\console\\Response") TSRMLS_CC)) {
		RETURN_CCTOR(result);
	} else {
		ZEPHIR_CALL_METHOD(&response, this_ptr, "getresponse",  NULL);
		zephir_check_call_status();
		zephir_update_property_zval(response, SL("exitStatus"), result TSRMLS_CC);
		RETURN_CCTOR(response);
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Runs a controller action specified by a route.
 * This method parses the specified route and creates the corresponding child module(s), controller and action
 * instances. It then calls [[Controller::runAction()]] to run the action with the given parameters.
 * If the route is empty, the method will use [[defaultRoute]].
 * @param string $route the route that specifies the action.
 * @param array $params the parameters to be passed to the action
 * @return integer the status code returned by the action execution. 0 means normal, and other values mean abnormal.
 * @throws Exception if the route is invalid
 */
PHP_METHOD(yii_console_Application, runAction) {

	zend_class_entry *_2;
	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_0 = NULL, *_4 = NULL;
	zval *route, *params = NULL, *e = NULL, *elements, *_1, *_3 = NULL, *_5 = NULL, *_6;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &route, &params);

	if (!params) {
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);



	/* try_start_1: */

		ZEPHIR_RETURN_CALL_PARENT(yii_console_application_ce, this_ptr, "runaction", &_0, route, params);
		zephir_check_call_status_or_jump(try_end_1);
		RETURN_MM();

	try_end_1:

	ZEPHIR_CPY_WRT(e, EG(exception));
	if (zephir_is_instance_of(e, SL("InvalidRouteException") TSRMLS_CC)) {
		zend_clear_exception(TSRMLS_C);
		zephir_array_update_string(&elements, SL("command"), &route, PH_COPY | PH_SEPARATE);
		ZEPHIR_INIT_VAR(_1);
		_2 = zend_fetch_class(SL("yii\\console\\Exception"), ZEND_FETCH_CLASS_AUTO TSRMLS_CC);
		object_init_ex(_1, _2);
		if (zephir_has_constructor(_1 TSRMLS_CC)) {
			ZEPHIR_INIT_VAR(_5);
			ZVAL_STRING(_5, "yii", 0);
			ZEPHIR_INIT_VAR(_6);
			ZVAL_STRING(_6, "Unknown command \"{command}\".", 0);
			ZEPHIR_CALL_CE_STATIC(&_3, yii_baseyii_ce, "t", &_4, _5, _6, elements);
			zephir_check_temp_parameter(_5);
			zephir_check_temp_parameter(_6);
			zephir_check_call_status();
			ZEPHIR_INIT_NVAR(_5);
			ZVAL_LONG(_5, 0);
			ZEPHIR_CALL_METHOD(NULL, _1, "__construct", NULL, _3, _5, e);
			zephir_check_call_status();
		}
		zephir_throw_exception_debug(_1, "yii/console/Application.zep", 184 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_MM_RESTORE();

}

/**
 * Returns the configuration of the built-in commands.
 * @return array the configuration of the built-in commands.
 */
PHP_METHOD(yii_console_Application, coreCommands) {

	zval *elements, *_0, *_1, *_2, *_3, *_4, *_5;

	ZEPHIR_MM_GROW();
	ZEPHIR_INIT_VAR(elements);
	array_init(elements);

	ZEPHIR_INIT_VAR(_0);
	ZVAL_STRING(_0, "yii\\console\\controllers\\MessageController", 1);
	zephir_array_update_string(&elements, SL("message"), &_0, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_VAR(_1);
	ZVAL_STRING(_1, "yii\\console\\controllers\\HelpController", 1);
	zephir_array_update_string(&elements, SL("help"), &_1, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_VAR(_2);
	ZVAL_STRING(_2, "yii\\console\\controllers\\MigrateController", 1);
	zephir_array_update_string(&elements, SL("migrate"), &_2, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_VAR(_3);
	ZVAL_STRING(_3, "yii\\console\\controllers\\CacheController", 1);
	zephir_array_update_string(&elements, SL("cache"), &_3, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_VAR(_4);
	ZVAL_STRING(_4, "yii\\console\\controllers\\AssetController", 1);
	zephir_array_update_string(&elements, SL("asset"), &_4, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_VAR(_5);
	ZVAL_STRING(_5, "yii\\console\\controllers\\FixtureController", 1);
	zephir_array_update_string(&elements, SL("fixture"), &_5, PH_COPY | PH_SEPARATE);
	RETURN_CCTOR(elements);

}

/**
 * @inheritdoc
 */
PHP_METHOD(yii_console_Application, coreComponents) {
        zval *_1, *_2;
        int ZEPHIR_LAST_CALL_STATUS;
        zephir_nts_static zephir_fcall_cache_entry *_0 = NULL;
        zval *parent_elements = NULL, *elements, *coreComponents;

        ZEPHIR_MM_GROW();
        ZEPHIR_INIT_VAR(elements);
        array_init(elements);

        ZEPHIR_CALL_PARENT(&parent_elements, yii_console_application_ce, this_ptr, "corecomponents", &_0);
        zephir_check_call_status();
        ZEPHIR_INIT_VAR(_1);
        array_init_size(_1, 2);
        add_assoc_stringl_ex(_1, SS("class"), SL("yii\\console\\Request"), 1);
        zephir_array_update_string(&elements, SL("request"), &_1, PH_COPY | PH_SEPARATE);
        ZEPHIR_INIT_VAR(_2);
        array_init_size(_2, 2);
        add_assoc_stringl_ex(_2, SS("class"), SL("yii\\console\\Response"), 1);
        zephir_array_update_string(&elements, SL("response"), &_2, PH_COPY | PH_SEPARATE);
        ZEPHIR_INIT_VAR(coreComponents);
        zephir_fast_array_merge(coreComponents, &(parent_elements), &(elements) TSRMLS_CC);
        RETURN_CCTOR(coreComponents);
}

/**
 * Registers the errorHandler component as a PHP error handler.
 */
PHP_METHOD(yii_console_Application, registerErrorHandler) {

	int ZEPHIR_LAST_CALL_STATUS;
	zephir_nts_static zephir_fcall_cache_entry *_1 = NULL;
	zval *config, *components, *errorHandler, *_0 = NULL;

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &config);

	Z_SET_ISREF_P(config);

	if (!(zephir_array_isset_string(config, SS("components")))) {
		ZEPHIR_INIT_VAR(_0);
		array_init(_0);
		zephir_array_update_string(&config, SL("components"), &_0, PH_COPY | PH_SEPARATE);
	}
	ZEPHIR_OBS_VAR(components);
	zephir_array_fetch_string(&components, config, SL("components"), PH_NOISY TSRMLS_CC);
	if (!(zephir_array_isset_string(components, SS("errorHandler")))) {
		ZEPHIR_INIT_NVAR(_0);
		array_init(_0);
		zephir_array_update_string(&components, SL("errorHandler"), &_0, PH_COPY | PH_SEPARATE);
	}
	ZEPHIR_OBS_VAR(errorHandler);
	zephir_array_fetch_string(&errorHandler, components, SL("errorHandler"), PH_NOISY TSRMLS_CC);
	if (!(zephir_array_isset_string(errorHandler, SS("class")))) {
		ZEPHIR_INIT_NVAR(_0);
		ZVAL_STRING(_0, "yii\\console\\ErrorHandler", 1);
		zephir_array_update_string(&errorHandler, SL("class"), &_0, PH_COPY | PH_SEPARATE);
		zephir_array_update_string(&components, SL("errorHandler"), &errorHandler, PH_COPY | PH_SEPARATE);
		zephir_array_update_string(&config, SL("components"), &components, PH_COPY | PH_SEPARATE);
	}
	ZEPHIR_CALL_PARENT(NULL, yii_console_application_ce, this_ptr, "registererrorhandler", &_1, config);
	zephir_check_call_status();
	Z_UNSET_ISREF_P(config);
	ZEPHIR_MM_RESTORE();

}

