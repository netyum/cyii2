
extern zend_class_entry *yii_validators_validator_ce;

ZEPHIR_INIT_CLASS(yii_validators_Validator);

PHP_METHOD(yii_validators_Validator, createValidator);
PHP_METHOD(yii_validators_Validator, init);
PHP_METHOD(yii_validators_Validator, validateAttributes);
PHP_METHOD(yii_validators_Validator, validateAttribute);
PHP_METHOD(yii_validators_Validator, validate);
PHP_METHOD(yii_validators_Validator, validateValue);
PHP_METHOD(yii_validators_Validator, clientValidateAttribute);
PHP_METHOD(yii_validators_Validator, isActive);
PHP_METHOD(yii_validators_Validator, addError);
PHP_METHOD(yii_validators_Validator, isEmpty);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_validators_validator_createvalidator, 0, 0, 3)
	ZEND_ARG_INFO(0, type)
	ZEND_ARG_INFO(0, object)
	ZEND_ARG_INFO(0, attributes)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_validators_validator_validateattributes, 0, 0, 1)
	ZEND_ARG_INFO(0, object)
	ZEND_ARG_INFO(0, attributes)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_validators_validator_validateattribute, 0, 0, 2)
	ZEND_ARG_INFO(0, object)
	ZEND_ARG_INFO(0, attribute)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_validators_validator_validate, 0, 0, 1)
	ZEND_ARG_INFO(0, value)
	ZEND_ARG_INFO(0, error)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_validators_validator_validatevalue, 0, 0, 1)
	ZEND_ARG_INFO(0, value)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_validators_validator_clientvalidateattribute, 0, 0, 3)
	ZEND_ARG_INFO(0, object)
	ZEND_ARG_INFO(0, attribute)
	ZEND_ARG_INFO(0, view)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_validators_validator_isactive, 0, 0, 1)
	ZEND_ARG_INFO(0, scenario)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_validators_validator_adderror, 0, 0, 3)
	ZEND_ARG_INFO(0, object)
	ZEND_ARG_INFO(0, attribute)
	ZEND_ARG_INFO(0, message)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_validators_validator_isempty, 0, 0, 1)
	ZEND_ARG_INFO(0, value)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_validators_validator_method_entry) {
	PHP_ME(yii_validators_Validator, createValidator, arginfo_yii_validators_validator_createvalidator, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_validators_Validator, init, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_validators_Validator, validateAttributes, arginfo_yii_validators_validator_validateattributes, ZEND_ACC_PUBLIC)
	PHP_ME(yii_validators_Validator, validateAttribute, arginfo_yii_validators_validator_validateattribute, ZEND_ACC_PUBLIC)
	PHP_ME(yii_validators_Validator, validate, arginfo_yii_validators_validator_validate, ZEND_ACC_PUBLIC)
	PHP_ME(yii_validators_Validator, validateValue, arginfo_yii_validators_validator_validatevalue, ZEND_ACC_PROTECTED)
	PHP_ME(yii_validators_Validator, clientValidateAttribute, arginfo_yii_validators_validator_clientvalidateattribute, ZEND_ACC_PUBLIC)
	PHP_ME(yii_validators_Validator, isActive, arginfo_yii_validators_validator_isactive, ZEND_ACC_PUBLIC)
	PHP_ME(yii_validators_Validator, addError, arginfo_yii_validators_validator_adderror, ZEND_ACC_PUBLIC)
	PHP_ME(yii_validators_Validator, isEmpty, arginfo_yii_validators_validator_isempty, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
