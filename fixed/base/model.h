
extern zend_class_entry *yii_base_model_ce;

ZEPHIR_INIT_CLASS(yii_base_Model);

PHP_METHOD(yii_base_Model, rules);
PHP_METHOD(yii_base_Model, scenarios);
PHP_METHOD(yii_base_Model, formName);
PHP_METHOD(yii_base_Model, attributes);
PHP_METHOD(yii_base_Model, attributeLabels);
PHP_METHOD(yii_base_Model, validate);
PHP_METHOD(yii_base_Model, beforeValidate);
PHP_METHOD(yii_base_Model, afterValidate);
PHP_METHOD(yii_base_Model, getValidators);
PHP_METHOD(yii_base_Model, getActiveValidators);
PHP_METHOD(yii_base_Model, createValidators);
PHP_METHOD(yii_base_Model, isAttributeRequired);
PHP_METHOD(yii_base_Model, isAttributeSafe);
PHP_METHOD(yii_base_Model, isAttributeActive);
PHP_METHOD(yii_base_Model, getAttributeLabel);
PHP_METHOD(yii_base_Model, hasErrors);
PHP_METHOD(yii_base_Model, getErrors);
PHP_METHOD(yii_base_Model, getFirstErrors);
PHP_METHOD(yii_base_Model, getFirstError);
PHP_METHOD(yii_base_Model, addError);
PHP_METHOD(yii_base_Model, clearErrors);
PHP_METHOD(yii_base_Model, generateAttributeLabel);
PHP_METHOD(yii_base_Model, getAttributes);
PHP_METHOD(yii_base_Model, setAttributes);
PHP_METHOD(yii_base_Model, onUnsafeAttribute);
PHP_METHOD(yii_base_Model, getScenario);
PHP_METHOD(yii_base_Model, setScenario);
PHP_METHOD(yii_base_Model, safeAttributes);
PHP_METHOD(yii_base_Model, activeAttributes);
PHP_METHOD(yii_base_Model, load);
PHP_METHOD(yii_base_Model, loadMultiple);
PHP_METHOD(yii_base_Model, validateMultiple);
PHP_METHOD(yii_base_Model, fields);
PHP_METHOD(yii_base_Model, resolveFields);
PHP_METHOD(yii_base_Model, getIterator);
PHP_METHOD(yii_base_Model, offsetExists);
PHP_METHOD(yii_base_Model, offsetGet);
PHP_METHOD(yii_base_Model, offsetSet);
PHP_METHOD(yii_base_Model, offsetUnset);

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_validate, 0, 0, 0)
	ZEND_ARG_INFO(0, attributeNames)
	ZEND_ARG_INFO(0, clearErrors)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_getactivevalidators, 0, 0, 0)
	ZEND_ARG_INFO(0, attribute)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_isattributerequired, 0, 0, 1)
	ZEND_ARG_INFO(0, attribute)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_isattributesafe, 0, 0, 1)
	ZEND_ARG_INFO(0, attribute)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_isattributeactive, 0, 0, 1)
	ZEND_ARG_INFO(0, attribute)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_getattributelabel, 0, 0, 1)
	ZEND_ARG_INFO(0, attribute)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_haserrors, 0, 0, 0)
	ZEND_ARG_INFO(0, attribute)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_geterrors, 0, 0, 0)
	ZEND_ARG_INFO(0, attribute)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_getfirsterror, 0, 0, 1)
	ZEND_ARG_INFO(0, attribute)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_adderror, 0, 0, 1)
	ZEND_ARG_INFO(0, attribute)
	ZEND_ARG_INFO(0, error)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_clearerrors, 0, 0, 0)
	ZEND_ARG_INFO(0, attribute)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_generateattributelabel, 0, 0, 1)
	ZEND_ARG_INFO(0, name)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_getattributes, 0, 0, 0)
	ZEND_ARG_INFO(0, names)
	ZEND_ARG_INFO(0, except)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_setattributes, 0, 0, 1)
	ZEND_ARG_INFO(0, values)
	ZEND_ARG_INFO(0, safeOnly)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_onunsafeattribute, 0, 0, 2)
	ZEND_ARG_INFO(0, name)
	ZEND_ARG_INFO(0, value)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_setscenario, 0, 0, 1)
	ZEND_ARG_INFO(0, value)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_load, 0, 0, 1)
	ZEND_ARG_INFO(0, data)
	ZEND_ARG_INFO(0, formName)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_loadmultiple, 0, 0, 2)
	ZEND_ARG_INFO(0, models)
	ZEND_ARG_INFO(0, data)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_validatemultiple, 0, 0, 1)
	ZEND_ARG_INFO(0, models)
	ZEND_ARG_INFO(0, attributeNames)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_resolvefields, 0, 0, 2)
	ZEND_ARG_INFO(0, fields)
	ZEND_ARG_INFO(0, expand)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_offsetexists, 0, 0, 1)
	ZEND_ARG_INFO(0, offset)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_offsetget, 0, 0, 1)
	ZEND_ARG_INFO(0, offset)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_offsetset, 0, 0, 2)
	ZEND_ARG_INFO(0, offset)
	ZEND_ARG_INFO(0, item)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_yii_base_model_offsetunset, 0, 0, 1)
	ZEND_ARG_INFO(0, offset)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(yii_base_model_method_entry) {
	PHP_ME(yii_base_Model, rules, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, scenarios, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, formName, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, attributes, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, attributeLabels, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, validate, arginfo_yii_base_model_validate, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, beforeValidate, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, afterValidate, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, getValidators, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, getActiveValidators, arginfo_yii_base_model_getactivevalidators, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, createValidators, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, isAttributeRequired, arginfo_yii_base_model_isattributerequired, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, isAttributeSafe, arginfo_yii_base_model_isattributesafe, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, isAttributeActive, arginfo_yii_base_model_isattributeactive, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, getAttributeLabel, arginfo_yii_base_model_getattributelabel, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, hasErrors, arginfo_yii_base_model_haserrors, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, getErrors, arginfo_yii_base_model_geterrors, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, getFirstErrors, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, getFirstError, arginfo_yii_base_model_getfirsterror, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, addError, arginfo_yii_base_model_adderror, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, clearErrors, arginfo_yii_base_model_clearerrors, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, generateAttributeLabel, arginfo_yii_base_model_generateattributelabel, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, getAttributes, arginfo_yii_base_model_getattributes, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, setAttributes, arginfo_yii_base_model_setattributes, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, onUnsafeAttribute, arginfo_yii_base_model_onunsafeattribute, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, getScenario, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, setScenario, arginfo_yii_base_model_setscenario, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, safeAttributes, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, activeAttributes, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, load, arginfo_yii_base_model_load, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, loadMultiple, arginfo_yii_base_model_loadmultiple, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_base_Model, validateMultiple, arginfo_yii_base_model_validatemultiple, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(yii_base_Model, fields, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, resolveFields, arginfo_yii_base_model_resolvefields, ZEND_ACC_PROTECTED)
	PHP_ME(yii_base_Model, getIterator, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, offsetExists, arginfo_yii_base_model_offsetexists, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, offsetGet, arginfo_yii_base_model_offsetget, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, offsetSet, arginfo_yii_base_model_offsetset, ZEND_ACC_PUBLIC)
	PHP_ME(yii_base_Model, offsetUnset, arginfo_yii_base_model_offsetunset, ZEND_ACC_PUBLIC)
  PHP_FE_END
};
