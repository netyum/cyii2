/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace yii\validators;

use yii\web\AssetBundle;

/**
 * This asset bundle provides the javascript files for client validation.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ValidationAsset extends AssetBundle
{
    public sourcePath = "@yii/assets";

    public function init() {
        if typeof this->js == "null" {
            let this->js = [];
        }

        let this->js[] = "yii.validation.js";

        if typeof this->depends == "null" {
            let this->depends = [];
        }

        let this->depends[] = "yii\\web\\YiiAsset";

        parent::init();
    }
}

