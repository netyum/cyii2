/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace yii\validators;

use yii\BaseYii;
use yii\base\Component;
use yii\base\NotSupportedException;

/**
 * Validator is the base class for all validators.
 *
 * Child classes should override the [[validateValue()]] and/or [[validateAttribute()]] methods to provide the actual
 * logic of performing data validation. Child classes may also override [[clientValidateAttribute()]]
 * to provide client-side validation support.
 *
 * Validator declares a set of [[builtInValidators|built-in validators] which can
 * be referenced using short names. They are listed as follows:
 *
 * - `boolean`: [[BooleanValidator]]
 * - `captcha`: [[\yii\captcha\CaptchaValidator]]
 * - `compare`: [[CompareValidator]]
 * - `date`: [[DateValidator]]
 * - `default`: [[DefaultValueValidator]]
 * - `double`: [[NumberValidator]]
 * - `email`: [[EmailValidator]]
 * - `exist`: [[ExistValidator]]
 * - `file`: [[FileValidator]]
 * - `filter`: [[FilterValidator]]
 * - `image`: [[ImageValidator]]
 * - `in`: [[RangeValidator]]
 * - `integer`: [[NumberValidator]]
 * - `match`: [[RegularExpressionValidator]]
 * - `required`: [[RequiredValidator]]
 * - `safe`: [[SafeValidator]]
 * - `string`: [[StringValidator]]
 * - `trim`: [[FilterValidator]]
 * - `unique`: [[UniqueValidator]]
 * - `url`: [[UrlValidator]]
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Validator extends Component
{
    /**
     * @var array list of built-in validators (name => class or configuration)
     */
     //built_in_validators
    public static builtInValidators;

    /**
     * @var array|string attributes to be validated by this validator. For multiple attributes,
     * please specify them as an array; for single attribute, you may use either a string or an array.
     */
     //    public $attributes = [];
    public attributes;
    /**
     * @var string the user-defined error message. It may contain the following placeholders which
     * will be replaced accordingly by the validator:
     *
     * - `{attribute}`: the label of the attribute being validated
     * - `{value}`: the value of the attribute being validated
     */
    public message;
    /**
     * @var array|string scenarios that the validator can be applied to. For multiple scenarios,
     * please specify them as an array; for single scenario, you may use either a string or an array.
     */
     //    public $on = [];
    public on;
    /**
     * @var array|string scenarios that the validator should not be applied to. For multiple scenarios,
     * please specify them as an array; for single scenario, you may use either a string or an array.
     */
     //    public $except = [];
    public except;
    /**
     * @var boolean whether this validation rule should be skipped if the attribute being validated
     * already has some validation error according to some previous rules. Defaults to true.
     */
    public skipOnError = true;
    /**
     * @var boolean whether this validation rule should be skipped if the attribute value
     * is null or an empty string.
     */
    public skipOnEmpty = true;
    /**
     * @var boolean whether to enable client-side validation for this validator.
     * The actual client-side validation is done via the JavaScript code returned
     * by [[clientValidateAttribute()]]. If that method returns null, even if this property
     * is true, no client-side validation will be done by this validator.
     */
    public enableClientValidation = true;
    /**
     * @var callable a PHP callable that replaces the default implementation of [[isEmpty()]].
     * If not set, [[isEmpty()]] will be used to check if a value is empty. The signature
     * of the callable should be `function ($value)` which returns a boolean indicating
     * whether the value is empty.
     */
    public isEmpty;
    /**
     * @var callable a PHP callable whose return value determines whether this validator should be applied.
     * The signature of the callable should be `function ($model, $attribute)`, where `$model` and `$attribute`
     * refer to the model and the attribute currently being validated. The callable should return a boolean value.
     *
     * This property is mainly provided to support conditional validation on the server side.
     * If this property is not set, this validator will be always applied on the server side.
     *
     * The following example will enable the validator only when the country currently selected is USA:
     *
     * ```php
     * function ($model) {
     *     return $model->country == Country::USA;
     * }
     * ```
     *
     * @see whenClient
     */
    public when;
    /**
     * @var string a JavaScript function name whose return value determines whether this validator should be applied
     * on the client side. The signature of the function should be `function (attribute, value)`, where
     * `attribute` is the name of the attribute being validated and `value` the current value of the attribute.
     *
     * This property is mainly provided to support conditional validation on the client side.
     * If this property is not set, this validator will be always applied on the client side.
     *
     * The following example will enable the validator only when the country currently selected is USA:
     *
     * ```php
     * function (attribute, value) {
     *     return $('#country').value == 'USA';
     * }
     * ```
     *
     * @see when
     */
    public whenClient;

    /**
     * Creates a validator object.
     * @param mixed $type the validator type. This can be a built-in validator name,
     * a method name of the model class, an anonymous function, or a validator class name.
     * @param \yii\base\Model $object the data object to be validated.
     * @param array|string $attributes list of attributes to be validated. This can be either an array of
     * the attribute names or a string of comma-separated attribute names.
     * @param array $params initial values to be applied to the validator properties
     * @return Validator the validator
     */
    public static function createValidator(type, $object, attributes, params = [])
    {
        let params["attributes"] = attributes;

        if (typeof type == "object") && (type instanceof \Closure) || $object->hasMethod(type) {
            // method-based validator
            //let params["class"] = __NAMESPACE__ . "\\InlineValidator",
            let params["class"] = "yii\\validators\\InlineValidator",
            	params["method"] = type;
        } else {
            var builtInValidators;
            let builtInValidators = $static::$builtInValidators;
            if typeof builtInValidators == "null" {
                let builtInValidators = built_in_validators(),
                    $static::$builtInValidators = builtInValidators;
            }
            var validator;
            if fetch validator, builtInValidators[type] {
                let type = validator;
            }
            var name, value;
            if typeof type == "array" {
            	for name, value in type {
                    let params[name] = value;
                }
            } else {
                let params["class"] = type;
            }
        }
        return BaseYii::createObject(params);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        var attributes, on, except;
        let attributes = this->attributes,
            on = this->on,
            except = this->except;

        if typeof attributes == "null" {
            let attributes = [];
        }

        if typeof on == "null" {
            let on = [];
        }

        if typeof except == "null" {
            let except = [];
        }

        let this->attributes = attributes,
        	this->on = on,
        	this->except = except;
    }

    /**
     * Validates the specified object.
     * @param \yii\base\Model $object the data object being validated
     * @param array|null $attributes the list of attributes to be validated.
     * Note that if an attribute is not associated with the validator,
     * it will be ignored.
     * If this parameter is null, every attribute listed in [[attributes]] will be validated.
     */
    public function validateAttributes($object, attributes = null)
    {
        if typeof attributes == "array" {
            let attributes = array_intersect(this->attributes, attributes);
        } else {
            let attributes = this->attributes;
        }
        var attribute, skip = false;
        for attribute in attributes {
            if this->skipOnError && $object->hasErrors(attribute) {
                let skip = true;
            }
            else {
                if this->skipOnEmpty && this->isEmpty($object->{attribute}) {
                    let skip = true;
                }
            }
            if !skip {
                if typeof this->when == "null" || call_user_func(this->when, $object, attribute) {
                    this->validateAttribute($object, attribute);
                }
            }
            let skip = false;
        }
    }

    /**
     * Validates a single attribute.
     * Child classes must implement this method to provide the actual validation logic.
     * @param \yii\base\Model $object the data object to be validated
     * @param string $attribute the name of the attribute to be validated.
     */
    public function validateAttribute($object, attribute)
    {
    	var result = null;
        let result = $this->validateValue($object->{attribute});
        if typeof result == "array" && count(result) > 0 {
            this->addError($object, attribute, result[0], result[1]);
        }
    }

    /**
     * Validates a given value.
     * You may use this method to validate a value out of the context of a data model.
     * @param mixed $value the data value to be validated.
     * @param string $error the error message to be returned, if the validation fails.
     * @return boolean whether the data is valid.
     */
     //    public function validate($value, &$error = null)
    public function validate(value, error = null)
    {
    	var result = null;
        let result = this->validateValue(value);
        if typeof result == "null" || ( typeof result =="array" && count(result) == 0 ) {
            return true;
        } else {
            var message, params;
            let message = result[0],
                params = result[1];

            let params["attribute"] = BaseYii::t("yii", "the input value");
            if typeof value == "array" {
            	let params["value"] = "array()";
            }
            else {
            	let params["value"] = value;
            }
            var app, i18n, language;
            let app = BaseYii::$app,
                language = app->language,
                i18n = app->getI18n();
            let error = BaseYii::$app->getI18n()->format(message, params, BaseYii::$app->language);

            return false;
        }
    }

    /**
     * Validates a value.
     * A validator class can implement this method to support data validation out of the context of a data model.
     * @param mixed $value the data value to be validated.
     * @return array|null the error message and the parameters to be inserted into the error message.
     * Null should be returned if the data is valid.
     * @throws NotSupportedException if the validator does not supporting data validation without a model
     */
    protected function validateValue(value)
    {
        throw new NotSupportedException(get_class(this) . " does not support validateValue().");
    }

    /**
     * Returns the JavaScript needed for performing client-side validation.
     *
     * You may override this method to return the JavaScript validation code if
     * the validator can support client-side validation.
     *
     * The following JavaScript variables are predefined and can be used in the validation code:
     *
     * - `attribute`: the name of the attribute being validated.
     * - `value`: the value being validated.
     * - `messages`: an array used to hold the validation error messages for the attribute.
     *
     * @param \yii\base\Model $object the data object being validated
     * @param string $attribute the name of the attribute to be validated.
     * @param \yii\web\View $view the view object that is going to be used to render views or view files
     * containing a model form with this validator applied.
     * @return string the client-side validation script. Null if the validator does not support
     * client-side validation.
     * @see \yii\widgets\ActiveForm::enableClientValidation
     */
    public function clientValidateAttribute($object, attribute, view)
    {
        return null;
    }

    /**
     * Returns a value indicating whether the validator is active for the given scenario and attribute.
     *
     * A validator is active if
     *
     * - the validator's `on` property is empty, or
     * - the validator's `on` property contains the specified scenario
     *
     * @param string $scenario scenario name
     * @return boolean whether the validator applies to the specified scenario.
     */
    public function isActive($scenario)
    {
    	var except, on;
    	let except = this->except;
    	if typeof except == "null" {
    		let except = [],
    			this->except = except;
    	}
 
 		let on = this->on;
 		if typeof on == "null" {
 			let on = [],
 				this->on = on;
 		}
    	if !in_array(scenario, this->except, true) {
    		if count(this->on) == 0 {
    			return true;
    		}
    		else {
    			if in_array(scenario, this->on, true) {
    				return true;
    			}
    		}
    	}
    	return false;
    }

    /**
     * Adds an error about the specified attribute to the model object.
     * This is a helper method that performs message selection and internationalization.
     * @param \yii\base\Model $object the data object being validated
     * @param string $attribute the attribute being validated
     * @param string $message the error message
     * @param array $params values for the placeholders in the error message
     */
    public function addError($object, attribute, message, params = [])
    {
    	var value;
        let value = $object->{attribute},
        	params["attribute"] = $object->getAttributeLabel(attribute);

        if typeof value == "array" {
        	let params["value"] = "array()";
        }
        else {
        	let params["value"] = value;
        }
        
        $object->addError(attribute, BaseYii::$app->getI18n()->format(message, params, BaseYii::$app->language));
    }

    /**
     * Checks if the given value is empty.
     * A value is considered empty if it is null, an empty array, or the trimmed result is an empty string.
     * Note that this method is different from PHP empty(). It will return false when the value is 0.
     * @param mixed $value the value to be checked
     * @return boolean whether the value is empty
     */
    public function isEmpty(value)
    {
        if typeof this->isEmpty != "null" {
            return call_user_func(this->isEmpty, value);
        } else {
        	if typeof value == "null" {
        		return true;
        	}
        	else {
        		if typeof value == "array" && count(value) == 0 {
        			return true;
        		}
        		else {
        			if typeof value == "string" && value == "" {
        				return true;
        			}
        		}
        	}
        	return false;
        }
    }
}
