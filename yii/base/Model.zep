/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace yii\base;

use yii\BaseYii;
use ArrayAccess;
use ArrayObject;
use ArrayIterator;
use ReflectionClass;
use IteratorAggregate;
use yii\helpers\Inflector;
use yii\validators\RequiredValidator;
use yii\validators\Validator;

/**
 * Model is the base class for data models.
 *
 * Model implements the following commonly used features:
 *
 * - attribute declaration: by default, every public class member is considered as
 *   a model attribute
 * - attribute labels: each attribute may be associated with a label for display purpose
 * - massive attribute assignment
 * - scenario-based validation
 *
 * Model also raises the following events when performing data validation:
 *
 * - [[EVENT_BEFORE_VALIDATE]]: an event raised at the beginning of [[validate()]]
 * - [[EVENT_AFTER_VALIDATE]]: an event raised at the end of [[validate()]]
 *
 * You may directly use Model to store model data, or extend it with customization.
 *
 * @property \yii\validators\Validator[] $activeValidators The validators applicable to the current
 * [[scenario]]. This property is read-only.
 * @property array $attributes Attribute values (name => value).
 * @property array $errors An array of errors for all attributes. Empty array is returned if no error. The
 * result is a two-dimensional array. See [[getErrors()]] for detailed description. This property is read-only.
 * @property array $firstErrors The first errors. The array keys are the attribute names, and the array values
 * are the corresponding error messages. An empty array will be returned if there is no error. This property is
 * read-only.
 * @property ArrayIterator $iterator An iterator for traversing the items in the list. This property is
 * read-only.
 * @property string $scenario The scenario that this model is in. Defaults to [[SCENARIO_DEFAULT]].
 * @property ArrayObject|\yii\validators\Validator[] $validators All the validators declared in the model.
 * This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
//class Model extends Component implements IteratorAggregate, ArrayAccess, Arrayable
class Model extends Component implements IteratorAggregate
{
//    use ArrayableTrait;

    /**
     * The name of the default scenario.
     */
    const SCENARIO_DEFAULT = "default";
    /**
     * @event ModelEvent an event raised at the beginning of [[validate()]]. You may set
     * [[ModelEvent::isValid]] to be false to stop the validation.
     */
    const EVENT_BEFORE_VALIDATE = "beforeValidate";
    /**
     * @event Event an event raised at the end of [[validate()]]
     */
    const EVENT_AFTER_VALIDATE = "afterValidate";

    /**
     * @var array validation errors (attribute name => array of errors)
     */
    protected _errors;
    /**
     * @var ArrayObject list of validators
     */
    protected _validators;
    /**
     * @var string current scenario
     */
    protected _scenario = "default";

    public function extraFields() {

    }

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * Each rule is an array with the following structure:
     *
     * ~~~
     * [
     *     ['attribute1', 'attribute2'],
     *     'validator type',
     *     'on' => ['scenario1', 'scenario2'],
     *     ...other parameters...
     * ]
     * ~~~
     *
     * where
     *
     *  - attribute list: required, specifies the attributes array to be validated, for single attribute you can pass string;
     *  - validator type: required, specifies the validator to be used. It can be a built-in validator name,
     *    a method name of the model class, an anonymous function, or a validator class name.
     *  - on: optional, specifies the [[scenario|scenarios]] array when the validation
     *    rule can be applied. If this option is not set, the rule will apply to all scenarios.
     *  - additional name-value pairs can be specified to initialize the corresponding validator properties.
     *    Please refer to individual validator class API for possible properties.
     *
     * A validator can be either an object of a class extending [[Validator]], or a model class method
     * (called *inline validator*) that has the following signature:
     *
     * ~~~
     * // $params refers to validation parameters given in the rule
     * function validatorName($attribute, $params)
     * ~~~
     *
     * In the above `$attribute` refers to currently validated attribute name while `$params` contains an array of
     * validator configuration options such as `max` in case of `string` validator. Currently validate attribute value
     * can be accessed as `$this->[$attribute]`.
     *
     * Yii also provides a set of [[Validator::builtInValidators|built-in validators]].
     * They each has an alias name which can be used when specifying a validation rule.
     *
     * Below are some examples:
     *
     * ~~~
     * [
     *     // built-in "required" validator
     *     [['username', 'password'], 'required'],
     *     // built-in "string" validator customized with "min" and "max" properties
     *     ['username', 'string', 'min' => 3, 'max' => 12],
     *     // built-in "compare" validator that is used in "register" scenario only
     *     ['password', 'compare', 'compareAttribute' => 'password2', 'on' => 'register'],
     *     // an inline validator defined via the "authenticate()" method in the model class
     *     ['password', 'authenticate', 'on' => 'login'],
     *     // a validator of class "DateRangeValidator"
     *     ['dateRange', 'DateRangeValidator'],
     * ];
     * ~~~
     *
     * Note, in order to inherit rules defined in the parent class, a child class needs to
     * merge the parent rules with child rules using functions such as `array_merge()`.
     *
     * @return array validation rules
     * @see scenarios()
     */
    public function rules()
    {
        return [];
    }


    /**
     * Returns a list of scenarios and the corresponding active attributes.
     * An active attribute is one that is subject to validation in the current scenario.
     * The returned array should be in the following format:
     *
     * ~~~
     * [
     *     'scenario1' => ['attribute11', 'attribute12', ...],
     *     'scenario2' => ['attribute21', 'attribute22', ...],
     *     ...
     * ]
     * ~~~
     *
     * By default, an active attribute is considered safe and can be massively assigned.
     * If an attribute should NOT be massively assigned (thus considered unsafe),
     * please prefix the attribute with an exclamation character (e.g. '!rank').
     *
     * The default implementation of this method will return all scenarios found in the [[rules()]]
     * declaration. A special scenario named [[SCENARIO_DEFAULT]] will contain all attributes
     * found in the [[rules()]]. Each scenario will be associated with the attributes that
     * are being validated by the validation rules that apply to the scenario.
     *
     * @return array a list of scenarios and the corresponding active attributes.
     */
    public function scenarios()
    {
        var scenarios = [], validators, validator, scenario, on, except;
        let scenarios[self::SCENARIO_DEFAULT] = [];
        let validators = this->getValidators();

        var iterator;
        if (typeof validators == "object") && (validators instanceof ArrayObject) {
            let iterator = validators->getIterator();
            while iterator->valid() {
                let validator = iterator->current();
                if typeof validator->on == "array" {
                    let on = validator->on;
                    for scenario in on {
                        let scenarios[scenario] = [];
                    }
                }

                if typeof validator->except == "array" {
                    let except = validator->except;
                    for scenario in except {
                        let scenarios[scenario] = [];
                    }
                }
                iterator->next();
            }
        }

        var names, name, attributes, attribute, scenarios_name;
        let names = array_keys(scenarios);

        if (typeof validators == "object") && (validators instanceof ArrayObject) {
            let iterator = validators->getIterator();
            while iterator->valid() {
                let validator = iterator->current();
                var on_empty = false, except_empty = false;
                let on_empty = typeof validator->on == "null" || (typeof validator->on == "array" && count(validator->on) == 0);
                let except_empty = typeof validator->except == "null" || (typeof validator->except == "array" && count(validator->except) == 0);

                if on_empty && except_empty {
                    for name in names {
                        if isset scenarios[name] {
                            let scenarios_name = scenarios[name];    
                        } else {
                            let scenarios_name = [];
                        }
                        
                        let attributes = validator->attributes;
                        for attribute in attributes {
                            let scenarios_name[attribute] = true;
                        }
                        let scenarios[name] = scenarios_name;
                    }
                } else {
                    if on_empty {
                        
                        for name in names {
                            let except = validator->except;
                            if typeof except == "null" {
                                let except = [];
                            }

                            if !in_array(name, except, true) {
                                if isset scenarios[name] {
                                    let scenarios_name = scenarios[name];    
                                } else {
                                    let scenarios_name = [];
                                }
                                let attributes = validator->attributes;
                                for attribute in attributes {
                                    let scenarios_name[attribute] = true;
                                }
                                let scenarios[name] = scenarios_name;
                            }
                        }
                    } else {
                        let on = validator->on,
                            attributes = validator->attributes;
    
                        for name in on {
                            if isset scenarios[name] {
                                let scenarios_name = scenarios[name];
                            } else {
                                let scenarios_name = [];
                            }
                            for attribute in attributes {
                                let scenarios_name[attribute] = true;
                            }
                            let scenarios[name] = scenarios_name;
                        }
                    }
                }
                iterator->next();
            }
        }

        var temp_scenarios;
        let temp_scenarios = scenarios;
        for scenario, attributes in temp_scenarios {
            if count(attributes) == 0 && scenario != self::SCENARIO_DEFAULT {
                unset scenarios[scenario];
            } else {
                let scenarios[scenario] = array_keys(attributes);
            }
        }

        return scenarios;
    }

    /**
     * Returns the form name that this model class should use.
     *
     * The form name is mainly used by [[\yii\widgets\ActiveForm]] to determine how to name
     * the input fields for the attributes in a model. If the form name is "A" and an attribute
     * name is "b", then the corresponding input name would be "A[b]". If the form name is
     * an empty string, then the input name would be "b".
     *
     * By default, this method returns the model class name (without the namespace part)
     * as the form name. You may override it when the model is used in different forms.
     *
     * @return string the form name of this model class.
     */
    public function formName()
    {
        var reflector;
        let reflector = new ReflectionClass(this);
        return reflector->getShortName();
    }

    /**
     * Returns the list of attribute names.
     * By default, this method returns all public non-static properties of the class.
     * You may override this method to change the default behavior.
     * @return array list of attribute names.
     */
    public function attributes()
    {
        var $class, names = [], property, properties;
        let $class = new ReflectionClass(this),
            properties = $class->getProperties(\ReflectionProperty::IS_PUBLIC);

        for property in properties {
            if !$property->isStatic() {
                let names[] = property->getName();
            }
        }

        return names;
    }

    /**
     * Returns the attribute labels.
     *
     * Attribute labels are mainly used for display purpose. For example, given an attribute
     * `firstName`, we can declare a label `First Name` which is more user-friendly and can
     * be displayed to end users.
     *
     * By default an attribute label is generated using [[generateAttributeLabel()]].
     * This method allows you to explicitly specify attribute labels.
     *
     * Note, in order to inherit labels defined in the parent class, a child class needs to
     * merge the parent labels with child labels using functions such as `array_merge()`.
     *
     * @return array attribute labels (name => label)
     * @see generateAttributeLabel()
     */
    public function attributeLabels()
    {
        return [];
    }


    /**
     * Performs the data validation.
     *
     * This method executes the validation rules applicable to the current [[scenario]].
     * The following criteria are used to determine whether a rule is currently applicable:
     *
     * - the rule must be associated with the attributes relevant to the current scenario;
     * - the rules must be effective for the current scenario.
     *
     * This method will call [[beforeValidate()]] and [[afterValidate()]] before and
     * after the actual validation, respectively. If [[beforeValidate()]] returns false,
     * the validation will be cancelled and [[afterValidate()]] will not be called.
     *
     * Errors found during the validation can be retrieved via [[getErrors()]],
     * [[getFirstErrors()]] and [[getFirstError()]].
     *
     * @param array $attributeNames list of attribute names that should be validated.
     * If this parameter is empty, it means any attribute listed in the applicable
     * validation rules should be validated.
     * @param boolean $clearErrors whether to call [[clearErrors()]] before performing validation
     * @return boolean whether the validation is successful without any error.
     * @throws InvalidParamException if the current scenario is unknown.
     */
    public function validate(attributeNames = null, clearErrors = true)
    {
        var scenarios, scenario, validator, active_validators;

        let scenarios = $this->scenarios(),
            scenario = $this->getScenario();

        if !isset scenarios[scenario] {
            throw new InvalidParamException("Unknown scenario: " . $scenario);
        }

        if clearErrors {
            this->clearErrors();
        }
        if typeof attributeNames == "null" {
            let attributeNames = this->activeAttributes();
        }

        if this->beforeValidate() {
            let active_validators = this->getActiveValidators();
            for validator in active_validators {
                validator->validateAttributes(this, attributeNames);
            }
            this->afterValidate();

            return !this->hasErrors();
        }

        return false;
    }

    /**
     * This method is invoked before validation starts.
     * The default implementation raises a `beforeValidate` event.
     * You may override this method to do preliminary checks before validation.
     * Make sure the parent implementation is invoked so that the event can be raised.
     * @return boolean whether the validation should be executed. Defaults to true.
     * If false is returned, the validation will stop and the model is considered invalid.
     */
    public function beforeValidate()
    {
        var event;
        let event = new ModelEvent;
        this->trigger(self::EVENT_BEFORE_VALIDATE, event);

        return event->isValid;
    }

    /**
     * This method is invoked after validation ends.
     * The default implementation raises an `afterValidate` event.
     * You may override this method to do postprocessing after validation.
     * Make sure the parent implementation is invoked so that the event can be raised.
     */
    public function afterValidate()
    {
        this->trigger(self::EVENT_AFTER_VALIDATE);
    }

    /**
     * Returns all the validators declared in [[rules()]].
     *
     * This method differs from [[getActiveValidators()]] in that the latter
     * only returns the validators applicable to the current [[scenario]].
     *
     * Because this method returns an ArrayObject object, you may
     * manipulate it by inserting or removing validators (useful in model behaviors).
     * For example,
     *
     * ~~~
     * $model->validators[] = $newValidator;
     * ~~~
     *
     * @return ArrayObject|\yii\validators\Validator[] all the validators declared in the model.
     */
    public function getValidators()
    {
        if typeof this->_validators == "null" {
            let this->_validators = this->createValidators();
        }

        return this->_validators;
    }

    /**
     * Returns the validators applicable to the current [[scenario]].
     * @param string $attribute the name of the attribute whose applicable validators should be returned.
     * If this is null, the validators for ALL attributes in the model will be returned.
     * @return \yii\validators\Validator[] the validators applicable to the current [[scenario]].
     */
    public function getActiveValidators(attribute = null)
    {
        var validators = [], scenario, def_validators, validator, iterator;
        let scenario = this->getScenario(),
            def_validators = this->getValidators();

        if (typeof def_validators == "object") && (def_validators instanceof ArrayObject) {
            let iterator = def_validators->getIterator();
            while iterator->valid() {
                let validator = iterator->current();
                if validator->isActive(scenario) && typeof attribute == "null" || in_array(attribute, validator->attributes, true) {
                    let validators[] = validator;
                }
                iterator->next();
            }
        }

        return validators;
    }

    /**
     * Creates validator objects based on the validation rules specified in [[rules()]].
     * Unlike [[getValidators()]], each time this method is called, a new list of validators will be returned.
     * @return ArrayObject validators
     * @throws InvalidConfigException if any validation rule configuration is invalid
     */
    public function createValidators()
    {
        var validators, rules, rule, validator;
        let validators = new ArrayObject,
            rules = this->rules();

        for rule in rules {
            if typeof rule == "object" {
                if rule instanceof Validator {
                    validators->append(rule);
                }
            } else {
                if typeof rule == "array" && isset rule[0] && isset rule[1] { // attributes, validator type
                    //(array) rule[0];
                    let validator = Validator::createValidator(rule[1], this, rule[0], array_slice(rule, 2));
                    validators->append(validator);
                } else {
                    throw new InvalidConfigException("Invalid validation rule: a rule must specify both attribute names and validator type.");
                }
            }
        }
        return validators;
    }


    /**
     * Returns a value indicating whether the attribute is required.
     * This is determined by checking if the attribute is associated with a
     * [[\yii\validators\RequiredValidator|required]] validation rule in the
     * current [[scenario]].
     * @param string $attribute attribute name
     * @return boolean whether the attribute is required
     */
    public function isAttributeRequired(attribute)
    {
        var validator, active_validators;
        let active_validators = this->getActiveValidators(attribute);

        for validator in  active_validators {
            if typeof validator == "object" {
                if validator instanceof RequiredValidator {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns a value indicating whether the attribute is safe for massive assignments.
     * @param string $attribute attribute name
     * @return boolean whether the attribute is safe for massive assignments
     * @see safeAttributes()
     */
    public function isAttributeSafe(attribute)
    {
        var safe_attributes;
        let safe_attributes = this->safeAttributes();
        return in_array(attribute, safe_attributes, true);
    }

    /**
     * Returns a value indicating whether the attribute is active in the current scenario.
     * @param string $attribute attribute name
     * @return boolean whether the attribute is active in the current scenario
     * @see activeAttributes()
     */
    public function isAttributeActive(attribute)
    {
        return in_array(attribute, this->activeAttributes(), true);
    }

    /**
     * Returns the text label for the specified attribute.
     * @param string $attribute the attribute name
     * @return string the attribute label
     * @see generateAttributeLabel()
     * @see attributeLabels()
     */
    public function getAttributeLabel(attribute)
    {
        var labels;
        let labels = this->attributeLabels();
        if isset labels[attribute] {
            return labels[attribute];
        } else {
            return this->generateAttributeLabel(attribute);
        }
    }

    /**
     * Returns a value indicating whether there is any validation error.
     * @param string|null $attribute attribute name. Use null to check all attributes.
     * @return boolean whether there is any error.
     */
    public function hasErrors($attribute = null)
    {
        return $attribute === null ? !empty($this->_errors) : isset($this->_errors[$attribute]);
    }

    /**
     * Returns the errors for all attribute or a single attribute.
     * @param string $attribute attribute name. Use null to retrieve errors for all attributes.
     * @property array An array of errors for all attributes. Empty array is returned if no error.
     * The result is a two-dimensional array. See [[getErrors()]] for detailed description.
     * @return array errors for all attributes or the specified attribute. Empty array is returned if no error.
     * Note that when returning errors for all attributes, the result is a two-dimensional array, like the following:
     *
     * ~~~
     * [
     *     'username' => [
     *         'Username is required.',
     *         'Username must contain only word characters.',
     *     ],
     *     'email' => [
     *         'Email address is invalid.',
     *     ]
     * ]
     * ~~~
     *
     * @see getFirstErrors()
     * @see getFirstError()
     */
    public function getErrors($attribute = null)
    {
        if ($attribute === null) {
            return $this->_errors === null ? [] : $this->_errors;
        } else {
            return isset($this->_errors[$attribute]) ? $this->_errors[$attribute] : [];
        }
    }

    /**
     * Returns the first error of every attribute in the model.
     * @return array the first errors. The array keys are the attribute names, and the array
     * values are the corresponding error messages. An empty array will be returned if there is no error.
     * @see getErrors()
     * @see getFirstError()
     */
    public function getFirstErrors()
    {
        var errors = [], def_errors, name, es;
        if typeof this->_errors == "null" || count(this->_errors) == 0 {
            return [];
        } else {
            let def_errors = this->_errors;
            for name, es in def_errors {
                if typeof es == "array" && count(es) > 0 {
                    let errors[name] = reset(es);
                }
            }

            return errors;
        }
    }

    /**
     * Returns the first error of the specified attribute.
     * @param string $attribute attribute name.
     * @return string the error message. Null is returned if no error.
     * @see getErrors()
     * @see getFirstErrors()
     */
    public function getFirstError(attribute)
    {
        var errors;
        let errors = this->_errors;

        if typeof errors == "array" && isset errors[attribute] {
            return reset(errors[attribute]);
        }
        else {
            return null;
        }
    }

    /**
     * Adds a new error to the specified attribute.
     * @param string $attribute attribute name
     * @param string $error new error message
     */
    public function addError(attribute, error = "")
    {
        var errors, attribute_errors;
        let errors = this->_errors;
        if typeof errors == "null" {
            let errors = [];
        }
        if isset errors[attribute] {
            let attribute_errors = errors[attribute];
        }
        else {
            let attribute_errors = [];
        }

        let attribute_errors[] = error,
            errors[attribute] = attribute_errors,
            this->_errors = errors;
    }

    /**
     * Removes errors for all attributes or a single attribute.
     * @param string $attribute attribute name. Use null to remove errors for all attribute.
     */
    public function clearErrors(attribute = null)
    {
        var errors;
        if typeof attribute == "null" {
            let this->_errors = [];
        } else {
            let errors = this->_errors;
            unset errors[attribute];
            let this->_errors = errors;
        }
    }

    /**
     * Generates a user friendly attribute label based on the give attribute name.
     * This is done by replacing underscores, dashes and dots with blanks and
     * changing the first letter of each word to upper case.
     * For example, 'department_name' or 'DepartmentName' will generate 'Department Name'.
     * @param string $name the column name
     * @return string the attribute label
     */
    public function generateAttributeLabel(name)
    {
        return Inflector::camel2words(name, true);
    }

    /**
     * Returns attribute values.
     * @param array $names list of attributes whose value needs to be returned.
     * Defaults to null, meaning all attributes listed in [[attributes()]] will be returned.
     * If it is an array, only the attributes in the array will be returned.
     * @param array $except list of attributes whose value should NOT be returned.
     * @return array attribute values (name => value).
     */
    public function getAttributes(names = null, except = [])
    {
        var values = [], name;
        if typeof names == "null" {
            let names = this->attributes();
        }

        for name in names {
            let values[name] = this->{name};
        }
        for name in except {
            unset values[name];
        }

        return values;
    }

    /**
     * Sets the attribute values in a massive way.
     * @param array $values attribute values (name => value) to be assigned to the model.
     * @param boolean $safeOnly whether the assignments should only be done to the safe attributes.
     * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
     * @see safeAttributes()
     * @see attributes()
     */
    public function setAttributes(values, safeOnly = true)
    {
        var attributes, name, value;
        if typeof values == "array" {
            if safeOnly {
                let attributes = array_flip(this->safeAttributes());
            } else {
                let attributes = array_flip(this->attributes());
            }
            for name, value in values {
                if isset attributes[name] {
                    let this->{name} = value;
                } else {
                    if safeOnly {
                        this->onUnsafeAttribute(name, value);
                    }
                }
            }
        }
    }

    /**
     * This method is invoked when an unsafe attribute is being massively assigned.
     * The default implementation will log a warning message if YII_DEBUG is on.
     * It does nothing otherwise.
     * @param string $name the unsafe attribute name
     * @param mixed $value the attribute value
     */
    public function onUnsafeAttribute(name, value)
    {
        if (YII_DEBUG) {
            BaseYii::trace("Failed to set unsafe attribute '". name ."' in '" . get_class(this) . "'.", __METHOD__);
        }
    }

    /**
     * Returns the scenario that this model is used in.
     *
     * Scenario affects how validation is performed and which attributes can
     * be massively assigned.
     *
     * @return string the scenario that this model is in. Defaults to [[SCENARIO_DEFAULT]].
     */
    public function getScenario()
    {
        return this->_scenario;
    }

    /**
     * Sets the scenario for the model.
     * Note that this method does not check if the scenario exists or not.
     * The method [[validate()]] will perform this check.
     * @param string $value the scenario that this model is in.
     */
    public function setScenario(value)
    {
        let this->_scenario = value;
    }

    /**
     * Returns the attribute names that are safe to be massively assigned in the current scenario.
     * @return string[] safe attribute names
     */
    public function safeAttributes()
    {
        var scenario, scenarios;
        let scenario = this->getScenario(),
            scenarios = this->scenarios();
        if !isset scenarios[scenario] {
            return [];
        }
        var attributes = [], attribute, first_char;
        for attribute in scenarios[scenario] {
            let first_char = substr(attribute, 0, 1);
            if first_char != "!" {
                let attributes[] = attribute;
            }
        }

        return attributes;
    }

    /**
     * Returns the attribute names that are subject to validation in the current scenario.
     * @return string[] safe attribute names
     */
    public function activeAttributes()
    {
        var scenario, scenarios;
        let scenario = $this->getScenario(),
            scenarios = $this->scenarios();

        if !isset scenarios[scenario] {
            return [];
        }
        var attributes, i, attribute, first_char;
        let attributes = scenarios[scenario];
        for i, attribute in attributes {
            let first_char = substr(attribute, 0, 1);
            if first_char == "!" {
                let attributes[i] = substr(attribute, 1);
            }
        }

        return attributes;
    }

    /**
     * Populates the model with the data from end user.
     * The data to be loaded is `$data[formName]`, where `formName` refers to the value of [[formName()]].
     * If [[formName()]] is empty, the whole `$data` array will be used to populate the model.
     * The data being populated is subject to the safety check by [[setAttributes()]].
     * @param array $data the data array. This is usually `$_POST` or `$_GET`, but can also be any valid array
     * supplied by end user.
     * @param string $formName the form name to be used for loading the data into the model.
     * If not set, [[formName()]] will be used.
     * @return boolean whether the model is successfully populated with some data.
     */
    public function load(data, formName = null)
    {
        var scope;
        if typeof formName == "null" {
            let scope = this->formName();
        } else {
            let scope = formName;
        }
        
        if scope == "" && count(data) > 0 {
            this->setAttributes(data);
            return true;
        } else {
            if isset data[scope] {
                this->setAttributes(data[scope]);
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Populates a set of models with the data from end user.
     * This method is mainly used to collect tabular data input.
     * The data to be loaded for each model is `$data[formName][index]`, where `formName`
     * refers to the value of [[formName()]], and `index` the index of the model in the `$models` array.
     * If [[formName()]] is empty, `$data[index]` will be used to populate each model.
     * The data being populated to each model is subject to the safety check by [[setAttributes()]].
     * @param array $models the models to be populated. Note that all models should have the same class.
     * @param array $data the data array. This is usually `$_POST` or `$_GET`, but can also be any valid array
     * supplied by end user.
     * @return boolean whether the model is successfully populated with some data.
     */
    public static function loadMultiple(models, data)
    {
        /** @var Model $model */
        var model, success, scope;
        let model = reset(models);
        if typeof model == "boolean" {
            return false;
        }

        let success = false,
            scope = model->formName();

        var i, new_model, temp;
        for i, new_model in models {
            if scope == "" {
                if isset data[i] {
                    new_model->setAttributes(data[i]);
                    let success = true;
                }
            } else {
                if fetch temp, data[scope][i] {
                    model->setAttributes(temp);
                    let success = true;
                }
            }
        }

        return success;
    }

    /**
     * Validates multiple models.
     * This method will validate every model. The models being validated may
     * be of the same or different types.
     * @param array $models the models to be validated
     * @param array $attributeNames list of attribute names that should be validated.
     * If this parameter is empty, it means any attribute listed in the applicable
     * validation rules should be validated.
     * @return boolean whether all models are valid. False will be returned if one
     * or multiple models have validation error.
     */
    public static function validateMultiple(models, attributeNames = null)
    {
        var valid, model;
        let valid = true;
        /** @var Model $model */
        for model in models {
            if model->validate(attributeNames) && valid {
                let valid = true;
            }
            else {
                let valid = false;
            }
        }

        return valid;
    }

    /**
     * Returns the list of fields that should be returned by default by [[toArray()]] when no specific fields are specified.
     *
     * A field is a named element in the returned array by [[toArray()]].
     *
     * This method should return an array of field names or field definitions.
     * If the former, the field name will be treated as an object property name whose value will be used
     * as the field value. If the latter, the array key should be the field name while the array value should be
     * the corresponding field definition which can be either an object property name or a PHP callable
     * returning the corresponding field value. The signature of the callable should be:
     *
     * ```php
     * function ($field, $model) {
     *     // return field value
     * }
     * ```
     *
     * For example, the following code declares four fields:
     *
     * - `email`: the field name is the same as the property name `email`;
     * - `firstName` and `lastName`: the field names are `firstName` and `lastName`, and their
     *   values are obtained from the `first_name` and `last_name` properties;
     * - `fullName`: the field name is `fullName`. Its value is obtained by concatenating `first_name`
     *   and `last_name`.
     *
     * ```php
     * return [
     *     'email',
     *     'firstName' => 'first_name',
     *     'lastName' => 'last_name',
     *     'fullName' => function () {
     *         return $this->first_name . ' ' . $this->last_name;
     *     },
     * ];
     * ```
     *
     * In this method, you may also want to return different lists of fields based on some context
     * information. For example, depending on [[scenario]] or the privilege of the current application user,
     * you may return different sets of visible fields or filter out some fields.
     *
     * The default implementation of this method returns [[attributes()]] indexed by the same attribute names.
     *
     * @return array the list of field names or field definitions.
     * @see toArray()
     */
    public function fields()
    {
        var fields;
        let fields = this->attributes();

        return array_combine(fields, fields);
    }

    /**
     * Determines which fields can be returned by [[toArray()]].
     * This method will check the requested fields against those declared in [[fields()]] and [[extraFields()]]
     * to determine which fields can be returned.
     * @param array $fields the fields being requested for exporting
     * @param array $expand the additional fields being requested for exporting
     * @return array the list of fields to be exported. The array keys are the field names, and the array values
     * are the corresponding object property names or PHP callables returning the field values.
     */
    protected function resolveFields(fields, expand)
    {
        var result = [], field, definition, def_fields, def_extra_fields;
        let def_fields = this->fields();
        for field, definition in def_fields {
            if typeof field == "integer" {
                let field = definition;
            }
            if count(fields) == 0 || in_array(field, fields, true) {
                let result[field] = definition;
            }
        }

        if count(expand) == 0 {
            return result;
        }

        let def_extra_fields = this->extraFields();
        for field, definition in def_extra_fields {
            if typeof field == "integer" {
                let field = definition;
            }
            if in_array(field, expand, true) {
                let result[field] = definition;
            }
        }

        return result;
    }

    /**
     * Returns an iterator for traversing the attributes in the model.
     * This method is required by the interface IteratorAggregate.
     * @return ArrayIterator an iterator for traversing the items in the list.
     */
    public function getIterator()
    {
        var attributes;
        let attributes = this->getAttributes();
        return new ArrayIterator(attributes);
    }

    /**
     * Returns whether there is an element at the specified offset.
     * This method is required by the SPL interface `ArrayAccess`.
     * It is implicitly called when you use something like `isset($model[$offset])`.
     * @param mixed $offset the offset to check on
     * @return boolean
     */
    public function offsetExists(offset)
    {
        return typeof this->{offset} != "null";
    }

    /**
     * Returns the element at the specified offset.
     * This method is required by the SPL interface `ArrayAccess`.
     * It is implicitly called when you use something like `$value = $model[$offset];`.
     * @param mixed $offset the offset to retrieve element.
     * @return mixed the element at the offset, null if no element is found at the offset
     */
    public function offsetGet(offset)
    {
        return this->{offset};
    }

    /**
     * Sets the element at the specified offset.
     * This method is required by the SPL interface `ArrayAccess`.
     * It is implicitly called when you use something like `$model[$offset] = $item;`.
     * @param integer $offset the offset to set element
     * @param mixed $item the element value
     */
    public function offsetSet(offset, item)
    {
        let this->{offset} = item;
    }

    /**
     * Sets the element value at the specified offset to null.
     * This method is required by the SPL interface `ArrayAccess`.
     * It is implicitly called when you use something like `unset($model[$offset])`.
     * @param mixed $offset the offset to unset element
     */
    public function offsetUnset(offset)
    {
        let this->{offset} = null;
    }
}
