/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace yii\base;

use yii\BaseYii;

/**
 * ErrorHandler handles uncaught PHP errors and exceptions.
 *
 * ErrorHandler is configured as an application component in [[\yii\base\Application]] by default.
 * You can access that instance via `Yii::$app->errorHandler`.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 * @author Carsten Brandt <mail@cebe.cc>
 * @since 2.0
 */
abstract class ErrorHandler extends Component
{
    /**
     * @var boolean whether to discard any existing page output before error display. Defaults to true.
     */
    public discardExistingOutput = true;
    /**
     * @var integer the size of the reserved memory. A portion of memory is pre-allocated so that
     * when an out-of-memory issue occurs, the error handler is able to handle the error with
     * the help of this reserved memory. If you set this value to be 0, no memory will be reserved.
     * Defaults to 256KB.
     */
    public memoryReserveSize = 262144;
    /**
     * @var \Exception the exception that is being handled currently.
     */
    public $exception;

    /**
     * @var string Used to reserve memory for fatal error handler.
     */
    protected _memoryReserve;


    /**
     * Register this error handler
     */
    public function register()
    {
        ini_set("display_errors", false);
        var handleException = [], handleError = [], handleFatalError = [];
        let handleException[] = this,
            handleException[] = "handleException",
            handleError[] = this,
            handleError[] = "handleError",
            handleFatalError[] = this,
            handleFatalError[] = "handleFatalError";

        set_exception_handler(handleException);
        set_error_handler(handleError);
        /*if this->memoryReserveSize > 0 {
            let this->_memoryReserve = str_repeat('x', $this->memoryReserveSize);
        }*/
        register_shutdown_function(handleFatalError);
    }

    /**
     * Handles uncaught PHP exceptions.
     *
     * This method is implemented as a PHP exception handler.
     *
     * @param \Exception $exception the exception that is not caught
     */
    public function handleException(obj_exception)
    {
        if obj_exception instanceof ExitException {
            return;
        }
        var e, msg;
        let this->$exception = obj_exception;

        // disable error capturing to avoid recursive errors while handling exceptions
        restore_error_handler();
        restore_exception_handler();
        try {
            this->logException(obj_exception);
            if this->discardExistingOutput {
                $this->clearOutput();
            }
            this->renderException(obj_exception);
            if !YII_ENV_TEST {
                exit(1);
            }
        } catch \Exception, e {

            // an other exception could be thrown while displaying the exception
            let msg = (string) e,
                msg .= "\nPrevious exception:\n",
                msg .= (string) obj_exception;

            if YII_DEBUG {
                if PHP_SAPI == "cli" {
                    echo msg . "\n";
                } else {
                    echo "<pre>" . htmlspecialchars(msg, ENT_QUOTES, BaseYii::$app->charset) . "</pre>";
                }
            }
            
            let msg .= "\n\$_SERVER = " . var_export(_SERVER, true);
            error_log(msg);
            exit(1);
        }

        let this->$exception = null;
    }

    /**
     * Handles PHP execution errors such as warnings and notices.
     *
     * This method is used as a PHP error handler. It will simply raise an [[ErrorException]].
     *
     * @param integer $code the level of the error raised.
     * @param string $message the error message.
     * @param string $file the filename that the error was raised in.
     * @param integer $line the line number the error was raised at.
     *
     * @throws ErrorException
     */
    public function handleError(code, message, file, line)
    {
        var error_reporting, obj_exception, trace, frame;
        let error_reporting = error_reporting();

        if error_reporting & code {
            let obj_exception = new \yii\base\ErrorException(message, code, code, file, line);

            let trace = debug_backtrace(0);
            array_shift(trace);
            for frame in trace {
                if frame["function"] == "__toString" {
                    this->handleException(obj_exception);
                    exit(1);
                }
            }

            throw obj_exception;
        }
    }

    /**
     * Handles fatal PHP errors
     */
    public function handleFatalError()
    {
        //unset($this->_memoryReserve);
        var error, obj_exception; 
        let error = error_get_last();

        if ErrorException::isFatalError(error) {
            let obj_exception = new \yii\base\ErrorException(error["message"], error["type"], error["type"], error["file"], error["line"]);
            let this->exception = obj_exception;
            // use error_log because it's too late to use Yii log
            // also do not log when on CLI SAPI because message will be sent to STDERR which has already been done by PHP
            if PHP_SAPI == "cli" {
                error_log(obj_exception);
                exit(1);
            }

            if this->discardExistingOutput {
                this->clearOutput();
            }
            this->renderException(obj_exception);
            exit(1);
        }
    }

    /**
     * Renders the exception.
     * @param \Exception $exception the exception to be rendered.
     */
    abstract protected function renderException(obj_exception);

    /**
     * Logs the given exception
     * @param \Exception $exception the exception to be logged
     */
    protected function logException(obj_exception)
    {
        var category;
        let category = get_class(obj_exception);
        if obj_exception instanceof "yii\\web\\HttpException" {
            let category = "yii\\web\\HttpException:" . obj_exception->statusCode;
        } else {
            if obj_exception instanceof \ErrorException {
                let category .= ":" . obj_exception->getSeverity();
            }
        }
        BaseYii::error((string) obj_exception, category);
    }

    /**
     * Removes all output echoed before calling this method.
     */
    public function clearOutput()
    {
        var level;
        let level = ob_get_level();
        // the following manual level counting is to deal with zlib.output_compression set to On
        while level > 0 {
            ob_clean();
            ob_end_clean();
            let level -= level;
        }
    }

    /**
     * Converts an exception into a PHP error.
     *
     * This method can be used to convert exceptions inside of methods like `__toString()`
     * to PHP errors because exceptions cannot be thrown inside of them.
     * @param \Exception $exception the exception to convert to a PHP error.
     */
    public static function convertExceptionToError(obj_exception)
    {
        trigger_error($static::convertExceptionToString(obj_exception), E_USER_ERROR);
    }

    /**
     * Converts an exception into a simple string.
     * @param \Exception $exception the exception being converted
     * @return string the string representation of the exception.
     */
    public static function convertExceptionToString(obj_exception)
    {
        var message;
        if ( obj_exception instanceof "yii\base\\Exception" ) && ( (obj_exception instanceof "yii\\base\\UserException") || !YII_DEBUG ) {
            let message = obj_exception->getName() .": ". obj_exception->getMessage();
        } else {
            if (YII_DEBUG) {
                if message instanceof Exception {
                    let message = "Exception (" . obj_exception->getName() .")";
                } else {
                    if obj_exception instanceof ErrorException {
                        let message = obj_exception->getName();
                    } else {
                        let message = "Exception";
                    }
                }

                let message .= " '" . get_class(obj_exception) . "' with message '". obj_exception->getMessage() ."' \n\nin ",
                    message .= obj_exception->getFile() . ":" . obj_exception->getLine() . "\n\n",
                    message .= "Stack trace:\n" . obj_exception->getTraceAsString();
            }
            else {
                let message = "Error: " . obj_exception->getMessage();
            }
        }
        return message;
    }
}
