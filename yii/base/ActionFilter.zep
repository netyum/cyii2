/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace yii\base;

/**
 * ActionFilter is the base class for action filters.
 *
 * An action filter will participate in the action execution workflow by responding to
 * the `beforeAction` and `afterAction` events triggered by modules and controllers.
 *
 * Check implementation of [[\yii\filters\AccessControl]], [[\yii\filters\PageCache]] and [[\yii\filters\HttpCache]] as examples on how to use it.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ActionFilter extends Behavior
{
    /**
     * @var array list of action IDs that this filter should apply to. If this property is not set,
     * then the filter applies to all actions, unless they are listed in [[except]].
     * If an action ID appears in both [[only]] and [[except]], this filter will NOT apply to it.
     *
     * Note that if the filter is attached to a module, the action IDs should also include child module IDs (if any)
     * and controller IDs.
     *
     * @see except
     */
    public only;
    /**
     * @var array list of action IDs that this filter should not apply to.
     * @see only
     */
    public except;


    /**
     * @inheritdoc
     */
    public function attach(owner)
    {
        let this->owner = owner;
        owner->on(Controller::EVENT_BEFORE_ACTION, [$this, "beforeFilter"]);
    }

    /**
     * @inheritdoc
     */
    public function detach()
    {
        if typeof this->owner != "null" {
            this->owner->off(Controller::EVENT_BEFORE_ACTION, [this, "beforeFilter"]);
            this->owner->off(Controller::EVENT_AFTER_ACTION, [this, "afterFilter"]);
            let this->owner = null;
        }
    }

    /**
     * @param ActionEvent $event
     */
    public function beforeFilter(event)
    {
        if !$this->isActive(event->action) {
            return;
        }

        let event->isValid = this->beforeAction(event->action);
        if event->isValid {
            // call afterFilter only if beforeFilter succeeds
            // beforeFilter and afterFilter should be properly nested
            this->owner->on(Controller::EVENT_AFTER_ACTION, [this, "afterFilter"], null, false);
        } else {
            let event->handled = true;
        }
		
    }

    /**
     * @param ActionEvent $event
     */
    public function afterFilter($event)
    {
        let event->result = this->afterAction(event->action, event->result);
        this->owner->off(Controller::EVENT_AFTER_ACTION, [this, "afterFilter"]);
    }

    /**
     * This method is invoked right before an action is to be executed (after all possible filters.)
     * You may override this method to do last-minute preparation for the action.
     * @param Action $action the action to be executed.
     * @return boolean whether the action should continue to be executed.
     */
    public function beforeAction(action)
    {
        return true;
    }

    /**
     * This method is invoked right after an action is executed.
     * You may override this method to do some postprocessing for the action.
     * @param Action $action the action just executed.
     * @param mixed $result the action execution result
     * @return mixed the processed action result.
     */
    public function afterAction(action, result)
    {
        return result;
    }

    /**
     * Returns a value indicating whether the filer is active for the given action.
     * @param Action $action the action being filtered
     * @return boolean whether the filer is active for the given action.
     */
    protected function isActive(action)
    {
        var mid, id, pos, owner;
        let owner = this->owner;
        if typeof owner == "object" && (owner instanceof Module) {
            // convert action uniqueId into an ID relative to the module
            let mid = this->owner->getUniqueId(),
                id = action->getUniqueId(),
                pos = strpos(id, mid);

            if (mid != "") && (typeof pos != "boolean") && pos == 0 {
                let id = substr(id, strlen(mid) + 1);
            }
        } else {
            let id = action->id;
        }

        if typeof this->except == "null" {
            let this->except = [];
        }

        if typeof this->only == "null" {
            let this->only = [];
        }
        if !in_array(id, this->except, true) {
            if count(this->only) == 0 || in_array(id, this->only, true) {
                return true;
            }
        }
        return false;
    }
}
