/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace yii\helpers;

use yii\BaseYii;

/**
 * BaseInflector provides concrete implementation for [[Inflector]].
 *
 * Do not use BaseInflector. Use [[Inflector]] instead.
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @since 2.0
 */
class Inflector
{
    /**
     * @var array the rules for converting a word into its plural form.
     * The keys are the regular expressions and the values are the corresponding replacements.
     */
    public static plurals;
    /**
     * @var array the rules for converting a word into its singular form.
     * The keys are the regular expressions and the values are the corresponding replacements.
     */
    public static singulars;
    /**
     * @var array the special rules for converting a word between its plural form and singular form.
     * The keys are the special words in singular form, and the values are the corresponding plural form.
     */
    public static specials;
    /**
     * @var array map of special chars and its translation. This is used by [[slug()]].
     */
    public static transliteration;
    /**
     * Converts a word to its plural form.
     * Note that this is for English only!
     * For example, 'apple' will become 'apples', and 'child' will become 'children'.
     * @param string $word the word to be pluralized
     * @return string the pluralized word
     */
    public static function pluralize(word)
    {
        var specials = null, plurals = null, w, rule, replacement;
        let specials = $static::$specials;
        if typeof specials == "null" {
            let specials = specials(),
                $static::$specials = specials;
        }

        if fetch w, specials[word] {
            return w;
        }

        let plurals = $static::$plurals;
        if typeof plurals == "null" {
            let plurals = plurals(),
                $static::$plurals = plurals;
        }

        for rule, replacement in plurals {
            if preg_match(rule, word) {
                return preg_replace(rule, replacement, word);
            }
        }

        return word;
    }

    /**
     * Returns the singular of the $word
     * @param string $word the english word to singularize
     * @return string Singular noun.
     */
    public static function singularize(word)
    {
        var specials, result, singulars, rule, replacement;
        let specials = $static::$specials;
        if typeof specials == "null" {
            let specials = specials(),
                $static::$specials = specials;
        }

        let result = array_search(word, $static::$specials, true);
        if typeof result != "boolean" {
            return result;
        }

        let singulars = $static::$singulars;
        if typeof singulars == "null" {
            let singulars = singulars(),
                $static::$singulars = singulars;
        }
        for rule, replacement in singulars {
            if preg_match(rule, word) {
                return preg_replace(rule, replacement, word);
            }
        }

        return word;
    }

    /**
     * Converts an underscored or CamelCase word into a English
     * sentence.
     * @param string $words
     * @param boolean $ucAll whether to set all words to uppercase
     * @return string
     */
    public static function titleize(words, ucAll = false)
    {
        let words = $static::humanize($static::underscore(words), ucAll);

        if ucAll {
            return ucwords(words);
        }
        else {
            return ucfirst(words);
        }
    }

    /**
     * Returns given word as CamelCased
     * Converts a word like "send_email" to "SendEmail". It
     * will remove non alphanumeric character from the word, so
     * "who's online" will be converted to "WhoSOnline"
     * @see variablize()
     * @param string $word the word to CamelCase
     * @return string
     */
    public static function camelize(word)
    {
        return str_replace(" ", "", ucwords(preg_replace("/[^A-Za-z0-9]+/", " ", word)));
    }

    /**
     * Converts a CamelCase name into space-separated words.
     * For example, 'PostTag' will be converted to 'Post Tag'.
     * @param string $name the string to be converted
     * @param boolean $ucwords whether to capitalize the first letter in each word
     * @return string the resulting words
     */
    public static function camel2words(name, ucwords = true)
    {
        var label, elements = [];

        let label = preg_replace("/(?<![A-Z])[A-Z]/", " \\0", name),
            elements = ["-", "_", "."],
            label = str_replace(elements, " ", label),
            label = strtolower(label),
            label = trim(label);

        if ucwords {
            return ucwords(label);
        }
        else {
            return label;
        }
    }

    /**
     * Converts a CamelCase name into an ID in lowercase.
     * Words in the ID may be concatenated using the specified character (defaults to '-').
     * For example, 'PostTag' will be converted to 'post-tag'.
     * @param string $name the string to be converted
     * @param string $separator the character used to concatenate the words in the ID
     * @return string the resulting ID
     */
    public static function camel2id(name, separator = "-")
    {
        if separator == "_" {
            return trim(strtolower(preg_replace("/(?<![A-Z])[A-Z]/", "_\\0", name)), "_");
        } else {
            return trim(strtolower(str_replace("_", separator, preg_replace("/(?<![A-Z])[A-Z]/", separator . "\\0", name))), separator);
        }
    }

    /**
     * Converts an ID into a CamelCase name.
     * Words in the ID separated by `$separator` (defaults to '-') will be concatenated into a CamelCase name.
     * For example, 'post-tag' is converted to 'PostTag'.
     * @param string $id the ID to be converted
     * @param string $separator the character used to separate the words in the ID
     * @return string the resulting CamelCase name
     */
    public static function id2camel(id, $separator = "-")
    {
        return str_replace(" ", "", ucwords(implode(" ", explode(separator, id))));
    }

    /**
     * Converts any "CamelCased" into an "underscored_word".
     * @param string $words the word(s) to underscore
     * @return string
     */
    public static function underscore(words)
    {
        return strtolower(preg_replace("/(?<=\\w)([A-Z])/", "_\\1", words));
    }

    /**
     * Returns a human-readable string from $word
     * @param string $word the string to humanize
     * @param boolean $ucAll whether to set all words to uppercase or not
     * @return string
     */
    public static function humanize($word, $ucAll = false)
    {
        let word = str_replace("_", " ", preg_replace("/_id$/", "", word));

        if ucAll {
            return ucwords(word);
        }
        else {
            return ucfirst(word);
        }
    }

    /**
     * Same as camelize but first char is in lowercase.
     * Converts a word like "send_email" to "sendEmail". It
     * will remove non alphanumeric character from the word, so
     * "who's online" will be converted to "whoSOnline"
     * @param string $word to lowerCamelCase
     * @return string
     */
    public static function variablize(word)
    {
        let word = $static::camelize(word);

        return strtolower(substr(word, 0, 1)) . substr(word, 1);
    }

    /**
     * Converts a class name to its table name (pluralized)
     * naming conventions. For example, converts "Person" to "people"
     * @param string $className the class name for getting related table_name
     * @return string
     */
    public static function tableize(className)
    {
        return $static::pluralize($static::underscore(className));
    }

    /**
     * Returns a string with all spaces converted to given replacement and
     * non word characters removed.  Maps special characters to ASCII using
     * [[$transliteration]] array.
     * @param string $string An arbitrary string to convert
     * @param string $replacement The replacement to use for spaces
     * @param boolean $lowercase whether to return the string in lowercase or not. Defaults to `true`.
     * @return string The converted string.
     */
    public static function slug(str, $replacement = "-", lowercase = true)
    {
        var options, transliteration;
        if extension_loaded("intl") == true {
            let options = "Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove;",
                str = transliterator_transliterate(options, str),
                str = preg_replace("/[-\\s]+/", replacement, str);
        } else {
            let transliteration = $static::$transliteration;
            if typeof transliteration == "null" {
                let transliteration = transliteration(),
                    $static::$transliteration = transliteration;
            }
            let str = str_replace(array_keys(transliteration), transliteration, str),
                str = preg_replace("/[^\\p{L}\\p{Nd}]+/u", replacement, str);
        }
        let str = trim(str, replacement);

        if lowercase {
            return strtolower(str);
        }
        else {
            return str;
        }
    }

    /**
     * Converts a table name to its class name. For example, converts "people" to "Person"
     * @param string $tableName
     * @return string
     */
    public static function classify(tableName)
    {
        return $static::camelize($static::singularize(tableName));
    }

    /**
     * Converts number to its ordinal English form. For example, converts 13 to 13th, 2 to 2nd ...
     * @param integer $number the number to get its ordinal value
     * @return string
     */
    public static function ordinalize(int number)
    {
	var remainder, copy_number;
	let remainder = number % 100;
	let copy_number = number;
        if in_array(number % 100, range(11, 13)) {
            return copy_number . "th";
        }
        switch (number % 10) {
            case 1:
                return copy_number . "st";
            case 2:
                return copy_number . "nd";
            case 3:
                return copy_number . "rd";
            default:
                return copy_number . "th";
        }
    }
}
