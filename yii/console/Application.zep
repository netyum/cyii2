/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace yii\console;

use yii\BaseYii;
use yii\base\InvalidRouteException;

/**
 * Application represents a console application.
 *
 * Application extends from [[\yii\base\Application]] by providing functionalities that are
 * specific to console requests. In particular, it deals with console requests
 * through a command-based approach:
 *
 * - A console application consists of one or several possible user commands;
 * - Each user command is implemented as a class extending [[\yii\console\Controller]];
 * - User specifies which command to run on the command line;
 * - The command processes the user request with the specified parameters.
 *
 * The command classes should be under the namespace specified by [[controllerNamespace]].
 * Their naming should follow the same naming convention as controllers. For example, the `help` command
 * is implemented using the `HelpController` class.
 *
 * To run the console application, enter the following on the command line:
 *
 * ~~~
 * yii <route> [--param1=value1 --param2 ...]
 * ~~~
 *
 * where `<route>` refers to a controller route in the form of `ModuleID/ControllerID/ActionID`
 * (e.g. `sitemap/create`), and `param1`, `param2` refers to a set of named parameters that
 * will be used to initialize the controller action (e.g. `--since=0` specifies a `since` parameter
 * whose value is 0 and a corresponding `$since` parameter is passed to the action method).
 *
 * A `help` command is provided by default, which lists available commands and shows their usage.
 * To use this command, simply type:
 *
 * ~~~
 * yii help
 * ~~~
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Application extends \yii\base\Application
{
    /**
     * The option name for specifying the application configuration file path.
     */
    const OPTION_APPCONFIG = "appconfig";

    /**
     * @var string the default route of this application. Defaults to 'help',
     * meaning the `help` command.
     */
    public defaultRoute = "help";
    /**
     * @var boolean whether to enable the commands provided by the core framework.
     * Defaults to true.
     */
    public enableCoreCommands = true;
    /**
     * @var Controller the currently active controller instance
     */
    public controller;

    /**
     * @inheritdoc
     */
    public function __construct(config = [])
    {
        let config = this->loadConfig(config);
        parent::__construct(config);
    }

    /**
     * Loads the configuration.
     * This method will check if the command line option [[OPTION_APPCONFIG]] is specified.
     * If so, the corresponding file will be loaded as the application configuration.
     * Otherwise, the configuration provided as the parameter will be returned back.
     * @param array $config the configuration provided in the constructor.
     * @return array the actual configuration to be used by the application.
     */
    protected function loadConfig(config)
    {
        var option, param, pos, path, file;
        if !isset _SERVER["argv"] {
            let option = "--" . self::OPTION_APPCONFIG . "=";
            for param in _SERVER["argv"] {
                let pos = strpos(param, option);
                if typeof pos != "boolean" {
                    let path = substr(param, strlen(option));
                    if typeof path == "string" && path != "" {
                        let file = BaseYii::getAlias(path);
                        if is_file(file) {
                            return require(file);
                        }
                    } else {
                        die("The configuration file does not exist: ". path . "\n");
                    }
                }
            }
        }

        return config;
    }

    /**
     * Initialize the application.
     */
    public function init()
    {
        var coreCommands, id, command, controllerMap;

        let controllerMap = this->controllerMap;
        if typeof controllerMap == "null" {
            let controllerMap = [];
        }
	let coreCommands = this->coreCommands();
        parent::init();
        if this->enableCoreCommands {
            for id, command in coreCommands {
                if !isset controllerMap[id] {
                    let controllerMap[id] = command;
                }
            }
        }
        // ensure we have the 'help' command so that we can list the available commands
        if !isset controllerMap["help"] {
            let controllerMap["help"] = "yii\\console\\controllers\\HelpController";
        }

        let this->controllerMap = controllerMap;
    }

    /**
     * Handles the specified request.
     * @param Request $request the request to be handled
     * @return Response the resulting response
     */
    public function handleRequest(request)
    {
        var requests, response, route, params, result;
        let requests = request->resolve(),
            route = requests[0],
            params = requests[1];

        let this->requestedRoute = route,
            result = this->runAction(route, params);

        if result instanceof Response {
            return result;
        } else {
            let response = $this->getResponse(),
                response->exitStatus = result;

            return response;
        }
    }

    /**
     * Runs a controller action specified by a route.
     * This method parses the specified route and creates the corresponding child module(s), controller and action
     * instances. It then calls [[Controller::runAction()]] to run the action with the given parameters.
     * If the route is empty, the method will use [[defaultRoute]].
     * @param string $route the route that specifies the action.
     * @param array $params the parameters to be passed to the action
     * @return integer the status code returned by the action execution. 0 means normal, and other values mean abnormal.
     * @throws Exception if the route is invalid
     */
    public function runAction(route, params = [])
    {
        var e, elements = [];
        try {
            return parent::runAction(route, params);
        } catch InvalidRouteException, e {
            let elements["command"] = route;

            throw new Exception(BaseYii::t("yii", "Unknown command \"{command}\".", elements), 0, e);
        }
    }

    /**
     * Returns the configuration of the built-in commands.
     * @return array the configuration of the built-in commands.
     */
    public function coreCommands()
    {
        var elements = [];

        let elements["message"]    = "yii\\console\\controllers\\MessageController",
            elements["help"]       = "yii\\console\\controllers\\HelpController",
            elements["migrate"]    = "yii\\console\\controllers\\MigrateController",
            elements["cache"]      = "yii\\console\\controllers\\CacheController",
            elements["asset"]      = "yii\\console\\controllers\\AssetController",
            elements["fixture"]    = "yii\\console\\controllers\\FixtureController";

        return elements;

    }

    /**
     * @inheritdoc
     */
    public function coreComponents()
    {
        var parent_elements, elements = [], coreComponents;

        let parent_elements = parent::coreComponents(),
            elements["request"]          = ["class" : "yii\\console\\Request"],
            elements["response"]         = ["class" : "yii\\console\\Response"],
            coreComponents = array_merge(parent_elements, elements);

        return coreComponents;
    }

    /**
     * Registers the errorHandler component as a PHP error handler.
     */
    //protected function registerErrorHandler(&$config)
    protected function registerErrorHandler(config)
    {
        var components, errorHandler;
        if !isset config["components"] {
            let config["components"] = [];
        }
        let components = config["components"];
        if !isset components["errorHandler"] {
            let components["errorHandler"] = [];
        }
        let errorHandler = components["errorHandler"];
        if !isset errorHandler["class"] {
            let errorHandler["class"] = "yii\\console\\ErrorHandler",
                components["errorHandler"] = errorHandler,
                config["components"] = components;
        }
        parent::registerErrorHandler(config);
    }
}
